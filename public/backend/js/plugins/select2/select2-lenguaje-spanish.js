//Este script se creo para poder utilizar el ajax en el select2 y que los mensajes esten en español.

var select2_lenguaje_spanish = {
  inputTooShort: function(args) {
    // args.minimum is the minimum required length
    // args.input is the user-typed text
    return "Ingrese al menos 3 caracteres para buscar";
  },
  inputTooLong: function(args) {
    // args.maximum is the maximum allowed length
    // args.input is the user-typed text
    return "Escribiste demasiado";
  },
  errorLoading: function() {
    return "Error al cargar resultados";
  },
  loadingMore: function() {
    return "Cargando mas resultados";
  },
  noResults: function() {
    return "Sin resultados encontrados";
  },
  searching: function() {
    return "Buscando...";
  },
  maximumSelected: function(args) {
    // args.maximum is the maximum number of items the user may select
    return "Error al cargar resultados";
  }
};
