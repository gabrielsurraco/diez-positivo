/*
 *
 *   Funciones propias para el sistema.
 *   version 1
 *
 */

String.prototype.padLeft = function (n,str){
    return Array(n-String(this).length+1).join(str||'0')+this;
}
