<?php

use Illuminate\Database\Seeder;
use App\Ciudad;

class CiudadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ciudad::create(array(
            'nombre'        => '1 de Mayo',
            'id_provincia'  => 1
        ));
        Ciudad::create(array(
            'nombre'        => '25 de Mayo',
            'id_provincia'  => 1
        ));
        Ciudad::create(array(
            'nombre'        => '9 de Julio Kilómetro 20',
            'id_provincia'  => 1
        ));
    }
}
