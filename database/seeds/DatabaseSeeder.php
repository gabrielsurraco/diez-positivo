<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PaisTableSeeder::class);
        $this->call(ProvinciasTableSeeder::class);
        //$this->call(CiudadesTableSeeder::class);
        // $this->call(CallesTableSeeder::class);
//        $this->call(PlanDeCuentaSeeder::class);
//        $this->call(PlanDeCuentaDetalleSeeder::class);
        $this->call(DepartamentosSeeder::class);
        $this->call(UserSeeder::class);

    }
}
