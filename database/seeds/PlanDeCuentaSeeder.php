<?php

use Illuminate\Database\Seeder;
use App\PlanDeCuenta;

class PlanDeCuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Activo',
//                'id_superior'   => null,
//                'codigo'        => '1'
//        ));
//        
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Pasivo',
//                'id_superior'   => null,
//                'codigo'        => '2'
//        ));
//        
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Patrimonio Neto',
//                'id_superior'   => null,
//                'codigo'        => '3'
//        ));
//        
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Resultados',
//                'id_superior'   => null,
//                'codigo'        => '4'
//        ));
//        
//        //id = 5
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Activo Corriente',
//                'id_superior'   => 1,
//                'codigo'        => '1.1'
//        ));
//        
//        //id = 6
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Activo No Corriente',
//                'id_superior'   => 1,
//                'codigo'        => '1.2'
//        ));
//        
//        //id = 7
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Caja y Bancos',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.1'
//        ));
//        
//        //id = 8
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Créditos por Ventas',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.2'
//        ));
//        
//        //id = 9
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Créditos Fiscales',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.3'
//        ));
//        
//        //id = 10
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Otros Créditos',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.4'
//        ));
//        
//        //id = 11
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Bienes de Cambio',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.5'
//        ));
//        
//        //id = 12
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Inversiones CP',
//                'id_superior'   => 5,
//                'codigo'        => '1.1.6'
//        ));
//        
//        //id = 13
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Bienes de Uso',
//                'id_superior'   => 6,
//                'codigo'        => '1.2.1'
//        ));
//        
//        //id = 14
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Pasivo Corriente',
//                'id_superior'   => 2,
//                'codigo'        => '2.1'
//        ));
//        
//        //id = 15
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Deudas Comerciales',
//                'id_superior'   => 14,
//                'codigo'        => '2.1.1'
//        ));
//        
//        //id = 16
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Deudas Bancarias',
//                'id_superior'   => 14,
//                'codigo'        => '2.1.2'
//        ));
//        
//        //id = 17
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Deudas Fiscales',
//                'id_superior'   => 14,
//                'codigo'        => '2.1.3'
//        ));
//        
//        //id = 18
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Deudas Previsionales',
//                'id_superior'   => 14,
//                'codigo'        => '2.1.4'
//        ));
//        
//        
//        
//        //id = 19
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Pasivo No Corriente',
//                'id_superior'   => 2,
//                'codigo'        => '2.2'
//        ));
//        
//        //id = 20
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Deudas a largo plazo',
//                'id_superior'   => 19,
//                'codigo'        => '2.2.1'
//        ));
//        
//        //id = 22
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Capital Social',
//                'id_superior'   => 3,
//                'codigo'        => '3.1'
//        ));
//        
//        //id = 23
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Resultados Acumulados',
//                'id_superior'   => 3,
//                'codigo'        => '3.2'
//        ));
//        
//        //id = 24
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Reservas',
//                'id_superior'   => 3,
//                'codigo'        => '3.3'
//        ));
//        
//        //id = 25
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Resultados Positivos',
//                'id_superior'   => 4,
//                'codigo'        => '4.1'
//        ));
//        
//         //id = 26
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Resultados Operativos',
//                'id_superior'   => 25,
//                'codigo'        => '4.1.1'
//        ));
//        
//         //id = 26
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Resultados Financieros',
//                'id_superior'   => 25,
//                'codigo'        => '4.1.2'
//        ));
//        
//        //id = 27
//        PlanDeCuenta::create(array(
//                'descripcion'   => 'Otros Ingresos',
//                'id_superior'   => 25,
//                'codigo'        => '4.1.3'
//        ));
        
        
        
    }
}
