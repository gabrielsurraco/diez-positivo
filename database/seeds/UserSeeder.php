<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Gabriel Surraco',
            'email'     => 'gabriel_surraco@hotmail.com',
            'dni'       => 34477486,
            'es_admin'  => 1,
            'password'  => bcrypt('admin'),
        ]);
        User::create([
            'name'      => 'Guille Balmaceda',
            'email'     => 'guillebalmacedaonline@gmail.com',
            'dni'       => 35694972 ,
            'es_admin'  => 1,
            'password'  => bcrypt('qwerty'),
        ]);
    }
}
