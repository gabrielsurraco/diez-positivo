<?php

use Illuminate\Database\Seeder;
use App\PlanDeCuentaDetalle;

class PlanDeCuentaDetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Caja',
//            'descripcion'   => null,
//            'codigo'        => '1.1.1.1.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 13
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Banco Macro CA',
//            'descripcion'   => 'Caja de Ahorro',
//            'codigo'        => '1.1.1.2.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 14
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Banco Macro CC',
//            'descripcion'   => 'Cuenta Corriente',
//            'codigo'        => '1.1.1.2.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 14
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Banco Frances CA',
//            'descripcion'   => 'Caja de Ahorro',
//            'codigo'        => '1.1.1.2.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 14
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Banco Frances CC',
//            'descripcion'   => 'Cuenta Corriente',
//            'codigo'        => '1.1.1.2.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 14
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Valores a Depositar',
//            'descripcion'   => null,
//            'codigo'        => '1.1.1.3.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 15
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Cheques Diferidos',
//            'descripcion'   => null,
//            'codigo'        => '1.1.1.4.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 16
//        ));
        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Deudores por Venta',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.6',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Percepción Ingresos Brutos Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Cargas Sociales Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Ingresos Brutos Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Ganancias Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Percepción de IVA Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.5',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención de IVA Sufrida',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.6',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'IVA Crédito Fiscal',
//            'descripcion'   => null,
//            'codigo'        => '1.1.3.7',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 9
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Depósitos a Plazo Fijo',
//            'descripcion'   => null,
//            'codigo'        => '1.1.6.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 12
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Préstamos Otorgados',
//            'descripcion'   => null,
//            'codigo'        => '1.1.6.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 12
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Deudores Cta Cte',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Deudores Morosos',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Documentos a Cobrar',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Tarjetas de Crédito',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Cheques de Pago Diferido',
//            'descripcion'   => null,
//            'codigo'        => '1.1.2.5',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 8
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Anticipos al Personal',
//            'descripcion'   => null,
//            'codigo'        => '1.1.4.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 10
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Anticipos a Proveedores',
//            'descripcion'   => null,
//            'codigo'        => '1.1.4.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 10
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Intereses a Cobrar',
//            'descripcion'   => null,
//            'codigo'        => '1.1.4.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 10
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Deudores Varios',
//            'descripcion'   => null,
//            'codigo'        => '1.1.4.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 10
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Mercadería de Reventa',
//            'descripcion'   => null,
//            'codigo'        => '1.1.5.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 11
//        ));
//        
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Insumos',
//            'descripcion'   => null,
//            'codigo'        => '1.1.5.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 11
//        ));
//        
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Antenas',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.1.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 36
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Routers',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.1.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 36
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Muebles de Oficina',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Instalaciones',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Herramientas',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Rodados',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Dispositivos Móviles',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.5',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Sistema Telefónico',
//            'descripcion'   => null,
//            'codigo'        => '1.2.1.2.6',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 37
//        ));
        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Marcas de Fábrica',
//            'descripcion'   => null,
//            'codigo'        => '1.2.2.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 18
//        ));
        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Acreedores',
//            'descripcion'   => null,
//            'codigo'        => '2.1.1.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 21
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Anticipos de Clientes',
//            'descripcion'   => null,
//            'codigo'        => '2.1.1.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 21
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Préstamos Bancarios',
//            'descripcion'   => null,
//            'codigo'        => '2.1.2.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 22
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Giro en Descubierto',
//            'descripcion'   => null,
//            'codigo'        => '2.1.2.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 22
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Municipalidad de Obera',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Percepción Ingresos Brutos Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Percepción de IVA Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.3',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención de IVA Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.4',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Cargas Sociales Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.5',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Ingresos Brutos Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.6',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'Retención Ganancias Efectuada',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.7',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'IVA Débito Fiscal',
//            'descripcion'   => null,
//            'codigo'        => '2.1.3.8',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 23
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'FAECyS',
//            'descripcion'   => null,
//            'codigo'        => '2.1.4.1',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 24
//        ));
//        
//        PlanDeCuentaDetalle::create(array(
//            'nombre'        => 'INACAP',
//            'descripcion'   => null,
//            'codigo'        => '2.1.4.2',
//            'rama'          => null,
//            'categoria'     => null,
//            'fk_plan_de_cuenta' => 24
//        ));
        
        
        
        
        
        
        
        
    }
}
