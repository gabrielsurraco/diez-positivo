<?php

use Illuminate\Database\Seeder;
use App\Calle;

class CallesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Calle::create(array(
            'nombre'        => 'Av. San Martin',
        ));
        
        Calle::create(array(
            'nombre'        => 'Av. Lavalle',
        ));
        
        Calle::create(array(
            'nombre'        => 'Av. 9 de Julio',
        ));
    }
}
