<?php

use Illuminate\Database\Seeder;
use App\Provincia;

class ProvinciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provincia::create(array(
                'nombre' => 'Misiones',
                'fk_pais'=> 1
        ));

//        Provincia::create(array(
//                'nombre' => 'Estado del Sur',
//                'id_pais'=> 2
//        ));
    }
}
