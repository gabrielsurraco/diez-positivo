<?php

use Illuminate\Database\Seeder;
use App\Departamento;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        Departamento::create(array(
            'nombre'        => 'Apóstoles',
            'fk_provincia'  => 1
        ));
        //2
        Departamento::create(array(
            'nombre'        => 'Cainguás',
            'fk_provincia'  => 1
        ));
        //3
        Departamento::create(array(
            'nombre'        => 'Candelaria',
            'fk_provincia'  => 1
        ));
        //4
        Departamento::create(array(
            'nombre'        => 'Capital',
            'fk_provincia'  => 1
        ));
        //5
        Departamento::create(array(
            'nombre'        => 'Concepción',
            'fk_provincia'  => 1
        ));
        //6
        Departamento::create(array(
            'nombre'        => 'Eldorado',
            'fk_provincia'  => 1
        ));
        //7
        Departamento::create(array(
            'nombre'        => 'General Manuel Belgrano',
            'fk_provincia'  => 1
        ));
        //8
        Departamento::create(array(
            'nombre'        => 'Guaraní',
            'fk_provincia'  => 1
        ));
        //9
        Departamento::create(array(
            'nombre'        => 'Iguazú',
            'fk_provincia'  => 1
        ));
        //10
        Departamento::create(array(
            'nombre'        => 'Leandro N. Alem',
            'fk_provincia'  => 1
        ));
        //11
        Departamento::create(array(
            'nombre'        => 'Lib. General San Martín',
            'fk_provincia'  => 1
        ));
        //12
        Departamento::create(array(
            'nombre'        => 'Montecarlo',
            'fk_provincia'  => 1
        ));
        //13
        Departamento::create(array(
            'nombre'        => 'Oberá',
            'fk_provincia'  => 1
        ));
        //14
        Departamento::create(array(
            'nombre'        => 'San Ignacio',
            'fk_provincia'  => 1
        ));
        //15
        Departamento::create(array(
            'nombre'        => 'San Javier',
            'fk_provincia'  => 1
        ));
        //16
        Departamento::create(array(
            'nombre'        => 'San Pedro',
            'fk_provincia'  => 1
        ));
        //17
        Departamento::create(array(
            'nombre'        => '25 de Mayo',
            'fk_provincia'  => 1
        ));
    }
}
