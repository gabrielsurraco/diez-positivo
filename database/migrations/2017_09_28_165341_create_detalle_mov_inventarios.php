<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleMovInventarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_mov_inventarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_mov_inventario')->unsigned();
            $table->integer('fk_almacen_origen')->unsigned()->nullable();
            $table->integer('fk_almacen_destino')->unsigned();
            $table->string('serie');
            $table->enum('estado', array('ACEPTADO', 'RECHAZADO'));
            $table->string('observacion');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_mov_inventarios');
    }
}
