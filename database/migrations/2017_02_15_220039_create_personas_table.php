<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::defaultStringLength(191);
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('apellido')->nullable();
            $table->string('razon_social')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->boolean('es_persona_fisica');
            $table->string('tipo_doc');
            $table->string('cuit');
            $table->string('condicion_fiscal');
            $table->string('email')->nullable();
            $table->string('email_2')->nullable();
            $table->string('profesion')->nullable();
            $table->string('observacion')->nullable();

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
