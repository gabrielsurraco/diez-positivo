<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturacion', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('fk_cuenta')->unsigned();
            $table->integer('fk_domicilio')->unsigned();
            $table->string('tipo_concepto')->nullable();
            $table->date('periodo_desde')->nullable();
            $table->date('periodo_hasta')->nullable();
            $table->date('vencimiento_pago')->nullable();
            $table->decimal('importeNeto'  ,15, 2);
            $table->decimal('importeIva' , 15, 2);
            $table->decimal('importeTotal' , 15, 2);
            $table->string('tipo_factura')->nullable();
            $table->string('punto_venta_fiscal')->nullable();
            $table->string('numero_factura_fiscal')->nullable();
            $table->string('codigo_barras')->nullable();
            $table->string('cae')->nullable();
            $table->date('vencimiento_cae')->nullable();
            $table->string('pdf_factura')->nullable();
            $table->boolean('es_facturaElectronica')->default(0);
            $table->enum('estado', array('FACTURA', 'PRESUPUESTO'));

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturacion');
    }
}
