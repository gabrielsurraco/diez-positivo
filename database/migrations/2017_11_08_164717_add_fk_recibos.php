<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkRecibos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recibos', function (Blueprint $table) {
            $table->foreign('fk_cuenta')->references('id')->on('cuentas');
            $table->foreign('fk_orden_venta')->references('id')->on('orden_venta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recibos', function (Blueprint $table) {
            //
            $table->dropForeign('recibos_fk_cuenta_foreign');
            $table->dropForeign('recibos_fk_orden_venta_foreign');
        });
    }
}
