<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDetalleMovInventarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_mov_inventarios', function (Blueprint $table) {
            $table->foreign('fk_mov_inventario')->references('id')->on('mov_inventarios');
            $table->foreign('fk_almacen_origen')->references('id')->on('almacenes');
            $table->foreign('fk_almacen_destino')->references('id')->on('almacenes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_mov_inventarios', function (Blueprint $table) {
            $table->dropForeign('detalle_mov_inventarios_fk_mov_inventario_foreign');
            $table->dropForeign('detalle_mov_inventarios_fk_almacen_origen_foreign');
            $table->dropForeign('detalle_mov_inventarios_fk_almacen_destino_foreign');
        });
    }
}
