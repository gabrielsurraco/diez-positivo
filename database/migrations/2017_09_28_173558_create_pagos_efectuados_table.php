<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosEfectuadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos_efectuados', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_pago');
            $table->integer('fk_cuenta')->unsigned();
            $table->decimal('importe', 15, 2);
            $table->string('nro_comprobante')->nullable();
            $table->integer('fk_recibo')->unsigned()->nullable();
            $table->integer('fk_facturacion')->unsigned()->nullable();
            $table->enum('medio_pago',array('EFECTIVO','TARJETA','CHEQUE','RAPIPAGO','MERCADO_PAGO','PAGOS_MIS_CUENTAS','TRANSFERENCIA'));
            $table->string('nro_ref_pago')->nullable();
            $table->integer('cuotas')->nullable();
            $table->integer('fk_persona_cobrador')->unsigned()->nullable();//gaby puse en comentario esto porque al momento no encontre relacion.
            $table->boolean('anulado')->default(false);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos_efectuados');
    }
}
