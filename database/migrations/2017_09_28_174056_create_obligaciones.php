<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObligaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obligaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cuenta')->unsigned();
            $table->string('tipo_obl')->nullable();
            $table->string('concepto');
            $table->decimal('monto',15,2);
            $table->string('periodo')->nullable();
            $table->integer('fk_orden_venta_detalle')->unsigned()->nullable();
            $table->integer('fk_orden_venta')->unsigned();
            $table->date('fecha_vencimiento')->nullable();
            $table->boolean('aplica_interes')->default(false);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obligaciones');
    }
}
