<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('dni')->unique();
            $table->string('email');
            $table->unique('email');
            $table->string('password');
            $table->boolean('es_cobrador')->nullable();
            $table->boolean('es_tecnico')->nullable();
            $table->boolean('es_admin')->nullable();
            $table->boolean('es_administrativo')->nullable();
            $table->boolean('es_contabilidad')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
