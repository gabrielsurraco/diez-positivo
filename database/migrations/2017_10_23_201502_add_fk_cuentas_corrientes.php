<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCuentasCorrientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_corrientes', function (Blueprint $table) {
           $table->foreign('fk_cuenta')->references('id')->on('cuentas');
           $table->foreign('fk_obligation')->references('id')->on('obligation');
           $table->foreign('fk_pagos_efectuado')->references('id')->on('pagos_efectuados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuentas_corrientes', function (Blueprint $table) {
            $table->dropForeign('cuentas_corrientes_fk_cuenta_foreign');
            $table->dropForeign('cuentas_corrientes_fk_obligation_foreign');
            $table->dropForeign('cuentas_corrientes_fk_pagos_efectuado_foreign');
        });
    }
}
