<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkEquipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipos', function (Blueprint $table) {
            $table->foreign('fk_torre')->references('id')->on('torres');
            $table->foreign('fk_articulo')->references('id')->on('articulos');
            $table->foreign('fk_dependencia_torres_equipos')->references('id')->on('dependencia_torres_equipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipos', function (Blueprint $table) {
            $table->dropForeign('equipos_fk_torre_foreign');
            $table->dropForeign('equipos_fk_articulo_foreign');
            $table->dropForeign('equipos_fk_dependencia_torres_equipos_foreign');
        });
    }
}
