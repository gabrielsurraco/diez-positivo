<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovInventarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mov_inventarios', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->boolean('es_entrada');
            $table->string('nro_factura_recibida')->nullable();
            $table->string('nro_remito_recibido')->nullable();
            $table->integer('fk_remito')->nullable()->unsigned();
            $table->integer('fk_factura')->nullable()->unsigned();
            $table->float('cantidad');
            $table->integer('fk_articulo')->unsigned();
            $table->integer('fk_orden_compra')->nullable()->unsigned();
            $table->integer('fk_orden_venta')->nullable()->unsigned();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mov_inventarios');
    }
}
