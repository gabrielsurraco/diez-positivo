<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDetalleOrdenVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_orden_venta', function (Blueprint $table) {
            $table->foreign('fk_orden_venta')->references('id')->on('orden_venta');
            $table->foreign('fk_articulo')->references('id')->on('articulos');
            $table->foreign('fk_servicio')->references('id')->on('servicios');
            $table->foreign('fk_facturacion')->references('id')->on('facturacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_orden_venta', function (Blueprint $table) {
            $table->dropForeign('detalle_orden_venta_fk_orden_venta_foreign');
            $table->dropForeign('detalle_orden_venta_fk_articulo_foreign');
            $table->dropForeign('detalle_orden_venta_fk_servicio_foreign');
            $table->dropForeign('detalle_orden_venta_fk_facturacion_foreign');
        });
    }
}
