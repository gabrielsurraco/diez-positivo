<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObligation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obligation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cuenta')->unsigned();
            $table->date('fecha');
            $table->integer('periodo')->nullable();
            $table->string('periodo_mensualidad')->nullable()->unique();
            $table->float('cantidad');
            $table->string('concepto');
            $table->string('descripcion');
            $table->decimal('precio_unitario', 15, 2);
            $table->boolean('aplica_intereses')->default(true);
            $table->decimal('bonificacion', 15, 2);
            $table->float('alicuta_iva');
            $table->decimal('importe_iva', 15, 2);
            $table->decimal('importe_siniva', 15, 2);
            $table->date('fecha_vencimiento_pago')->nullable();
            $table->integer('fk_facturacion')->unsigned()->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obligation');
    }
}
