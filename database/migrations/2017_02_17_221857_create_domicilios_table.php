<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_persona')->unsigned();
            $table->integer('fk_provincia')->unsigned();
            $table->integer('fk_departamento')->unsigned();
            $table->integer('fk_localidad')->unsigned();
            $table->integer('fk_cliente_servicio')->nullable()->unsigned();
            $table->enum('tipo_domicilio', array('servicio','fiscal','envio'));
            $table->string('piso')->nullable();
            $table->string('depto')->nullable();
            $table->string('cp')->nullable();

            $table->string('calle_paralela')->nullable();
            $table->string('calle_perpendicular_1')->nullable();
            $table->string('calle_perpendicular_2')->nullable();
            $table->text('observacion')->nullable();

            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->string('link_mapa')->nullable();
            $table->string('calle')->nullable();
            $table->string('altura',10)->nullable();

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
