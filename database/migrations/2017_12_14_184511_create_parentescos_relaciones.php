<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentescosRelaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parentescos_relaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cliente')->unsigned()->nullable();
            $table->integer('fk_proveedor')->unsigned()->nullable();
            $table->integer('fk_inversor')->unsigned()->nullable();
            $table->integer('fk_tecnico')->unsigned()->nullable();
            $table->integer('fk_persona')->unsigned();
            $table->string('rol');
            $table->boolean('activo')->dafault(true);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parentescos_relaciones');
    }
}
