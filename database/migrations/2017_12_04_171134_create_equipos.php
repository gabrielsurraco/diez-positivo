<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->enum('tipo', array('ENLACE','EMISORA','SWITCH'));
            $table->integer('fk_articulo')->unsigned();
            $table->string('serie');
            $table->string('ip');
            $table->integer('fk_torre')->unsigned();
            $table->integer('fk_dependencia_torres_equipos')->unsigned()->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
