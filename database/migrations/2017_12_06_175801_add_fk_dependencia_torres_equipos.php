<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDependenciaTorresEquipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dependencia_torres_equipos', function (Blueprint $table) {
            $table->foreign('fk_torre')->references('id')->on('torres');
            $table->foreign('fk_equipo')->references('id')->on('equipos');
            $table->foreign('fk_equipo_depende')->references('id')->on('equipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dependencia_torres_equipos', function (Blueprint $table) {
          $table->dropForeign('dependencia_torres_equipos_fk_torre_foreign');
          $table->dropForeign('dependencia_torres_equipos_fk_equipo_foreign');
          $table->dropForeign('dependencia_torres_equipos_fk_equipo_depende_foreign');
        });
    }
}
