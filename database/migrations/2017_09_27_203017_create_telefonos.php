<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelefonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telefonos', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo',array('fijo','movil'));
            $table->string('empresa');
            $table->string('cod_pais')->nulleable();
            $table->integer('area');
            $table->integer('numero');
            $table->string('horario_contacto')->nulleable();
            $table->string('observaciones')->nulleable();
            $table->integer('fk_persona')->unsigned();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telefonos');
    }
}
