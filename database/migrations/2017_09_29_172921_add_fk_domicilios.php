<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDomicilios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domicilios', function (Blueprint $table) {
            $table->foreign('fk_persona')->references('id')->on('personas');
            $table->foreign('fk_departamento')->references('id')->on('departamentos');
            $table->foreign('fk_localidad')->references('id')->on('localidades');
            $table->foreign('fk_provincia')->references('id')->on('provincias');
            $table->foreign('fk_cliente_servicio')->references('id')->on('clientes_servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domicilios', function (Blueprint $table) {
            $table->dropForeign('domicilios_fk_persona_foreign');
            $table->dropForeign('domicilios_fk_departamento_foreign');
            $table->dropForeign('domicilios_fk_localidad_foreign');
            $table->dropForeign('domicilios_fk_provincia_foreign');
            $table->dropForeign('domicilios_fk_cliente_servicio_foreign');
        });
    }
}
