<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cuenta')->unsigned();
            $table->string('codigo_interno')->nullable()->unique();
            $table->string('marca');
            $table->string('modelo');
            $table->string('frecuencia')->nullable();
            $table->float('porcentaje_ganancia');
            $table->float('alicuota_iva');
            $table->decimal('precio', 8,2);
            $table->string('unidad');
            $table->enum('moneda', array('ARS', 'USD'));
            $table->boolean('activo')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
