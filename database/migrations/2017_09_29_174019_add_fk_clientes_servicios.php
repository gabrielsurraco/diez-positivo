<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkClientesServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_servicios', function (Blueprint $table) {
            $table->foreign('fk_cliente')->references('id')->on('clientes');
            $table->foreign('fk_servicio')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_servicios', function (Blueprint $table) {
            $table->dropForeign('clientes_servicios_fk_cliente_foreign');
            $table->dropForeign('clientes_servicios_fk_servicio_foreign');
        });
    }
}
