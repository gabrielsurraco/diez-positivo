<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkFacturacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturacion', function (Blueprint $table) {
            $table->foreign('fk_cuenta')->references('id')->on('cuentas');
            //$table->foreign('fk_recibo')->references('id')->on('recibos');
            $table->foreign('fk_domicilio')->references('id')->on('domicilios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturacion', function (Blueprint $table) {
            $table->dropForeign('facturacion_fk_cuenta_foreign');
            $table->dropForeign('facturacion_fk_domicilio_foreign');
        });
    }
}
