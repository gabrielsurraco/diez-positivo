<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleOrdenCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_orden_compra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_orden_de_compra')->unsigned();
            $table->float('cantidad');
            $table->integer('fk_articulo')->unsigned();
            $table->decimal('precio' , 8,2);
            $table->enum('estado', array('a_cobrar','cobrado','agendado','confirmado','activo','suspendido','mudanza','promo','baja'));
            $table->boolean('activo')->default(true);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_orden_compra');
    }
}
