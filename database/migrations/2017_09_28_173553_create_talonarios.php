<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalonarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talonarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('punto_de_venta');
            $table->string('tipo_talonario');
            $table->integer('numero_actual');
            $table->integer('numero_inicial');
            $table->integer('numero_final');
            $table->date('fecha_vencimiento');
            $table->string('ubicacion')->nulleable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talonarios');
    }
}
