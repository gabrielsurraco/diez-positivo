<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkObligaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obligaciones', function (Blueprint $table) {
            //
            $table->foreign('fk_cuenta')->references('id')->on('cuentas');
            $table->foreign('fk_orden_venta_detalle')->references('id')->on('detalle_orden_venta');
            $table->foreign('fk_orden_venta')->references('id')->on('orden_venta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obligaciones', function (Blueprint $table) {
            $table->dropForeign('obligaciones_fk_cuenta_foreign');
            $table->dropForeign('obligaciones_fk_orden_venta_detalle_foreign');
            $table->dropForeign('obligaciones_fk_orden_venta_foreign');
        });
    }
}
