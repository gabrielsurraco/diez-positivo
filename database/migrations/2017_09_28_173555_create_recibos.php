<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecibos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recibos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cuenta')->unsigned();
            $table->date('fecha');
            $table->decimal('importe_total', 15 ,2);
            $table->decimal('saldo_cliente', 15 ,2);
            $table->integer('fk_orden_venta')->unsigned();
            $table->boolean('anulado')->defualt(false);
            $table->text('motivo');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recibos');
    }
}
