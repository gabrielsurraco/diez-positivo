<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleOrdenVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_orden_venta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_orden_venta')->unsigned();
            $table->integer('fk_articulo')->nullable()->unsigned();
            $table->integer('fk_servicio')->nullable()->unsigned();
            $table->float('cantidad');
            $table->string('observacion');
            $table->decimal('precio_unitario', 8 , 2);
            $table->float('bonificacion');
            $table->float('alicuota_iva');
            $table->decimal('importeIva', 8 , 2);
            $table->decimal('importe_totalsiniva', 8 , 2);
            $table->integer('fk_facturacion')->nullable()->unsigned();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_orden_venta');
    }
}
