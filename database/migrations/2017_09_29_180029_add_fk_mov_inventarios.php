<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkMovInventarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mov_inventarios', function (Blueprint $table) {
            $table->foreign('fk_articulo')->references('id')->on('articulos');
            $table->foreign('fk_orden_compra')->references('id')->on('orden_compra');
            $table->foreign('fk_orden_venta')->references('id')->on('orden_venta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mov_inventarios', function (Blueprint $table) {
            $table->dropForeign('mov_inventarios_fk_articulo_foreign');
            $table->dropForeign('mov_inventarios_fk_orden_compra_foreign');
            $table->dropForeign('mov_inventarios_fk_orden_venta_foreign');
        });
    }
}
