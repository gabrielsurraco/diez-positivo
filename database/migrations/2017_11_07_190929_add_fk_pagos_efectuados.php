<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkPagosEfectuados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagos_efectuados', function (Blueprint $table) {
            $table->foreign('fk_recibo')->references('id')->on('recibos');
            $table->foreign('fk_facturacion')->references('id')->on('facturacion');
            $table->foreign('fk_persona_cobrador')->references('id')->on('personas');
            $table->foreign('fk_cuenta')->references('id')->on('cuentas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagos_efectuados', function (Blueprint $table) {
            $table->dropForeign('pagos_efectuados_fk_recibo_foreign');
            $table->dropForeign('pagos_efectuados_fk_facturacion_foreign');
            $table->dropForeign('pagos_efectuados_fk_persona_cobrador_foreign');
            $table->dropForeign('pagos_efectuados_fk_cuenta_foreign');
        });
    }
}
