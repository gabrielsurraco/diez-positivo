<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkTorres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('torres', function (Blueprint $table) {
            $table->foreign('fk_departamento')->references('id')->on('departamentos');
            $table->foreign('fk_localidad')->references('id')->on('localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('torres', function (Blueprint $table) {
          $table->dropForeign('torres_fk_departamento_foreign');
          $table->dropForeign('torres_fk_localidad_foreign');
        });
    }
}
