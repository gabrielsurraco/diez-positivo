<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkTecnicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tecnicos', function (Blueprint $table) {
          $table->foreign('fk_persona')->references('id')->on('personas');
          $table->foreign('fk_cuenta')->references('id')->on('cuentas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tecnicos', function (Blueprint $table) {
          $table->dropForeign('tecnicos_fk_persona_foreign');
          $table->dropForeign('tecnicos_fk_cuenta_foreign');
        });
    }
}
