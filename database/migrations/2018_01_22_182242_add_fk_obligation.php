<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkObligation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obligation', function (Blueprint $table) {
            //
            $table->foreign('fk_cuenta')->references('id')->on('cuentas');
            $table->foreign('fk_facturacion')->references('id')->on('facturacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obligation', function (Blueprint $table) {
            //
            $table->dropForeign('obligation_fk_cuenta_foreign');
            $table->dropForeign('obligation_fk_facturacion_foreign');
        });
    }
}
