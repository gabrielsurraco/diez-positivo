<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tareas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cliente_servicio')->unsigned();
            $table->date('fecha_vencimiento')->nullable();
            $table->text('correos_aviso')->nullable();
            $table->enum('estado', array('PENDIENTE', 'RECHAZADA','FINALIZADA'));
            $table->text('observa_coordinacion');
            $table->text('observa_tecnico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tareas');
    }
}
