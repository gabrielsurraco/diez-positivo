<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->float('alicuota_iva');
            $table->decimal('precio', 8,2);
            $table->enum('zona', array('ESTANDAR','SIMETRICO','COMERCIAL','DEDICADO','RURAL','URBANO','PREMIUM'));
            $table->integer('fk_localidad')->unsigned()->nullable();
            $table->integer('fk_plan_cuenta')->unsigned();
            $table->string('descripcion')->nullable();
            $table->boolean('activo')->default(true);
            $table->string('tipo_servicio')->nullable(); // 1-  'ABONO'
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
