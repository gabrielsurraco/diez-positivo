<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cliente')->unsigned();
            $table->integer('fk_servicio')->unsigned();
            $table->date('fecha_alta');
            $table->date('fecha_baja')->nullable();
            $table->enum('estado', array('preventa','a_cobrar','cobrado','agendado','pendiente','primer_mes','activo',
                                        'promo','bonificado','mudanza','suspendido','a_cortar','cortado','reconectar',
                                        'baja','particular'));
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }
//    "fk_cliente","fk_lista_precio_det","estado","fk_domicilio_servicio","fecha_inicio","fecha_fin","created_by","updated_by"
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes_servicios');
    }
}
