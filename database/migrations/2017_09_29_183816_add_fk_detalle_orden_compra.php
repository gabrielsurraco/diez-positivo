<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDetalleOrdenCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_orden_compra', function (Blueprint $table) {
            $table->foreign('fk_orden_de_compra')->references('id')->on('orden_compra');
            $table->foreign('fk_articulo')->references('id')->on('articulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_orden_compra', function (Blueprint $table) {
            $table->dropForeign('detalle_orden_compra_fk_orden_de_compra_foreign');
            $table->dropForeign('detalle_orden_compra_fk_articulo_foreign');
        });
    }
}
