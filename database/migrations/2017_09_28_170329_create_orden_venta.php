<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_venta', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('fk_persona')->unsigned();
            $table->decimal('importe_neto', 15, 2);
            $table->decimal('importe_iva105', 15, 2);
            $table->decimal('importe_iva21', 15, 2);
            $table->enum('estado', array('borrador', 'completa', 'pagado', 'facturada'));
            $table->date('vencimiento_pago')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_venta');
    }
}
