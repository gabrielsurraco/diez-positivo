<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkParentescosRelaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parentescos_relaciones', function (Blueprint $table) {
            $table->foreign('fk_cliente')->references('id')->on('clientes');
            $table->foreign('fk_proveedor')->references('id')->on('proveedores');
            $table->foreign('fk_inversor')->references('id')->on('inversores');
            $table->foreign('fk_tecnico')->references('id')->on('tecnicos');
            $table->foreign('fk_persona')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parentescos_relaciones', function (Blueprint $table) {
            $table->dropForeign('parentescos_relaciones_fk_cliente_foreign');
            $table->dropForeign('parentescos_relaciones_fk_proveedor_foreign');
            $table->dropForeign('parentescos_relaciones_fk_inversor_foreign');
            $table->dropForeign('parentescos_relaciones_fk_tecnico_foreign');
            $table->dropForeign('parentescos_relaciones_fk_persona_foreign');            
        });
    }
}
