<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDetalleRecibos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_recibos', function (Blueprint $table) {
            //
            $table->foreign('fk_recibo')->references('id')->on('recibos');
            $table->foreign('fk_detalle_orden_venta')->references('id')->on('detalle_orden_venta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_recibos', function (Blueprint $table) {
            //
            $table->dropForeign('detalle_recibos_fk_recibo_foreign');
            $table->dropForeign('detalle_recibos_fk_detalle_orden_venta_foreign');
        });
    }
}
