<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasCorrientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_corrientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_cuenta')->unsigned();
            $table->integer('nro_secuencia')->unsigned();
            $table->date('fecha')->nullable();
            $table->string('concepto')->nullable();
            $table->decimal('debe',15,2)->nullable();
            $table->decimal('haber',15,2)->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('fk_obligation')->unsigned()->nullable();
            $table->integer('fk_pagos_efectuado')->unsigned()->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_corrientes');
    }
}
