<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependenciaTorresEquipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependencia_torres_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_torre')->unsigned()->nullable();
            $table->integer('fk_equipo')->unsigned()->nullable();
            $table->integer('fk_equipo_depende')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependencia_torres_equipos');
    }
}
