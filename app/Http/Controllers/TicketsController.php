<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;
use Illuminate\Support\Facades\Validator;
use App\Ticket;
use PDOException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Illuminate\Support\Facades\Redirect;

class TicketsController extends Controller
{
    public function indexTickets(){
        $clientes = Clientes::all();
        return view("tickets.tickets_index", compact("clientes"));
    }

    public function crearTicket(){
        $clientes = Clientes::all();
        return view("tickets.tickets_create", compact("clientes"));
    }

    //Creado por Guille Uniting
    //esta funcion permite generar un nuevo ticket a partir de la seleccion de un cliente en listado clientes.
    public function crearTicketFromReference($idCliente){ //Metodo GET
        $clientes = Clientes::find($idCliente);
        return view("tickets.tickets_create", [ 'clientes' => $clientes, 'fromReference'=> 'true']);
    }
    //Fin creado por Guille

    public function guardarTicket(Request $request){

        if($request->es_cliente == "si"){
            $this->validate($request, [
                'cliente'       => 'required',
                'descripcion'   => 'required'
            ]);
        }else if($request->es_cliente == "no"){
            $this->validate($request, [
                'nombre'        => 'required',
                'descripcion'   => 'required'
            ]);
        }

        if(isset($request->cliente))
            $cliente = $request->cliente;
        else
            $cliente = NULL;

        if(isset($request->nombre))
            $nombre = $request->nombre;
        else
            $nombre = NULL;

        if($request->input('sel-serv-cliente') != "default"){
            $idServicio = $request->input('sel-serv-cliente');
        }else{
            $idServicio = NULL;
        }

        if(isset($request->telefono))
            $telefono = $request->telefono;
        else
            $telefono = NULL;

        if(isset($request->email))
            $email = $request->email;
        else
            $email = NULL;

        if(isset($request->descripcion))
            $descripcion = $request->descripcion;
        else
            $descripcion = NULL;

        if(isset($request->respuesta))
            $respuesta = $request->respuesta;
        else
            $respuesta = NULL;

        try{
            if(isset($request->editando)){

                $ticket = Ticket::where('id', $request->ticket_id)->update([
                    'es_cliente'    => $request->es_cliente,
                    'fk_cliente'    => $cliente,
                    'nombre'        => $nombre,
                    'fk_servicio'   => $idServicio,
                    'telefono'      => $telefono,
                    'email'         => $email,
                    'estado'        => $request->estado,
                    'categoria'     => $request->categoria,
                    'consulta'      => $descripcion,
                    'respuesta'     => $respuesta,
                    'updated_by'    => Auth::user()->name
                ]);

            }else{
                $ticket = new Ticket;
                $ticket->fecha         = date('Y-m-d');
                $ticket->es_cliente    = $request->es_cliente;
                $ticket->fk_cliente    = $cliente;
                $ticket->nombre        = $nombre;
                $ticket->fk_servicio   = $idServicio;
                $ticket->telefono      = $telefono;
                $ticket->email         = $email;
                $ticket->estado        = $request->estado;
                $ticket->categoria     = $request->categoria;
                $ticket->consulta      = $descripcion;
                $ticket->respuesta     = $respuesta;
                $ticket->created_by    = Auth::user()->name;
                $ticket->save();
            }

        } catch (\PDOException $ex) {
            return back()->with('descripcion', $ex->getMessage());
        }


        if(isset($request->editando)){
            return Redirect::to('/tickets')->with('msg','El Ticket se medificó correctamente!');
        }else{
            return Redirect::to('/tickets')->with('msg','El Ticket se creo correctamente!');
        }

    }

    public function buscarTickets(Request $request){

        try{

        $tickets = Ticket::select('id','fecha','es_cliente','fk_cliente','nombre','telefono','email','estado','categoria','consulta','respuesta','created_by','updated_by');

        if(isset($request->fecha_desde))
            $tickets = $tickets->where('fecha','>=', Carbon::createFromFormat('d/m/Y', $request->fecha_desde)->format('Y-m-d'));

        if(isset($request->fecha_hasta))
            $tickets = $tickets->where('fecha','<=', Carbon::createFromFormat('d/m/Y', $request->fecha_hasta)->format('Y-m-d'));

        if(isset($request->nro_ticket))
            $tickets = $tickets->where('id',$request->nro_ticket);

        if(isset($request->estado))
            $tickets = $tickets->where('estado', $request->estado);

        if(isset($request->categoria))
            $tickets = $tickets->where('categoria', $request->categoria);

        if(isset($request->contacto)){
            $contacto = $request->contacto;
            $tickets = $tickets->where(function($query) use ($contacto){
                        $query->where('telefono', 'LIKE', '%'.$contacto.'%');
                        $query->orWhere('nombre', 'LIKE', '%'.$contacto.'%');
                        $query->orWhere('email', 'LIKE', '%'.$contacto.'%');
                    });
        }

        if(isset($request->es_cliente)){
            $tickets = $tickets->where('es_cliente', 'no');
        }else{
            $tickets = $tickets->where('es_cliente', 'si');
            if(isset($request->cliente)){
                $tickets = $tickets->where('fk_cliente', $request->cliente);
            }
        }

        $tickets = $tickets->get();

        $tickets_fixed = array();
        foreach($tickets as $ticket => $value){

            if($value->es_cliente == "si" )
                $cliente = Clientes::findOrFail($value->fk_cliente)->persona->apellido . ' ' . Clientes::findOrFail($value->fk_cliente)->persona->nombre;
            else
                $cliente = $value->nombre;

            $tickets_fixed[] = array(
                'id'            => $value->id,
                'fecha'         => $value->fecha,
                'es_cliente'    => $value->es_cliente,
                'fk_cliente'    => $cliente,
                'nombre'        => $value->nombre,
                'telefono'      => $value->telefono,
                'email'         => $value->email,
                'estado'        => $value->estado,
                'categoria'     => $value->categoria,
                'consulta'      => str_limit($value->consulta . '...', 20),
                'respuesta'     => $value->respuesta,
            );
        }

        return response()->json($tickets_fixed, 200);

        }catch(\PDOException $ex){
            return response()->json($ex->getMessage(), 400);
        }


    }

    public function editarTicket($ticket_id){
        $ticket = Ticket::findOrFail($ticket_id);
        $clientes = Clientes::all();
        return view("tickets.tickets_edit", compact("clientes",'ticket'));
    }

}
