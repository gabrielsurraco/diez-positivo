<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Personas;
use App\Clientes;
use App\Cliente;
use App\CuentaCorriente;
use Carbon\Carbon;
use App\Cuenta;
use App\PagosEfectuados;
use App\OrdenVenta;
use PDOException;
use App\Http\Controllers\OrdenVentaController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Obligacion;



class CuentaCorrienteController extends Controller
{
    public function index(){
        return view('cuenta_corriente.index');
    }

    public function setPago(Request $request){
        try{

            //INSERT EN CTA CTE
            //INSERT EN PAGOS EFECTUADOS
            //MARCA ORDEN VENTA


            if(isset($request->cuenta_id)){
                //busco si existe la cuenta
                $count_cuenta = Cuenta::findOrFail($request->cuenta_id)->count();
            }

            if($count_cuenta > 0){
                //INSERTO EN CTA CTE
//                CuentaCorriente::create([
//                    "fk_cuenta"         => $request->cuenta_id,
//                    "nro_secuencia"     => CuentaCorriente::where('fk_cuenta', $request->cuenta_id)->max('nro_secuencia'),
//                    "fecha"             => $request->fecha_pago,
//                    "debe"              => 0,
//                    "haber"             => (float)$request->monto,
//                    "descripcion"       => $request->descripcion,
//                    "fk_orden_venta"    => ,
//                    "fk_recibo"         => ,
//                    "created_by"        =>
//                ]);

//                PagosEfectuados::create([
//                    "fecha_pago"            => '',
//                    "importe"               => '',
//                    "nro_comprobante"       => '',
//                    "fk_recibo"             => '',
//                    'fk_orden_venta'        => '',
//                    'medio_pago'            => '',
//                    'cuotas'                => '',
//                    'fk_persona_cobrador'   => '',
//                    'descripcion'           => '',
//                    'fk_cuenta'             => '',
//                    'created_by'            => ''
//                ]);

//                OrdenVenta::where('id',1)->update([
//                    'estado'    => 'pagado'
//                ]);



            }else{
                return  Response::json(array(
                            'mensaje'   => $ex->getMessage()
                        ), 500);
            }


            echo $request->cuenta_id;
            echo $request->fecha_pago;
            echo $request->medio_pago;
            echo $request->cuotas;

            if(isset($request->carga_manual))
                echo $request->carga_manual;

            echo $request->nro_comprobante;
            echo $request->factura;
            echo $request->descripcion;
            echo $request->monto;



            return  Response::json(array(
                            'mensaje'   => "asd"
                        ), 200);
        } catch (\PDOException $ex) {
            return  Response::json(array(
                            'mensaje'   => $ex->getMessage()
                        ), 500);
        }
    }

    public function getInfoCuenta(Request $request){

        try{
            if ($request->cuit == ''){
              $c = Clientes::with('persona')->where('id', $request->input('sel-cliente'))->first();
              $persona = Personas::where('cuit',$c->persona->cuit)->get(["id","nombre","apellido","razon_social","es_persona_fisica"]);
            }else {
              $persona = Personas::where('cuit',$request->cuit)->get(["id","nombre","apellido","razon_social","es_persona_fisica"]);
            }


            if (count($persona)) {
              $cliente = Clientes::where('fk_persona', $persona[0]->id)->get(["fk_cuenta"]);

              if($cliente[0]->fk_cuenta != null){

                  $items    = CuentaCorriente::where("fk_cuenta",$cliente[0]->fk_cuenta);

                  if (isset($request->concepto)) {
                      $items    = $items->where("descripcion",'LIKE','%' . $request->concepto . '%');
                  }

                  if (isset($request->fecha_desde)) {
                      $items    = $items->where("fecha",'>=', Carbon::createFromFormat('d/m/Y', $request->fecha_desde)->format('Y-m-d'));
                  }

                  if (isset($request->fecha_hasta)) {
                      $items    = $items->where("fecha",'<=', Carbon::createFromFormat('d/m/Y', $request->fecha_hasta)->format('Y-m-d') );
                  }

                  $items    = $items->get();


                  $debe     = 0.00;
                  $haber    = 0.00;

                  foreach ($items as $item){
                      $debe     = $debe     + (float)$item->debe;
                      $haber    = $haber    + (float)$item->haber;
                  }

                  $saldo = (float)$haber - (float)$debe;

                  if($saldo >= 0){
                      $color = '#21d336';
                  }else{
                      $color = '#ea2323';
                  }

                  if($persona[0]->es_persona_fisica){
                      $nombre_persona = $persona[0]->nombre ." ". $persona[0]->apellido;
                  }else{
                      $nombre_persona = $persona[0]->razon_social;
                  }

                  $list_ov_pendiente_pago = OrdenVenta::where('fk_persona',$persona[0]->id)->where('estado','completa')->get();

                  $cuenta_corriente =   array(
                                                "items_cuenta_corriente"    => $items,
                                                "saldo"                     => number_format($saldo, 2),
                                                "color"                     => $color,
                                                "persona"                   => strtoupper($nombre_persona),
                                                "id_persona"                => $persona[0]->id,
                                                "fk_cuenta"                 => $cliente[0]->fk_cuenta,
                                                "ov_pendientes"             => $list_ov_pendiente_pago
                                        );
                  $response         = $cuenta_corriente;
              }else{
                  $response = "No se encontraron resultados.";
              }

            }else {
                $response = "No se encontraron resultados.";
            }

            return  Response::json(array(
                            'mensaje'   => $response
                        ), 200);
        } catch (\PDOException $ex) {
            return  Response::json(array(
                            'mensaje'   => $ex->getMessage()
                        ), 500);
        }

    }

    public function calcularIntereses(Request $request){

            if(!isset($request->cuenta_id) || $request->cuenta_id == null || $request->cuenta_id == ''){
              return  Response::json(array('response'   => 'REVISAR PARAMETROS ENVIADOS'), 500);
            }


            try {
              $cuenta = Cuenta::find($request->cuenta_id);

              if($cuenta == null)
                return  Response::json(array('response'   => 'LA CUENTA CON ID ' . $request->cuenta_id . ' NO EXISTE'), 500);



              //TRAER LAS ORDENES DE VENTA IMPAGAS Y VENCIDAS, POR CADA UNA DE ESTAS GENERO UN MOV DE INTERES
              $ordenes_venta_vencidas = OrdenVentaController::getOrdenesVentaVencidasEImpagas($request->cuenta_id);
              $items = array();

              foreach($ordenes_venta_vencidas as $ov){
                  $cont_primer_interes  = CuentaCorriente::where('concepto', 'interes')->where('fk_obligacion', $ov->obligacion->id)->count();
                  $segundo_interes      = CuentaCorriente::where('concepto', 'segundo_interes')->where('fk_obligacion', $ov->obligacion->id)->count();

                  // CANTIDAD DE ITEMS EN LA ORDEN DE VENTA
                  $cont_items_ov = count($ov->detalleOrdenVenta);
                  if($cont_items_ov == 1){
                      echo $ov->detalleOrdenVenta[0]->fk_articulo;
                      if($ov->detalleOrdenVenta[0]->fk_articulo == NULL){
                        echo $ov->detalleOrdenVenta[0]->services->tipo_servicio;
                          if($ov->detalleOrdenVenta[0]->services->tipo_servicio == 'ABONO'){
                              echo 'ingreso';
                              if($cont_primer_interes == 0){ //SI NO TIENE CARGOS DE INTERES, LO INSERTO
                                  $items[] = self::setPrimerRecargo($request->cuenta_id, $ov->obligacion->id, $ov->id, auth()->user()->name, $ov->vencimiento_pago);
                              }


                              $fecha_2do_venc = Carbon::createFromFormat('d/m/Y', $ov->vencimiento_pago);
                              $fecha_2do_venc->addDays(6);
                              $now            = Carbon::now();

                              $flag = $now->gte($fecha_2do_venc);

                              if($flag){ //CORRESPONDE RECARGO POR SEGUNDO VENCIMIENTO

                                  if($segundo_interes == 0){

                                      $monto_ov           = $ov->importe_neto + $ov->importe_iva105 + $ov->importe_iva21;
                                      $monto_interes      = ($monto_ov * 5) / 100;
                                      $next_nro_secuencia = CuentaCorriente::where('fk_cuenta', $request->cuenta_id)->max('nro_secuencia') + 1;


                                      CuentaCorriente::create([
                                          'fk_cuenta'             => $request->cuenta_id,
                                          'nro_secuencia'         => $next_nro_secuencia,
                                          'fecha'                 => date('d/m/Y'),
                                          'concepto'              => 'segundo_interes',
                                          'debe'                  => $monto_interes,
                                          'haber'                 => 0,
                                          'descripcion'           => 'RECARGO 5% (RECONEXIÓN) 2do. Venc. POR MORA O.V: ' . $ov->id,
                                          'fk_obligacion'         => $ov->obligacion->id,
                                          'fk_pagos_efectuado'    => NULL,
                                          'created_by'            => auth()->user()->name
                                      ]);

                                      Obligacion::create([
                                          'fk_cuenta'               => (int)$request->cuenta_id,
                                          'tipo_obl'                => 'segundo_interes',
                                          'concepto'                => 'RECARGO 5% (RECONEXIÓN) 2do. Venc. POR MORA O.V: ' . $ov->id,
                                          'monto'                   => $monto_interes,
                                          'periodo'                 => NULL,
                                          'fk_orden_venta_detalle'  => NULL,
                                          'fk_orden_venta'          => $ov->id,
                                          'fecha_vencimiento'       => date($ov->vencimiento_pago),//Carbon::createFromFormat('Y-m-d', $ov->vencimiento_pago),
                                          'aplica_interes'          => false,
                                          'created_by'              => auth()->user()->name
                                      ]);


                                      $items[] = array(
                                          'nro_secuencia'   => $next_nro_secuencia,
                                          'fecha'           => date('d/m/Y'),
                                          'concepto'        => 'RECARGO 5% (RECONEXIÓN) 2do. Venc. POR MORA O.V: ' . $ov->id,
                                          'debe'            => $monto_interes,
                                          'haber'           => 0
                                      );

                                  }
                              }


                          }
                      }

                  }else{
                      if($cont_primer_interes == 0){ //SI NO TIENE CARGOS DE INTERES, LO INSERTO
                          $items[] = self::setPrimerRecargo($request->cuenta_id, $ov->obligacion->id, $ov->id, auth()->user()->name, $ov->vencimiento_pago);
                      }

                  }

              }

                $saldo = self::getSaldoCuentaCorriente($request->cuenta_id);

                if($saldo >= 0){
                      $color = '#21d336';
                  }else{
                      $color = '#ea2323';
                  }

                return  Response::json(array('response'   => 'EL PROCESO FINALIZO EXITOSAMENTE','items' => $items, 'saldo' => $saldo, 'color' => $color), 200);
            } catch (Exception $e) {
                return  Response::json(array('response'   => 'ERROR EN EL PROCESO DE CALCULO DE INTERES: '. $e->getMessage()), 500);
            }




    }

    public static function setPrimerRecargo( $fk_cuenta, $id_obligacion, $id_ov, $usuario, $fecha_vencimiento){


            $saldo = self::getSaldoCuentaCorriente($fk_cuenta);

            if($saldo < 0){

                $monto_interes      = ((self::getSaldoCuentaCorriente($fk_cuenta) * -1) * 5) / 100;
                $next_nro_secuencia = CuentaCorriente::where('fk_cuenta', $fk_cuenta)->max('nro_secuencia') + 1;

                CuentaCorriente::create([
                    'fk_cuenta'             => $fk_cuenta,
                    'nro_secuencia'         => $next_nro_secuencia,
                    'fecha'                 => date('d/m/Y'),
                    'concepto'              => 'interes',
                    'debe'                  => $monto_interes,
                    'haber'                 => 0,
                    'descripcion'           => 'RECARGO 5% (SOBRE SALDO) POR MORA O.V: ' . $id_ov,
                    'fk_obligacion'         => $id_obligacion,
                    'fk_pagos_efectuado'    => NULL,
                    'created_by'            => $usuario
                ]);

                Obligacion::create([
                    'fk_cuenta'               => (int)$fk_cuenta,
                    'tipo_obl'                => 'interes',
                    'concepto'                => 'RECARGO 5% (SOBRE SALDO) POR MORA O.V: ' . $id_ov,
                    'monto'                   => $monto_interes,
                    'periodo'                 => NULL,
                    'fk_orden_venta_detalle'  => NULL,
                    'fk_orden_venta'          => $id_ov,
                    'fecha_vencimiento'       => date($fecha_vencimiento), //Carbon::createFromFormat('Y-m-d', $fecha_vencimiento),
                    'aplica_interes'          => FALSE,
                    'created_by'              => $usuario
                ]);

            }

            return array(
                        'nro_secuencia'   => $next_nro_secuencia,
                        'fecha'           => date('d/m/Y'),
                        'concepto'        => 'RECARGO 5% (SOBRE SALDO) POR MORA O.V: ' . $id_ov,
                        'debe'            => $monto_interes,
                        'haber'           => 0
                    );


    }


    public static function getSaldoCuentaCorriente($cuenta_id){
        $debe   = CuentaCorriente::select(DB::raw('SUM(debe) as total_debe'))->where('fk_cuenta',$cuenta_id)->get(['total_debe']);
        $haber  = CuentaCorriente::select(DB::raw('SUM(haber) as total_haber'))->where('fk_cuenta',$cuenta_id)->get(['total_haber']);
        $saldo  =  (float)$haber[0]->total_haber - (float)$debe[0]->total_debe;

        return $saldo;
    }

}
