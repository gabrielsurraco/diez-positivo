<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use App\Provincia;
use App\Ciudad;
use App\Calle;
use Illuminate\Support\Facades\Auth;

class CabDomiciliosController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        
        $paises = Pais::all();
        $provincias = Provincia::with('pais')->get();
        $ciudades = Ciudad::with('provincia')->get();
        $calles = Calle::all();
        
//        foreach($provincias as $provincia => $value){
//            echo $value->pais['nombre'].'<br/>';
//        }
        
        //var_dump($calles);
        
        return view('domicilio.index', compact('paises','provincias','ciudades','calles'));
    }
    
    public function storePais(Request $request){
        $pais = new Pais();
        $pais->nombre = $request->toArray()['nombre'];
        $pais->save();
    }
    
    public function storeProvincia(Request $request){
        $provincia = new Provincia();
        $provincia->nombre = $request->toArray()['nombre'];
        $provincia->id_pais = $request->toArray()['id_pais'];
        $provincia->save();
    }
    
    public function storeCiudad(Request $request){
        $ciudad = new Ciudad();
        $ciudad->nombre = $request->toArray()['nombre'];
        $ciudad->id_provincia = $request->toArray()['id_provincia'];
        $ciudad->save();
    }
    
    public function storeCalle(Request $request){
        $calle = new Calle();
        $calle->nombre = $request->toArray()['nombre'];
        $calle->save();
    }
    
    public function updatePais(Request $request, $id){
        Pais::where('id', $id)
        ->update(['nombre' => $request->toArray()['nombre'],'updated_by' => Auth::user()->dni]);
    }
    
    public function updateProvincia(Request $request, $id){
        Provincia::where('id', $id)
        ->update(['nombre' => $request->toArray()['nombre'],'updated_by' => Auth::user()->dni]);
    }
    
    public function updateCiudad(Request $request, $id){
        Ciudad::where('id', $id)
        ->update(['nombre' => $request->toArray()['nombre'],'id_provincia' => $request->toArray()['id_provincia'],'cp' => $request->toArray()['cp'], 'updated_by' => Auth::user()->dni]);
    }
    
    public function updateCalle(Request $request, $id){
        Calle::where('id', $id)
        ->update(['nombre' => $request->toArray()['nombre'],'updated_by' => Auth::user()->dni]);
    }
    
    public function destroyPais($id){
        Pais::destroy($id);
    }
    
    public function destroyProvincia($id){
        Provincia::destroy($id);
    }
    
    public function destroyCiudad($id){
        Ciudad::destroy($id);
    }
    
    public function destroyCalle($id){
        Calle::destroy($id);
    }
    
}
