<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Pais;
use App\Departamento;
use App\Localidad;
use App\Domicilio;
use App\Proveedores;
use App\Personas;
use App\Cuenta;
use App\Helper;

class ProveedoresController extends Controller
{

    public function listarProveedores(){
        $proveedores = Proveedores::with(['persona.domicilio.localidad', 'persona.domicilio.departamento'])->where('activo', true)->get();
        //dd($clientes);
        return view('proveedores.index', ['proveedores' => $proveedores]);
    }

    //Guardar Proveedor nuevo.
    public function guardarProveedor(Request $request){
        try {
          DB::beginTransaction();
          $persona = Personas::findOrFail( $request->input('idPersona'));

          // CREO LA CUENTA DE LA PERSONA EN EL PLAN DE CUENTA
          $sub_cuenta                 = new Cuenta();
          $sub_cuenta->nombre         = 'CUENTA PROVEEDOR - ' . strtoupper($persona->apellido). ' '. strtoupper($persona->nombre). ' - '. strtoupper($persona->cuit);
          $sub_cuenta->codigo         = Helper::buscarSiguienteCodigoCuenta(8, NULL , 0);
          $sub_cuenta->imputable      = 1;
          $sub_cuenta->id_superior    = 15; //el superior es DEUDAS COMERCIALES COMERCIALES
          $sub_cuenta->save();

          $p = new Proveedores();
          $p->codigo     = $request->input('codigo');
          $p->fk_persona = $request->input('idPersona');
          $p->fk_cuenta  = $sub_cuenta->id;
          $p->created_by = Auth::user()->name;
          $p->save();
          DB::commit();
          return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idProveedor' => $p->id), 200);
        } catch (Exception $ex) {
          DB::rollback();
          return response(array('mensage' => 'Error al guardar.', 'detalleError' => $e, 'idProveedor' => null), 400);
        }
    }

    //Este Json permite utilizarlo en el Select2 para realizar una busqueda de
    //proveedores con 3 caracteres.
    public function jsonSearchProveedor(Request $request){
        $proveedor = Proveedores::whereHas('persona', function ($query) use ($request) {
                      $query->where('razon_social', 'like', '%' . $request->input('palabraClave') . '%')
                             ->orWhere('cuit', 'like', '%' . $request->input('palabraClave') . '%')
                             ->orWhere('codigo', 'like', '%' . $request->input('palabraClave') . '%');
                  })->where('activo', 1)->get();

        $createArray = array();
        foreach ($proveedor as $value) {
          array_push($createArray, array('id' => $value->id , 'text' => $value->codigo.' - '. $value->persona->razon_social.' ('.$value->persona->cuit.')'));
        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }





}
