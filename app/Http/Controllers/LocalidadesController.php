<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Localidad;

class LocalidadesController extends Controller
{

    public function getLocalidades(Request $request){        
        $localidades = Localidad::where('fk_departamento','=',$request->depto)->get();
        return json_encode($localidades);
    }

}
