<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personas;
use App\Clientes;
use App\Departamento;
use App\Localidad;
use App\ListaPrecio;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\ClientesServicios;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Helper;
use Illuminate\Support\Facades\Redirect;
use App\ListaPrecioDetalle;
use PDOException;
use App\Asientos;
use Illuminate\Support\Facades\DB;
use App\PlanDeCuentaDetalle;
use App\Servicios;
use App\Domicilio;
use App\CuentaCorriente;

class ServiciosController extends Controller{


    //Inicio Servicios
    public function index(){
        $s = Servicios::with('localidad')->where('activo',1)->get();
        return view('servicios.index', [
          "ubicacion" => "servicios.index",
          "servicios"   => $s
        ]);
    }

    public function serviciosNuevo(){
        $l = Localidad::all();
        return view('servicios.nuevoServicio', [
          "ubicacion" => "servicios.nuevo",
          "localidades" => $l,
        ]);
    }

    public function serviciosEdit($id){
        $s  = Servicios::find($id);
        $l = Localidad::all();
        return view('servicios.nuevoServicio', [
          "ubicacion" => "servicios.edit",
          "servicios"   => $s,
          "localidades" => $l
        ]);
    }

    public function guardarServicio(Request $request){
      $this->validate($request, [
            'inp-nombre'    => 'required|max:100'
      ],[
            'inp-nombre.required'    => 'Este campo es requerido.',
            'inp-nombre.max'         => 'El maximo de caracteres es 100.'
      ]);
      try {
          DB::beginTransaction();
          if (!is_null($request->input('inp-id-servicio'))){
              $s = Servicios::find($request->input('inp-id-servicio'));
          }else{
              $s = new Servicios;
          }

          $s->nombre            = strtoupper($request->input('inp-nombre'));
          $s->alicuota_iva      = $request->input('sel-alicuota-iva');
          $s->precio            = $request->input('inp-precio');
          $s->zona              = $request->input('sel-zona');
          $s->fk_localidad      = $request->input('sel-localidad');
          $s->fk_plan_cuenta    = 1;
          $s->descripcion       = $request->input('inp-descripcion');
          $s->created_by        = auth()->user()->name;

          $s->save();
          DB::commit();

          return response()->view('servicios.nuevoServicio', array('message' => 'Guardado Correctamente.', 'status' => 1), 200);
      } catch (Exception $ex) {
          DB::rollback();
          return response()->view('servicios.nuevoServicio', array('message' => 'Ocurrio un error Intentelo nuevamente.', 'status' => 0), 400);
      }

    }

    public function deleteServicio(Request $request){

        try {
            DB::beginTransaction();
            Servicios::where('id', $request->input('id'))
                       ->update(['activo' => 0, 'updated_by' => auth()->user()->name ]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::rollback();
            return response(array('mensage' => 'Error al Eliminar Servicio.', 'detalleError' => $e), 400);
        }
    }

    //Fin Servicios


    public function getServiciosCliente($id){
        $cliente = Clientes::findOrFail($id);

        $departamentos = Departamento::all();
        $array_departamentos = array();
        foreach($departamentos as $key => $value){
           $array_departamentos[$value->id] =  $value->nombre;
        }

        $localidades = Localidad::all();
        $array_localidades = array();
        foreach($localidades as $key => $value){
           $array_localidades[$value->id] =  $value->nombre;
        }

        $cs = ClientesServicios::where('fk_cliente', $id)->get();

        //dd($cs[0]->servicio);
        /*
        foreach($cliente->getservicios as $key => $value){
            $lista_precio_detalle = ListaPrecioDetalle::findOrFail($value->fk_lista_precio_det);
            $lista_precio = ListaPrecio::findOrFail($lista_precio_detalle->fk_lista_precio);

            $servicios_fixed[$cont] = array(
                "concepto"      => $lista_precio->concepto,
                "sub-concepto"  => $lista_precio_detalle->sub_concepto,
                "fecha_inicio"  => $value->fecha_inicio,
                "fecha_fin"     => $value->fecha_fin,
                "estado"        => $value->estado,
                "costo"         => $lista_precio_detalle->costo
            );
            $cont++;
        }
        */
        return view('ventas.clientes_servicios.cliente', compact('cliente','array_departamentos','array_localidades','cs'));
    }

    public function getServiciosClienteJSON(Request $request){
        $id = $request->input('idCliente');

        $cs = ClientesServicios::with(['servicio', 'cliente', 'domicilio', 'domicilio.localidad'])->where('fk_cliente', $id)->get();
        return response(json_encode($cs));
    }

    public function getServiciosClienteFromIDJSON(Request $request){
        $id = $request->input('idClienteServicio');

        $cs = ClientesServicios::with(['servicio', 'cliente', 'domicilio', 'domicilio.localidad'])->find($id);
        return response(json_encode($cs));
    }

    //Direcciones Funciones
    function changeStatusServicio(Request $request){
        try {
            DB::beginTransaction();

            $cs = ClientesServicios::find($request->input('id'));
            $cs->estado = $request->input('estado');
            $cs->updated_by = auth()->user()->name;
            if($cs->estado == 'baja'){
                $cs->fecha_baja = date('d/m/Y');
            }
            $cs->save();
            return response(array('mensage' => 'Estado Servicio Editado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response(array('mensage' => 'Error al Editar Estado Servicio.', 'detalleError' => $e), 400);
        }finally {
            DB::commit();
        }
    }

    public function getServiciosClienteAJAX(Request $request){
        $cliente = Clientes::findOrFail($request->input('idCliente'));

        $departamentos = Departamento::all();
        $array_departamentos = array();
        foreach($departamentos as $key => $value){
           $array_departamentos[$value->id] =  $value->nombre;
        }

        $localidades = Localidad::all();
        $array_localidades = array();
        foreach($localidades as $key => $value){
           $array_localidades[$value->id] =  $value->nombre;
        }

        $servicios_fixed = array();
        $cont = 0;
        foreach($cliente->getservicios as $key => $value){
            $lista_precio_detalle = ListaPrecioDetalle::findOrFail($value->fk_lista_precio_det);
            $lista_precio = ListaPrecio::findOrFail($lista_precio_detalle->fk_lista_precio);

            $servicios_fixed[$cont] = array(
                "concepto"      => $lista_precio->concepto,
                "sub_concepto"  => $lista_precio_detalle->sub_concepto,
                "fecha_inicio"  => $value->fecha_inicio,
                "fecha_fin"     => $value->fecha_fin,
                "estado"        => $value->estado,
                "costo"         => $lista_precio_detalle->costo
            );
            $cont++;
        }

        $datos = array();

        $datos['cliente'] = $cliente;
        $datos['array_departamentos'] = $array_departamentos;
        $datos['array_localidades'] = $array_localidades;
        $datos['servicios_fixed'] = $servicios_fixed;

        return json_encode($datos);
    }

    public function getServiciosFromLocalidadAJAX(Request $request){
        $s = Servicios::where('activo', 1)
                      ->where('fk_localidad', $request->input('idLocalidad'))->get();
        return json_encode($s);
    }

    public function agregarServiciosCliente($id){

        $departamentos = Departamento::all();

        $array_departamentos = array();
        foreach($departamentos as $key => $value){
           $array_departamentos[$value->id] =  $value->nombre;
        }

        $servicios  = Servicios::all();

        $calles = Domicilio::select("calle")->distinct()->get();
        $cliente = Clientes::find($id);
        $estados    = Helper::getEstadosDelServicio();
        return view("ventas.clientes_servicios.create",  compact('servicios','estados','id','array_departamentos', 'cliente', 'calles'));
    }

    //******Actualizacion de Megas
    //Esta funcion permite cambiar el servicio al cliente calculando un proporcional de lo ya consumido
    //para contrarestar el nuevo servicio. Esta funcion aplica movimientos en cuenta corriente.
    public function actualizarServiciosCliente(Request $request){
        try{
            DB::beginTransaction();
            //Leemos los datos del servicio actual del cliente
            $sc = ClientesServicios::findOrFail($request->input('idClienteServicio')); //$request->input('idClienteServicio')

            $precioServicioAnterior = $sc->servicio->precio;
            //En base a la fecha actual tomamos el año y el mes y pedimos a Carbon
            //que calcule la diferencia sumando un mes al mes actual
            $fechaActual = Carbon::now();
            $pdma = Carbon::create($fechaActual->year, $fechaActual->month, 1); //primer dia del mes actual.
            $diasDelMes = $pdma->diffInDays($pdma->copy()->addMonth());

            $diasDeUsoDelServicioActual = $pdma->diffInDays(Carbon::now());
            $importeAfavorCliente       = $precioServicioAnterior - (($precioServicioAnterior / $diasDelMes) * $diasDeUsoDelServicioActual);
            //cargamos en la cuenta corriente lo que resta del abono no consumido.
            $cc = new CuentaCorriente;
            $cc->fk_cuenta        = $sc->cliente->fk_cuenta;
            $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $sc->cliente->fk_cuenta)->max('nro_secuencia') + 1;
            $cc->fecha            = date('d/m/Y');
            $cc->debe             = 0.00;
            $cc->haber            = $importeAfavorCliente;
            $cc->descripcion      = $sc->servicio->nombre.' '.$sc->servicio->zona.' (NO CONSUMIDO)';
            $cc->created_by       = auth()->user()->name;
            $cc->save();

            //calculo del servicio actual elegido.
            $s = Servicios::findOrFail($request->input('idServicioElegido'));
            $importeServicioActual = ($s->precio / $diasDelMes) * ($diasDelMes - $diasDeUsoDelServicioActual);
            $cc = new CuentaCorriente;
            $cc->fk_cuenta        = $sc->cliente->fk_cuenta;
            $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $sc->cliente->fk_cuenta)->max('nro_secuencia') + 1;
            $cc->fecha            = date('d/m/Y');
            $cc->debe             = $importeServicioActual;
            $cc->haber            = 0.00;
            $cc->descripcion      = $s->nombre.' '.$s->zona. ' (CAMBIO DE SERVICIO)';
            $cc->created_by       = auth()->user()->name;
            $cc->save();

            $sc->fecha_baja = date('d/m/Y');
            $sc->estado     = 'baja';
            $sc->updated_by = auth()->user()->name;
            $sc->save();

            //Agregamos la nueva vinculacion de cliente servicio.
            $clientes_servicios = new ClientesServicios;
            $clientes_servicios->fk_cliente      = $sc->fk_cliente;
            $clientes_servicios->fk_servicio     = $request->input('idServicioElegido');
            $clientes_servicios->fecha_alta      = date('d/m/Y');
            $clientes_servicios->estado          = 'a_coordinar';
            $clientes_servicios->created_by      = auth()->user()->name;
            $clientes_servicios->save();

            $dir = Domicilio::where('fk_cliente_servicio', $request->input('idClienteServicio'))->first();
            $dirNew = $dir->replicate();
            $dirNew->fk_cliente_servicio = $clientes_servicios->id;
            $dirNew->created_by          = auth()->user()->name;
            $dirNew->save();

            DB::commit();
            return response('OK', 200);

        } catch (\PDOException $ex) {
            DB::rollback();
            return response()->view('ventas.clientes_servicios.cliente', array('message' => 'Guardado Correctamente.', 'status' => 1 ), 400);
        }
    }

    public function getLastNroAsiento(){
        $nro_asiento = Asientos::select(DB::raw("max(nro_asiento) as nro_asiento"))->get();

        if(empty($nro_asiento[0]->nro_asiento) or $nro_asiento[0]->nro_asiento == NULL)
            $last_nro_asiento = 0;
        else
            $last_nro_asiento = $nro_asiento[0]->nro_asiento;

        return $last_nro_asiento;
    }

    public function guardarServicioCliente(Request $request){

        $this->validate($request, [
            'departamento'          => 'required|not_in:0',
            'localidad'             => 'required|not_in:0',
            'calle'                 => 'required|max:100',
            'altura'                => 'required',
            'nro_depto'             => 'max:10',
            'cp'                    => 'max:10',
            'link_mapa'             => 'max:255',
            'observaciones'         => 'max:255',
        ]);

        try{
            DB::beginTransaction();
            $clientes_servicios = new ClientesServicios;
            $clientes_servicios->fk_cliente      = $request->input('id_cliente');
            $clientes_servicios->fk_servicio     = $request->input('sel-id-servicio');
            $clientes_servicios->fecha_alta      = $request->input('fecha_inicio');
            //$clientes_servicios->fecha_baja      = $request->input('fecha_fin');
            $clientes_servicios->estado          = 'activo';
            $clientes_servicios->created_by       = auth()->user()->name;
            $clientes_servicios->save();

            $domicilio = new Domicilio();
            $domicilio->fk_persona                 = $request->input('id_persona');
            $domicilio->fk_provincia               = $request->input('provincia');
            $domicilio->fk_departamento            = $request->input('departamento');
            $domicilio->fk_localidad               = $request->input('localidad');
            $domicilio->fk_cliente_servicio        = $clientes_servicios->id;
            $domicilio->tipo_domicilio             = 'servicio';
            $domicilio->piso                       = $request["piso"];
            $domicilio->depto                      = $request["nro_depto"];
            $domicilio->cp                         = $request["cp"];
            $domicilio->calle_paralela             = $request["calle_paralela"];
            $domicilio->calle_perpendicular_1      = $request["calle_perpendicular_1"];
            $domicilio->calle_perpendicular_2      = $request["calle_perpendicular_2"];
            $domicilio->observacion                = $request["observaciones"];
            $domicilio->latitud                    = $request["latitud"];
            $domicilio->longitud                   = $request["longitud"];
            $domicilio->fecha_desde                = $request["fecha_inicio"];
            $domicilio->fecha_hasta                = $request["fecha_fin"];
            $domicilio->link_mapa                  = $request["link_mapa"];
            $domicilio->calle                      = $request["calle"];
            $domicilio->altura                     = $request["altura"];
            $domicilio->created_by                 = Auth::user()->name;
            $domicilio->save();

            //insertar el registro en la orden de ventas
            /*
            $ov = new OrdenVenta;

            $ov->fecha          = date('d/m/Y');
            $ov->estado         = 'completa';
            $ov->fk_persona     = $c->persona->id;
            $ov->save();
            //total iva21 y 10.5
            $iva21 = 0;
            $iva105 = 0;
            $netoTotalSinIva = 0;
            foreach ($obj as $value) {
                $dov = new DetalleOrdenVenta;
                $dov->fk_orden_venta        = $ov->id;
                $dov->fk_articulo           = $value->articulo_id;
                $dov->fk_servicio           = $value->servicio_id;
                $dov->cantidad              = $value->cantidad;
                $dov->precio_unitario       = $value->precioUnitario;
                $dov->bonificacion          = $value->bonificacion;
                $divisor                    = 2 - ((100 - $value->alicuota_iva) / 100);
                $dov->importeIva            = ($value->precioUnitario - ($value->precioUnitario / $divisor)) * $value->cantidad;
                //sumamos el iva a la alicuota que corresponda.
                if($value->alicuota_iva == 21){
                    $iva21 += $dov->importeIva;
                }else if($value->alicuota_iva == 10.5){
                    $iva105 += $dov->importeIva;
                }
                $dov->alicuota_iva = $value->alicuota_iva;
                $dov->importe_totalsiniva   = ($value->precioUnitario / $divisor) * $value->cantidad;
                //sumamos el importe total sin iva al importe neto global
                $netoTotalSinIva += $dov->importe_totalsiniva;
                $dov->vencimiento_pago      = date("Y-m-d");
                $dov->save();
            }

            //actualizamos la orden de venta con el importen neto sin iva y las alicuotas.
            $ov = OrdenVenta::find($ov->id);
            $ov->importe_neto     = $netoTotalSinIva;
            $ov->importe_iva105   = $iva105;
            $ov->importe_iva21    = $iva21;
            $ov->save();
            */
            DB::commit();
            return redirect('/clientes/'. $request->input('id_cliente') . '/servicios');

        } catch (\PDOException $ex) {
            DB::rollback();
            dd($ex);
            return response()->view('ventas.clientes_servicios.cliente', array('message' => 'Guardado Correctamente.', 'status' => 1 ), 400);
        }
    }

    public function readServicioAJAX(Request $request){

    }
}
