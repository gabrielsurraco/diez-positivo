<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PlanDeCuentaDetalle;
use App\Pais;
use App\Departamento;
use App\Localidad;
use App\Personas;
use App\Clientes;
use App\Domicilio;
use App\Helper;
use App\Recibos;
use App\DetalleRecibos;
use App\OrdenVenta;
use App\PagosEfectuados;
use App\CuentaCorriente;
use App\Obligacion;
use DB;
use App\Facturacion;
use Carbon\Carbon;


class CobrosController extends Controller
{
    //
    public function index(){

        return view('ventas.cobros.indexCobros');
    }

    public function nuevoCobro(){
        return view('ventas.cobros.nuevoCobro');//compact('clientes','conceptos')
    }

    public function generarCobranza(Request $request){

          //$obj = json_decode($request->input('globalItems'));
          try {
              DB::beginTransaction();
              $r = new Recibos;
              $c = Clientes::findOrFail($request->input('idCliente'));

              $arrayObli   = $request->input('arrayObli');
              $importe_total       = 0;
              $pagoDelCliente      = floatval($request->input('pagoCliente'));

              foreach ($arrayObli as $ido) {
                  $o = Obligacion::with('ordenVenta.detalleOrdenVenta')->findOrFail($ido['idObligacion']);

                  $r->fk_cuenta       = $c->fk_cuenta;
                  $r->fecha           = date("Y-m-d");
                  $r->importe_total	  = $request->input('pagoCliente');
                  $r->fk_orden_venta  = $o->ordenVenta->id;
                  $r->created_by      = auth()->user()->name;
                  $r->save();


                  if($pagoDelCliente >= floatval($ido['saldoObli'][0]['saldoOV'])){
                      $rd = new DetalleRecibos;
                      $rd->concepto	                = $o->concepto. ' - PAGO TOTAL';
                      $rd->tipo_pago	              = 'total';
                      $rd->importe	                = floatval($ido['saldoObli'][0]['saldoOV']);
                      $rd->fk_recibo	              = $r->id;
                      $rd->fk_detalle_orden_venta   = null;
                      $rd->created_by               = auth()->user()->name;
                      $rd->save();
                      if($o->tipo_obl == null){
                        foreach ($o->ordenVenta->detalleOrdenVenta as $value) {
                            $rd = new DetalleRecibos;
                            $detalle = null;
                            if ($value->fk_servicio != null){
                                $detalle = $value->servicio->nombre . ' ('.$value->servicio->zona.')';
                            }else if ($value->fk_articulo != null){
                                $detalle = $value->articulo->marca . ' '.$value->articulo->modelo;
                            }else if ($value->fk_cliente_servicio != null){
                                $detalle = $value->cliente_servicio->servicio->nombre . ' ('.$value->cliente_servicio->servicio->zona.')';
                            }
                            $rd->concepto	                = $detalle;
                            $rd->tipo_pago	              = 'detalle';
                            $rd->importe	                = 0.00;
                            $rd->fk_recibo	              = $r->id;
                            $rd->fk_detalle_orden_venta   = null;
                            $rd->created_by               = auth()->user()->name;
                            $rd->save();
                        }//fin del foreach
                      }
                  }else{
                    $rd = new DetalleRecibos;
                    $rd->concepto	                = $o->concepto. ' - PAGO PARCIAL';
                    $rd->tipo_pago	              = 'parcial';
                    $rd->importe	                = $pagoDelCliente;
                    $rd->fk_recibo	              = $r->id;
                    $rd->fk_detalle_orden_venta   = null;
                    $rd->created_by               = auth()->user()->name;
                    $rd->save();
                    if($o->tipo_obl == null){
                      foreach ($o->ordenVenta->detalleOrdenVenta as $value) {
                          $rd = new DetalleRecibos;
                          $detalle = null;
                          if ($value->fk_servicio != null){
                              $detalle = $value->servicio->nombre . ' ('.$value->servicio->zona.')';
                          }else if ($value->fk_articulo != null){
                              $detalle = $value->articulo->marca . ' '.$value->articulo->modelo;
                          }else if ($value->fk_cliente_servicio != null){
                              $detalle = $value->cliente_servicio->servicio->nombre . ' ('.$value->cliente_servicio->servicio->zona.')';
                          }
                          $rd->concepto	                = $detalle;
                          $rd->tipo_pago	              = 'detalle';
                          $rd->importe	                = 0.00;
                          $rd->fk_recibo	              = $r->id;
                          $rd->fk_detalle_orden_venta   = null;
                          $rd->created_by               = auth()->user()->name;
                          $rd->save();
                      }//fin del foreach
                    }
                  }

                  //cargamos el pago efectuado del cliente en Pagos efectuados.
                  if($pagoDelCliente >= floatval($ido['saldoObli'][0]['saldoOV'])){
                      //cargamos el pago efectuado TOTAL
                      $pe                      = new PagosEfectuados;
                      $pe->fecha_pago          = date('d/m/Y');
                      $pe->importe             = floatval($ido['saldoObli'][0]['saldoOV']);
                      $pe->nro_comprobante     = null;
                      $pe->fk_orden_venta      = $o->ordenVenta->id;
                      $pe->fk_recibo           = $r->id;
                      $pe->fk_obligacion       = $ido['idObligacion'];
                      $pe->medio_pago          = 'EFECTIVO';
                      $pe->fk_obligacion       = $o->id;
                      $pe->cuotas              = 1;
                      $pe->fk_persona_cobrador = Auth::id();
                      $pe->descripcion         = 'PAGO A TRAVES DE COBRANZA MONTO TOTAL DE LA OBLIGACION';
                      $pe->fk_cuenta           = $c->fk_cuenta;
                      $pe->created_by          = auth()->user()->name;
                      $pe->save();
                      //Cargamos el pago en la cuenta corriente del cliente.
                      $cc = new CuentaCorriente;
                      $cc->fk_cuenta            = $c->fk_cuenta;
                      $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                      $cc->fecha                = date('d/m/Y');
                      $cc->debe                 = 0.00;
                      $cc->haber                = floatval($ido['saldoObli'][0]['saldoOV']);
                      $cc->descripcion          = 'PAGO DEL CLIENTE - OBLIGACION: '.$o->concepto;
                      $cc->fk_pagos_efectuado   = $pe->id;
                      $cc->created_by           = auth()->user()->name;
                      $cc->save();
                      //Marca la orden de venta como pagada.
                      OrdenVenta::find($o->ordenVenta->id)->update(['estado' => 'pagado']);
                      //actualiza el importe de lo que le resta al cliente de dinero para pagar.
                      $pagoDelCliente -= floatval($ido['saldoObli'][0]['saldoOV']);
                  }else{
                    if($pagoDelCliente != 0){
                      //cargamos el pago efectuado PARCIAL
                      $pe                      = new PagosEfectuados;
                      $pe->fecha_pago          = date('d/m/Y');
                      $pe->importe             = $pagoDelCliente;
                      $pe->nro_comprobante     = null;
                      $pe->fk_orden_venta      = $o->ordenVenta->id;
                      $pe->fk_recibo           = $r->id;
                      $pe->fk_obligacion       = $ido['idObligacion'];
                      $pe->medio_pago          = 'EFECTIVO';
                      $pe->cuotas              = 1;
                      $pe->fk_persona_cobrador = Auth::id();
                      $pe->descripcion         = 'PAGO A TRAVES DE COBRANZA MONTO PARCIAL DE LA OBLIGACION';
                      $pe->fk_cuenta           = $c->fk_cuenta;
                      $pe->created_by          = auth()->user()->name;
                      $pe->save();
                      //Cargamos el pago en la cuenta corriente del cliente.
                      $cc = new CuentaCorriente;
                      $cc->fk_cuenta            = $c->fk_cuenta;
                      $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                      $cc->fecha                = date('d/m/Y');
                      $cc->debe                 = 0.00;
                      $cc->haber                = $pagoDelCliente;
                      $cc->descripcion          = 'PAGO DEL CLIENTE - OBLIGACION: '.$o->concepto.' - PAGO PARCIAL';
                      $cc->fk_pagos_efectuado   = $pe->id;
                      $cc->created_by           = auth()->user()->name;
                      $cc->save();
                      //actualiza el importe de lo que le resta al cliente de dinero para pagar.
                      $pagoDelCliente -= $pagoDelCliente;
                    }
                  }
              }
              //si todavia hay saldo de pago a favor del cliente lo cargamos en
              //cuenta corriente como saldo a favor y pago efectuado sin Asignar
              //orden de venta y obligacion.
              if($pagoDelCliente > 0){
                //cargamos el pago efectuado TOTAL
                $pe                      = new PagosEfectuados;
                $pe->fecha_pago          = date('d/m/Y');
                $pe->importe             = $pagoDelCliente;
                $pe->nro_comprobante     = null;
                $pe->fk_orden_venta      = null;
                $pe->fk_recibo           = null;
                $pe->fk_obligacion       = null;
                $pe->medio_pago          = 'EFECTIVO';
                $pe->fk_obligacion       = null;
                $pe->cuotas              = 1;
                $pe->fk_persona_cobrador = Auth::id();
                $pe->descripcion         = 'PAGO A CUENTA';
                $pe->fk_cuenta           = $c->fk_cuenta;
                $pe->created_by          = auth()->user()->name;
                $pe->save();
                //Cargamos el pago en la cuenta corriente del cliente.
                $cc = new CuentaCorriente;
                $cc->fk_cuenta            = $c->fk_cuenta;
                $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                $cc->fecha                = date('d/m/Y');
                $cc->debe                 = 0.00;
                $cc->haber                = $pagoDelCliente;
                $cc->descripcion          = 'PAGO A CUENTA';
                $cc->fk_pagos_efectuado   = $pe->id;
                $cc->created_by           = auth()->user()->name;
                $cc->save();
                //Marca la orden de venta como pagada.
              }
              //consultar saldo del cliente y cargarlo en el recibo
              $items    = CuentaCorriente::where("fk_cuenta", $c->fk_cuenta)->get();
              $debe     = 0.00;
              $haber    = 0.00;
              foreach ($items as $item){
                  $debe     = $debe     + (float)$item->debe;
                  $haber    = $haber    + (float)$item->haber;
              }
              $saldo = (float)$haber - (float)$debe;

              $r = Recibos::findOrFail($r->id);
              $r->saldo_cliente = $saldo;
              $r->save();
              //DB::rollback();
              DB::commit();
              return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idRecibo' => $r->id), 200);
          } catch (Exception $ex) {
              DB::rollback();
              return response(array('mensage' => 'Error al guardar Recibo.', 'detalleError' => $e, 'idRecibo' => null), 400);
          }
    }

    public function informarPago(Request $request){
          //$obj = json_decode($request->input('globalItems'));

          try {
              DB::beginTransaction();
              $c = Clientes::findOrFail($request->input('idCliente'));

              $arrayObli           = $request->input('arrayOV');
              $importe_total       = 0;
              $pagoDelCliente      = floatval($request->input('pagoCliente'));

              foreach ($arrayObli as $ido) {
                  $o = Obligacion::with('ordenVenta.detalleOrdenVenta')->findOrFail($ido['idObligacion']);

                  if($pagoDelCliente >= floatval($ido['saldoObli'][0]['saldoOV'])){
                      //cargamos el pago efectuado TOTAL
                      $pe                      = new PagosEfectuados;
                      $pe->fecha_pago          = $request->input('fecha');
                      $pe->importe             = floatval($ido['saldoObli'][0]['saldoOV']);
                      $pe->nro_comprobante     = $request->input('reciboManualNum');
                      $pe->fk_orden_venta      = $o->ordenVenta->id;
                      $pe->fk_recibo           = null;
                      $pe->fk_obligacion       = $ido['idObligacion'];
                      $pe->medio_pago          = 'EFECTIVO';
                      $pe->fk_obligacion       = $o->id;
                      $pe->cuotas              = 1;
                      $pe->fk_persona_cobrador = Auth::id();
                      $pe->descripcion         = $request->input('descripcion');
                      $pe->fk_cuenta           = $c->fk_cuenta;
                      $pe->created_by          = auth()->user()->name;
                      $pe->save();
                      //Cargamos el pago en la cuenta corriente del cliente.
                      $cc = new CuentaCorriente;
                      $cc->fk_cuenta            = $c->fk_cuenta;
                      $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                      $cc->fecha                = $request->input('fecha');
                      $cc->debe                 = 0.00;
                      $cc->haber                = floatval($ido['saldoObli'][0]['saldoOV']);
                      $cc->descripcion          = 'PAGO DEL CLIENTE - RECIBO MANUAL NRO: '. $request->input('reciboManualNum') .' - OBLIGACION: '.$o->concepto;
                      $cc->fk_pagos_efectuado   = $pe->id;
                      $cc->created_by           = auth()->user()->name;
                      $cc->save();
                      //Marca la orden de venta como pagada.
                      OrdenVenta::find($o->ordenVenta->id)->update(['estado' => 'pagado']);
                      //actualiza el importe de lo que le resta al cliente de dinero para pagar.
                      $pagoDelCliente -= floatval($ido['saldoObli'][0]['saldoOV']);
                  }else{
                    if($pagoDelCliente != 0){
                      //cargamos el pago efectuado PARCIAL
                      $pe                      = new PagosEfectuados;
                      $pe->fecha_pago          = $request->input('fecha');
                      $pe->importe             = $pagoDelCliente;
                      $pe->nro_comprobante     = $request->input('reciboManualNum');
                      $pe->fk_orden_venta      = $o->ordenVenta->id;
                      $pe->fk_recibo           = null;
                      $pe->fk_obligacion       = $ido['idObligacion'];
                      $pe->medio_pago          = 'EFECTIVO';
                      $pe->cuotas              = 1;
                      $pe->fk_persona_cobrador = Auth::id();
                      $pe->descripcion         = 'PAGO A TRAVES DE COBRANZA MONTO PARCIAL DE LA OBLIGACION - RECIBO MANUAL NRO: '. $request->input('reciboManualNum') ;
                      $pe->fk_cuenta           = $c->fk_cuenta;
                      $pe->created_by          = auth()->user()->name;
                      $pe->save();
                      //Cargamos el pago en la cuenta corriente del cliente.
                      $cc = new CuentaCorriente;
                      $cc->fk_cuenta            = $c->fk_cuenta;
                      $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                      $cc->fecha                = $request->input('fecha');
                      $cc->debe                 = 0.00;
                      $cc->haber                = $pagoDelCliente;
                      $cc->descripcion          = 'PAGO DEL CLIENTE - RECIBO MANUAL NRO: '. $request->input('reciboManualNum') .' - OBLIGACION: '.$o->concepto.' - PAGO PARCIAL';
                      $cc->fk_pagos_efectuado   = $pe->id;
                      $cc->created_by           = auth()->user()->name;
                      $cc->save();
                      //actualiza el importe de lo que le resta al cliente de dinero para pagar.
                      $pagoDelCliente -= $pagoDelCliente;
                    }
                  }
              }

              //DB::rollback();
              DB::commit();
              return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null), 200);
          } catch (Exception $ex) {
              DB::rollback();
              return response(array('mensage' => 'Error al guardar Recibo.', 'detalleError' => $e), 400);
          }
    }


    //--Get datos de la deuda del cliente.
    public function getDatosDeudaFromClienteJSON(Request $request){
        $c        = Clientes::findOrFail(1344); //$request->input('idCliente')

        //sacamos datos de la cuenta corriente
        $cc       = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->get();
        $debe     = 0.00;
        $haber    = 0.00;

        foreach ($cc as $item){
            $debe     = $debe     + (float)$item->debe;
            $haber    = $haber    + (float)$item->haber;
        }

        $saldo = (float)$haber - (float)$debe;

        $cuentaC = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)
                                  ->orderBy('id', 'desc')
                                  ->take(10)
                                  ->get();

        //sacamos datos de Facturacion
        $fact = Facturacion::where('fk_cuenta', $c->fk_cuenta)
                                 ->orderBy('id', 'desc')
                                 ->take(10)
                                 ->get();
        //pagos efectuados por el cliente
        $pagRea = PagosEfectuados::where('fk_cuenta', $c->fk_cuenta)
                                  ->orderBy('id', 'desc')
                                  ->take(1)
                                  ->get();
        $json = Array('CuentaCorriente'=> $cuentaC, 'Facturas' => $fact, 'SaldoCC'=>$saldo, 'PagoEfectuado' => $pagRea);

      return response(json_encode($json),200);
    }
}
