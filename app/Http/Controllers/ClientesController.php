<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanDeCuentaDetalle;
use App\Pais;
use App\Departamento;
use App\Localidad;
use App\Personas;
use App\Clientes;
use App\Domicilio;
use App\Helper;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Telefono;
use App\Cuenta;
use App\ClientesServicios;


//cargo este controlador por la funcion que consulta el proximo codigo de cuenta. buscarSiguienteCodigoCuenta
use App\Http\Controllers\ContabilidadController;
use App\ParentescosRelaciones;



class ClientesController extends Controller
{
    public function listarClientes(){
        $clientes = Clientes::with(['persona.domicilio.localidad', 'persona.domicilio.departamento'])->where('activo', true)->get();
        //dd($clientes);
        return view('ventas.clientes.index', ['clientes' => $clientes]);
    }
    public function index(Request $request){

        if (is_null($request->input('busqueda-nombre')) && is_null($request->input('busqueda-apellido')) && is_null($request->input('busqueda-departamento')) && is_null($request->input('busqueda-localidades'))){
            $filtro = [];
        }else{
            //tomamos los valores y hacemos la busqueda en base a lo que el usuario necesita:
            $input_nombre        = $request['busqueda-nombre'];
            $input_apellido      = $request['busqueda-apellido'];
            $input_departamento  = $request['busqueda-departamento'];
            $input_localidades   = $request['busqueda-localidades'];

            $datosFiltrados = array();
            $datosFiltrados['bs_nombre']        = $input_nombre;
            $datosFiltrados['bs_apellido']      = $input_apellido;
            $datosFiltrados['bs_departamento']  = $input_departamento;

            if($input_localidades != null)
                $datosFiltrados['bs_localidades']   = implode(',', $input_localidades);
            else
                $datosFiltrados['bs_localidades']   = "";

            $filtro =   DB::table('personas as p')
                        ->select(   'c.id as clienteID',
                                    'c.codigo as clienteCodigo',
                                    'p.nombre as clienteNombre',
                                    'p.apellido as clienteApellido',
                                    'p.es_persona_fisica as clientePersonaFisica',
                                    'p.razon_social as clienteRazonSocial',
                                    'p.tipo_doc as clienteTipoDoc',
                                    'p.cuit as clienteCUIT',
                                    'dom.piso as clienteDomicilioPiso',
                                    'dom.cp as clienteDomicilioCP',
                                    'dom.calle as clienteDomicilioCalle',
                                    'dom.altura as clienteDomicilioCalleAltura',
                                    'prov.nombre as clienteProvincia',
                                    'dept.nombre as clienteDepartamento',
                                    'loc.nombre as clienteLocalidad',
                                    'p.email as clienteEmail1'
                        )
                        ->join('clientes as c', 'c.fk_persona', 'p.id')
                        ->join('domicilios as dom', 'dom.fk_persona', 'p.id')
                        ->join('provincias as prov', 'dom.fk_provincia', 'prov.id')
                        ->join('departamentos as dept','dom.fk_departamento','dept.id')
                        ->join('localidades as loc','dom.fk_localidad','loc.id')
                        ->where('tipo_domicilio', '=', 'fiscal');

            if($input_nombre != "")
                  $filtro->where('p.nombre', 'LIKE', '%'.$input_nombre. '%');

            if($input_apellido != "")
                $filtro->where('p.apellido', 'LIKE', '%'.$input_apellido. '%');

            if($input_departamento != "default")
                $filtro->where('dept.id', '=', $input_departamento);

            if($input_localidades != "")
                $filtro->whereIn('loc.id', $input_localidades);

            $filtro = $filtro->groupBy('c.id')->get();

      }

      $departamentos = Departamento::all();
      $array_departamentos = array();
      foreach($departamentos as $key => $value){
         $array_departamentos[$value->id] =  $value->nombre;
      }

      return view('ventas.clientes.index',  compact('filtro','array_departamentos', 'datosFiltrados'));
    }

    //leer todos los datos del cliente y pasar a Json
    public function getDatosClienteToJson(Request $request){
        $cliente = Clientes::find($request->input('idCliente'));
        $persona = Personas::find($cliente->fk_persona);
        $domicilio = Domicilio::with('localidad', 'departamento')->where('fk_persona', '=', $cliente->fk_persona)->get();

        $cliente['datosPersona'] = $persona;
        $cliente['datosPersona']['domicilio'] = $domicilio;

        return response(json_encode($cliente),200);
    }


    //Paginacion de listar cliente mediante Ajax
    public function ajaxPaginacion(Request $request){

        $busqueda = $request->input('search');

        $filtro = DB::table('personas as p')
                            ->select('p.nombre as clienteNombre','p.apellido as clienteApellido', 'p.tipo_doc as clienteTipoDoc',
                                      'p.cuit as clienteCUIT',
                                      'dp.piso as clienteDomicilioPiso', 'dp.cp as clienteDomicilioCP',
                                      'p.email as clienteEmail1'
                                      )
                            ->join('clientes as c','c.fk_persona','p.id')
                            ->join('`domicilios` as dp','dp.fk_persona','p.id')
                            ->get();

        dd($filtro);
         /*Clientes::with('persona', 'persona.domicilio','persona.domicilio.departamento','persona.domicilio.localidad','persona.domicilio.provincia')
                        ->whereHas('persona', function($query) use ($busqueda)
                        {
                        		$query->where('personas.nombre', 'like', '%' . $busqueda['value'] . '%')
                            ->orWhere('personas.apellido', 'like', '%' . $busqueda['value'] . '%');
                        })
                        ->get();*/

        $arr = array();
        $arrData = array();
        foreach ($filtro as $key => $cliente) {
            $arr["codigo"] = $cliente->codigo;
            $arr["cliente"] = $cliente->persona->apellido . "-". $cliente->persona->nombre;
            $arr["cuit"] = $cliente->persona->cuit;
            $arr["email"] = $cliente->persona->email;
            $arr["telefono"] = $cliente->persona->telefono;
            $arr["domicilio"] = substr($cliente->persona->domicilio->localidad->nombre .' - '. $cliente->persona->domicilio->calle .' '. $cliente->persona->domicilio->altura . ' CP: ' . $cliente->persona->domicilio->altura . ' Piso: ' . $cliente->persona->domicilio->piso . 'Depto: ' . $cliente->persona->domicilio->departamento , 0, 40);
            $arr["estado"] = "NORMAL";
            $arr["accion"] = '<a class="btn btn-xs btn-success" href="/ventas/clientes/' . $cliente->id . '/edit" title="Editar"><i class="fa fa-edit"></i></a>
            <a class="btn btn-xs btn-warning" title="Seguimiento de Ticket`s"><i class="fa fa-ticket"></i></a>
            <a class="btn btn-xs btn-primary" href="/ventas/clientes/' . $cliente->id . '/servicios" title="Servicios"><i class="fa fa-shopping-cart"></i></a>
            <a class="btn btn-xs btn-danger"  title="Cuenta Corriente"><i class="fa fa-money"></i></a>';
            array_push($arrData, $arr);
        }


        $obj = (object) array('data' => $arrData);
        return json_encode($obj);
    }

    //Este Json permite utilizarlo en el Select2 para realizar una busqueda de
    //cliente con 3 caracteres.
    public function jsonSearchCliente(Request $request){
        $clientes = Clientes::whereHas('persona', function ($query) use ($request) {
                      $query->where('nombre', 'like', '%' . $request->input('palabraClave') . '%')
                             ->orWhere('apellido', 'like', '%' . $request->input('palabraClave') . '%')
                             ->orWhere('cuit', 'like', '%' . $request->input('palabraClave') . '%')
                             ->orWhere('codigo', 'like', '%' . $request->input('palabraClave') . '%');
                  })->where('activo', 1)->get();

        $createArray = array();
        foreach ($clientes as $value) {
          array_push($createArray, array('id' => $value->id , 'text' => $value->codigo.' - '. $value->persona->apellido.' '.$value->persona->nombre.' ('.$value->persona->cuit.')'));
        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }

    public function getNextCodeOfSubCuenta($id_cuenta){
        $max_cod_subcuenta = PlanDeCuentaDetalle::select(DB::raw('max(codigo) as codigo'))->where("id_superior",$id_cuenta)->get();
        $id_superior = $id_cuenta;

        if(count($max_cod_subcuenta[0]["codigo"]) == 0){
            $cod_raiz_cuenta    = PlanDeCuentaDetalle::select(DB::raw('codigo'))->where("id",$id_superior)->get();
            $cod                = $cod_raiz_cuenta[0]["codigo"] . ".1";
        }else{
            $resultado              = explode(".", $max_cod_subcuenta[0]["codigo"]);
            $cant                   = count($resultado);
            $resultado[$cant - 1]   = $resultado[$cant - 1] + 1;

            for($i=0;$i < $cant; $i++){
                if($i==0){
                    $cod = $resultado[$i];
                }else{
                    $cod .= "." . $resultado[$i];
                }
            }
        }

        return $cod;
    }


    //Guardar cliente nuevo.
    public function guardarCliente(Request $request){
        try {
          DB::beginTransaction();
          $persona = Personas::findOrFail( $request->input('idPersona'));

          // CREO LA CUENTA DE LA PERSONA EN EL PLAN DE CUENTA
          $sub_cuenta                 = new Cuenta();
          $sub_cuenta->nombre         = 'CUENTA CLIENTE - ' . strtoupper($persona->apellido). ' '. strtoupper($persona->nombre). ' - '. strtoupper($persona->cuit);
          $sub_cuenta->codigo         = Helper::buscarSiguienteCodigoCuenta(8, NULL , 0);
          $sub_cuenta->imputable      = 1;
          $sub_cuenta->id_superior    = 8; //el superior es CREDITOS POR VENTAS
          $sub_cuenta->save();

          $c = new Clientes();
          $c->codigo     = $request->input('codigo');
          $c->fk_persona = $request->input('idPersona');
          $c->fk_cuenta  = $sub_cuenta->id;
          $c->created_by = Auth::user()->name;
          $c->save();
          DB::commit();
          return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idCliente' => $c->id), 200);
        } catch (Exception $ex) {
          DB::rollback();
          return response(array('mensage' => 'Error al guardar Recibo.', 'detalleError' => $e, 'idCliente' => null), 400);
        }
    }

    //ver datlles del cliente
    public function detalleCliente($id){
      $cliente     = Clientes::findOrFail($id);
      $parentescos = ParentescosRelaciones::where('fk_cliente', $id)->where('activo',true)->get();
      //dd($parentescos);
      $cliente = Clientes::findOrFail($id);

      $departamentos = Departamento::all();
      $array_departamentos = array();
      foreach($departamentos as $key => $value){
         $array_departamentos[$value->id] =  $value->nombre;
      }

      $localidades = Localidad::all();
      $array_localidades = array();
      foreach($localidades as $key => $value){
         $array_localidades[$value->id] =  $value->nombre;
      }

      $cs = ClientesServicios::where('fk_cliente', $id)
                              ->where('estado', '!=', 'baja')
                              ->get();
      //dd($cliente);
      return view('ventas.clientes.detalle', ['cliente'=> $cliente, 'parentescos' => $parentescos, 'cs' => $cs]);
    }

}
