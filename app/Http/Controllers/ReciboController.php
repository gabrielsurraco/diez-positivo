<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Recibos;
use App\DetalleRecibos;
use App\Clientes;
use App\OrdenVenta;
use App\PagosEfectuados;
use App\CuentaCorriente;
use DB;

class ReciboController extends Controller
{
    //Inicio Servicios
    public function list(){
        $r = Recibos::all();

        return view('recibos.index', [
          "ubicacion" => "recibos.index",
          "recibos"    => $r
        ]);
    }

    //Inicio Servicios
    public function reciboPrint($id){
        $r = Recibos::findOrFail($id);
        $c = Clientes::where('fk_cuenta', $r->fk_cuenta)->first();

        return view('recibos.imprimir', [
          "ubicacion" => "recibos.imprimir",
          "recibo"    => $r,
          "cliente"   => $c
        ]);
    }

    //Guardar recibo
    public function generarRecibo(Request $request){

          $obj = json_decode($request->input('globalItems'));
          try {
              DB::beginTransaction();
              $r = new Recibos;
              $c = Clientes::where('fk_persona', $request->input('idCliente'))->first();

              $r->fk_cuenta       = $c->fk_cuenta;
              $r->fecha           = date("Y-m-d");
              $r->importe_total	  = $request->input('importeTotal');
              $r->fk_orden_venta  = $request->input('idOrdenVenta');
              $r->save();

              foreach ($obj as $value) {
                $rd = new DetalleRecibos;
                $rd->concepto	                = $value->descripcion_item;
                $rd->tipo_pago	              = 'total';
                $rd->importe	                = $value->total;
                $rd->fk_recibo	              = $r->id;
                $rd->fk_detalle_orden_venta   = $value->idDetalleOV;
                $rd->save();
              }

              DB::commit();
              return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idRecibo' => $r->id), 200);
          } catch (Exception $ex) {
              DB::roolback();
              return response(array('mensage' => 'Error al guardar Recibo.', 'detalleError' => $e, 'idRecibo' => null), 400);
          }
    }

    public function anularRecibo(Request $request){
        try {
            DB::beginTransaction();
            $pe = PagosEfectuados::where('fk_recibo', $request->input('idRecibo'))->get();
            $recibo = Recibos::findOrFail($request->input('idRecibo'));

            foreach ($pe as $value) {
                OrdenVenta::find($value->fk_orden_venta)->update(['estado' => 'completa', 'updated_by' => auth()->user()->name]);
            }
            PagosEfectuados::where('fk_recibo', $request->input('idRecibo'))->update(['anulado' => 1, 'updated_by' => auth()->user()->name]);

            $cc = new CuentaCorriente;
            $cc->fk_cuenta            = $recibo->fk_cuenta;
            $cc->nro_secuencia        = CuentaCorriente::where('fk_cuenta', $recibo->fk_cuenta)->max('nro_secuencia') + 1;
            $cc->fecha                = date('d/m/Y');
            $cc->debe                 = $recibo->importe_total;
            $cc->haber                = 0.00;
            $cc->descripcion          = 'ANULACION DE RECIBO NRO: '.str_pad($request->input('idRecibo'), 8, "0", STR_PAD_LEFT);
            $cc->created_by           = auth()->user()->name;
            $cc->save();

            $recibo->update(['anulado' => 1, 'motivo' => $request->input('motivo'), 'updated_by' => auth()->user()->name]);

            DB::commit();
            return response(array('mensage' => 'Anulado Correctamente.', 'detalleError' => null, 'idRecibo' => $recibo->id), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al anular Recibo.', 'detalleError' => $e, 'idRecibo' => null), 400);
        }
    }
}
