<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PDOException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class UsuariosController extends Controller
{
    public function index(){
        $usuarios = User::all();
        return view("usuarios.usuarios_index", compact("usuarios"));
    }
    
    public function nuevoUsuario(){
        return view("usuarios.usuarios_create");
    }
    
    public function editarUsuario(Request $request){
        $usuario = User::findOrFail($request->id);
        return view("usuarios.usuarios_edit", compact('usuario'));
    }
    
    public function storeEditionUser(Request $request){
        $this->validate($request, [
            'name'                      => 'required|min:3',
            'email'                     => 'required|min:5',
            'dni'                       => 'required',
            'password'                  => 'required|same:password_confirmation',
            'password_confirmation'     => 'required',
        ]);
        
        try{
            User::where('id',$request->id_usuario)->update([
                'name'                  => $request->name,
                'dni'                   => $request->dni,
                'email'                 => $request->email,
                'password'              => bcrypt($request->password),
                'es_cobrador'           => (isset($request->cobrador)) ? $request->cobrador : 0,
                'es_tecnico'            => (isset($request->tecnico)) ? $request->tecnico : 0,
                'es_administrativo'     => (isset($request->administracion)) ? $request->administracion : 0,
                'es_contabilidad'       => (isset($request->contabilidad)) ? $request->contabilidad : 0,
                'es_admin'              => (isset($request->admin)) ? $request->admin : 0,
            ]);
        } catch (\PDOException $ex) {
            return redirect()->back()->with('msg', 'El usuario no pudo ser actualizado! '.$ex->getMessage());
        }
        
        return redirect('/usuarios')->with('msg','Se actualizó el usuario correctamente!');
    }

    public function eliminarUsuario(Request $request){
        
        try{
            User::destroy($request["id_usuario"]);
        } catch (PDOException $ex) {
            return Response::json(array(
                'result'    => $ex->getMessage()
            ), 400);
        }
        
        return Response::json(array('result' => 'ok'), 200);
    }
    
    public function crearUsuario(Request $request){
        
        $this->validate($request, [
            'name'                      => 'required|min:3',
            'email'                     => 'required|min:5',
            'dni'                       => 'required',
            'password'                  => 'required|same:password_confirmation',
            'password_confirmation'     => 'required',
        ]);
        
        try{
            User::create([
                'name'                  => $request->name,
                'dni'                   => $request->dni,
                'email'                 => $request->email,
                'password'              => bcrypt($request->password),
                'es_cobrador'           => (isset($request->cobrador)) ? $request->cobrador : 0,
                'es_tecnico'            => (isset($request->tecnico)) ? $request->tecnico : 0,
                'es_administrativo'     => (isset($request->administracion)) ? $request->administracion : 0,
                'es_contabilidad'       => (isset($request->contabilidad)) ? $request->contabilidad : 0,
                'es_admin'              => (isset($request->admin)) ? $request->admin : 0,
                
            ]);
        } catch (\PDOException $ex) {
            return redirect()->back()->with('msg', 'El usuario no pudo ser creado! '.$ex->getMessage());
        }
        
        return redirect('/usuarios')->with('msg','Se creo el usuario correctamente!');
        
    }
    
}
