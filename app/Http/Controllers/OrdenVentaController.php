<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Articulos;
use App\Servicios;
use App\Clientes;
use App\OrdenVenta;
use App\DetalleOrdenVenta;
use App\Recibos;
use App\PagosEfectuados;
use App\CuentaCorriente;
use App\Obligacion;
use DB;

class OrdenVentaController extends Controller
{

    //
    //**************************************************************************
    ///////////////////////////////Orden de Ventas//////////////////////////////
    //**************************************************************************


    public function indexOrdenesVenta(){
        $ordenVenta = OrdenVenta::all();
        return view('ventas.cobros.indexOrdenesVenta', compact('ordenVenta'));//compact('clientes','conceptos')
    }

    public function editOrdenVenta($id){
        $ov  = OrdenVenta::find($id);
        $dov = DetalleOrdenVenta::with(['articulo', 'servicio', 'cliente_servicio.servicio'])->where('fk_orden_venta', $id)->get();
        $r   = Recibos::where('fk_orden_venta', $id)->first();
        return view('ventas.cobros.nuevaOrdenVenta', [
          "ubicacion" => "compras.editOrdenVenta",
          "ov"   => $ov,
          "dov"  => $dov,
          "recibo" => $r
        ]);
    }

    public function nuevaOrdenVenta(){
        $recibo = null;
        return view('ventas.cobros.nuevaOrdenVenta', compact('clientes','recibo'));//compact('clientes','conceptos')
    }

    public function jsonSearchArticuloServicio(Request $request){
        $a = Articulos::where('activo',1)
                        ->where('marca', 'like', '%' . $request->input('palabraClave') . '%')
                        ->orwhere('modelo', 'like', '%' . $request->input('palabraClave') . '%')->get();
        $createArray = array();
        foreach ($a as $value) {
          array_push($createArray, array('id' => $value->id.'-art' , 'text' => $value->marca.'-'. $value->modelo));
        }

        $s = Servicios::where('activo',1)
                        ->where('nombre', 'like', '%' . $request->input('palabraClave') . '%')
                        ->orWhereHas('localidad', function ($query) use ($request) {
                            $query->where('nombre', 'like', '%' . $request->input('palabraClave') . '%');
                        })
                        ->get();
        foreach ($s as $value) {
          if($value->localidad != null){
              array_push($createArray, array('id' => $value->id.'-serv'  , 'text' => $value->nombre.'-'. $value->zona. ' ('. $value->localidad->nombre.')'));
          }else{
              array_push($createArray, array('id' => $value->id.'-serv'  , 'text' => $value->nombre.'-'. $value->zona. ' ( Todas Loc. Exepto Obera)'));
          }
        }

        $send = array('results' => $createArray);
        return response(json_encode($send));
    }

    //traer las ordenes de venta from cliente
    public function getOrdenesVentaFromClienteJSON(Request $request){
        $c = Clientes::findOrFail($request->input('idCliente'));
        if($request->input('soloCobradas') == "true"){
          $o = Obligacion::with('ordenVenta.detalleOrdenVenta')
                          ->whereHas('ordenVenta.detalleOrdenVenta', function ($query) {
                              $query->where('orden_venta.estado', 'completa');
                          })
                          ->where('fk_cuenta', $c->fk_cuenta)
                          ->get();
        }else{
          $o = Obligacion::with('ordenVenta.detalleOrdenVenta')
                          ->whereHas('ordenVenta.detalleOrdenVenta', function ($query) {
                              $query->where('orden_venta.estado', '!=', 'borrador');
                          })
                          ->where('fk_cuenta', $c->fk_cuenta)
                          ->get();
        }

        foreach ($o as $value) {
            $pe = PagosEfectuados::where('fk_obligacion', $value->id)->where('anulado', false)->sum('importe');
            $value->pagoTotalEfectuado = $pe;
            $value->saldoOV  = $value->monto - $pe;
            $value->pagos_efectuados = PagosEfectuados::where('fk_obligacion', $value->id)->where('anulado', false)->get();
        }
        return response(json_encode($o));
    }

    public static function getOrdenesVentaVencidasEImpagas($cuenta_id){
        $c  = Clientes::where('fk_cuenta' , (int)$cuenta_id)->first();
        $ov = OrdenVenta::with('detalleOrdenVenta','pagosEfectuados')->where('fk_persona', $c->fk_persona)->where('vencimiento_pago','<',date('Y-m-d'))->whereIn('estado',['completa','facturada'])->get();

        foreach ($ov as $value) {
            $pe = PagosEfectuados::where('fk_orden_venta', $value->id)->sum('importe');
            $value->pagoTotalEfectuado = $pe;
        }

        return $ov;
    }

    public function getPrecioIVAArticuloOServicio(Request $request){
        $item = explode('-', $request->input('id'));
        if ($item[1] == 'art'){
          $a = Articulos::find($item[0]);
          return array('precio' => $a->precio, 'iva' => $a->alicuota_iva);
        }else{
          $s = Servicios::find($item[0]);
          return array('precio' => $s->precio, 'iva' => $s->alicuota_iva);
        }
    }

    //Guardar orden de Venta
    public function saveOrdenVenta(Request $request){

          $obj = json_decode($request->input('globalItems'));
          try {
              DB::beginTransaction();

              if ($request->input('idOrdenVenta') != null){
                  $ov = OrdenVenta::find($request->input('idOrdenVenta'));
                  $c = Clientes::with('persona')->where('fk_persona', $request->input('idCliente'))->first();
                  $ov->updated_by       = auth()->user()->name;
              }else{
                  $ov = new OrdenVenta;
                  $c = Clientes::find($request->input('idCliente'));
                  $ov->created_by       = auth()->user()->name;
              }

              $ov->fecha            = $request->input('fecha');
              $ov->estado           = $request->input('estado');
              $ov->vencimiento_pago = $request->input('fechaVenc');
              $ov->fk_persona       = $c->persona->id;
              $ov->save();

              if ($request->input('idOrdenVenta') != null){
                  DetalleOrdenVenta::where('fk_orden_venta', '=', $request->input('idOrdenVenta'))->delete();
              }

              //total iva21 y 10.5
              $iva21 = 0;
              $iva105 = 0;
              $netoTotalSinIva = 0;
              foreach ($obj as $value) {
                  $dov = new DetalleOrdenVenta;
                  $dov->fk_orden_venta        = $ov->id;
                  $dov->fk_articulo           = $value->articulo_id;
                  $dov->fk_servicio           = $value->servicio_id;
                  $dov->cantidad              = $value->cantidad;
                  $dov->observacion           = $value->observacion;
                  $dov->precio_unitario       = $value->precioUnitario;
                  $dov->bonificacion          = $value->bonificacion;
                  $divisor                    = 2 - ((100 - $value->alicuota_iva) / 100);
                  $dov->importeIva            = ($value->precioUnitario - ($value->precioUnitario / $divisor)) * $value->cantidad;
                  //sumamos el iva a la alicuota que corresponda.
                  if($value->alicuota_iva == 21){
                      $iva21 += $dov->importeIva;
                  }else if($value->alicuota_iva == 10.5){
                      $iva105 += $dov->importeIva;
                  }
                  $dov->alicuota_iva = $value->alicuota_iva;
                  $dov->importe_totalsiniva   = ($value->precioUnitario / $divisor) * $value->cantidad;
                  //sumamos el importe total sin iva al importe neto global
                  $netoTotalSinIva += $dov->importe_totalsiniva;
                  $dov->created_by       = auth()->user()->name;
                  $dov->save();
              }

              //actualizamos la orden de venta con el importen neto sin iva y las alicuotas.
              $ov = OrdenVenta::find($ov->id);
              $ov->importe_neto     = $netoTotalSinIva;
              $ov->importe_iva105   = $iva105;
              $ov->importe_iva21    = $iva21;
              $ov->save();

              if ($request->input('estado') == 'completa'){
                  //registrar item por item a la Obligacion
                  $o = new Obligacion;
                  $o->fk_cuenta              = $c->fk_cuenta;
                  $o->concepto               = 'O.V. NRO: '.str_pad($ov->id, 8, "0", STR_PAD_LEFT);
                  $o->monto                  = $netoTotalSinIva + $iva105 + $iva21;
                  $o->periodo                = (String) Date('Ym');
                  $o->fk_orden_venta_detalle = null;
                  $o->fk_orden_venta         = $ov->id;
                  $o->fecha_vencimiento      = $request->input('fechaVenc');
                  $o->aplica_interes         = true;
                  $o->created_by             = auth()->user()->name;
                  $o->save();

                  $cc = new CuentaCorriente;
                  $cc->fk_cuenta        = $c->fk_cuenta;
                  $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $c->fk_cuenta)->max('nro_secuencia') + 1;
                  $cc->fecha            = $request->input('fecha');
                  $cc->debe             = $netoTotalSinIva + $iva105 + $iva21;
                  $cc->haber            = 0.00;
                  $cc->descripcion      = 'ORDEN DE VENTA NRO: '.str_pad($ov->id, 8, "0", STR_PAD_LEFT);
                  $cc->fk_obligacion    = $o->id;
                  $cc->created_by       = auth()->user()->name;
                  $cc->save();
              }

              DB::commit();
              return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idOrdenVenta' => $ov->id), 200);
          } catch (Exception $ex) {
              DB::roolback();
              return response(array('mensage' => 'Error al guardar Orden de Venta.', 'detalleError' => $e, 'idOrdenVenta' => null), 400);
          }
    }

    //Esta funcion
    public function asociarFacturaToOV(Request $request){
          try {
              DB::beginTransaction();

              PagosEfectuados::where('fk_orden_venta', $request->input('idOrdenVenta'))->update(['nro_comprobante' => $request->input('asocFactura'), 'updated_by' => auth()->user()->name]);

              DB::commit();
              return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null), 200);
          } catch (Exception $ex) {
              DB::roolback();
              return response(array('mensage' => 'Error al guardar Orden de Venta.', 'detalleError' => $e), 400);
          }
    }

    //Imprimir Orden de Venta diferentes modalidades (Presupuesto - Factura)
    public function printOrdenVenta($tipo, $id){
          if($tipo == 'PRESUPUESTO'){
              $ov = OrdenVenta::with('detalleOrdenVenta')->where('id', $id)->first();
          }else{
              //programar facturacion electronica.
          }
          return view( 'ventas.cobros.imprimirOrdenVenta', ['ordenVenta' => $ov]);
    }

}
