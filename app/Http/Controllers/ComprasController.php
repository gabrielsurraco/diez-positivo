<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\OrdenCompra;
use App\Proveedores;
use App\DetalleOrdenCompra;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class ComprasController extends Controller
{
    //
    public function index(){
        $importes = DB::select(DB::raw('SELECT fk_orden_de_compra, SUM(precio*cantidad) as importe FROM detalle_orden_compra WHERE activo = 1 GROUP BY fk_orden_de_compra'));
        $ordenCompra = OrdenCompra::where('activo', 1)->paginate(10);

        $p = Proveedores::with('persona')
                         ->where('activo', true)
                         ->get();
        return view( 'compras.index', [
          "ordenCompra" => $ordenCompra,
          "importes"    => $importes,
          "proveedores" => $p
        ]);
    }

    public function jsonListOrdenCompraSolicitadas(){
        $ordenCompra = new OrdenCompra;
        $importes = DB::select(DB::raw('SELECT fk_orden_de_compra, SUM(precio*cantidad) as importe FROM detalle_orden_compra WHERE activo = 1 GROUP BY fk_orden_de_compra'));
        $ordenCompra = $ordenCompra->where('activo', 1)
                                   ->where('estado', 'solicitado')->get();

        $arraySend = array('ordenCompra' => $ordenCompra, 'importes' => $importes);
        return response(json_encode($arraySend));
    }

    public function jsonListOrdenCompraWithDetalle(Request $request){
        $oc  = OrdenCompra::find($request->input('idOrdenCompra'));
        $doc = DetalleOrdenCompra::with('articulo')->where('fk_orden_de_compra', $request->input('idOrdenCompra'))->get();
        $arraySend = array('ordenCompra' => $oc, 'detalle' => $doc);
        return response(json_encode($arraySend));
    }

    public function buscarFiltro(Request $request){
        $ordenCompra = new OrdenCompra;
        if ($request->input('start') != $request->input('end')){
            $ordenCompra = $ordenCompra->whereBetween('fecha', [Carbon::createFromFormat('d/m/Y', $request->input('start'))->toDateString(), Carbon::createFromFormat('d/m/Y', $request->input('end'))->toDateString()]);
        }
        if (trim($request->input('nro-orden')) != ""){
            $ordenCompra = $ordenCompra->where('id', '=', $request->input('nro-orden'));
        }
        if ($request->input('sel-proveedor') != null){
            $ordenCompra = $ordenCompra->where('fk_proveedor', '=', $request->input('sel-proveedor'));
        }
        $importes = DB::select(DB::raw('SELECT fk_orden_de_compra, SUM(precio*cantidad) as importe FROM detalle_orden_compra WHERE activo = 1 GROUP BY fk_orden_de_compra'));
        $ordenCompra = $ordenCompra->where('activo', 1)
                                    ->paginate(10);

        return view( 'compras.index', [
          "ordenCompra" => $ordenCompra,
          "importes"    => $importes,
          "old"         => Input::all()
        ]);
    }

    /**/
    public function nuevaOrdenCompra(){
        return view('compras.nuevaOrdenCompra', [
          "ubicacion" => "compras.nuevoOrdenCompra"
        ]);
    }

    public function editOrdenCompra($id){
        $oc  = OrdenCompra::find($id);
        $doc = DetalleOrdenCompra::with('articulo')->where('fk_orden_de_compra', $id)->get();

        return view('compras.nuevaOrdenCompra', [
          "ubicacion" => "compras.editOrdenCompra",
          "oc"   => $oc,
          "doc"  => $doc
        ]);
    }

    public function printOrdenCompra($id){
        $oc  = OrdenCompra::find($id);
        $doc = DetalleOrdenCompra::with('articulo')->where('fk_orden_de_compra', $id)->get();
        //dd($doc);
        return view('compras.imprimirOrdenCompra', [
          "ubicacion" => "compras.imprimirOrdenCompra",
          "oc"   => $oc,
          "doc"  => $doc
        ]);
    }

    public function deleteOrdenCompra(Request $request){

        try {
            DB::beginTransaction();
            OrdenCompra::where('id', $request->input('id'))
                       ->update(['activo' => 0]);
            DetalleOrdenCompra::where('fk_orden_de_compra', '=', $request->input('id'))->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null, 'idOrdenCompra' => $request->input('id')), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al Eliminar OrdenCompra.', 'detalleError' => $e, 'idOrdenCompra' => null), 400);
        }
    }

    //Guardar orden de Compra
    public function saveOrdenCompra(Request $request){

          $obj = json_decode($request->input('globalItems'));
          try {
              DB::beginTransaction();

              if ($request->input('idOrdenCompra') != null){
                  $oc = OrdenCompra::find($request->input('idOrdenCompra'));
              }else{
                  $oc = new OrdenCompra;
              }

              $oc->fecha                              = $request->input('fecha');
              $oc->fk_proveedor                       = $request->input('proveedor');
              $oc->estado                             = $request->input('estado');
              $oc->fecha_entrega_producto_estimada    = $request->input('entrega_aprox');
              $oc->save();

              if ($request->input('idOrdenCompra') != null){
                  DetalleOrdenCompra::where('fk_orden_de_compra', '=', $request->input('idOrdenCompra'))->delete();
              }

              foreach ($obj as $value) {
                  $doc = new DetalleOrdenCompra;
                  $doc->fk_orden_de_compra    = $oc->id;
                  $doc->cantidad              = $value->cantidad;
                  $doc->fk_articulo           = $value->articulo_id;
                  $doc->precio                = $value->importe;
                  $doc->save();
              }
              DB::commit();
              return response(array('mensage' => 'Estado Cliente Editado Correctamente.', 'detalleError' => null, 'idOrdenCompra' => $oc->id), 200);
          } catch (Exception $ex) {
              DB::roolback();
              return response(array('mensage' => 'Error al Editar Estado Cliente.', 'detalleError' => $e, 'idOrdenCompra' => null), 400);
          }


    }
}
