<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Obligation;
use App\Facturacion;
use App\Clientes;

class ObligacionesController extends Controller
{
  //-----------------------------------------------------
  //-----------------------OBLIGACIONES------------------
  //-----------------------------------------------------

    public function index(){
       return view('obligaciones.indexObligaciones');
    }

    //leer todas las obligaciones del cliente y pasarlo a JSON
    public function getObligacionesFromCuentaJSON(Request $request){
        $cliente = Clientes::find($request->input('idCliente'));
        if($request->input('soloFacturadas') == 'true'){
          $obl = Obligation::where('fk_cuenta', $cliente->fk_cuenta)
                            ->where('fk_facturacion', null)
                            ->get();
        }else{
          $obl = Obligation::where('fk_cuenta', $cliente->fk_cuenta)->get();
        }



        return response(json_encode($obl),200);
    }

    //Esta funcion permite generar una factura a partir de los items de Obligaciones
    //seleccionados.

    public function generarFacturaFromItems(Request $request){
        //calculos para totales e iva
        $total     = floatVal($request->input('total'));
        $totaliva  = floatVal($request->input('iva21')) + floatVal($request->input('iva1050'));
        $netoSinIva= $total - $totaliva;
        //fin de calculos
        $var = $request->input('items');
        //echo var_dump($var);
        try {
            DB::beginTransaction();
            $cliente = Clientes::findOrFail($request->input('idCliente'));
            //creamos la nueva factura (todavia no fiscal)
            $fact = new Facturacion();
            $fact->fecha                    = Date('d/m/Y');
            $fact->fk_cuenta                = $cliente->fk_cuenta;
            $fact->fk_domicilio             = $request->input('domicilioFact');
            $fact->tipo_concepto            = $request->input('tipoConcepto');
            $fact->periodo_desde            = $request->input('periodoDesde');
            $fact->periodo_hasta            = $request->input('periodoHasta');
            $fact->vencimiento_pago         = $request->input('vencPago');
            $fact->importeNeto              = $netoSinIva;
            $fact->importeIva               = $totaliva;
            $fact->importeTotal             = $total;
            $fact->tipo_factura             = $request->input('tipoFactura');
            $fact->punto_venta_fiscal       = null;
            $fact->numero_factura_fiscal    = null;
            $fact->codigo_barras            = null;
            $fact->cae                      = null;
            $fact->vencimiento_cae          = null;
            $fact->pdf_factura              = null;
            $fact->es_facturaElectronica    = 0;
            $fact->estado                   = 'PRESUPUESTO';
            $fact->created_by               = auth()->user()->name;
            $fact->save();

            foreach ($var as $value) {
              # code...
              Obligation::where('id', $value['idObligacion'])
                        ->update(['fk_facturacion' => $fact->id]);

            }

            DB::commit();
            return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al guardar Orden de Venta.', 'detalleError' => $e, 'idOrdenVenta' => null), 400);
        }
    }

    public function listarFacturasJSON(Request $request){
        $cliente  = Clientes::find($request->input('idCliente'));
        $facturas = Facturacion::where('fk_cuenta', $cliente->fk_cuenta)->get();

        return response(json_encode($facturas),200);
    }

    public function listarItemsFacturaJSON(Request $request){
        $items = Obligation::where('fk_facturacion', $request->input('idFacturacion'))->get();

        return response(json_encode($items),200);
    }
}
