<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\ClientesServicios;
use App\Obligacion;
use App\OrdenVenta;
use App\Obligation;
use App\DetalleOrdenVenta;
use App\CuentaCorriente;
use App\Clientes;

class procesosController extends Controller
{
    //

    //***************************************************************************************

    //Procesos para generacion de deuda por abono, aplicar intereses, cargo por reconexion.

    //***************************************************************************************
    public function indexProceso(){
        return view('procesos.indexProcesos');
    }

    public function procesoAbonosMensuales(Request $request){
        try {
            DB::beginTransaction();
            $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

            //Traemos todos los clientes con servicios activos
            $cliActivos = ClientesServicios::with('cliente.persona','servicio')
                                            ->whereIn('estado', ['activo'])
                                            ->get();
            $mesPer = Carbon::now()->month;
            if($mesPer < 10){
              $mesPer = '0'.$mesPer;
            }

            //recorremos los clientes activos y mostramos los precios de sus abonos.
            foreach ($cliActivos as $value) {
               //echo $value->servicio->precio;

               //Simplemente cargamos nuevas obligaciones para el cliente en la
               //tabla Obligation
               $ob = new Obligation();
               //
               $ob->fk_cuenta                  = $value->cliente->fk_cuenta;
               $ob->fecha                      = Date('d/m/Y');
               $ob->periodo                    = Carbon::now()->year. $mesPer;
               $ob->periodo_mensualidad        = $value->cliente->fk_cuenta.'-'.$value->id.'-'.Carbon::now()->year. $mesPer; //Numero de cuenta - Numero de Conexion - Anno mes
               $ob->cantidad                   = 1;
               $ob->concepto                   = $value->servicio->nombre. ' ('.$value->servicio->zona.') ';
               $ob->descripcion                = "ABONO MES DE ".$meses[Carbon::now()->month - 1];
               $divisor                        = 2 - ((100 - $value->servicio->alicuota_iva) / 100);
               $ob->precio_unitario            = $value->servicio->precio;
               $ob->aplica_intereses           = 1;
               $ob->bonificacion               = 0;
               $ob->alicuta_iva                = $value->servicio->alicuota_iva;
               $ob->importe_iva                = ($value->servicio->precio - ($value->servicio->precio / $divisor));
               $ob->importe_siniva             = ($value->servicio->precio / $divisor);
               $ob->fecha_vencimiento_pago     = Carbon::create(Carbon::now()->year, Carbon::now()->month, 10)->format('d/m/Y');
               $ob->fk_facturacion             = null;
               $ob->created_by                 = auth()->user()->name;
               $ob->save();

               $cc = new CuentaCorriente;
               $cc->fk_cuenta        = $value->cliente->fk_cuenta;
               $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $value->cliente->fk_cuenta)->max('nro_secuencia') + 1;
               $cc->fecha            = Date('d/m/Y');
               $cc->debe             = $value->servicio->precio;
               $cc->haber            = 0.00;
               $cc->descripcion      = $value->servicio->nombre. ' ('.$value->servicio->zona.') ' . "ABONO MES DE ".$meses[Carbon::now()->month - 1];
               $cc->fk_obligation    = $ob->id;
               $cc->created_by       = auth()->user()->name;
               $cc->save();

             }
/*


               //creamos la orden de Venta en cuestion al abono mensual
               $ov = new OrdenVenta;
               $c = Clientes::find($value->cliente->id);
               $ov->created_by       = auth()->user()->name;
               $ov->fecha            = Date('d/m/Y');
               $ov->estado           = 'completa';
               $ov->vencimiento_pago = Carbon::create(Carbon::now()->year, Carbon::now()->month, 10)->format('d/m/Y');
               $ov->fk_persona       = $value->cliente->persona->id;
               $ov->save();

               //cargamos los detalles de la orden de venta.
               $dov = new DetalleOrdenVenta;
               $dov->fk_orden_venta        = $ov->id;
               $dov->fk_articulo           = null;
               $dov->fk_servicio           = $value->fk_servicio;
               $dov->cantidad              = 1;
               $dov->observacion           = "ABONO MES DE ".$meses[Carbon::now()->month - 1];
               $dov->precio_unitario       = $value->servicio->precio;
               $dov->bonificacion          = 0;
               $divisor                    = 2 - ((100 - $value->servicio->alicuota_iva) / 100);
               $dov->importeIva            = ($value->servicio->precio - ($value->servicio->precio / $divisor)) * 1;
               //sumamos el iva a la alicuota que corresponda.
               $dov->alicuota_iva = $value->servicio->alicuota_iva;
               $dov->importe_totalsiniva   = ($value->servicio->precio / $divisor) * 1;
               $dov->created_by       = auth()->user()->name;
               $dov->save();

               $ov = OrdenVenta::find($ov->id);
               $ov->importe_neto     = ($value->servicio->precio / $divisor) * 1;
               if($value->servicio->alicuota_iva == 10.5){
                 $ov->importe_iva105   = ($value->servicio->precio - ($value->servicio->precio / $divisor)) * 1;
                 $ov->importe_iva21    = 0;
               }else{
                 $ov->importe_iva105   = 0;
                 $ov->importe_iva21    = ($value->servicio->precio - ($value->servicio->precio / $divisor)) * 1;
               }
               $ov->save();

               $o = new Obligacion;
               $o->fk_cuenta              = $value->cliente->fk_cuenta;
               $o->concepto               = 'O.V. NRO: '.str_pad($ov->id, 8, "0", STR_PAD_LEFT);
               $o->monto                  = $value->servicio->precio;
               $o->periodo                = (String) Date('Ym');
               $o->fk_orden_venta_detalle = null;
               $o->fk_orden_venta         = $ov->id;
               $o->fecha_vencimiento      = Carbon::create(Carbon::now()->year, Carbon::now()->month, 10)->format('d/m/Y');
               $o->aplica_interes         = true;
               $o->created_by             = auth()->user()->name;
               $o->save();

               $cc = new CuentaCorriente;
               $cc->fk_cuenta        = $value->cliente->fk_cuenta;
               $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $value->cliente->fk_cuenta)->max('nro_secuencia') + 1;
               $cc->fecha            = Date('d/m/Y');
               $cc->debe             = $value->servicio->precio;
               $cc->haber            = 0.00;
               $cc->descripcion      = 'ORDEN DE VENTA NRO: '.str_pad($ov->id, 8, "0", STR_PAD_LEFT);
               $cc->fk_obligacion    = $o->id;
               $cc->created_by       = auth()->user()->name;
               $cc->save();
            }
*/
            DB::commit();
            return response(array('mensage' => 'Todo Correcto.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error.', 'detalleError' => $e, 'idOrdenVenta' => null), 400);
        }

    }

    public function runProcesoAplicarRecargos(Request $request){
        try {
            DB::beginTransaction();
            $porcentajeInteres = 5;
            $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
            //Traemos todos los clientes con servicios activos
            $cliActivos = ClientesServicios::with('cliente.persona','servicio')
                                            ->whereIn('estado', ['activo'])
                                            ->groupBy('fk_cliente')
                                            ->get();
            //dd($cliActivos);
            $mesPer = Carbon::now()->month;
            if($mesPer < 10){
              $mesPer = '0'.$mesPer;
            }
            //recorremos los clientes activos y mostramos los precios de sus abonos.
            foreach ($cliActivos as $value) {
               $cc = CuentaCorriente::where('fk_cuenta',  $value->cliente->fk_cuenta)->get();
               for ($i=0; $i < count($cc); $i++) {
                 $debe  = $cc[$i]->debe;
                 $haber = $cc[$i]->haber;
               }
               $saldo = $haber - $debe;
               if ($saldo < 0){
                   $interes = abs(($saldo * $porcentajeInteres) / 100);
                   $next_nro_secuencia = CuentaCorriente::where('fk_cuenta', $value->cliente->fk_cuenta)->max('nro_secuencia') + 1;

                   $ob = new Obligation();
                   //
                   $ob->fk_cuenta                  = $value->cliente->fk_cuenta;
                   $ob->fecha                      = Date('d/m/Y');
                   $ob->periodo                    = Carbon::now()->year. $mesPer;
                   $ob->periodo_mensualidad        = null; //Numero de cuenta - Numero de Conexion - Anno mes
                   $ob->cantidad                   = 1;
                   $ob->concepto                   = 'RECARGO';
                   $ob->descripcion                = 'RECARGO 5% (SOBRE SALDO DE LA CUENTA: $'.$saldo.') POR MORA';
                   $divisor                        = 2 - ((100 - 21) / 100);
                   $ob->precio_unitario            = $interes;
                   $ob->aplica_intereses           = 0;
                   $ob->bonificacion               = 0;
                   $ob->alicuta_iva                = 21;
                   $ob->importe_iva                = ($interes - ($interes / $divisor)) * 1;
                   $ob->importe_siniva             = ($interes / $divisor) * 1;
                   $ob->fecha_vencimiento_pago     = null;//Carbon::create(Carbon::now()->year, Carbon::now()->month, 10)->format('d/m/Y')
                   $ob->fk_facturacion             = null;
                   $ob->created_by                 = auth()->user()->name;
                   $ob->save();

                   $cc = new CuentaCorriente;
                   $cc->fk_cuenta        = $value->cliente->fk_cuenta;
                   $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $value->cliente->fk_cuenta)->max('nro_secuencia') + 1;
                   $cc->fecha            = Date('d/m/Y');
                   $cc->debe             = $interes;
                   $cc->haber            = 0.00;
                   $cc->descripcion      = 'RECARGO 5% (SOBRE SALDO DE LA CUENTA: $'.$saldo.') POR MORA';
                   $cc->fk_obligacion    = $ob->id;
                   $cc->created_by       = auth()->user()->name;
                   $cc->save();

                   /*
                   //creamos la orden de Venta en cuestion al abono mensual
                   $ov = new OrdenVenta;
                   $c = Clientes::find($value->cliente->id);
                   $ov->created_by       = auth()->user()->name;
                   $ov->fecha            = Date('d/m/Y');
                   $ov->estado           = 'completa';
                   $ov->vencimiento_pago = null;
                   $ov->fk_persona       = $value->cliente->persona->id;
                   $ov->save();

                   //cargamos los detalles de la orden de venta.
                   $dov = new DetalleOrdenVenta;
                   $dov->fk_orden_venta        = $ov->id;
                   $dov->fk_articulo           = null;
                   $dov->fk_servicio           = null;
                   $dov->cantidad              = 1;
                   $dov->observacion           = 'RECARGO 5% (SOBRE SALDO DE LA CUENTA: $'.$saldo.') POR MORA';
                   $dov->precio_unitario       = $interes;
                   $dov->bonificacion          = 0;
                   $divisor                    = 2 - ((100 - 21) / 100);
                   $dov->importeIva            = ($interes - ($interes / $divisor)) * 1;
                   //sumamos el iva a la alicuota que corresponda.
                   $dov->alicuota_iva = 21;
                   $dov->importe_totalsiniva   = ($interes / $divisor) * 1;
                   $dov->created_by       = auth()->user()->name;
                   $dov->save();

                   $ov = OrdenVenta::find($ov->id);
                   $ov->importe_neto     = ($interes / $divisor) * 1;

                   $ov->importe_iva105   = 0;
                   $ov->importe_iva21    = ($interes - ($interes / $divisor)) * 1;
                   $ov->save();

                   $o = new Obligacion;
                   $o->fk_cuenta              = $value->cliente->fk_cuenta;
                   $o->concepto               = 'RECARGO 5% (SOBRE SALDO DE LA CUENTA: $'.$saldo.') POR MORA';
                   $o->monto                  = $interes;
                   $o->periodo                = (String) Date('Ym');
                   $o->fk_orden_venta_detalle = null;
                   $o->fk_orden_venta         = $ov->id;
                   $o->fecha_vencimiento      = null;
                   $o->aplica_interes         = true;
                   $o->created_by             = auth()->user()->name;
                   $o->save();

                   $cc = new CuentaCorriente;
                   $cc->fk_cuenta        = $value->cliente->fk_cuenta;
                   $cc->nro_secuencia    = CuentaCorriente::where('fk_cuenta', $value->cliente->fk_cuenta)->max('nro_secuencia') + 1;
                   $cc->fecha            = Date('d/m/Y');
                   $cc->debe             = $interes;
                   $cc->haber            = 0.00;
                   $cc->descripcion      = 'RECARGO 5% (SOBRE SALDO DE LA CUENTA: $'.$saldo.') POR MORA';
                   $cc->fk_obligacion    = $o->id;
                   $cc->created_by       = auth()->user()->name;
                   $cc->save();
                   */
                }
            }
            DB::commit();
            return response(array('mensage' => 'Todo Correcto.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error.', 'detalleError' => $e, 'idOrdenVenta' => null), 400);
        }

    }
}
