<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Localidad;
use App\Articulos;
use App\Departamento;
use App\Torres;
use DB;
use App\DependenciaTorresEquipos;
use App\Equipos;

class ConexionesController extends Controller
{
    //
      public function listar(){
          $t = Torres::with('equipos')->where('activo',true)->get();

          $departamentos = Departamento::all();
          $array_departamentos = array();
          foreach($departamentos as $key => $value){
             $array_departamentos[$value->id] =  $value->nombre;
          }
          return view('conexiones.listar', [
            "ubicacion" => "conexiones.listar",
            "torres"    => $t,
            "array_departamentos" => $array_departamentos
          ]);
      }

      public function agregarTorre(){

          $departamentos = Departamento::all();
          $array_departamentos = array();
          foreach($departamentos as $key => $value){
             $array_departamentos[$value->id] =  $value->nombre;
          }
          return view('conexiones.crearTorre', [
            "ubicacion" => "conexiones.crearTorre",
            "array_departamentos" => $array_departamentos
          ]);
      }

      public function agregarEquipo(){

          $departamentos = Departamento::all();
          $array_departamentos = array();
          foreach($departamentos as $key => $value){
             $array_departamentos[$value->id] =  $value->nombre;
          }
          $a = Articulos::where('activo',true)->get();
          $t = Torres::where('activo', true)->get();

          return view('conexiones.crearEquipo', [
            "ubicacion" => "conexiones.crearEquipo",
            "articulos"  => $a,
            "torres"    => $t,
          ]);
      }

      public function guardarTorre(Request $request){
          try {
              DB::beginTransaction();
              $t = new Torres;//id	nombre	capacidad	altura	direccion	latitud	longitud	fk_departamento	fk_localidad	HQ	created_by	updated_by
              $t->nombre          = strtoupper($request->input('inp-nombre-torre'));
              $t->altura          = $request->input('inp-altura-torre');
              $t->direccion       = $request->input('inp-direccion-torre');
              $t->latitud         = $request->input('inp-latitud-torre');
              $t->longitud        = $request->input('inp-longitud-torre');
              $t->fk_departamento = $request->input('select-depto');
              $t->fk_localidad    = $request->input('select-localidad');
              if($request->input('check-hq') == 'on'){
                $t->HQ              = true;
              }else{
                $t->HQ              = false;
              }
              $t->created_by      = Auth::user()->name;
              $t->save();
              return redirect('/conexiones/agregarTorre')->with('mesage', $mesage = array('status' => 200, 'text' => "Torre Creada Correctamente."));
          } catch (Exception $ex) {
              return redirect('/conexiones/agregarTorre')->with('mesage', $mesage = array('status' => 400, 'text' => "Error Al Crear Torre. Por favor verifique los datos."));
              DB::roolback();
          }finally {
              DB::commit();
          }
      }

      public function guardarEquipo(Request $request){
          $this->validate($request, [
            'inp-nombre-equipo'   => 'required',
            'sel-tipo-equipo'     => 'required',
            'sel-articulo'        => 'required',
            'inp-serie-equipo'    => 'required',
            'inp-ip-equipo'       => 'required',
            'sel-torre-pertenece' => 'required',
            'sel-torre-depende'   => 'required_if:check-dependencia,on',
            'sel-equipo-depende'  => 'required_if:check-dependencia,on',
          ]);
          try {
              DB::beginTransaction();
              //id	nombre	tipo	fk_articulo	serie	ip	fk_torre	fk_dependencia_torres_equipos	created_by	updated_by
              $e = new Equipos;
              $e->nombre                        = strtoupper($request->input('inp-nombre-equipo'));
              $e->tipo                          = $request->input('sel-tipo-equipo');
              $e->fk_articulo                   = $request->input('sel-articulo');
              $e->serie                         = $request->input('inp-serie-equipo');
              $e->ip                            = $request->input('inp-ip-equipo');
              $e->fk_torre                      = $request->input('sel-torre-pertenece');
              if($request->input('check-dependencia') == 'on'){
                  $dep = new DependenciaTorresEquipos;
                  $dep->fk_torre  = $request->input('sel-torre-depende');
                  $dep->fk_equipo = $request->input('sel-equipo-depende');
                  $dep->save();
                  $e->fk_dependencia_torres_equipos = $dep->id;
              }else{
                  $e->fk_dependencia_torres_equipos = null;
              }
              $e->created_by = Auth::user()->name;
              $e->save();

              return redirect('/conexiones/agregarEquipo')->with('mesage', $mesage = array('status' => 200, 'text' => "Equipo Creada Correctamente."));
          } catch (Exception $ex) {
              return redirect('/conexiones/agregarEquipo')->with('mesage', $mesage = array('status' => 400, 'text' => "Error Al Crear Equipo. Por favor verifique los datos."));
              DB::roolback();
          }finally {
              DB::commit();
          }
      }

      public function jsonGetEquiposFromTorre(Request $request){
          $e = Equipos::with('articulo')->where('fk_torre', $request->input('idTorre'))->get();
          return response( json_encode($e));
      }

      public function jsonGetDatosFromTorre(Request $request){
          $t = Torres::with('equipos.articulo', 'equipos.dependeDe.equipo', 'equipos.dependeDe.torre')->where('id', $request->input('idTorre'))->first();
          return response(json_encode($t));
      }
}
