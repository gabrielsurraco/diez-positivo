<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Almacenes;
use App\Articulos;
use App\Inventario;

class StockController extends Controller
{
    //-----------------------------------------------------
    //-----------------------Almacenes---------------------
    //-----------------------------------------------------
    public function almacenesList(){
        $a = Almacenes::where('activo',1)->get();
        return view('stock.almacenes.index', [
          "ubicacion" => "stock.almacenes.list",
          "almacenes"   => $a
        ]);
    }

    public function almacenesNuevo(){
        return view('stock.almacenes.nuevoAlmacen', [
          "ubicacion" => "stock.almacenes.nuevo",
          //"cliente"   => $contacto->cliente
        ]);
    }

    public function almacenesEdit($id){
        $a  = Almacenes::find($id);

        return view('stock.almacenes.nuevoAlmacen', [
          "ubicacion" => "stock.almacenes.edit",
          "almacenes"   => $a
        ]);
    }

    public function almacenesGuardar(Request $request){
      $this->validate($request, [
            'inp-nombre'    => 'required|max:100'
      ],[
            'inp-nombre.required'    => 'Este campo es requerido.',
            'inp-nombre.max'         => 'El maximo de caracteres es 100.'
      ]);
      try {
          DB::beginTransaction();
          if (!is_null($request->input('inp-id-almacen'))){
              $a = Almacenes::find($request->input('inp-id-almacen'));
          }else{
              $a = new Almacenes;
          }

          $a->nombre          = $request->input('inp-nombre');
          $a->fk_persona      = 1;//$request->input('sel-persona');
          $a->seccion         = $request->input('inp-seccion');
          $a->estante_nro     = $request->input('inp-estanteNro');
          $a->caja            = $request->input('inp-caja');
          $a->columna         = $request->input('inp-columna');
          $a->fila            = $request->input('inp-fila');
          $a->save();
          DB::commit();

          return response()->view('stock.almacenes.nuevoAlmacen', array('message' => 'Guardado Correctamente.', 'status' => 1), 200);
      } catch (Exception $ex) {
          DB::roolback();
          return response()->view('stock.almacenes.nuevo', array('message' => 'Ocurrio un error Intentelo nuevamente.', 'status' => 0), 400);
      }

    }

    public function deleteAlmacen(Request $request){

        try {
            DB::beginTransaction();
            Almacenes::where('id', $request->input('id'))
                       ->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al Eliminar OrdenCompra.', 'detalleError' => $e), 400);
        }
    }
    //Fin Almacenes

    //-----------------------------------------------------
    //-----------------------Articulos---------------------
    //-----------------------------------------------------
    public function articulosList(){
        $a = Articulos::where('activo',1)->get();
        return view('stock.articulos.index', [
          "ubicacion" => "stock.articulos.list",
          "articulos"   => $a
        ]);
    }

    public function jsonSearch(Request $request){
        $a = Articulos::where('activo',1)
                        ->where('marca', 'like', '%' . $request->input('palabraClave') . '%')
                        ->orwhere('modelo', 'like', '%' . $request->input('palabraClave') . '%')->get();
        $createArray = array();
        foreach ($a as $value) {
          array_push($createArray, array('id' => $value->id , 'text' => $value->marca.'-'. $value->modelo));
        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }

    public function articulosNuevo(){
        $proximoCodigo = Articulos::where('activo',1)->max('codigo_interno');
        $proximoCodigo = DB::table('articulos')
                    ->select(DB::raw('MAX(CONVERT(codigo_interno,UNSIGNED INTEGER)) as maxCodigo'))
                    ->where('activo',1)
                    ->first();
        
        return view('stock.articulos.nuevoArticulo', [
          "ubicacion" => "stock.articulos.nuevo",
          "proximoCodigo" => $proximoCodigo->maxCodigo
        ]);
    }

    public function articulosEdit($id){
        $a  = Articulos::find($id);
        $i  = Inventario::where('fk_articulo', '=', $id)->get();
        return view('stock.articulos.nuevoArticulo', [
          "ubicacion" => "stock.articulos.nuevo",
          "articulos"   => $a,
          "inventario"  => $i
        ]);
    }

    public function articulosGuardar(Request $request){
      $this->validate($request, ['inp-codigo' => 'unique:articulos,codigo_interno'], ['inp-codigo.unique' => 'Este código ya fue asignado en un articulo.']);
      try {
          DB::beginTransaction();
          if (!is_null($request->input('inp-id-articulo'))){
              $a = Articulos::find($request->input('inp-id-articulo'));
              $i = Inventario::where('fk_articulo', '=', $request->input('inp-id-articulo'))->first();
          }else{
              $a = new Articulos;
              $i = new Inventario;
          }

          $a->fk_cuenta           = 1;
          $a->codigo_interno      = strtoupper ($request->input('inp-codigo'));
          $a->marca               = strtoupper ($request->input('inp-marca'));
          $a->modelo              = strtoupper ($request->input('inp-modelo'));
          $a->frecuencia          = strtoupper ($request->input('inp-frecuencia'));
          $a->porcentaje_ganancia = $request->input('inp-porc-ganancia');
          $a->alicuota_iva        = $request->input('sel-alicuota-iva');
          $a->precio              = $request->input('inp-precio');
          $a->unidad              = $request->input('sel-unidad');
          $a->moneda              = $request->input('sel-moneda');
          $a->save();

          //Carga de inventario a partir del articulos
          $i->fk_articulo         = $a->id;
          $i->stock_min           = $request->input('inp-stockMin');
          $i->stock_max           = $request->input('inp-stockMax');
          $i->save();
          DB::commit();

          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Guardado Correctamente.', 'status' => 1), 200);
      } catch (Exception $ex) {
          DB::rollback();
          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Ocurrio un error Intentelo nuevamente.', 'status' => 0), 400);
      }

    }

    public function deleteArticulo(Request $request){

        try {
            DB::beginTransaction();
            Articulos::where('id', $request->input('id'))
                       ->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al Eliminar OrdenCompra.', 'detalleError' => $e), 400);
        }
    }

    //Fin Articulos


    //-----------------------------------------------------
    //---------------Recepcion de Mercaderias--------------
    //-----------------------------------------------------
    public function recep_mercaderiasList(){
        $a = Articulos::where('activo',1)->get();
        return view('stock.recep_mercaderias.index', [
          "ubicacion" => "stock.articulos.list",
          "articulos"   => $a
        ]);
    }

    public function recep_mercaderias(){
        return view('stock.recep_mercaderias.nuevoIngreso', [
          "ubicacion" => "stock.recep_mercaderias.nuevo",
          //"cliente"   => $contacto->cliente
        ]);
    }

    public function articulosEdit2($id){
        $a  = Articulos::find($id);

        return view('stock.articulos.nuevoArticulo', [
          "ubicacion" => "stock.articulos.nuevo",
          "articulos"   => $a
        ]);
    }

    public function recep_mercaderiasIngreso(Request $request){
      dd($request);
      try {
          DB::beginTransaction();
          if (!is_null($request->input('inp-id-articulo'))){
              $a = Articulos::find($request->input('inp-id-articulo'));
          }else{
              $a = new Articulos;
          }

          $a->fk_cuenta           = 1;
          $a->codigo_interno      = strtoupper ($request->input('inp-codigo'));
          $a->marca               = strtoupper ($request->input('inp-marca'));
          $a->modelo              = strtoupper ($request->input('inp-modelo'));
          $a->frecuencia          = strtoupper ($request->input('inp-frecuencia'));
          $a->porcentaje_ganancia = $request->input('inp-porc-ganancia');
          $a->alicuota_iva        = $request->input('sel-alicuota-iva');
          $a->precio              = $request->input('inp-precio');
          $a->unidad              = $request->input('sel-unidad');
          $a->moneda              = $request->input('sel-moneda');
          $a->save();
          DB::commit();

          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Guardado Correctamente.', 'status' => 1), 200);
      } catch (Exception $ex) {
          DB::roolback();
          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Ocurrio un error Intentelo nuevamente.', 'status' => 0), 400);
      }

    }

    public function deleteArticulo2(Request $request){

        try {
            DB::beginTransaction();
            Articulos::where('id', $request->input('id'))
                       ->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al Eliminar OrdenCompra.', 'detalleError' => $e), 400);
        }
    }
    //Fin Recepcion Mercaderias

    //-----------------------------------------------------
    //-----------------------Inventario--------------------
    //-----------------------------------------------------
    public function inventarioList(){
        $i = Inventario::all();
        return view('stock.inventario.index', [
          "ubicacion" => "stock.inventario.list",
          "inventarioFull"   => $i
        ]);
    }

    public function recep_mercaderias2(){
        return view('stock.recep_mercaderias.nuevoIngreso', [
          "ubicacion" => "stock.recep_mercaderias.nuevo",
          //"cliente"   => $contacto->cliente
        ]);
    }

    public function articulosEdit3($id){
        $a  = Articulos::find($id);

        return view('stock.articulos.nuevoArticulo', [
          "ubicacion" => "stock.articulos.nuevo",
          "articulos"   => $a
        ]);
    }

    public function articulosGuardar3(Request $request){
      try {
          DB::beginTransaction();
          if (!is_null($request->input('inp-id-articulo'))){
              $a = Articulos::find($request->input('inp-id-articulo'));
          }else{
              $a = new Articulos;
          }

          $a->fk_cuenta           = 1;
          $a->codigo_interno      = strtoupper ($request->input('inp-codigo'));
          $a->marca               = strtoupper ($request->input('inp-marca'));
          $a->modelo              = strtoupper ($request->input('inp-modelo'));
          $a->frecuencia          = strtoupper ($request->input('inp-frecuencia'));
          $a->porcentaje_ganancia = $request->input('inp-porc-ganancia');
          $a->alicuota_iva        = $request->input('sel-alicuota-iva');
          $a->precio              = $request->input('inp-precio');
          $a->unidad              = $request->input('sel-unidad');
          $a->moneda              = $request->input('sel-moneda');
          $a->save();
          DB::commit();

          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Guardado Correctamente.', 'status' => 1), 200);
      } catch (Exception $ex) {
          DB::roolback();
          return response()->view('stock.articulos.nuevoArticulo', array('message' => 'Ocurrio un error Intentelo nuevamente.', 'status' => 0), 400);
      }

    }

    public function deleteArticulo3(Request $request){

        try {
            DB::beginTransaction();
            Articulos::where('id', $request->input('id'))
                       ->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Eliminado Correctamente.', 'detalleError' => null), 200);
        } catch (Exception $ex) {
            DB::roolback();
            return response(array('mensage' => 'Error al Eliminar OrdenCompra.', 'detalleError' => $e), 400);
        }
    }
    //Fin Inventario
}
