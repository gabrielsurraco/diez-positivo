<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanDeCuenta;
use Validator;
use App\Helper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
//use Whossun\Toastr\Facades\Toastr;
//use Illuminate\Support\Facades\Validator;
use App\Asientos;
use Carbon\Carbon;
use App\Cuenta;


class ContabilidadController extends Controller{

   //<editor-fold defaultstate="collapsed" desc="PLAN DE CUENTAS">


    public function buscarCuentas(Request $request){

        try{
            $cuentas = Cuenta::where('nombre','LIKE', '%'.$request->data . '%')->orWhere('codigo','LIKE', '%' . $request->data . '%')->get(['id','nombre','codigo']);
            return Response::json(array(
                            'datos'   => $cuentas
                        ), 200);
        } catch (\PDOException $ex) {
            return Response::json(array(
                            'mensaje'   => $ex->getMessage()
                        ), 400);
        }

    }


    public function getCuentasDetalles(Request $request){
        $cuentas = Cuenta::where('fk_plan_de_cuenta',$request->id)->get();

        $max_cod_cuenta = Cuenta::select(DB::raw('max(codigo) as codigo'))->where("fk_plan_de_cuenta",$request->id)->get();

        $resultado              = explode(".", $max_cod_cuenta[0]["codigo"]);
        $cant                   = count($resultado);
        $resultado[$cant - 1]   = $resultado[$cant - 1] + 1;

        for($i=0;$i < $cant; $i++){
            if($i==0){
                $cod = $resultado[$i];
            }else{
                $cod .= "." . $resultado[$i];
            }
        }

        $i = 0;
        foreach($cuentas as $cuenta => $value){
            $cuenta_fix[$i] = array("id" => $value->id, "nombre" => $value->nombre, "descripcion" => $value->descripcion, "codigo" => $value->codigo, "fk_plan_de_cuenta" => $value->fk_plan_de_cuenta);
            $i++;
        }

        $cuenta_fix[$i++] = array("max_cod" => $cod);

        if(count($cuenta_fix) > 0)
            return json_encode($cuenta_fix);
        else
            return json_encode(0);
    }

    public function editCuenta($id){
        $cuenta = Cuenta::findOrFail($id);
        $categorias = PlanDeCuenta::all();

        $aux_categorias = array();
        foreach ($categorias as $categoria => $key){
            $aux_categorias[$key->id] = $key->codigo . ' ' . $key->descripcion;
        }

        return view('contabilidad.edit_plan_de_cuenta',  compact('cuenta','aux_categorias'));
    }

    public function updateCuenta(Request $request){

        $codigo = Cuenta::find($request->id);

        /*
         * Si el codigo ingresado es el mismo que el de la BD no se valida el formulario
         */

        $this->validate($request, [
            'nombre' => 'required|min:3|max:255',
        ]);

        Cuenta::where('id', $request->id)
        ->update(['nombre' => $request->nombre, 'imputable' => $request->imputable]);

        $resultado = "La cuenta se modifico correctamente!";
        return redirect()->route('plan-de-cuentas')->with('msg', $resultado);

    }

    public function deleteCuenta(Request $request){

        try{
            Cuenta::destroy($request["id"]);
        } catch (\PDOException $ex) {
            return Response::json(array(
                'success'   => false,
                'errors'    => $ex->getMessage()
            ), 400);
        }

        return Response::json(array('success' => true), 200);

    }

    public function editCategoria($id){
        $cuenta = Cuenta::findOrFail($id);
        return view("contabilidad.editar_categoria",  compact("cuenta"));
    }

    public function updateCategoria(Request $request){
        try{
            Cuenta::where("id",$request->id)->update([
                "nombre" => $request->nombre
            ]);
        } catch (\PDOException $ex) {
            return redirect()->back()->with('msg', 'El nombre de la cuenta no pudo ser modificado!');
        }

        return redirect()->back()->with('msg', 'El nombre de la cuenta ha sido modificado correctamente!');
    }

    public function saveCuenta(Request $request){

        $rules = array('nombre' => 'required','cuenta' => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Response::json(array(
                'success'   => false,
                'errors'    => $validator->getMessageBag()->toArray()
            ), 400);
        }

        try{

                $cuenta                 = new Cuenta;
                $cuenta->nombre         = $request["nombre"];
                $cuenta->codigo         = Helper::buscarSiguienteCodigoCuenta($request["cuenta"]);
                $cuenta->imputable      = $request["imputable"];
                $cuenta->id_superior    = $request["cuenta"];
                $cuenta->save();

            return Response::json(array('success' => $cuenta), 200);

        } catch (Exception $ex) {

            return Response::json(array(
                'success'   => false,
                'errors'    => JSON.parse_str($ex)
            ), 400);

        }


    }

    //Movi la funcion buscarSiguienteCodigoCuenta a Helper guille@uniting

    public function getIndexPlanDeCuentas(){
            $cuentas = Cuenta::all();
            return view('contabilidad.index_plan_de_cuenta',  compact('cuentas'));
    }
    //</editor-fold>

    public function indexAsientos(){
        $cuentas = Cuenta::select("id","codigo","nombre")->get();

        return view('contabilidad.asientos.asientos_index', compact('asientos','cuentas'));
    }

   public function buscarAsientos(Request $request){

       try{

            $asientos = new Asientos;

            if(isset($request->fecha_desde)){
                $fecha_desde = Carbon::createFromFormat('d/m/Y', $request->fecha_desde)->format('Y-m-d');
                $asientos = $asientos->where('fecha','>=',$fecha_desde);
            }

            if(isset($request->fecha_hasta)){
                $fecha_hasta = Carbon::createFromFormat('d/m/Y', $request->fecha_hasta)->format('Y-m-d');
                $asientos = $asientos->where('fecha','<=',$fecha_hasta);
            }

            if(isset($request->nro_asiento) && $request->nro_asiento !== " ")
                $asientos = $asientos->where('nro_asiento',$request->nro_asiento);
            if(isset($request->cuenta) && $request->cuenta !== " ")
                $asientos = $asientos->where('fk_cuenta',$request->cuenta);
            if(isset($request->descripcion))
                $asientos = $asientos->where('descripcion','LIKE','%'.$request->descripcion .'%');

            $asientos = $asientos->get();

            $asientos_fix = array();
            $total_debe = (float)0.00;
            $total_haber = (float)0.00;

            foreach($asientos as $key => $value){
                $total_debe = $total_debe + (float)$value->debe;
                $total_haber = $total_haber + (float)$value->haber;
            }

            foreach($asientos as $key => $value){
                $asientos_fix[] = array(
                    $value->fecha,
                    $value->nro_asiento,
                    $value->cuenta->nombre,
                    number_format((float)$value->debe, 2, '.', '') ,
                    number_format((float)$value->haber, 2, '.', ''),
                    $value->descripcion,
                    number_format((float)$total_debe, 2, '.', ''),
                    number_format((float)$total_haber, 2, '.', '')
                );
            }

       } catch (\PDOException $ex) {
            return Response::json(array(
                'mensaje'   => $ex->getMessage()
            ), 400);
       }

       return Response::json(array('mensaje' => $asientos_fix), 200);

   }

   public function guardarAsiento(Request $request){
       $datos = json_decode($request->datos);
        try{
            DB::beginTransaction();
            $next_nro_asiento = Asientos::max('nro_asiento') + 1;

            $control_debe = 0;
            $control_haber = 0;
            foreach($datos as $key => $value){
                $control_debe   = $control_debe + $value->debe;
                $control_haber  = $control_haber + $value->haber;
            }

            $diferencia = $control_debe - $control_haber;

            if($diferencia != 0){
                DB::rollBack();
                return  Response::json(array(
                            'mensaje'   => "Favor verifique los asientos creados, la suma del DEBE y la suma del HABER deben ser iguales!"
                        ), 500);
            }


            foreach($datos as $key => $value){

                Asientos::create([
                    'fecha'         => $value->fecha,
                    'nro_asiento'   => $next_nro_asiento,
                    'fk_cuenta'     => $value->cuenta_id,
                    'debe'          => $value->debe,
                    'haber'         => $value->haber,
                    'descripcion'   => $value->descripcion
                ]);

            }

            DB::commit();
        } catch (\PDOException $ex) {
            DB::rollBack();
            return Response::json(array(
                            'mensaje'   => $ex->getMessage()
                        ), 400);
        }

       return Response::json(array('mensaje' => "Los asientos fueron creados correctamente!"), 200);
   }

   public function nuevoAsientos(){
       $cuentas = Cuenta::select("id","codigo","nombre")->get();
       return view('contabilidad.asientos.asientos_create', compact('cuentas'));
   }

}
