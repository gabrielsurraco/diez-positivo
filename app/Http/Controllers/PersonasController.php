<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use DateTime;
use App\Helper;
use App\Personas;
use PDOException;
use App\Proveedores;
use App\Domicilio;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Proveedore;
use App\Cuenta;
use App\Pais;
use App\Departamento;
use App\Localidad;
use App\Clientes;
use App\Telefono;

class PersonasController extends Controller{

    public function index(){
        $personas = Personas::with(['domicilio.localidad', 'domicilio.departamento'])->get();
        //dd($personas);
        return view('personas.listado', compact('personas'));
    }

    public function create(){
        $calles = Domicilio::select("calle")->distinct()->get();

        $paises = Pais::all();
        $array_paises = array();
        foreach($paises as $key => $value){
           $array_paises[$value->id] =  $value->nombre;
        }

        $departamentos = Departamento::all();
        $array_departamentos = array();
        foreach($departamentos as $key => $value){
           $array_departamentos[$value->id] =  $value->nombre;
        }

        $localidades = Localidad::all();
        $array_localidades = array();

        foreach($localidades as $key => $value){
           $array_localidades[$value->id] =  $value->nombre;
        }


        return view('personas.create',  compact('array_cuentas','array_paises','array_departamentos','array_localidades','calles'));

    }

    //guardar datos de la persona.
    public function storePersona(Request $request){

         $this->validate($request, [
            'tipo_persona'          => 'required',
            'nombre'                => 'required_if:tipo_persona,==,f|max:100',
            'apellido'              => 'required_if:tipo_persona,==,f|max:100',
            'razon_social'          => 'required_if:tipo_persona,==,j|max:100',
            'num_doc'               => 'required|unique:personas,cuit',
            'fecha_nacimiento'      => 'required_if:tipo_persona,==,f',
            'email'                 => 'max:150',
            'email_2'               => 'max:150',
            'celular'               => 'max:10',
            'telefono'              => 'max:10',
            'telefono_2'            => 'max:10',
            'profesion'             => 'max:150',
            'departamento'          => 'required|not_in:0',
            'localidad'             => 'required|not_in:0',
            'calle'                 => 'required|max:100',
            'altura'                => 'required',
            'nro_depto'             => 'max:10',
            'cp'                    => 'max:10',
            'link_mapa'             => 'max:255',
            'observaciones_dom'     => 'max:255',
            'observaciones_per'     => 'max:255',
        ],[
            'nombre.required'           => 'Por favor indique nombre del cliente.',
            'nombre.max'                => 'Por favor indique un nombre con menos de 100 caracteres.',
            'apellido.required'         => 'Por favor indique apellido del cliente.',
            'apellido.max'              => 'Por favor indique un apellido con menos de 100 caracteres.',

            'razon_social.max'          => 'Por favor indique un razon social con menos de 100 caracteres.',
            'num_doc.required'          => 'Por favor indique num de cuit del cliente.',
            'num_doc.unique'            => 'Este cuit ya existe en la base de datos.',
            'fecha_nacimiento.require'  => 'Indique fecha de nacimiento.',
            'email.max'                 => 'Por favor indique un email con menos de 150 caracteres.',
            'email_2.max'               => 'Por favor indique un email 2 con menos de 150 caracteres.',
            'celular.max'               => 'Por favor indique un número de celular con menos de 10 caracteres.',
            'telefono.max'              => 'Por favor indique un número de telefono con menos de 10 caracteres.',
            'telefono_2.max'            => 'Por favor indique un número de telefono 2 con menos de 10 caracteres.',
            'departamento.required'     => 'Por favor indique departamento.',
            'departamento.not_in'       => 'Por favor indique departamento.',
            'localidad.required'        => 'Por favor indique localidad.',
            'localidad.not_in'          => 'Por favor indique localidad.',
            'calle.required'            => 'Por favor indique calle del cliente.',
            'calle.max'                 => 'Por favor indique una calle con menos de 100 caracteres.',
            'altura.required'           => 'Por favor indique la altura de la calle del cliente.',
            'nro_depto.max'             => 'Por favor indique num de depto con menos de 10 caracteres.',
            'cp.max'                    => 'Por favor indique codigo postal con menos de 10 caracteres.',
            'link_mapa.max'             => 'Por favor indique el dato con menos de 255 caracteres.',
            'observaciones_dom.max'     => 'Por favor indique el dato con menos de 255 caracteres.',
            'observaciones_per.max'     => 'Por favor indique el dato con menos de 255 caracteres.',
        ]);

            try {
                DB::beginTransaction();
                $persona = new Personas();
                if($request->tipo_persona == "f"){
                    $persona->es_persona_fisica = TRUE;
                    $nombre_cuenta = $request["apellido"] . ' ' . $request["nombre"];
                }else{
                    $persona->es_persona_fisica = FALSE;
                    $nombre_cuenta = $request["razon_social"];
                }

                if(isset($request["razon_social"]) || $request["razon_social"] == ""){
                    $persona->razon_social      = strtoupper($request["razon_social"]);
                }else{
                    $persona->razon_social      = NULL;
                }

                if(isset($request["nombre"]) || $request["nombre"] == ""){
                    $persona->nombre      = strtoupper($request["nombre"]);
                }else{
                    $persona->nombre      = NULL;
                }

                if(isset($request["apellido"]) || $request["apellido"] == ""){
                    $persona->apellido      = strtoupper($request["apellido"]);
                }else{
                    $persona->apellido      = NULL;
                }




                $persona->tipo_doc          = $request["tipo_doc"];
                $persona->cuit              = $request["num_doc"];

                if($request->tipo_persona == 1)
                    $persona->fecha_nacimiento  = DateTime::createFromFormat('d/m/Y', $request["fecha_nacimiento"])->format('Y-m-d');

                $persona->email             = $request["email"];
                $persona->email_2           = $request["email_2"];
                $persona->profesion         = $request["profesion"];
                $persona->condicion_fiscal  = $request["cat_fiscal"];
                $persona->observacion       = $request["observaciones_per"];
                $persona->created_by        = Auth::user()->name;
                $persona->save();

                $cont_telefonos = count($request["tipo"]);
                if($cont_telefonos > 1){
                    for($i = 1; $i < $cont_telefonos;$i++){
                        $telefono                   = new Telefono();
                        $telefono->tipo             = $request["tipo"][$i];
                        $telefono->empresa          = $request["compania"][$i];
                        $telefono->cod_pais         = $request["cod_pais"][$i];
                        $telefono->area             = $request["cod_area"][$i];
                        $telefono->numero           = $request["numero"][$i];
                        $telefono->horario_contacto = ($request["horario_contacto"][$i] == "")? "-" : $request["horario_contacto"][$i];
                        $telefono->observaciones    = ($request["observaciones"][$i] == "") ? "-" : $request["observaciones"][$i];
                        $telefono->fk_persona       = $persona->id;
                        $telefono->save();

                    }
                }

                $domicilio = new Domicilio();
                $domicilio->fk_persona          = $persona->id;
                $domicilio->fk_provincia        = $request["provincia"];
                $domicilio->fk_departamento     = $request["departamento"];
                $domicilio->fk_localidad        = $request["localidad"];
                $domicilio->tipo_domicilio      = "fiscal";
                $domicilio->fecha_desde         = date("d/m/Y"); //DateTime::createFromFormat('d/m/Y', date('d/m/Y'))->format('Y-m-d');
                $domicilio->calle               = strtoupper($request["calle"]);
                $domicilio->altura              = $request["altura"];
                $domicilio->piso                = $request["piso"];
                $domicilio->depto               = $request["nro_depto"];
                $domicilio->cp                  = $request["cp"];
                $domicilio->link_mapa           = $request["link_mapa"];
                $domicilio->latitud             = $request["latitud"];
                $domicilio->longitud            = $request["longitud"];
                $domicilio->observacion         = $request["observaciones_dom"];
                $domicilio->created_by          = Auth::user()->name;
                $domicilio->save();

                flash('El cliente se creó correctamente!', 'success');
                DB::commit();
            } catch (Exception $ex) {
                flash('Error al crear el cliente!', 'error');
                DB::roolback();
            }

            return redirect('/personas/create');
    }

    //Editar Persona
    public function editPersona($id){

        try {
            $persona = Personas::findOrFail($id);
        } catch (Exception $ex) {
            flash('Error al Editar el Cliente!', 'warning');
            return back();
        }
        $paises = Pais::all();
        $array_paises = array();
        foreach($paises as $key => $value){
           $array_paises[$value->id] =  $value->nombre;
        }

        $departamentos = Departamento::all();
        $array_departamentos = array();
        foreach($departamentos as $key => $value){
           $array_departamentos[$value->id] =  $value->nombre;
        }

        $localidades = Localidad::all();
        $array_localidades = array();
        foreach($localidades as $key => $value){
           $array_localidades[$value->id] =  $value->nombre;
        }
        return view('personas.edit',  compact('persona','array_paises','array_departamentos','array_localidades'));

    }


    //Upadate informacion persona.
    public function updatePersona(Request $request){

          $this->validate($request, [
             'tipo_persona'          => 'required',
             'nombre'                => 'required_if:tipo_persona,==,f|max:100',
             'apellido'              => 'required_if:tipo_persona,==,f|max:100',
             'razon_social'          => 'required_if:tipo_persona,==,j|max:100',
             'fecha_nacimiento'      => 'required_if:tipo_persona,==,f',
             'email'                 => 'max:150',
             'email_2'               => 'max:150',
             'celular'               => 'max:10',
             'telefono'              => 'max:10',
             'telefono_2'            => 'max:10',
             'profesion'             => 'max:150',
             'departamento'          => 'required|not_in:0',
             'localidad'             => 'required|not_in:0',
             'calle'                 => 'required|max:100',
             'altura'                => 'required',
             'nro_depto'             => 'max:10',
             'cp'                    => 'max:10',
             'link_mapa'             => 'max:255',
             'observaciones_dom'     => 'max:255',
             'observaciones_per'     => 'max:255',
         ],[
             'nombre.required'           => 'Por favor indique nombre del cliente.',
             'nombre.max'                => 'Por favor indique un nombre con menos de 100 caracteres.',
             'apellido.required'         => 'Por favor indique apellido del cliente.',
             'apellido.max'              => 'Por favor indique un apellido con menos de 100 caracteres.',
             'razon_social.max'          => 'Por favor indique un razon social con menos de 100 caracteres.',
             'num_doc.required'          => 'Por favor indique num de cuit del cliente.',
             'num_doc.unique'            => 'Este cuit ya existe en la base de datos.',
             'fecha_nacimiento.require'  => 'Indique fecha de nacimiento.',
             'email.max'                 => 'Por favor indique un email con menos de 150 caracteres.',
             'email_2.max'               => 'Por favor indique un email 2 con menos de 150 caracteres.',
             'celular.max'               => 'Por favor indique un número de celular con menos de 10 caracteres.',
             'telefono.max'              => 'Por favor indique un número de telefono con menos de 10 caracteres.',
             'telefono_2.max'            => 'Por favor indique un número de telefono 2 con menos de 10 caracteres.',
             'departamento.required'     => 'Por favor indique departamento.',
             'departamento.not_in'       => 'Por favor indique departamento.',
             'localidad.required'        => 'Por favor indique localidad.',
             'localidad.not_in'          => 'Por favor indique localidad.',
             'calle.required'            => 'Por favor indique calle del cliente.',
             'calle.max'                 => 'Por favor indique una calle con menos de 100 caracteres.',
             'altura.required'           => 'Por favor indique la altura de la calle del cliente.',
             'nro_depto.max'             => 'Por favor indique num de depto con menos de 10 caracteres.',
             'cp.max'                    => 'Por favor indique codigo postal con menos de 10 caracteres.',
             'link_mapa.max'             => 'Por favor indique el dato con menos de 255 caracteres.',
             'observaciones_dom.max'     => 'Por favor indique el dato con menos de 255 caracteres.',
             'observaciones_per.max'     => 'Por favor indique el dato con menos de 255 caracteres.',
         ]);
        //cantidad de telefonos
        $cont = 0;
        $cant_telefonos = count($request->id_telefono);

        try{
            if($request->tipo_persona == 'f'){
              Personas::where('id', $request->id_persona)
              ->update([  'es_persona_fisica' => $request->tipo_persona,
                          'razon_social'      => $request->razon_social,
                          'tipo_doc'          => $request->tipo_doc,
                          'fecha_nacimiento'  => DateTime::createFromFormat('d/m/Y', $request["fecha_nacimiento"])->format('Y-m-d'),
                          'email'             => $request->email,
                          'email_2'           => $request->email_2,
                          'profesion'         => $request->profesion,
                          'observacion'       => $request->observaciones_per,
                          'condicion_fiscal'  => $request->cat_fiscal,
                          'updated_by'        => Auth::user()->name]);
            }else{
              Personas::where('id', $request->id_persona)
              ->update([  'es_persona_fisica' => $request->tipo_persona,
                          'razon_social'      => $request->razon_social,
                          'tipo_doc'          => $request->tipo_doc,
                          'email'             => $request->email,
                          'email_2'           => $request->email_2,
                          'profesion'         => $request->profesion,
                          'observacion'       => $request->observaciones_per,
                          'condicion_fiscal'  => $request->cat_fiscal,
                          'updated_by'        => Auth::user()->name]);
            }

            $persona = Personas::findOrFail($request->id_persona);
            //lleno un array con los id's de los telefonos de la persona en la BD.
            $telefonos = $persona->telefonos;
            foreach($telefonos as $telefono){
                $id_telefonos[] = $telefono['id'];
            }

            //si viene con un ID de telefono, actualizamos. De lo contrario CREAMOS

            if($cant_telefonos > 0){
                while($cont < $cant_telefonos){

                    if($request->id_telefono[$cont] == NULL){
                        //DOY DE ALTA EL TELEFONO
                        $new_telefono                   = new Telefono();
                        $new_telefono->tipo             = $request->tipo[$cont];
                        $new_telefono->empresa          = $request->compania[$cont];
                        $new_telefono->cod_pais         = $request->cod_pais[$cont];
                        $new_telefono->area             = $request->cod_area[$cont];
                        $new_telefono->numero           = $request->numero[$cont];
                        $new_telefono->horario_contacto = ($request->horario_contacto[$cont] == "") ? "-" : $request->horario_contacto[$cont];
                        $new_telefono->observaciones    = ($request->observaciones[$cont] == "") ? "-" : $request->observaciones[$cont];
                        $new_telefono->fk_persona       = $request->id_persona;
                        $new_telefono->save();
                    }
                    else{
                        //ACTUALIZO LOS DATOS DEL TELEFONO
                            Telefono::where('id', $request->id_telefono[$cont])
                            ->update([
                                "tipo"              => $request->tipo[$cont],
                                "empresa"           => $request->compania[$cont],
                                "cod_pais"          => $request->cod_pais[$cont],
                                "area"              => $request->cod_area[$cont],
                                "numero"            => $request->numero[$cont],
                                "horario_contacto"  => $request->horario_contacto[$cont],
                                "observaciones"     => $request->observaciones[$cont]
                            ]);
                    }

                    $id_telefonos_recibidos[] = $request->id_telefono[$cont];
                    $cont++;
                }

            }else{
                $id_telefonos_recibidos = array();
            }

            //Si los id de los telefonos en la BD no exiten en los input de entrada quiere decir que hay que borrarlos.
            $cont2 = 0;
            if(isset($id_telefonos)){
                while($cont2 < count($id_telefonos)){
                    if( !in_array($id_telefonos[$cont2], $id_telefonos_recibidos) ){
                        Telefono::where('id', $id_telefonos[$cont2])->delete();
                    }
                    $cont2++;
                }
            }

            Domicilio::where('fk_persona', $request->id_persona)->where('tipo_domicilio','fiscal')
            ->update([  'fk_provincia'          => $request->provincia,
                        'fk_departamento'       => $request->departamento,
                        'fk_localidad'          => $request->localidad,
                        'calle'                 => $request->calle,
                        'altura'                => $request->altura,
                        'piso'                  => $request->piso,
                        'depto'                 => $request->nro_depto,
                        'cp'                    => $request->cp,
                        'link_mapa'             => $request->link_mapa,
                        'latitud'               => $request->latitud,
                        'longitud'              => $request->longitud,
                        'observacion'           => $request->observaciones_dom,
                        'updated_by'            => Auth::user()->name]);

        } catch (\PDOException $ex) {
            echo $ex->getLine();
            echo $ex->getMessage();
            die();
        }

        flash("El Cliente se actualizó correctamente!", "success");
        return Redirect::to('/personas');

    }

    //Este Json permite utilizarlo en el Select2 para realizar una busqueda de
    //personas con 3 caracteres que no sean clientes.
    public function jsonSearchPersonaNotClient(Request $request){
        //esta funcion trae todos las personas que no estan asociadas a clientes.
        $c = Clientes::pluck('fk_persona')->all();
        $personas = Personas::whereNotIn('id', $c)
                            ->where(function ($query) use ($request) {
                                $query->where('nombre', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('apellido', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('cuit', 'like', '%' . $request->input('palabraClave') . '%');
                            })->get();
        $createArray = array();
        foreach ($personas as $value) {
          array_push($createArray, array('id' => $value->id , 'text' =>  $value->apellido.' '.$value->nombre.' ('.$value->cuit.')'));
        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }

    //Este Json permite utilizarlo en el Select2 para realizar una busqueda de
    //personas con 3 caracteres que no sean proveedores.
    public function jsonSearchPersonaNotProvee(Request $request){
        //esta funcion trae todos las personas que no estan asociadas a proveedores.
        $c = Proveedores::pluck('fk_persona')->all();
        $personas = Personas::whereNotIn('id', $c)
                            ->where(function ($query) use ($request) {
                                $query->where('nombre', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('apellido', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('razon_social', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('cuit', 'like', '%' . $request->input('palabraClave') . '%');
                            })->get();
        $createArray = array();
        foreach ($personas as $value) {
          if($value->apellido != ''){
              array_push($createArray, array('id' => $value->id , 'text' =>  $value->apellido.' '.$value->nombre.' ('.$value->cuit.')'));
          }else{
              array_push($createArray, array('id' => $value->id , 'text' =>  $value->razon_social.' ('.$value->cuit.')'));
          }

        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }

    //Este Json permite utilizarlo en el Select2 para realizar una busqueda de
    //personas con 3 caracteres.
    public function jsonSearchPersona(Request $request){
        //esta funcion trae todos las personas que no estan asociadas a clientes
        $personas = Personas::where('nombre', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('apellido', 'like', '%' . $request->input('palabraClave') . '%')
                                ->orWhere('cuit', 'like', '%' . $request->input('palabraClave') . '%')->get();
        $createArray = array();
        foreach ($personas as $value) {
          array_push($createArray, array('id' => $value->id , 'text' =>  $value->apellido.' '.$value->nombre.' ('.$value->cuit.')'));
        }
        $send = array('results' => $createArray);
        return response(json_encode($send));
    }


    //Permite recuperar todos los domicilios de una persona en particular
    public function jsonGetDomiciliosFromPersona(Request $request){
        $cliente = Clientes::findOrFail($request->input('idCliente'));
        //esta funcion trae todos las personas que no estan asociadas a clientes
        $personas = Personas::with('domicilios.localidad', 'domicilios.departamento')->where('id',$cliente->fk_persona)->get();

        return response(json_encode($personas));
    }


}
