<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListaPrecio;
use Illuminate\Support\Facades\Response;
use PDOException;
use Illuminate\Support\Facades\Auth;
use App\ListaPrecioDetalle;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\PlanDeCuenta;
use App\PlanDeCuentaDetalle;
use Illuminate\Support\Facades\DB;


class ListaPreciosController extends Controller
{


    public function index(){
        $asd = 1;
        $listado = ListaPrecioDetalle::with("concepto")->get();
        $categorias_cuentas = PlanDeCuenta::all();


        $all_categorias = array();
        foreach($categorias_cuentas as $id => $categoria){
            $all_categorias[$categoria->id] = $categoria->codigo .' - '. $categoria->descripcion;
        }

        $conceptos = ListaPrecio::all();
        $all_conceptos = array();
        foreach($conceptos as $concepto => $value){
            $all_conceptos[$value->id] = $value->concepto;
        }

        return view('ventas.lista_precios.index',  compact('listado','all_conceptos','all_categorias'));
    }

    public function editSubConceptos($id = null){

        $conceptos = ListaPrecio::all();
        $all_conceptos = array();
        foreach($conceptos as $concepto => $value){
            $all_conceptos[$value->id] = $value->concepto;
        }

        $sub_concepto = ListaPrecioDetalle::where('id',$id)->first();

//        echo $sub_concepto->id; die();

        return view('ventas.lista_precios.editar', compact('all_conceptos','sub_concepto'));
    }

    public function guardarSubConcepto(Request $request){
//        echo $request["concepto"]; die();

        $this->validate($request, [
            'sub_concepto'          => 'required|max:100',
            'costo'                  => 'required|numeric',
        ]);

        try{
            ListaPrecioDetalle::where('id', $request->id_item)
            ->update([  'fk_lista_precio'   => $request->concepto,
                        'sub_concepto'      => $request->sub_concepto,
                        'categoria'         => $request->categoria,
                        'unidad'            => $request->unidad,
                        'moneda'            => $request->moneda,
                        'costo'             => round($request->costo,2),
                        'updated_by'        => Auth::user()->name]);


        } catch (\PDOException $ex) {
            return redirect()->back()->with('msg', 'No se pudo modificar! '.$ex->getMessage());
        }

        return redirect()->back()->with('msg', 'Se modificó correctamente!');
    }

    public function newConcepto(Request $request){

        try{

            $lista_precio = ListaPrecio::create([
                'concepto'              => $request->concepto,
                'fk_plan_de_cuentas'    => $request->categoria,
                'created_by'            => Auth::user()->name
            ]);

        } catch (\PDOException $ex) {
            return Response::json(array(
                'success'   => false,
                'errors'    => $ex->getMessage()
            ), 400);
        }

        return Response::json(array(
            'success'   => true,
            'data'      => ['id_concepto' => $lista_precio->id,'concepto' => $lista_precio->concepto]
        ), 200);
    }

    public function getNextCodeOfCuenta($id_categoria){
        $max_cod_cuenta = PlanDeCuentaDetalle::select(DB::raw('max(codigo) as codigo'))->where("fk_plan_de_cuenta",$id_categoria)->get();

        if(count($max_cod_cuenta[0]["codigo"]) == 0){
            $cod_raiz_cuenta    = PlanDeCuenta::select(DB::raw('codigo'))->where("id",$id_categoria)->get();
            $cod                = $cod_raiz_cuenta[0]["codigo"] . ".1";
        }else{
            $resultado              = explode(".", $max_cod_cuenta[0]["codigo"]);
            $cant                   = count($resultado);
            $resultado[$cant - 1]   = $resultado[$cant - 1] + 1;

            for($i=0;$i < $cant; $i++){
                if($i==0){
                    $cod = $resultado[$i];
                }else{
                    $cod .= "." . $resultado[$i];
                }
            }
        }

        return $cod;
    }

    public function newSubConceptos(Request $request){

        foreach($request->data as $data){

            if(empty($data["sub_concepto"]) || $data["sub_concepto"] === ""){
                return Response::json(array(
                    'mensaje'   => "Verificar campo/s sub-conceptos",
                ), 400);
            }

            if(empty($data["precio"]) || $data["precio"] === ""){
                return Response::json(array(
                    'mensaje'   => "Verificar campo/s precio",
                ), 400);
            }

        }

        try{
            $datos = array();
            foreach($request->data as $data){

                $item = ListaPrecioDetalle::create([
                    'fk_lista_precio'   => $data["concepto"],
                    'sub_concepto'      => $data["sub_concepto"],
                    'categoria'         => $data["categoria"],
                    'unidad'            => $data["unidad"],
                    'costo'             => round($data["precio"],2),
                    'moneda'            => $data["moneda"],
                    'created_by'        => Auth::user()->name
                ]);
                $item['fk_lista_precio'] = $this->getNombreConcepto($item['fk_lista_precio']);
                $item['categoria']       = $this->getCategoriaWithLabel($item['categoria']);

                //buscar id plan de cuenta de la lista de precio
                $id_plan_de_cuenta = ListaPrecio::findOrFail($data["concepto"]);
                //crear en plan de cuenta detalle
                PlanDeCuentaDetalle::create([
                    'nombre'                    => 'CUENTA - ' . $data["sub_concepto"],
                    'codigo'                    => $this->getNextCodeOfCuenta($id_plan_de_cuenta["fk_plan_de_cuentas"]),
                    'imputable'                 => 0,
                    'fk_lista_precio_detalle'   => $item["id"],
                    'fk_plan_de_cuenta'         => $id_plan_de_cuenta["fk_plan_de_cuentas"],
                    'id_superior'               => NULL,
                    'created_by'                => Auth::user()->name
                ]);

                array_push($datos, $item);
            }
        } catch (\PDOException $ex) {
            return Response::json(array(
                    'mensaje'   => "Error al crear items ".$ex->getMessage(),
            ), 400);
        }
            return Response::json(array(
                    'mensaje'   => "Items Creados Correctamente!",
                    'datos'     => $datos
            ), 200);
    }

    public function getNombreConcepto($id){
        return ListaPrecio::where('id',$id)->first()->concepto;
    }

    public function getCategoriaWithLabel($categoria){
      if($categoria == "Servicio"){
          return '<span class="label label-primary">Servicio</span>';
      }else{
          return '<span class="label label-success">Producto</span>';
      }
    }

    public function delSubConceptos(Request $request){

        try{
            ListaPrecioDetalle::destroy($request->id_item);
        } catch (\PDOException $ex) {
            return Response::json(array(
                    'mensaje'   => "Error al eliminar el item ".$ex->getMessage(),
            ), 400);
        }

        return Response::json(array(
                    'mensaje'   => "Item Eliminado Correctamente!",
        ), 200);

    }

    public function deleteConcepto(Request $request){
        try{

            ListaPrecio::destroy($request->id);

        } catch (\PDOException $ex) {
            return Response::json(array(
                'success'   => false,
                'errors'    => $ex->getMessage()
            ), 400);
        }

        return Response::json(array(
            'success'   => true
        ), 200);

    }

    public function getConcepto(){

    }

    public function updateConcepto(Request $request){

        try{

            $lista_precio = ListaPrecio::where('id',$request->id_concepto)->update(["concepto" => $request->concepto, "updated_by" => Auth::user()->name]);

        } catch (\PDOException $ex) {
            return Response::json(array(
                'success'   => false,
                'errors'    => $ex->getMessage()
            ), 400);
        }


        return Response::json(array(
            'success'   => true,
            'data'      => ['id_concepto' => $request->id_concepto ,'concepto' => $request->concepto]
        ), 200);

    }

}
