<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ParentescosRelaciones;
use Illuminate\Support\Facades\Auth;
use DB;

class ParentescosRelacionesController extends Controller
{
    //
    //Guardar relacion de persona con un cliente en particular.
    public function guardarParentescoRelacion(Request $request){
      try {
        DB::beginTransaction();
        //fk_cliente	fk_proveedor	fk_inversor	fk_tecnico	fk_persona	rol	created_by	updated_by
        $pc = new ParentescosRelaciones();
        $pc->fk_cliente   = $request->input('idCliente');
        $pc->fk_proveedor = null;
        $pc->fk_inversor  = null;
        $pc->fk_tecnico   = null;
        $pc->fk_persona   = $request->input('idPersona');
        $pc->rol          = implode(";", $request->input('rol'));;
        $pc->created_by   = Auth::user()->name;
        $pc->save();
        DB::commit();
        return response(array('mensage' => 'Guardado Correctamente.', 'detalleError' => null, 'idParentescoRelacion' => $pc->id), 200);
      } catch (Exception $ex) {
        DB::rollback();
        return response(array('mensage' => 'Error al guardar parentesco.', 'detalleError' => $e, 'idParentescoRelacion' => null), 400);
      }
    }

    //borrar parentesco entre persona y cliente
    public function borrarParentescoRelacion(Request $request){
          try {
            DB::beginTransaction();
            ParentescosRelaciones::where('id', $request->input('id'))->update(['activo' => 0]);
            DB::commit();
            return response(array('mensage' => 'Actualizado Correctamente.'), 200);
          } catch (Exception $ex) {
            DB::rollback();
            return response(array('mensage' => 'Error al guardar parentesco.'), 400);
          }

    }
}
