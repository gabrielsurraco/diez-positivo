<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleRecibos extends Model
{
    //
    protected $guarded = [];
    protected $table = 'detalle_recibos';
}
