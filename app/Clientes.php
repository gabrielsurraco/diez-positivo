<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Clientes extends Model
{
    protected $table = 'clientes';

//    protected $dateFormat = 'd-m-Y';

//    protected $fillable = ["id","fk_persona","observaciones","codigo","fk_cuenta_contable","created_by","updated_by"];




    public function persona(){
        return $this->hasOne('App\Personas', 'id', 'fk_persona');
    }

    public function getservicios(){
        return $this->hasMany('App\ServiciosClientes','fk_cliente');
    }



}
