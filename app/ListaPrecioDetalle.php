<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaPrecioDetalle extends Model
{
    protected $table = "lista_precios_detalles";
    
    protected $fillable = ['fk_lista_precio','sub_concepto','categoria','costo','unidad','codigo','moneda','created_by','updated_by'];

    public function concepto(){
        return $this->belongsTo('App\ListaPrecio','fk_lista_precio');
    }


}
