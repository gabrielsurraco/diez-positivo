<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ticket extends Model
{
    protected $table = "tickets";

    protected $fillable = ['fecha','es_cliente','fk_cliente','fk_servicio','nombre','telefono','email','estado','categoria','consulta','respuesta','created_by','updated_by'];

    public function getFechaAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaAttribute( $value ) {
        $this->attributes['fecha'] = (new Carbon($value))->format('Y-m-d');
    }

}
