<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDeCuentaDetalle extends Model
{
    protected $table = "plan_de_cuenta_detalles";
    
//    protected $guarded = ['nombre','codigo','descripcion','fk_plan_de_cuenta', 'id_superior','fk_persona'];
    
    protected $fillable = ['nombre','codigo','descripcion','imputable','fk_plan_de_cuenta',"fk_lista_precio_detalle", 'id_superior','fk_persona'];
}
