<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagoDetalle extends Model
{
    protected $table = "pagos_efectuados_detalle";
}
