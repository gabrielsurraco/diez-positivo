<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Facturacion extends Model
{
    //
    protected $table = 'facturacion';
    protected $guarded  =  [];

//id	fk_persona	fk_domicilio	tipo_concepto	periodo_desde	periodo_hasta	vencimiento_pago	importeNeto	importeIva	importeTotal	tipo_factura	punto_venta_fiscal	numero_factura_fiscal	codigo_barras	cae	vencimiento_cae	pdf_factura	es_facturaElectronica	estado	created_by	updated_by	created_at	updated_at
    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        if($value != null){
          $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getPeriordoDesdeAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setPeriodoDesdeAttribute($value)
    {
        if($value != null){
          $this->attributes['periodo_desde'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getPeriordoHastaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setPeriodoHastaAttribute($value)
    {
        if($value != null){
          $this->attributes['periodo_hasta'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getVencimientoPagoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setVencimientoPagoAttribute($value)
    {
        if($value != null){
          $this->attributes['vencimiento_pago'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getVencimientoCaeAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setVencimientoCaeAttribute($value)
    {
        if($value != null){
          $this->attributes['vencimiento_cae'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    //relaciones
    public function persona(){
      return $this->hasOne('App\Personas', 'id', 'fk_persona');
    }
    public function domicilio(){
      return $this->hasOne('App\Domicilio', 'id', 'fk_domicilio');
    }

    public function obligations(){
      return $this->hasMany('App\Obligation', 'fk_facturacion');
    }

}
