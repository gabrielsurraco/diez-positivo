<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacenes extends Model
{
    //
    protected $table = 'almacenes';



    //relaciones
    public function persona(){
      return $this->hasOne('App\Personas', 'fk_persona', 'id');
    }
}
