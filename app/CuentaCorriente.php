<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CuentaCorriente extends Model
{
    protected $table = 'cuentas_corrientes';
    protected $guarded = [];


    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

}
