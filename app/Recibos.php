<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Cuenta;

class Recibos extends Model
{
    //
    protected $guarded = [];
    protected $table = 'recibos';

    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }


    //relaciones
    public function detalleRecibos(){
      return $this->hasMany('App\DetalleRecibos', 'fk_recibo', 'id');
    }

    public function cuenta(){
      return $this->hasOne('App\Cuenta', 'id', 'fk_cuenta');
    }
}
