<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PagosEfectuados extends Model
{
    //
    protected $guarded = [];
    protected $table = 'pagos_efectuados';

    public function getFechaPagoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaPagoAttribute($value)
    {
        $this->attributes['fecha_pago'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    //relaciones
    public function recibo(){
      return $this->hasOne('App\Recibos', 'id', 'fk_recibo');
    }

    public function orden_venta(){
      return $this->hasMany('App\OrdenVenta', 'id', 'fk_orden_venta');
    }

    public function cuenta(){
      return $this->hasOne('App\Cuenta', 'id', 'fk_cuenta');
    }
}
