<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteFamiliares extends Model
{
    protected $table = "cliente_familiares";
}
