<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentescosRelaciones extends Model
{
    //

    //relaciones
    public function cliente(){
        return $this->hasOne('App\Clientes', 'id', 'fk_cliente');
    }

    public function proveedor(){
        return $this->hasOne('App\Proveedores', 'id', 'fk_proveedor');
    }

    public function inversor(){
        return $this->hasOne('App\Inversores', 'id', 'fk_inversor');
    }

    public function tecnico(){
        return $this->hasOne('App\Tecnicos', 'id', 'fk_tecnico');
    }

    public function persona(){
        return $this->hasOne('App\Personas', 'id', 'fk_persona');
    }

}
