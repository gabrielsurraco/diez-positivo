<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomicilioServicio extends Model
{
    protected $table = "domicilio_servicios";
    
    protected $fillable = ["fk_servicio","fk_provincia","fk_departamento","fk_localidad","calle","altura","cp","piso","nro_depto","observaciones","latitud","longitud","link_mapa","created_by","updated_by"];

    
    
}
