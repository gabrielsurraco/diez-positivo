<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DetalleOrdenVenta extends Model
{
    //
    protected $guarded = [];
    protected $table = 'detalle_orden_venta';

    public function getVencimientoPagoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setVencimientoPagoAttribute($value)
    {
        $this->attributes['vencimiento_pago'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    //relaciones
    public function articulo(){
      return $this->hasOne('App\Articulos', 'id', 'fk_articulo');
    }

    public function servicio(){
      return $this->hasOne('App\Servicios', 'id', 'fk_servicio');
    }
    
    public function services(){
      return $this->belongsTo('App\Servicios', 'fk_servicio');
    }

    public function cliente_servicio(){
      return $this->hasOne('App\ClientesServicios', 'id', 'fk_cliente_servicio');
    }

}
