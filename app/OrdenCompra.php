<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class OrdenCompra extends Model
{
    protected $guarded = [];
    protected $table = 'orden_compra';

    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    public function getFechaEntregaProductoEstimadaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaEntregaProductoEstimadaAttribute($value)
    {
        $this->attributes['fecha_entrega_producto_estimada'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }


    //relaciones
    public function detalleOrdenCompra(){
      return $this->hasMany('App\DetalleOrdenCompra', 'fk_orden_de_compra', 'id');
    }

    public function proveedor(){
      return $this->hasOne('App\Proveedores', 'id', 'fk_proveedor');
    }
}
