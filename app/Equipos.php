<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
    //

    //relaciones
    public function articulo(){
      return $this->hasOne('App\Articulos', 'id', 'fk_articulo');
    }

    public function dependeDe(){
      return $this->hasOne('App\DependenciaTorresEquipos', 'id', 'fk_dependencia_torres_equipos');
    }
}
