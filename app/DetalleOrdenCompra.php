<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenCompra extends Model
{
    //
    protected $guarded = [];
    protected $table = 'detalle_orden_compra';


    //relaciones
    public function articulo(){
      return $this->hasOne('App\Articulos', 'id', 'fk_articulo');
    }
}
