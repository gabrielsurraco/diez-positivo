<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class ClientesServicios extends Model
{
    //
    protected $table = 'clientes_servicios';

    public function servicio(){
        return $this->hasOne('App\Servicios', 'id', 'fk_servicio');
    }

    public function cliente(){
        return $this->hasOne('App\Clientes', 'id', 'fk_cliente');
    }

    public function domicilio(){
        return $this->belongsTo('App\Domicilio', 'id', 'fk_cliente_servicio');
    }

    public function getFechaAltaAttribute($value)
    {
        if ($value !== null) {
          return Carbon::parse($value)->format('d/m/Y');
        }else{
          return null;
        }

    }

    public function setFechaAltaAttribute($value)
    {
        if ($value !== null) {
          $this->attributes['fecha_alta'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getFechaBajaAttribute($value)
    {
      if ($value !== null) {
        return Carbon::parse($value)->format('d/m/Y');
      }else{
        return null;
      }
    }

    public function setFechaBajaAttribute($value)
    {
        if ($value !== null) {
          $this->attributes['fecha_baja'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }
}
