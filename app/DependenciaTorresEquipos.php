<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DependenciaTorresEquipos extends Model
{
    //
    protected $table = 'dependencia_torres_equipos';


    //relaciones
    public function torre(){
      return $this->hasOne('App\Torres', 'id', 'fk_torre');
    }

    //relaciones
    public function equipo(){
      return $this->hasOne('App\Equipos', 'id', 'fk_equipo');
    }
}
