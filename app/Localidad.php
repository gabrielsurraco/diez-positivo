<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = "localidades";

    public function departamento(){
        return $this->belongsTo(Departamento::class, 'fk_departamento');
    }
}
