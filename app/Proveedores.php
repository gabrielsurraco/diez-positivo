<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $table = "proveedores";

    protected $guarded = [];

    //relaciones
    public function persona(){
        return $this->hasOne('App\Personas', 'id', 'fk_persona');
    }
}
