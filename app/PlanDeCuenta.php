<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDeCuenta extends Model
{
    protected $table = "plan_de_cuentas";
    
    protected $fillable = ["id_superior","codigo","descripcion"];
}
