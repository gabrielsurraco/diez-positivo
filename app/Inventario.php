<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    //
    protected $table = 'inventarios';




    //relaciones
    public function articulo(){
      return $this->hasOne('App\Articulos', 'id', 'fk_articulo');
    }
}
