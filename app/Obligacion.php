<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Obligacion extends Model
{
    protected $table    = "obligaciones";

    protected $guarded  =  [];


    public function getFechaVencimientoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaVencimientoAttribute($value)
    {
        if($value != null){
          $this->attributes['fecha_vencimiento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }        
    }

    //relaciones
    public function ordenVenta(){
      return $this->hasOne('App\OrdenVenta', 'id', 'fk_orden_venta');
    }
}
