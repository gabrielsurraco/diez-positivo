<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table = 'domicilios';

//    public function persona(){
//        return $this->belongsTo('App\Persona');
//    }

    public function departamento(){
        return $this->belongsTo('App\Departamento','fk_departamento');
    }

    public function localidad(){
        return $this->belongsTo('App\Localidad','fk_localidad');
    }

    public function provincia(){
        return $this->belongsTo('App\Provincia','fk_provincia');
    }


    public function getFechaDesdeAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaDesdeAttribute($value)
    {
        if ($value !== null) {
          $this->attributes['fecha_desde'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getFechaHastaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaHastaAttribute($value)
    {
        if ($value !== null) {
          $this->attributes['fecha_hasta'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

}
