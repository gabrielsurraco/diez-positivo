<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class OrdenVenta extends Model
{
    //
    protected $guarded = [];
    protected $table = 'orden_venta';

    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    public function getVencimientoPagoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setVencimientoPagoAttribute($value)
    {
        if($value != null){
          $this->attributes['vencimiento_pago'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }        
    }

    //relaciones
    public function persona(){
      return $this->hasOne('App\Personas', 'id', 'fk_persona');
    }

    public function getRecibo(){
      return $this->belongsTo('App\Recibos', 'fk_orden_venta');
    }

    public function detalleOrdenVenta(){
      return $this->hasMany('App\DetalleOrdenVenta', 'fk_orden_venta');
    }

    public function pagosEfectuados(){
      return $this->hasMany('App\pagosEfectuados', 'fk_orden_venta');
    }

    public function obligacion(){
      return $this->belongsTo('App\Obligacion', 'id', 'fk_orden_venta');
    }
}
