<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaPrecio extends Model
{
    protected $table = "lista_precios";
    
    protected $fillable = ["concepto","fk_plan_de_cuentas","created_by","updated_by"];
}
