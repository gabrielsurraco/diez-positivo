<?php

namespace App;

use App\Localidad;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Nicolas
 */
class Helper {
    //put your code here

    public static function getNameLocalidad($id){
        return Localidad::findOrFail($id)->nombre;
    }

    public static function checkContent($value){
        if(!isset($value) || empty($value)){
            return "-";
        }else{
            return $value;
        }
    }

    public static function getEstadosDelServicio(){
        $estados_servicios = array( 'a_cobrar'      => 'A Cobrar',
                                    'cobrado'       => 'Cobrado',
                                    'agendado'      => 'Agendado',
                                    'confirmado'    => 'Confirmado',
                                    'activo'        => 'Activo',
                                    'suspendido'    => 'Suspendido',
                                    'mudanza'       => 'Mudanza',
                                    'promo'         => 'Promo',
                                    'baja'          => 'Baja');
        return $estados_servicios;
    }

    public static function buscarSiguienteCodigoCuenta(/*$id_categoria, $cuenta_detalle_id ,*/ $id_cuenta_superior){

        $cont = Cuenta::where('id_superior',$id_cuenta_superior)->count();

        if($cont == 0){
            $codigo = Cuenta::findOrFail($id_cuenta_superior)->codigo;
            return $codigo . ".1";
        }else{
            $codigo = Cuenta::where('id_superior',$id_cuenta_superior)->max('codigo');

            $resultado              = explode(".",  ((string)$codigo));
            $cant                   = count($resultado);
            $resultado[$cant - 1]   = ((string)($resultado[$cant - 1] + 1));


            for($i=0;$i < $cant; $i++){
                if($i==0){
                    $cod = $resultado[$i];
                }else{
                    $cod .= "." . $resultado[$i];
                }
            }

            return $cod;
        }

    }

    //agregado por Guille@uniting.com.ar
    public static function getUnidadfromID($id){
      switch ($id) {
        case '1':
          return 'KILOGRAMO';
          break;

        case '2':
          return 'METRO';
          break;
        case '3':
          return 'METRO CUADRADO';
          break;

        case '4':
          return 'METRO CUBICO';
          break;
        case '5':
          return 'LITROS';
          break;
        case '6':
          return '1000 KILOWATT HORA';
          break;

        case '7':
          return 'UNIDAD';
          break;
        case '8':
          return 'PAR';
          break;

        case '9':
          return 'DOCENA';
          break;
        case '10':
          return 'QUILATE';
          break;
        case '11':
          return 'MILLAR';
          break;

        case '12':
          return 'MEGA-U. INT. ACT. ANTIB';
          break;
        case '13':
          return 'UNIDAD INT. ACT. INMUNG';
          break;

        case '14':
          return 'GRAMO';
          break;
        case '15':
          return 'MILIMETRO';
          break;
        case '16':
          return 'MILIMETRO CUBICO';
          break;

        case '17':
          return 'KILOMETROS';
          break;
        case '18':
          return 'HECTOLITRO';
          break;

        case '19':
          return 'MEGA U. INT. ACT. INMUNG.';
          break;
        case '20':
          return 'CENTIMETRO';
          break;
        case '21':
          return 'KILOGRAMO ACTIVO';
          break;

        case '22':
          return 'GRAMO ACTIVO';
          break;
        case '23':
          return 'GRAMO BASE';
          break;

        case '24':
          return 'UIACTHOR';
          break;
        case '25':
          return 'JUEGO O PAQUETE MAZO DE NAIPES';
          break;
        case '26':
          return 'MUIACTHOR';
          break;

        case '27':
          return 'CENTIMETRO CUBICO';
          break;
        case '28':
          return 'UIACTANT';
          break;

        case '29':
          return 'TONELADA';
          break;
        case '30':
          return 'DECAMETRO CUBICO';
          break;
        case '31':
          return 'HECTOMETRO CUBICO';
          break;
        case '32':
          return 'KILOMETRO CUBICO';
          break;

        case '33':
          return 'MICROGRAMO';
          break;
        case '34':
          return 'NANOGRAMO';
          break;

        case '35':
          return 'PICOGRAMO';
          break;
        case '36':
          return 'MUIACTANT';
          break;
        case '37':
          return 'UIACTIG';
          break;
        case '41':
          return 'MILIGRAMO';
          break;
        case '47':
          return 'MILILITRO';
          break;

        case '48':
          return 'CURIE';
          break;
        case '49':
          return 'MILICURIE';
          break;

        case '50':
          return 'MICROCURIE';
          break;
        case '51':
          return 'U. INTER. ACT. HOR.';
          break;
        case '52':
          return 'MEGA U. INTER. ACT. HOR.';
          break;
        case '53':
          return 'KILOGRAMO BASE';
          break;
        case '54':
          return 'GRUESA';
          break;
        case '55':
          return 'MUIACTIG';
          break;
        case '61':
          return 'KG. BRUTO';
          break;
        case '62':
          return 'PACK';
          break;
        case '63':
          return 'HORMA';
          break;
        case '98':
          return 'OTRAS UNIDADES';
          break;
        case '99':
          return 'BONIFICACION';
          break;
        default:
        return '-';
          break;
      }
    }

    //agregado por Guille@uniting.com.ar
    public static function getTipoDoc($value){
      switch ($value) {
        case '80':
          return 'CUIT';
          break;

        case '87':
          return 'CDI';
          break;
        case '89':
          return 'LE';
          break;

        case '90':
          return 'LC';
          break;
        case '91':
          return 'CI Extranjera';
          break;
        case '96':
          return 'DNI';
          break;

        case '94':
          return 'Pasaporte';
          break;
        case '00':
          return 'CI Policía Federal';
          break;

        case '30':
          return 'Certificado de Migración';
          break;
      }
    }

    //agregado por guille@uniting.com.ar
    public static function getCondicionFiscal($value){
      switch ($value) {
        case '5':
          return 'CONSUMIDOR FINAL';
          break;
        case '4':
          return 'IVA EXENTO';
          break;
        case '9':
          return 'CLIENTE DEL EXTERIOR';
          break;
        case '6':
          return 'MONOTRIBUTISTA';
          break;
        case '1':
          return 'RESPONSABLE INSCRIPTO';
          break;
        case '13':
          return 'MONOTRIBUTISTA SOCIAL';
          break;
        default:
          return '-';
          break;
      }
    }

    //transformar array de rol en label legibles.
    public static function getLabelFromRol($value){
      $array = explode(';', $value);
      $labels = "";
      foreach ($array as $value) {
          switch ($value) {
            case '1':
              $labels = $labels.' <span class="label">Marido</span>';
              break;
            case '2':
              $labels = $labels.' <span class="label">Mujer</span>';
              break;
            case '3':
              $labels = $labels.' <span class="label">Hijo/a</span>';
              break;
            case '4':
              $labels = $labels.' <span class="label">Pariente</span>';
              break;
            case '5':
              $labels = $labels.' <span class="label">Tesorero</span>';
              break;
            case '6':
              $labels = $labels.' <span class="label">Dueño</span>';
              break;
            case '7':
              $labels = $labels.' <span class="label">Secretario</span>';
              break;
            case '8':
              $labels = $labels.' <span class="label">Tecnico</span>';
              break;
          }

      }
      return $labels;      
    }

}
