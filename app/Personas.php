<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Personas extends Model
{
    protected $table = "personas";

//    protected $dates = [
//        'fecha_nacimiento'
//    ];

    public function domicilio(){
        return $this->belongsTo('App\Domicilio', 'id', 'fk_persona');
    }

    public function domicilios(){
        return $this->hasMany('App\Domicilio', 'fk_persona');
    }

    public function telefonos(){
        return $this->hasMany('App\Telefono','fk_persona');
    }

    public function persona(){
        return $this->hasOne('App\Personas','fk_persona');
    }


//    protected function getDateFormat(){
//        return 'Y/m/d';
//    }

    public function getFechaNacimientoAttribute( $value ) {
//        $this->attributes['fecha_nacimiento'] = (new Carbon($value))->format('d/m/y');
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaNacimientoAttribute( $value ) {
        $this->attributes['fecha_nacimiento'] = (new Carbon($value))->format('Y-m-d');
    }

//    public function domicilio(){
//        return $this->belongsTo('App\Domicilio','id');
//    }

}
