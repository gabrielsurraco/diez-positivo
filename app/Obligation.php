<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Obligation extends Model
{
    protected $table    = "obligation";

    protected $guarded  =  [];

    public function getFechaAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        if($value != null){
          $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    public function getFechaVencimientoPagoAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaVencimientoPagoAttribute($value)
    {
        if($value != null){
          $this->attributes['fecha_vencimiento_pago'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }

    //relaciones
    public function ordenVenta(){
      return $this->hasOne('App\OrdenVenta', 'id', 'fk_orden_venta');
    }

    public function cuenta(){
      return $this->hasOne('App\Cuenta', 'id', 'fk_cuenta');
    }

    public function factura(){
      return $this->hasOne('App\Facturacion', 'id', 'fk_facturacion');
    }
}
