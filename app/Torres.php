<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torres extends Model
{
    //
    protected $table = 'torres';

    //relaciones
    public function equipos(){
      return $this->hasMany('App\Equipos', 'fk_torre', 'id');
    }
}
