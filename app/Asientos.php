<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Asientos extends Model
{
    protected $table = "asientos";

    protected $fillable = ["fecha","nro_asiento","fk_cuenta","descripcion","debe","haber","observacion","created_by","updated_by"];

    public function getFechaAttribute( $value ) {
        return (new Carbon($value))->format('d/m/Y');
    }

    public function setFechaAttribute( $value ) {
        $fecha =  Carbon::createFromFormat('d/m/Y', $value); //->toDateTimeString()
        $this->attributes['fecha'] = $fecha->format('Y-m-d');
    }

    public function cuenta(){
        return $this->belongsTo('App\Cuenta','fk_cuenta');
    }

}
