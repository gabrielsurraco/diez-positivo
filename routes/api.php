<?php

use Illuminate\Http\Request;
use App\PlanDeCuentaDetalle;
use Illuminate\Support\Facades\Response;
use App\Cuenta;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-cuentas/', function (Request  $request) {
    
    if ($request->ajax()){
        
        try {
            $page = Input::get('page');
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;

            $breeds =   Cuenta::select('id',DB::raw('CONCAT(codigo, " ", nombre," - (Imputable: ",imputable,")" ) AS text'))
                        ->where('codigo','LIKE', '%' . Input::get("q"). '%')
                        ->Orwhere('nombre', 'LIKE',  '%' . Input::get("q"). '%')
                        ->orderBy('codigo')
                        ->skip($offset)
                        ->take($resultCount)
                        ->get();

            $count = Cuenta::where('codigo','LIKE', '%' . Input::get("q"). '%')
                        ->Orwhere('nombre', 'LIKE',  '%' . Input::get("q"). '%')->count();

            $endCount = $offset + $resultCount;

            if($count < 25)
                $morePages = false;
            else
                $morePages = $endCount < $count;

            $results = array(
              "results" => $breeds,
              "pagination" => array(
                "more" => $morePages
              )
            );
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(),500);
        }
           
            
        return response()->json($results,200);
    }
   
     return response()->json($results,200);
});