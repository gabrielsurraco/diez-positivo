<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/cab_domicilios', ['as' => 'cab_domicilios', 'uses' => 'CabDomiciliosController@index']);
    Route::get('/contabilidad/plan-de-cuentas', ['as' => 'plan-de-cuentas', 'uses' => 'ContabilidadController@getIndexPlanDeCuentas']);
    Route::post('/contabilidad/get-cuentas',['as' => 'getCuentas', 'uses' => 'ContabilidadController@getCuentasDetalles']);
    Route::post('/contabilidad/plan-de-cuentas/buscarCuentas',[ 'uses' => 'ContabilidadController@buscarCuentas']);
    Route::post('/contabilidad/guardar-cuenta',['as' => 'saveCuenta', 'uses' => 'ContabilidadController@saveCuenta']);
    Route::post('/contabilidad/plan-de-cuentas/delete', ['as' => 'deleteCuenta', 'uses' => 'ContabilidadController@deleteCuenta']);
    Route::get('/contabilidad/plan-de-cuentas/{id}/edit',['as' => 'editCuenta', 'uses' => 'ContabilidadController@editCuenta']);
    Route::post('/contabilidad/plan-de-cuentas/update',['as' => 'updateCuenta', 'uses' => 'ContabilidadController@updateCuenta']);
    Route::post('/contabilidad/plan-de-cuentas/categoria/delete',['as' => 'deleteCategoria', 'uses' => 'ContabilidadController@deleteCategoria']);
    Route::get('/contabilidad/plan-de-cuentas/categoria/{id}/edit',['as' => 'editCategoria', 'uses' => 'ContabilidadController@editCategoria']);
    Route::post('/contabilidad/plan-de-cuentas/categoria/update',['as' => 'updateCategoria', 'uses' => 'ContabilidadController@updateCategoria']);
    Route::get('/contabilidad/plan-de-cuentas/categoria/{id}/new',['as' => 'newCategoria', 'uses' => 'ContabilidadController@newCategoria']);
//    Route::post('/contabilidad/plan-de-cuentas/categoria/save',['as' => 'saveCategoria', 'uses' => 'ContabilidadController@saveCategoria']);

    //ASIENTOS MANUALES
    Route::get('/contabilidad/asientos',['as' => 'asientos.manuales', 'uses' => 'ContabilidadController@indexAsientos']);
    Route::get('/contabilidad/asientos/nuevo',['as' => 'asientos.nuevo', 'uses' => 'ContabilidadController@nuevoAsientos']);
    Route::post('/contabilidad/asientos/buscar',['as' => 'asientos.buscar', 'uses' => 'ContabilidadController@buscarAsientos']);
    Route::post('/contabilidad/asientos/guardar',['as' => 'asientos.guardar', 'uses' => 'ContabilidadController@guardarAsiento']);


    //TICKETS
    Route::get('/tickets',['as' => 'tickets', 'uses' => 'TicketsController@indexTickets']);
    Route::get('/tickets/nuevo',['as' => 'tickets.nuevo', 'uses' => 'TicketsController@crearTicket']);
    Route::get('/tickets/nuevo/fromReference/{id}',['as' => 'tickets.nuevo', 'uses' => 'TicketsController@crearTicketFromReference']);
    Route::get('/tickets/{id}/editar',['as' => 'tickets.editar', 'uses' => 'TicketsController@editarTicket']);
    Route::post('/tickets/guardar',['as' => 'tickets.guardar', 'uses' => 'TicketsController@guardarTicket']);
    Route::post('/tickets/buscar',['as' => 'tickets.buscar', 'uses' => 'TicketsController@buscarTickets']);



    Route::get('/agenda',['as' => 'agenda', 'uses' => 'AgendaController@index']);




    //CUENTA-CORRIENTE
    Route::get('/cuenta-corriente',['as' => 'cuentaCorriente.cuentaCorriente', 'uses' => 'CuentaCorrienteController@index']);
    Route::post('/cuenta-corriente/buscar',[ 'uses' => 'CuentaCorrienteController@getInfoCuenta']);
    Route::post('/cuenta-corriente/setPago',[ 'uses' => 'CuentaCorrienteController@setPago']);
    Route::post('/cuenta-corriente/calcularIntereses',[ 'uses' => 'CuentaCorrienteController@calcularIntereses']);

});

/*
 * Personas
 */
Route::group(['prefix' => 'personas'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', ['as' => 'personas', 'uses' => 'PersonasController@index']);
        Route::get('/create', ['as' => 'personas', 'uses' => 'PersonasController@create']);
        Route::get('/{id}/edit', ['as' => 'personas.editar', 'uses' => 'PersonasController@editPersona']);

        //Rutas de acciones
        Route::post('/guardar', ['as' => 'personas.guardar', 'uses' => 'PersonasController@storePersona']);
        Route::post('/update', ['as' => 'personas.update', 'uses' => 'PersonasController@updatePersona']);
        Route::post('/jsonSearchPersona', ['as' => 'personas.jsonSearchPersona', 'uses' => 'PersonasController@jsonSearchPersona']);
        Route::post('/jsonSearchPersonaNotClient', ['as' => 'personas.jsonSearchPersonaNotClient', 'uses' => 'PersonasController@jsonSearchPersonaNotClient']);
        Route::post('/jsonSearchPersonaNotProvee', ['as' => 'personas.jsonSearchPersonaNotProvee', 'uses' => 'PersonasController@jsonSearchPersonaNotProvee']);
        Route::post('/jsonGetDomiciliosFromPersona', ['as' => 'personas.jsonGetDomiciliosFromPersona', 'uses' => 'PersonasController@jsonGetDomiciliosFromPersona']);

    });
});

/*
 * Servicios
 */
Route::group(['prefix' => 'servicios'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', ['as' => 'servicios.index', 'uses' => 'ServiciosController@index']);

        Route::get('/nuevo', ['as' => 'servicios.index', 'uses' => 'ServiciosController@serviciosNuevo']);
        Route::post('/guardar', ['as' => 'servicios.index', 'uses' => 'ServiciosController@serviciosGuardar']);
        Route::get('/edit/{id}', ['as' => 'servicios.index', 'uses' => 'ServiciosController@serviciosEdit']);
        Route::post('/delete', ['as' => 'servicios.index', 'uses' => 'ServiciosController@deleteServicio']);
    });
});

/*
 * Proveedores
 */
Route::group(['prefix' => 'proveedores'], function () {
    Route::group(['middleware' => 'auth'], function () {
        //PROVEEDORES
        Route::get('/', ['as' => 'proveedores', 'uses' => 'ProveedoresController@listarProveedores']);
        Route::post('/guardarProveedor', ['as' => 'proveedores.guardar', 'uses' => 'ProveedoresController@guardarProveedor']);

        //Acciones
        Route::post('/jsonSearchProveedor', ['as' => 'proveedores.jsonSearchProveedor', 'uses' => 'ProveedoresController@jsonSearchProveedor']);
    });
});

/*
 * Conexiones
 */
Route::group(['prefix' => 'conexiones'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/listar', ['as' => 'conexiones.listar', 'uses' => 'ConexionesController@listar']);
        Route::get('/agregarTorre', ['as' => 'conexiones.agregarTorre', 'uses' => 'ConexionesController@agregarTorre']);
        Route::get('/agregarEquipo', ['as' => 'conexiones.agregarEquipo', 'uses' => 'ConexionesController@agregarEquipo']);
        Route::post('/guardarTorre', ['as' => 'conexiones.listar', 'uses' => 'ConexionesController@guardarTorre']);
        Route::post('/guardarEquipo', ['as' => 'conexiones.guardarEquipo', 'uses' => 'ConexionesController@guardarEquipo']);

        //Acciones
        Route::post('/jsonGetEquiposFromTorre', ['as' => 'conexiones.listar', 'uses' => 'ConexionesController@jsonGetEquiposFromTorre']);
        Route::post('/getDatosFromTorre', ['as' => 'conexiones.getDatosFromTorre', 'uses' => 'ConexionesController@jsonGetDatosFromTorre']);
    });
});

/*
 * Ventas
 */
Route::group(['prefix' => 'ventas'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/lista-precios', ['as' => 'ventas.lista.precios', 'uses' => 'ListaPreciosController@index']);

        //Rutas de ventas / cobros
        Route::get('/cobros', ['as' => 'cuentaCorriente.cobros', 'uses' => 'CobrosController@index']);
        Route::get('/cobros/nuevoCobro', ['as' => 'cuentaCorriente.cobros', 'uses' => 'CobrosController@nuevoCobro']);

        //Rutas de Ordenes de Venta
        Route::get('/ordenesVenta', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@indexOrdenesVenta']);
        Route::get('/nuevaOrdenVenta', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@nuevaOrdenVenta']);
        Route::get('/nuevaOrdenVenta/{id}', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@nuevaOrdenVenta']);
        Route::get('/editOrdenVenta/{id}', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@editOrdenVenta']);
        Route::post('/ordenesVenta/jsonSearchArticuloServicio', ['as' => 'clientes.jsonSearchArticuloServicio', 'uses' => 'OrdenVentaController@jsonSearchArticuloServicio']);
        Route::post('/ordenesVenta/save', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@saveOrdenVenta']);
        Route::get('/ordenesVenta/print/{tipo}/{id}', ['as' => 'cuentaCorriente.ordenesVenta', 'uses' => 'OrdenVentaController@printOrdenVenta']);
    });
});

/*
 * Compras
 */
Route::group(['prefix' => 'compras'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/ordenCompra', ['as' => 'compras.index', 'uses' => 'ComprasController@index']);
        Route::get('/nuevaOrdenCompra', ['as' => 'compras.index', 'uses' => 'ComprasController@nuevaOrdenCompra']);
        Route::post('/guardarOrdenCompra', ['as' => 'compras.index', 'uses' => 'ComprasController@saveOrdenCompra']);
        Route::get('/editOrdenCompra/{id}', ['as' => 'compras.index', 'uses' => 'ComprasController@editOrdenCompra']);
        Route::post('/deleteOrdenCompra', ['as' => 'compras.index', 'uses' => 'ComprasController@deleteOrdenCompra']);
        Route::get('/printOrdenCompra/{id}', ['as' => 'compras.index', 'uses' => 'ComprasController@printOrdenCompra']);
        Route::post('/buscarFiltrosOrdenCompra', ['as' => 'compras.index', 'uses' => 'ComprasController@buscarFiltro']);

        //json listar ordenes de compras
        Route::post('/jsonListOCSolicitadas', ['as' => 'compras.index', 'uses' => 'ComprasController@jsonListOrdenCompraSolicitadas']);
        Route::post('/jsonListOrdenCompraWithDetalle', ['as' => 'compras.index', 'uses' => 'ComprasController@jsonListOrdenCompraWithDetalle']);
    });
});

/*
 * Stock
 */
Route::group(['prefix' => 'stock'], function () {
    Route::group(['middleware' => 'auth'], function () {
        //Almacenes
        Route::match(['get', 'post'], '/almacenes', ['as' => 'stock.almacenes.list', 'uses' => 'StockController@almacenesList']);
        Route::get('/almacenes/nuevo', ['as' => 'stock.almacenes.nuevo', 'uses' => 'StockController@almacenesNuevo']);
        Route::post('/almacenes/guardar', ['as' => 'stock.almacenes.guardar', 'uses' => 'StockController@almacenesGuardar']);
        Route::get('/almacenes/edit/{id}', ['as' => 'stock.almacenes.edit', 'uses' => 'StockController@almacenesEdit']);
        Route::post('/almacenes/delete', ['as' => 'stock.almacenes.delete', 'uses' => 'StockController@deleteAlmacen']);

        //Articuos
        Route::match(['get', 'post'], '/articulos', ['as' => 'stock.articulos.list', 'uses' => 'StockController@articulosList']);
        Route::get('/articulos/nuevo', ['as' => 'stock.articulos.nuevo', 'uses' => 'StockController@articulosNuevo']);
        Route::post('/articulos/guardar', ['as' => 'stock.articulos.guardar', 'uses' => 'StockController@articulosGuardar']);
        Route::get('/articulos/edit/{id}', ['as' => 'stock.articulos.edit', 'uses' => 'StockController@articulosEdit']);
        Route::post('/articulos/delete', ['as' => 'stock.articulos.delete', 'uses' => 'StockController@deleteArticulo']);

        Route::post('/articulos/jsonSearch', ['as' => 'stock.articulos.delete', 'uses' => 'StockController@jsonSearch']);

        //Recepcion de Mercaderias
        Route::match(['get', 'post'], '/recep_mercaderias', ['as' => 'stock.recep_mercaderias.list', 'uses' => 'StockController@recep_mercaderias']);
        Route::post('/recep_mercaderias/ingreso', ['as' => 'stock.recep_mercaderias.ingreso', 'uses' => 'StockController@recep_mercaderiasIngreso']);
        Route::post('/articulos/guardar', ['as' => 'stock.articulos.guardar', 'uses' => 'StockController@articulosGuardar']);
        Route::get('/articulos/edit/{id}', ['as' => 'stock.articulos.edit', 'uses' => 'StockController@articulosEdit']);
        Route::post('/articulos/delete', ['as' => 'stock.articulos.delete', 'uses' => 'StockController@deleteArticulo']);

        //Inventario
        Route::match(['get', 'post'], '/inventario', ['as' => 'stock.inventario.list', 'uses' => 'StockController@inventarioList']);
        Route::get('/articulos/nuevo', ['as' => 'stock.articulos.nuevo', 'uses' => 'StockController@articulosNuevo']);
        Route::post('/articulos/guardar', ['as' => 'stock.articulos.guardar', 'uses' => 'StockController@articulosGuardar']);
        Route::get('/articulos/edit/{id}', ['as' => 'stock.articulos.edit', 'uses' => 'StockController@articulosEdit']);
        Route::post('/articulos/delete', ['as' => 'stock.articulos.delete', 'uses' => 'StockController@deleteArticulo']);
    });
});

/*
 * Recibos
 */
Route::group(['prefix' => 'recibos'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', ['as' => 'cuentaCorriente.recibos', 'uses' => 'ReciboController@list']);
        Route::get('/print/{id}', ['as' => 'cuentaCorriente.recibos', 'uses' => 'ReciboController@reciboPrint']);
        Route::post('/generarRecibo', ['as' => 'cuentaCorriente.recibos', 'uses' => 'ReciboController@generarRecibo']);
        Route::post('/anularRecibo', ['as' => 'cuentaCorriente.recibos', 'uses' => 'ReciboController@anularRecibo']);
    });
});

/*
 * Procesos
 */
Route::group(['prefix' => 'procesos'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', ['as' => 'cuentaCorriente.procesos', 'uses' => 'procesosController@indexProceso']);
        Route::post('/runProcesoAbonoMensual', ['as' => 'cuentaCorriente.procesos', 'uses' => 'procesosController@procesoAbonosMensuales']);
        Route::post('/runProcesoAplicarRecargos', ['as' => 'cuentaCorriente.procesos', 'uses' => 'procesosController@runProcesoAplicarRecargos']);
    });
});

/*
 * Obligaciones
 */
Route::group(['prefix' => 'obligaciones'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', ['as' => 'obligaciones.index', 'uses' => 'ObligacionesController@index']);
        Route::post('/getObligacionesFromCuentaJSON', ['as' => 'obligaciones.getObligacionesFromCuentaJSON', 'uses' => 'ObligacionesController@getObligacionesFromCuentaJSON']);
        //Route::post('/runProcesoAplicarRecargos', ['as' => 'cuentaCorriente.procesos', 'uses' => 'procesosController@runProcesoAplicarRecargos']);
        Route::post('/generarFacturaFromItems', ['as' => 'obligaciones.generarFacturaFromItems', 'uses' => 'ObligacionesController@generarFacturaFromItems']);
        Route::post('/listarFacturasJSON', ['as' => 'obligaciones.listarFacturasJSON', 'uses' => 'ObligacionesController@listarFacturasJSON']);
        Route::post('/listarItemsFacturaJSON', ['as' => 'obligaciones.listarItemsFacturaJSON', 'uses' => 'ObligacionesController@listarItemsFacturaJSON']);

    });
});
/*
 * Localidades
 */
Route::group(['middleware' => 'auth'], function () {
        Route::get('/localidades/get', ['as' => 'localidades.get', 'uses' => 'LocalidadesController@getLocalidades']);
        Route::post('/clientes/servicios/guardar', ['uses' => 'ServiciosController@setServicioCliente']);
        Route::post('/servicios/delete', ['uses' => 'ServiciosController@deleteServicio']);
        Route::get('/servicios/{id}/edit', ['uses' => 'ServiciosController@editServicio']);
        Route::post('/servicios/guardar', ['uses' => 'ServiciosController@guardarServicio']);
        Route::post('/servicios/readServicio', ['uses' => 'ServiciosController@readServicioAJAX']);
        Route::post('/servicios/nuevo-estado', ['uses' => 'ServiciosController@nuevoEstado']);
        Route::post('/lista-precios/new', ['as' => 'lista.precios.new', 'uses' => 'ListaPreciosController@newConcepto']);
        Route::post('/lista-precios/delete', ['as' => 'lista.precios.delete', 'uses' => 'ListaPreciosController@deleteConcepto']);
        Route::post('/lista-precios/update', ['as' => 'lista.precios.update', 'uses' => 'ListaPreciosController@updateConcepto']);
        Route::post('/lista-precios/new-sub-conceptos', ['as' => 'lista.precios.save', 'uses' => 'ListaPreciosController@newSubConceptos']);
        Route::post('/lista-precios/del-sub-conceptos', ['as' => 'lista.precios.del', 'uses' => 'ListaPreciosController@delSubConceptos']);
        Route::get('/lista-precios/{id}/edit', ['as' => 'lista.precios.edit', 'uses' => 'ListaPreciosController@editSubConceptos']);
        Route::post('/lista-precios/guardar', ['as' => 'lista.precios.guardar', 'uses' => 'ListaPreciosController@guardarSubConcepto']);


        //USUARIOS
        Route::get('/usuarios', ['as' => 'usuarios', 'uses' => 'UsuariosController@index']);
        Route::post('/usuarios/eliminar', ['as' => 'usuarios.eliminar', 'uses' => 'UsuariosController@eliminarUsuario']);
        Route::get('/usuarios/nuevo', ['as' => 'usuarios.nuevo', 'uses' => 'UsuariosController@nuevoUsuario']);
        Route::post('/usuarios/crear', ['as' => 'usuarios.crear', 'uses' => 'UsuariosController@crearUsuario']);
        Route::get('/usuarios/{id}/edit', ['as' => 'usuarios.editar', 'uses' => 'UsuariosController@editarUsuario']);
        Route::post('/usuarios/edit/store', ['as' => 'usuarios.storeuser', 'uses' => 'UsuariosController@storeEditionUser']);

        //CLIENTES
        Route::match(['get', 'post'], '/clientes', ['as' => 'clientes', 'uses' => 'ClientesController@listarClientes']);
        Route::get('/clientes/{id}/servicios', ['as' => 'servicios.cliente', 'uses' => 'ServiciosController@getServiciosCliente']);
        Route::get('/clientes/{id}/detalle', ['as' => 'clientes.detalle', 'uses' => 'ClientesController@detalleCliente']);
        Route::post('/clientes/get-servicios', ['as' => 'servicios.cliente', 'uses' => 'ServiciosController@getServiciosClienteAJAX']);
        Route::post('/clientes/get-all-data', ['as' => 'servicios.cliente', 'uses' => 'ClientesController@getDatosClienteToJson']);
        Route::post('/clientes/guardarCliente', ['as' => 'clientes.guardarCliente', 'uses' => 'ClientesController@guardarCliente']);
        Route::post('/clientes/guardarParentescoRelacion', ['as' => 'clientes.guardarParentescoRelacion', 'uses' => 'ParentescosRelacionesController@guardarParentescoRelacion']);
        Route::post('/clientes/borrarParentescoRelacion', ['as' => 'clientes.borrarParentescoRelacion', 'uses' => 'ParentescosRelacionesController@borrarParentescoRelacion']);

        Route::get('/clientes/{id}/agregar_servicios', ['as' => 'clientes.agregarservicio', 'uses' => 'ServiciosController@agregarServiciosCliente']);
        Route::post('/clientes/guardar_servicio', ['as' => 'clientes.guardarservicio', 'uses' => 'ServiciosController@guardarServicioCliente']);

        //Clientes - Servicios Funciones
        Route::post('/clientes/getServiciosFromLocalidad', ['as' => 'clientes.guardarservicio', 'uses' => 'ServiciosController@getServiciosFromLocalidadAJAX']);
        Route::post('/clientes/changeStatusServicio', ['as' => 'clientes.changeStatusServicio', 'uses' => 'ServiciosController@changeStatusServicio']);
        Route::post('/clientes/getServiciosClienteJSON', ['as' => 'clientes.getServiciosClienteJSON', 'uses' => 'ServiciosController@getServiciosClienteJSON']);
        Route::post('/clientes/getServiciosClienteFromIDJSON', ['as' => 'clientes.getServiciosClienteFromIDJSON', 'uses' => 'ServiciosController@getServiciosClienteFromIDJSON']);
        Route::post('/clientes/getPrecioIVAArticuloOServicio', ['as' => 'clientes.getPrecioIVAArticuloOServicio', 'uses' => 'OrdenVentaController@getPrecioIVAArticuloOServicio']);
        Route::post('/clientes/jsonSearchCliente', ['as' => 'clientes.jsonSearchCliente', 'uses' => 'ClientesController@jsonSearchCliente']);
        Route::post('/clientes/actualizar_servicios', ['as' => 'clientes.actualizarServicio', 'uses' => 'ServiciosController@actualizarServiciosCliente']);

        //Cobros Funciones
        Route::post('/clientes/getOrdenesVentaFromClienteJSON', ['as' => 'clientes.getOrdenesVentaFromClienteJSON', 'uses' => 'OrdenVentaController@getOrdenesVentaFromClienteJSON']);
        Route::post('/clientes/getDatosDeudaFromClienteJSON', ['as' => 'clientes.getDatosDeudaFromClienteJSON', 'uses' => 'CobrosController@getDatosDeudaFromClienteJSON']);
        Route::post('/cobranza/generarCobro', ['as' => 'cobranza.generarCobranza', 'uses' => 'CobrosController@generarCobranza']);
        Route::post('/cobranza/informarPago', ['as' => 'cobranza.informarPago', 'uses' => 'CobrosController@informarPago']);
        //Route::post('/clientes/generarReciboFromPagoParcial', ['as' => 'clientes.generarReciboFromPagoParcial', 'uses' => 'ReciboController@generarReciboFromPagoParcial']);

        Route::post('/pagoEfectuado/asociarFactura', ['as' => 'cobranza.asociarFactura', 'uses' => 'OrdenVentaController@asociarFacturaToOV']);

        //PETICIONES AJAX
        Route::post('/personas/buscar', [ 'uses' => 'PersonasController@buscarPersona']);
        Route::post('/personas/setRol', [ 'uses' => 'PersonasController@setRol']);


});
