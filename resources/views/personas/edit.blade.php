@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
    <div class="row">

        <div class="col-md-12 m-b-md">

            <div class="panel panel-default  ">
                <div class="panel-heading">Edición de Persona</div>

                <div class="panel-body" >

                    {!! Form::open(['url' => '/personas/update', 'method' => 'post','id' => 'form-cuenta']) !!}

                            <input name="id_persona" type="hidden" value="{{ $persona->id }}" />

                            <div class="row">
                                <div class="form-group col-md-12 col-md-offset-5">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_persona" value="1" @if($persona->es_persona_fisica == 1) checked="{{"checked"}}" @else disabled @endif > Física
                                    </label>
                                    <label class="radio-inline">
                                        <input  type="radio" name="tipo_persona" value="0" @if($persona->es_persona_fisica == 0) checked="{{"checked"}}" @else disabled @endif > Jurídica
                                    </label>
                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-2">
                                    {{ Form::label('tipo_doc', 'Tipo Documento') }}
                                    {{ Form::select("tipo_doc", array('96' => 'DNI','80' => 'CUIT','87' => 'CDI','90' => 'LC','89' => 'LE','94' => 'PASAPORTE','91' => 'CI Extrajera','00' => 'Policia Federal','30' => 'Certificado de Migracion') , $persona->tipo_doc , array("class"=>"form-control","readonly" => "")) }}
                                </div>
                                <div class="form-group col-md-3">
                                    {{ Form::label('num_doc', 'Número Doc. *') }}
                                    {{ Form::text("num_doc", $persona->cuit , array("class"=>"form-control","readonly" => "", "placeholder"=>"Número de documento")) }}
                                    @if ($errors->has('num_doc'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('num_doc') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                @if ($persona->es_persona_fisica == 1)
                                <div class="persona-fisica">
                                    <div class="form-group col-md-3">
                                        {{ Form::label('apellido', 'Apellido *') }}
                                        {{ Form::text("apellido", $persona->apellido, array("class"=>"form-control", "placeholder"=>"Apellido")) }}
                                        @if ($errors->has('apellido'))
                                        <span class="help-block text-danger">
                                                <strong>{{ $errors->first('apellido') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{ Form::label('nombre', 'Nombre *') }}
                                        {{ Form::text("nombre", $persona->nombre, array("class"=>"form-control", "placeholder"=>"Nombre")) }}
                                        @if ($errors->has('nombre'))
                                        <span class="help-block text-danger">
                                                <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-md-offset-4 col-md-4">
                                      {{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
                                      <div class="input-group date">
                                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_nacimiento" class="form-control" value="{{ $persona->fecha_nacimiento }}">
                                      </div>
                                      @if ($errors->has('fecha_nacimiento'))
                                      <span class="help-block text-danger">
                                          <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                </div>
                                @else
                                  <div class="form-group col-md-7">
                                      {{ Form::label('razon_social', 'Razón Social *') }}
                                      {{ Form::text("razon_social",  $persona->razon_social , array("class"=>"form-control", "placeholder"=>"Razón Social")) }}
                                      @if ($errors->has('razon_social'))
                                      <span class="help-block text-danger">
                                              <strong>{{ $errors->first('razon_social') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                  @endif
                              </div>

                              <div class="row">

                                <div class="form-group col-md-offset-2 col-md-4">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::email("email", $persona->email, array("class"=>"form-control", "placeholder"=>"Correo")) }}
                                    @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::label('email_2', 'Email Alternativo') }}
                                    {{ Form::email("email_2", $persona->email_2, array("class"=>"form-control", "placeholder"=>"Correo Alternativo")) }}
                                    @if ($errors->has('email_2'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                              </div>
                              <div class="row">
                                <div class="form-group col-md-offset-2 col-md-4">
                                    {{ Form::label('profesion', 'Profesión / Rubro') }}
                                    {{ Form::select("profesion", array("estudiante" => "Estudiante", "profesional" => "Profesional", "comerciante" => "Comerciante", "jubilado" => "Jubilado"),$persona->profesion, array("class"=>"form-control", "placeholder"=>"Seleccionar...")) }}
                                    @if ($errors->has('profesion'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('profesion') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    {{ Form::label('cat_fiscal', 'Categoría Fiscal') }}
                                    {{ Form::select("cat_fiscal", array('5' => 'CONSUMIDOR FINAL','4' => 'IVA EXENTO','9' => 'CLIENTE DEL EXTERIOR','6' => 'MONOTRIBUTISTA','1' => 'RESPONSABLE INSCRIPTO','13' => 'MONOTRIBUTISTA SOCIAL') ,$persona->condicion_fiscal, array("class"=>"form-control")) }}
                                </div>
                              </div>
                              <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <strong>Domicilio de la Persona</strong>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group col-md-4">
                                                {{ Form::label('provincia', 'Provincia') }}
                                                {{ Form::select("provincia", array('1' => 'Misiones') , $persona->domicilios[0]->fk_provincia , array("class"=>"form-control")) }}
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('departamento', 'Departamento *') }}
                                                {{ Form::select("departamento",array('Seleccionar...')+$array_departamentos ,$persona->domicilios[0]->fk_departamento, array("class"=>"form-control")) }}
                                                @if ($errors->has('departamento'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('departamento') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('localidad', 'Localidad *') }}
                                                {{ Form::select("localidad", array('Seleccionar...')+$array_localidades ,$persona->domicilios[0]->fk_localidad, array("class"=>"form-control")) }}
                                                @if ($errors->has('localidad'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('localidad') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('calle', 'Calle *') }}
                                                {{ Form::text("calle", $persona->domicilios[0]->calle , array("class"=>"form-control", "placeholder"=>"Nombre de la calle")) }}
                                                @if ($errors->has('calle'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('calle') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('altura', 'Número') }}
                                                {{ Form::number("altura", $persona->domicilios[0]->altura, array("class"=>"form-control","value" => "0", "placeholder"=>"Altura de la Calle")) }}
                                                @if ($errors->has('altura'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('altura') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('piso', 'Piso') }}
                                                {{ Form::number("piso", $persona->domicilios[0]->piso , array("class"=>"form-control", "placeholder"=>"Piso del Depto.")) }}
                                                @if ($errors->has('piso'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('piso') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('nro_depto', 'Depto.') }}
                                                {{ Form::text("nro_depto", $persona->domicilios[0]->depto, array("class"=>"form-control", "placeholder"=>"Depto.")) }}
                                                @if ($errors->has('nro_depto'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('nro_depto') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('cp', 'Código Postal') }}
                                                {{ Form::text("cp", $persona->domicilios[0]->cp , array("class"=>"form-control", "placeholder"=>"Código Postal")) }}
                                                @if ($errors->has('cp'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('cp') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('link_mapa', 'Link Mapa') }}
                                                {{ Form::text("link_mapa", $persona->domicilios[0]->link_mapa, array("class"=>"form-control", "placeholder"=>"Link Mapa Geolocalización")) }}
                                                @if ($errors->has('link_mapa'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('link_mapa') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                           <div class="form-group col-md-4">
                                                {{ Form::label('latitud', 'Latidud') }}
                                                {{ Form::text("latitud", $persona->domicilios[0]->latitud , array("class"=>"form-control", "placeholder"=>"Latitud")) }}
                                                @if ($errors->has('latitud'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('latitud') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('longitud', 'Longitud') }}
                                                {{ Form::text("longitud", $persona->domicilios[0]->longitud , array("class"=>"form-control", "placeholder"=>"Longitud")) }}
                                                @if ($errors->has('longitud'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('longitud') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                {{ Form::label('observaciones_dom', 'Observaciones') }}
                                                {{ Form::text("observaciones_dom", $persona->domicilios[0]->observacion, array("class"=>"form-control", "placeholder"=>"Observaciones del domicilio...")) }}
                                                @if ($errors->has('observaciones_dom'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('observaciones_dom') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <strong>Teléfonos de Contacto</strong>
                                        </div>
                                        <div class="panel-body">
                                            <a class="btn btn-primary btn-sm m-b-sm add-tel"><i class="fa fa-plus"></i> Agregar</a>
                                            <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Tipo</th>
                                                    <th>Compañia</th>
                                                    <th>Cod Pais</th>
                                                    <th>Cód. Área</th>
                                                    <th>Número</th>
                                                    <th>Horario Contacto</th>
                                                    <th>Observaciones</th>
                                                    <th>Acción</th>
                                                </tr>
                                                </thead>
                                                <tbody class="body-tel">

                                                @foreach($persona->telefonos as $telefono)

                                                <tr>
                                                    <input type="hidden" name="id_telefono[]" value="{{ $telefono->id }}" />
                                                    <td style="width: auto;">
                                                        <select name="tipo[]">
                                                            <option value="fijo" <?php if($telefono->tipo == 'fijo'){ echo 'selected'; } ?> >Fijo</option>
                                                            <option value="movil" <?php if($telefono->tipo == 'movil'){ echo 'selected'; } ?> >Móvil</option>
                                                        </select>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <select name="compania[]">
                                                            <option value="personal" <?php if($telefono->empresa == 'personal'){ echo 'selected'; } ?>>Personal</option>
                                                            <option value="claro" <?php if($telefono->empresa == 'claro'){ echo 'selected'; } ?>>Claro</option>
                                                            <option value="movistar" <?php if($telefono->empresa == 'movistar'){ echo 'selected'; } ?>>Movistar</option>
                                                            <option value="telecom" <?php if($telefono->empresa == 'telecom'){ echo 'selected'; } ?>>Telecom</option>
                                                            <option value="otro" <?php if($telefono->empresa == 'otro'){ echo 'selected'; } ?>>Otro</option>
                                                        </select>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="cod_pais[]" value="{{ $telefono->cod_pais }}"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="cod_area[]"  value="{{ $telefono->area }}"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="numero[]" value="{{ $telefono->numero }}" />
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="text" name="horario_contacto[]" value="{{ $telefono->horario_contacto }}" />
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="text" name="observaciones[]" value='{{ $telefono->observaciones }}' />
                                                    </td>
                                                    <td style="width: auto;"><a class="btn btn-danger btn-xs remove-tel"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-md-12">
                                    {{ Form::label('observaciones_per', 'Observaciones') }}
                                    {{ Form::textarea("observaciones_per", $persona->observaciones , array("class"=>"form-control", "placeholder"=>"Observaciones de la Persona...","rows" => "5")) }}
                                    @if ($errors->has('observaciones_per'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('observaciones_per') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                </div>

                        <div class="modal-footer">
                            <a href="/personas"><button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button></a>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script>
    $(document).ready(function(){

//        var cont = 0;
        $(".add-tel").click(function(){
//            cont = cont + 1;
//            var tr = $(".tr-ejemplo").clone(true);
            var tr = '<tr >'+
                        '<input type="hidden" name="id_telefono[]" value="" />'+
                        '<td style="width: auto;">'+
                            '<select name="tipo[]" >'+
                                '<option value="fijo">Fijo</option>'+
                                '<option value="movil">Móvil</option>'+
                            '</select>'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<select name="compania[]" >'+
                                '<option value="personal">Personal</option>'+
                                '<option value="claro">Claro</option>'+
                                '<option value="movistar">Movistar</option>'+
                                '<option value="telecom">Telecom</option>'+
                                '<option value="otro">Otro</option>'+
                            '</select>'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<input type="number" name="cod_pais[]" required/>'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<input type="number" name="cod_area[]" required/>'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<input type="number" name="numero[]" required/>'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<input type="text" name="horario_contacto[]" />'+
                        '</td>'+
                        '<td style="width: auto;">'+
                            '<input type="text" name="observaciones[]" />'+
                        '</td>'+
                        '<td style="width: auto;"><a class="btn btn-danger btn-xs remove-tel"><i class="fa fa-trash"></i></a></td>'+
                    '</tr>';
            $(".body-tel").append(tr);
        });

        $(".remove-tel").click(function(){
            $(this).parent().parent().empty();
        });

        $(".select2_demo_3").select2({
            placeholder: "Seleccione una Opción...",
            allowClear: true
        });

        $('.input-group.date').datepicker({
            startView: 3,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('#departamento').on('change',function(){
            if(this.value !== 0){
                var valor = this.value;
                $.ajax({
                   type: "GET",
                   url: "/localidades/get",
                   data: "depto="+valor,
                   success: function(data)
                   {
                       $('#localidad').not(":nth-child(1)").empty();
                        var a = JSON.parse(data);
                        $.each(a, function(key, value) {
                            $('#localidad')
                                .append($("<option></option>")
                                           .attr("value",value.id)
                                           .text(value.nombre));
                       });
                   },
                   error: function(msg){
                       alert(msg);
                   }

                });
            }

        });

    });
</script>
@stop
