@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">

@stop
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
    <div class="row">
      @if (Session::has('flash_notification.message'))
      <div class="container">
          <div class="row ">
              <div class="col-md-12 ">
                  <div class="alert alert-{{ session('flash_notification.level') }} ">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {!! Session('flash_notification.message') !!}
                  </div>
              </div>
          </div>
      </div>

      @endif
        <div class="col-md-12 m-b-md">

            <div class="panel panel-default  ">
                <div class="panel-heading"><strong>Alta de Persona</strong></div>

                <div class="panel-body" >
                    {!! Form::open(['url' => '/personas/guardar', 'method' => 'post','id' => 'form-cuenta']) !!}
                            <div class="row">
                                <div class="form-group col-md-12 col-md-offset-5">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_persona" value="f" @if(old('tipo_persona') == 'f') checked @endif @if(old('tipo_persona') == null) checked @endif> Física </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_persona" value="j" @if(old('tipo_persona') == 'j') checked @endif> Jurídica </label>
                                </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-2 tipo_doc">
                                  {{ Form::label('tipo_doc', 'Tipo Documento') }}
                                  <select name="tipo_doc" class="form-control">
                                        <option value="80">CUIT</option>
                                        <option value="87">CDI</option>
                                        <option value="89">LE</option>
                                        <option value="90">LC</option>
                                        <option value="91">CI Extranjera</option>
                                        <option value="96" selected>DNI</option>
                                        <option value="94">Pasaporte</option>
                                        <option value="00">CI Policía Federal</option>
                                        <option value="30">Certificado de Migración</option>
                                  </select>
                              </div>
                              <div class="form-group col-md-3">
                                  <label for="num_doc">Numero Documento: *</label>
                                  <input type="text" class="form-control" name="num_doc" value="">
                                  @if ($errors->has('num_doc'))
                                  <span class="help-block text-danger">
                                      <strong>{{ $errors->first('num_doc') }}</strong>
                                  </span>
                                  @endif
                              </div>
                              <div class="persona-fisica">
                                  <div class="form-group col-md-3">
                                      {{ Form::label('apellido', 'Apellido *') }}
                                      {{ Form::text("apellido", null, array("class"=>"form-control", "placeholder"=>"Apellido")) }}
                                      @if ($errors->has('apellido'))
                                      <span class="help-block text-danger">
                                              <strong>{{ $errors->first('apellido') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                  <div class="form-group col-md-4">
                                      {{ Form::label('nombre', 'Nombre *') }}
                                      {{ Form::text("nombre", null, array("class"=>"form-control", "placeholder"=>"Nombre")) }}
                                      @if ($errors->has('nombre'))
                                      <span class="help-block text-danger">
                                              <strong>{{ $errors->first('nombre') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                  <div class="form-group col-md-offset-4 col-md-4">
                                      {{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
                                      <div class="input-group date">
                                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_nacimiento" class="form-control" value="10/11/2013">
                                      </div>
                                      @if ($errors->has('fecha_nacimiento'))
                                      <span class="help-block text-danger">
                                          <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="persona-juridica" style="display: none;">
                                  <div class="form-group col-md-7">
                                      {{ Form::label('razon_social', 'Razón Social *') }}
                                      {{ Form::text("razon_social", null, array("class"=>"form-control", "placeholder"=>"Razón Social")) }}
                                      @if ($errors->has('razon_social'))
                                      <span class="help-block text-danger">
                                              <strong>{{ $errors->first('razon_social') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-offset-2 col-md-4">
                                  {{ Form::label('email', 'Email') }}
                                  {{ Form::email("email", null, array("class"=>"form-control", "placeholder"=>"Correo")) }}
                                  @if ($errors->has('email'))
                                  <span class="help-block text-danger">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                                  @endif
                              </div>
                              <div class="form-group col-md-4">
                                  {{ Form::label('email_2', 'Email Alternativo') }}
                                  {{ Form::email("email_2", null, array("class"=>"form-control", "placeholder"=>"Correo Alternativo")) }}
                                  @if ($errors->has('email_2'))
                                  <span class="help-block text-danger">
                                      <strong>{{ $errors->first('email_2') }}</strong>
                                  </span>
                                  @endif
                              </div>
                            </div>

                                <div class="row">
                                  <div class="form-group col-md-offset-2 col-md-4">
                                      {{ Form::label('profesion', 'Profesión / Rubro') }}
                                      {{ Form::select("profesion", array("estudiante" => "Estudiante", "profesional" => "Profesional", "comerciante" => "Comerciante", "jubilado" => "Jubilado"),null, array("class"=>"form-control", "placeholder"=>"Seleccionar...")) }}
                                      @if ($errors->has('profesion'))
                                      <span class="help-block text-danger">
                                          <strong>{{ $errors->first('profesion') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                  <div class="form-group col-md-4">
                                      {{ Form::label('cat_fiscal', 'Categoría Fiscal') }}
                                      {{ Form::select("cat_fiscal", array('5' => 'CONSUMIDOR FINAL','4' => 'IVA EXENTO','9' => 'CLIENTE DEL EXTERIOR','6' => 'MONOTRIBUTISTA','1' => 'RESPONSABLE INSCRIPTO','13' => 'MONOTRIBUTISTA SOCIAL') ,null, array("class"=>"form-control")) }}
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-12">
                                      <div class="panel panel-default">
                                          <div class="panel-heading ">
                                              <strong>Domicilio de la Persona</strong>
                                          </div>
                                          <div class="panel-body">
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('provincia', 'Provincia') }}
                                                  {{ Form::select("provincia", array('1' => 'Misiones') ,null, array("class"=>"form-control")) }}
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('departamento', 'Departamento *') }}
                                                  {{ Form::select("departamento",array('Seleccionar...')+$array_departamentos ,null, array("class"=>"form-control")) }}
                                                  @if ($errors->has('departamento'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('departamento') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('localidad', 'Localidad *') }}
                                                  {{ Form::select("localidad", array('Seleccionar...') ,null, array("class"=>"form-control")) }}
                                                  @if ($errors->has('localidad'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('localidad') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('calle', 'Calle *') }}
                                                  {{ Form::text("calle", null, array("class"=>"form-control", "placeholder"=>"Nombre de la calle", "list" => "calles")) }}
                                                  <datalist id="calles">
                                                      @foreach($calles as $calle)
                                                          <option value="{{ $calle['calle'] }}">
                                                      @endforeach
                                                  </datalist>
                                                  @if ($errors->has('calle'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('calle') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-2">
                                                  {{ Form::label('altura', 'Número') }}
                                                  {{ Form::number("altura", "0", array("class"=>"form-control","value" => "0", "placeholder"=>"Altura de la Calle")) }}
                                                  @if ($errors->has('altura'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('altura') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-2">
                                                  {{ Form::label('piso', 'Piso') }}
                                                  {{ Form::number("piso", "0", array("class"=>"form-control", "placeholder"=>"Piso del Depto.")) }}
                                                  @if ($errors->has('piso'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('piso') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-2">
                                                  {{ Form::label('nro_depto', 'Depto.') }}
                                                  {{ Form::text("nro_depto", null, array("class"=>"form-control", "placeholder"=>"Depto.")) }}
                                                  @if ($errors->has('nro_depto'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('nro_depto') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-2">
                                                  {{ Form::label('cp', 'Código Postal') }}
                                                  {{ Form::text("cp", null, array("class"=>"form-control", "placeholder"=>"Código Postal")) }}
                                                  @if ($errors->has('cp'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('cp') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('link_mapa', 'Link Mapa') }}
                                                  {{ Form::text("link_mapa", null, array("class"=>"form-control", "placeholder"=>"Link Mapa Geolocalización")) }}
                                                  @if ($errors->has('link_mapa'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('link_mapa') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('latitud', 'Latidud') }}
                                                  {{ Form::text("latitud", null, array("class"=>"form-control", "placeholder"=>"Latitud")) }}
                                                  @if ($errors->has('latitud'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('latitud') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-4">
                                                  {{ Form::label('longitud', 'Longitud') }}
                                                  {{ Form::text("longitud", null, array("class"=>"form-control", "placeholder"=>"Longitud")) }}
                                                  @if ($errors->has('longitud'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('longitud') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                              <div class="form-group col-md-6">
                                                  {{ Form::label('observaciones_dom', 'Observaciones del Domicilio') }}
                                                  {{ Form::text("observaciones_dom", null, array("class"=>"form-control", "placeholder"=>"Observaciones del domicilio...")) }}
                                                  @if ($errors->has('observaciones_dom'))
                                                  <span class="help-block text-danger">
                                                      <strong>{{ $errors->first('observaciones_dom') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-12">
                                      <div class="panel panel-default">
                                          <div class="panel-heading ">
                                              <strong>Teléfonos de Contacto</strong>
                                          </div>
                                          <div class="panel-body">
                                              <a class="btn btn-primary btn-sm m-b-sm add-tel"><i class="fa fa-plus"></i> Agregar</a>
                                              <div class="table-responsive">
                                              <table class="table table-bordered">
                                                  <thead>
                                                  <tr>
                                                      <th>#</th>
                                                      <th>Tipo</th>
                                                      <th>Compañia</th>
                                                      <th>Cod Pais</th>
                                                      <th>Cód. Área</th>
                                                      <th>Número</th>
                                                      <th>Horario Contacto</th>
                                                      <th>Observaciones</th>
                                                      <th>Acción</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody class="body-tel">
                                                  <tr class="tr-ejemplo hidden">
                                                      <td style="width: auto;">1</td>
                                                      <td style="width: auto;">
                                                          <select name="tipo[]">
                                                              <option value="fijo">Fijo</option>
                                                              <option value="movil">Móvil</option>
                                                          </select>
                                                      </td>
                                                      <td style="width: auto;">
                                                          <select name="compania[]">
                                                              <option value="personal">Personal</option>
                                                              <option value="claro">Claro</option>
                                                              <option value="movistar">Movistar</option>
                                                              <option value="telecom">Telecom</option>
                                                              <option value="otro">Otro</option>
                                                          </select>
                                                      </td>
                                                      <td style="width: auto;">
                                                          <input type="number" name="cod_pais[]" />
                                                      </td>
                                                      <td style="width: auto;">
                                                          <input type="number" name="cod_area[]" />
                                                      </td>
                                                      <td style="width: auto;">
                                                          <input type="number" name="numero[]"/>
                                                      </td>
                                                      <td style="width: auto;">
                                                          <input type="text" name="horario_contacto[]" />
                                                      </td>
                                                      <td style="width: auto;">
                                                          <input type="text" name="observaciones[]"/>
                                                      </td>
                                                      <td style="width: auto;"><a class="btn btn-danger btn-xs remove-tel"><i class="fa fa-trash"></i></a></td>
                                                  </tr>
                                                  </tbody>
                                              </table>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-12">
                                      {{ Form::label('observaciones_per', 'Observaciones del Cliente') }}
                                      {{ Form::textarea("observaciones_per", null, array("class"=>"form-control", "placeholder"=>"Observaciones del Cliente...","rows" => "5")) }}
                                      @if ($errors->has('observaciones_per'))
                                      <span class="help-block text-danger">
                                          <strong>{{ $errors->first('observaciones_per') }}</strong>
                                      </span>
                                      @endif
                                  </div>
                                </div>
                        <!--</div>-->
                        <div class="modal-footer">
                            <a href="/personas"><button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button></a>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Input Mask-->
 <script src="{{ asset('/backend/js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

<script>
    $(document).ready(function(){
        var cont = 0;
        $(".add-tel").click(function(){
            cont = cont + 1;
            var tr = $(".tr-ejemplo").clone(true);
            tr.removeClass("hidden");
            tr.removeClass("tr-ejemplo");
            tr.find("td:eq(0)").text(cont);
            $(".body-tel").append(tr);
        });

        $(".remove-tel").click(function(){
            $(this).parent().parent().empty();
        });

        @if(old('tipo_persona') == 'j')
          $(".persona-fisica").hide();
          $(".persona-juridica").show();
        @endif

        $("input[name='tipo_persona']").change(function(evnt){
            var asd = $(this).val();
            if(asd === 'f'){
                $(".persona-fisica").show();
                $(".persona-juridica").hide();
            }else if(asd === 'j'){
                $(".persona-fisica").hide();
                $(".persona-juridica").show();
            }
        });

        $(".select2_demo_3").select2({
            placeholder: "Seleccione una Opción...",
            allowClear: true
        });

        $('.input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('#departamento').on('change',function(){
            if(this.value !== 0){
                var valor = this.value;
                $.ajax({
                   type: "GET",
                   url: "/localidades/get",
                   data: "depto="+valor,
                   success: function(data)
                   {
                       $('#localidad').not(":nth-child(1)").empty();
                        var a = JSON.parse(data);
                        $.each(a, function(key, value) {
                            $('#localidad')
                                .append($("<option></option>")
                                           .attr("value",value.id)
                                           .text(value.nombre));
                       });
                   },
                   error: function(msg){
                       alert(msg);
                   }

                });
            }

        });

        $('[name="tipo_doc"]').on('change', function(event) {
          event.preventDefault();
          if ($(this).val() == 80) {
            $('[name="num_doc"]').attr('data-mask', '99-99999999-9');
          }else {
            $('[name="num_doc"]').removeAttr('data-mask');
          }
        });

    });
</script>
@stop
