@extends('layouts.backend')


@section('css')
<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
  <div class="col-lg-4">
    <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5>EJECUTAR <small>Proceso generar deuda abonos mensuales</small></h5>
              <div class="ibox-tools">
                  <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                  </a>
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-wrench"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                      <li><a href="#">Config option 1</a>
                      </li>
                      <li><a href="#">Config option 2</a>
                      </li>
                  </ul>
                  <a class="close-link">
                      <i class="fa fa-times"></i>
                  </a>
              </div>
          </div>
          <div class="ibox-content">
              <div class="text-center">
                <form class="" action="/procesos/runProcesoAbonoMensual" method="post">
                  {{csrf_field()}}
                  <button type="submit" onclick="runProcesoAbonoMensual()" class="btn btn-primary" name="btn-run-abonos"> <span class="fa fa-play"></span> COMENZAR</button>
                  <p>Recomendamos hacer este proceso el dia 1 del mes.</p>
                </form>


              </div>

      </div>
  </div>
  </div>

  <div class="col-lg-4">
    <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5>EJECUTAR <small>Proceso aplicar recargo sobre saldos impagos</small></h5>
              <div class="ibox-tools">
                  <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                  </a>
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-wrench"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                      <li><a href="#">Config option 1</a>
                      </li>
                      <li><a href="#">Config option 2</a>
                      </li>
                  </ul>
                  <a class="close-link">
                      <i class="fa fa-times"></i>
                  </a>
              </div>
          </div>
          <div class="ibox-content">
              <div class="text-center">
                  @php
                  if(Carbon\Carbon::now()->day > 10){
                    $recargoActivo = true;
                  }else{
                    $recargoActivo = false;
                  }
                  @endphp

                    <button type="button" onclick="runProcesoAplicarRecargos();" class="btn btn-warning @if($recargoActivo == false) disabled @endif" name="btn-run-abonos"> <span class="fa fa-play"></span> COMENZAR</button>

                  <p>Los recargos podran ser aplicados despues del dia 10.</p>
              </div>

      </div>
  </div>
  </div>
</div>

</div>
<!-- Fin Contenido Principal -->
<!-- Inicio de Modal -->
  <div class="modal inmodal fade" id="modal-assocFac" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title">Asociar Factura</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="">Ingrese Nro de Factura:</label>
                      <input type="text" id="inp-num-factura" class="form-control" id="" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                  <button type="button" class="btn btn-primary" id="btn-save-asociacion">Guardar</button>
              </div>
          </div>
      </div>
  </div>
<!-- Fin Modal -->




@endsection

@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('/backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- DatePicker  -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>



<!-- Page-Level Scripts -->
<script>


  $(document).ready(function() {

  });

  function runProcesoAbonoMensual(){
      swal({
        title: "Ejecutar proceso?",
        text: "Este proceso implica agregar a cada cliente la obligacion de pagar su abono mensual.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: "POST",
            url: "/procesos/runProcesoAbonoMensual",
            data: { "_token": "{{ csrf_token() }}"},
            dataType: 'json'
          }).done(function( msg ) {
              console.log(msg);
              swal("Exito! El proceso se ejecuto correctamente!", {
                icon: "success",
              });
          }).fail(function(mgs){
              swal({
                title: "Error!",
                text: "Algo esta mal, por favor Intentelo nuevamente.",
                type: "error",
                confirmButtonText: "Cerrar"
              });
          });
        } else {
          swal("No se ejecuto ningun proceso!");
        }
      });
    }

    @if($recargoActivo == true)

    function runProcesoAplicarRecargos(){
        swal({
          title: "Ejecutar proceso?",
          text: "Este proceso implica aplicar recargos a cada cuenta con saldo negativo.",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              method: "POST",
              url: "/procesos/runProcesoAplicarRecargos",
              data: { "_token": "{{ csrf_token() }}"},
              dataType: 'json'
            }).done(function( msg ) {
                console.log(msg);
                swal("Exito! El proceso se ejecuto correctamente!", {
                  icon: "success",
                });
            }).fail(function(mgs){
                swal({
                  title: "Error!",
                  text: "Algo esta mal, por favor Intentelo nuevamente.",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            });
          } else {
            swal("No se ejecuto ningun proceso!");
          }
        });
    }
  @endif
</script>

@stop
