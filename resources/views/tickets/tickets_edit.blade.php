@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <!--<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">-->
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">
    
    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">
    
    <!-- Data Tables -->
    <link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default m-b-lg">
                <div class="panel-heading">Editar Ticket</div>
                    
                <div class="panel-body">
                    
                    <div class="row m-t-lg">
                        <form action="/tickets/guardar" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="editando" value="si" />
                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}" />
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                                {{ Form::label('es_cliente', 'Es Cliente?') }}
                                <select name="es_cliente" class="form-control es_cliente" style="width: 100% !important;">
                                    <option value="si" selected="">Si</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 cliente">
                                {{ Form::label('cliente', 'Cliente') }}
                                <select name="cliente" class="form-control select2_demo_3 cliente_select " style="width: 100% !important;">
                                    <option></option>
                                    @foreach($clientes as $cliente => $value)
                                        <option value="{{ $value->id }}"  > {{ $value->persona->nombre . ' ' . $value->persona->apellido . ' - ' . $value->persona->cuit }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('cliente'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('cliente') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3 no_cliente" <?php if($ticket->es_cliente == "si"){ echo "style='display: none;'"; } ?>>
                                {{ Form::label('nombre', 'Apellido y Nombre') }}
                                <input name="nombre" type="text" class="form-control" placeholder="Apellido y Nombre" value="{{ $ticket->nombre }}" />
                                @if ($errors->has('nombre'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3 no_cliente" <?php if($ticket->es_cliente == "si"){ echo "style='display: none;'"; } ?>>
                                {{ Form::label('telefono', 'Teléfono de Contacto') }}
                                <input name="telefono" type="number" class="form-control" value="{{ $ticket->telefono }}" placeholder="10 digitos (Sin Cód. de Área)"/>
                            </div>
                            <div class="form-group col-md-4 no_cliente" <?php if($ticket->es_cliente == "si"){ echo "style='display: none;'"; } ?>>
                                {{ Form::label('email', 'Email') }}
                                <input name="email" type="email" class="form-control" value="{{ $ticket->email }}" placeholder="ejemplo@ejemplo.com" />
                            </div>
                            <script type="text/javascript">
                                
                            </script>
                            <div class="form-group col-md-3">
                                {{ Form::label('estado', 'Estado del Ticket') }}
                                <select name="estado" class="form-control estado">
                                    <option value="abierto">Abierto</option>
                                    <option value="cerrado">Cerrado</option>
                                    <option value="solucionado">Solucionado</option>
                                    <option value="enprogreso">En Progreso</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('categoria', 'Categoria') }}
                                <select name="categoria" class="form-control categoria" style="width: 100% !important;" >
                                    <option value="administrativo" >Administrativo</option>
                                    <option value="cambio_megas" >Cambio de Megas</option>
                                    <option value="consulta" >Consulta</option>
                                    <option value="mudanza">Mudanza</option>
                                    <option value="internet_lento">Internet Lento</option>
                                    <option value="otro_problema_tecnico">Otro Problema Técnico</option>
                                    <option value="problema_pago">Problema con el pago</option>
                                    <option value="sin_internet">Sin Internet</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('descripcion', 'Descripción del Problema / Consulta') }}
                                <textarea name="descripcion" class="form-control" placeholder="Ingrese la descripción del problema..." rows="5">{{ $ticket->consulta }}</textarea>
                                @if ($errors->has('descripcion'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('respuesta', 'Respuesta al Problema / Consulta') }}
                                <textarea name="respuesta" class="form-control" placeholder="Ingrese la respuesta al problema / consulta..." rows="5">{{ $ticket->respuesta }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;">
                            <button type="submit" class="btn btn-sm btn-primary">Guardar Cambios</button>
                        </div>
                        </form>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 center" style="text-align: center;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')


<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        
        $(".categoria").val(<?php echo "'".$ticket->categoria . "'"; ?>);
        $(".estado").val(<?php echo "'".$ticket->estado . "'"; ?>);
        $(".es_cliente").val(<?php echo "'".$ticket->es_cliente . "'"; ?>);
//        var $example = $(".select2_demo_3").select2();
         
        
        $(".es_cliente").change(function(){
            if($('.es_cliente option:selected').val() === "si"){
                $('.no_cliente').hide();
                $('.cliente').show();
            }else{
                $('.no_cliente').show();
                $('.cliente').hide();
            }

        });
        
        $('.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });
        
        
        var $example = $(".select2_demo_3").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Cliente...",
            allowClear: true
        });
        
        $example.val(<?php if($ticket->fk_cliente != NULL ){ echo $ticket->fk_cliente; } ?>).trigger("change");
        
        var data_table = $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });
        
        
        
    });
</script>
@stop
