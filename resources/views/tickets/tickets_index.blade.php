@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <!--<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">-->
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

    <!-- Data Tables -->
    <link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default m-b-lg">
                <div class="panel-heading">Ticket's</div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-sm btn-primary" href="/tickets/nuevo"><i class="fa fa-plus-square"></i> Nuevo Ticket</a>
                        </div>
                    </div>
                    <div class="row m-t-lg">
                        <form id="form" method="POST">
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                                {{ Form::label('fecha_desde', 'Fecha Desde') }}
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_desde" placeholder="dd/mm/yyyy" class="form-control date" value="{{ date('d/m/Y') }}">
                                </div>
                                @if ($errors->has('fecha_desde'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_desde') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_hasta" placeholder="dd/mm/yyyy" class="form-control date" value="{{ date('d/m/Y') }}">
                                </div>
                                @if ($errors->has('fecha_hasta'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_hasta') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('nro_ticket', 'Nro. Ticket') }}
                                {{ Form::number("nro_ticket", null , array("class"=>"form-control ", "placeholder"=>"0")) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('cliente', 'Cliente') }}
                                <select name="cliente" class="form-control select2_demo_3 " style="width: 100% !important;">
                                    <option value=""></option>
                                    @foreach($clientes as $cliente => $value)
                                    <option value="{{ $value->id }}">{{ $value->persona->nombre . " " . $value->persona->apellido }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('estado', 'Estado del Ticket') }}
                                <select name="estado" class="form-control estado_servicio " style="width: 100% !important;">
                                    <option></option>
                                    <option value="abierto">Abierto</option>
                                    <option value="cerrado">Cerrado</option>
                                    <option value="solucionado">Solucionado</option>
                                    <option value="enprogreso">En Progreso</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {{ Form::label('categoria', 'Categoria') }}
                                <select name="categoria" class="form-control categoria " style="width: 100% !important;">
                                    <option></option>
                                    <option value="administrativo">Administrativo</option>
                                    <option value="cambio_megas">Cambio de Megas</option>
                                    <option value="consulta">Consulta</option>
                                    <option value="mudanza">Mudanza</option>
                                    <option value="internet_lento">Internet Lento</option>
                                    <option value="otro_problema_tecnico">Otro Problema Técnico</option>
                                    <option value="problema_pago">Problema con el pago</option>
                                    <option value="sin_internet">Sin Internet</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="checkbox-inline">
                                    <input class="checkbox" type="checkbox" name="es_cliente" value="no"/> <strong>No es Cliente</strong>
                                </label>
                            </div>
                            <div class="form-group col-md-3 contacto" style="display: none;">
                                {{ Form::label('contacto', 'Teléfono / Nombre / Email') }}
                                {{ Form::text("contacto", null , array("class"=>"form-control ", "placeholder"=>"Datos del contacto...")) }}
                            </div>

                        </div>
                        </form>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-primary" id="buscar-tickets"><i class="fa fa-search"></i> Buscar</a>
                            </div>
                        </div>


                    </div>
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <div class="col-md-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Nro. Ticket</th>
                                        <th>Fecha</th>
                                        <th>Cliente</th>
                                        <th>Estado</th>
                                        <th>Categoria</th>
                                        <th>Consulta</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody id="body_tickets">

                                </tbody>
                            </table>
<!--                            <div style="text-align: center; " class="m-b-md">
                                <div><strong>Total Debe: $ </strong> <label class="total-debe">0</label> - <strong>Total Haber: $ </strong> <label class="total-haber">0</label></div>
                            </div>-->
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<!--<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>-->

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.checkbox').change(function(){
            if($(this).is(':checked'))
                $('.contacto').show();
            else
                $('.contacto').hide();
        });

        $('.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $(".select2_demo_3").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Cliente...",
            allowClear: true
        });

        $(".estado_servicio").select2({
            theme: "bootstrap",
            placeholder: "Estado del Servicio...",
            allowClear: true
        });

        $(".categoria").select2({
            theme: "bootstrap",
            placeholder: "Categoria...",
            allowClear: true
        });


        var data_table = $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });

        $("#buscar-tickets").click(function(){
            var datos = $("#form").serialize();
            var token = $("meta[name='csrf-token']").attr('content');
            $('.dataTables-example').DataTable().rows().remove().draw();
            $.ajax({
                type: "POST",
                url: "/tickets/buscar",
                data: "_token="+token+"&"+datos,
                success: function(data){
                    $.each(data,function(key, value){

                        $('.dataTables-example').DataTable().row.add( {
                            0:      value.id,
                            1:      value.fecha,
                            2:      value.fk_cliente,
                            3:      value.estado,
                            4:      value.categoria,
                            5:      value.consulta,
                            6:      "<a class='btn btn-success btn-xs editar_ticket' href='/tickets/"+value.id+"/editar' title='Editar Ticket'><i class='fa fa-edit'></i></a>"
                        }).draw();

                    });
                },
                error: function(data){
                    console.log(data);
                }
            });
        });

//        $("#body_tickets").on('click','a.editar_ticket', function(){
//            alert("asd");
//        });

    });
</script>
@stop
