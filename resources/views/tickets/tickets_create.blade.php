@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <!--<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">-->
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

    <!-- Data Tables -->
    <link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default m-b-lg">
                <div class="panel-heading">Nuevo Ticket</div>

                <div class="panel-body">
                    <div class="row m-t-lg">
                        <form action="/tickets/guardar" method="POST">
                            {{ csrf_field() }}
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                                {{ Form::label('es_cliente', 'Es Cliente?') }}
                                <select name="es_cliente" class="form-control es_cliente" style="width: 100% !important;">
                                    <option value="si" selected="">Si</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 cliente">
                                {{ Form::label('cliente', 'Cliente') }}
                                <select name="cliente" id="sel-cliente" class="form-control select2_demo_3 cliente_select " style="width: 100% !important;">
                                    <option></option>
                                    @if (isset($fromReference))
                                      <option value="{{ $clientes->id }}" selected> {{ $clientes->persona->nombre . ' ' . $clientes->persona->apellido . ' - ' . $clientes->persona->cuit }}</option>
                                    @else
                                      @foreach($clientes as $cliente => $value)
                                          <option value="{{ $value->id }}"> {{ $value->persona->nombre . ' ' . $value->persona->apellido . ' - ' . $value->persona->cuit }}</option>
                                      @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('cliente'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('cliente') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3 cliente">
                                {{ Form::label('servicioDelClienteLabel', 'Servicio del Cliente') }}
                                <select name="sel-serv-cliente" id="sel-serv-cliente" class="form-control select2_demo_3 cliente_select " style="width: 100% !important;">
                                    <option value="default" selected>Irrelevante</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 no_cliente" style="display: none;">
                                {{ Form::label('nombre', 'Apellido y Nombre') }}
                                <input name="nombre" type="text" class="form-control" placeholder="Apellido y Nombre" />
                                @if ($errors->has('nombre'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-3 no_cliente" style="display: none;">
                                {{ Form::label('telefono', 'Teléfono de Contacto') }}
                                <input name="telefono" type="number" class="form-control" placeholder="10 digitos (Sin Cód. de Área)"/>
                            </div>
                            <div class="form-group col-md-4 no_cliente" style="display: none;">
                                {{ Form::label('email', 'Email') }}
                                <input name="email" type="email" class="form-control" placeholder="ejemplo@ejemplo.com" />
                            </div>
                            <script type="text/javascript">

                            </script>
                            <div class="form-group col-md-3">
                                {{ Form::label('estado', 'Estado del Ticket') }}
                                <select name="estado" class="form-control ">
                                    <option value="abierto">Abierto</option>
                                    <option value="cerrado">Cerrado</option>
                                    <option value="solucionado">Solucionado</option>
                                    <option value="enprogreso">En Progreso</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('categoria', 'Categoria') }}
                                <select name="categoria" class="form-control " style="width: 100% !important;">
                                    <option value="administrativo">Administrativo</option>
                                    <option value="cambio_megas">Cambio de Megas</option>
                                    <option value="consulta">Consulta</option>
                                    <option value="mudanza">Mudanza</option>
                                    <option value="internet_lento">Internet Lento</option>
                                    <option value="otro_problema_tecnico">Otro Problema Técnico</option>
                                    <option value="problema_pago">Problema con el pago</option>
                                    <option value="sin_internet">Sin Internet</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('descripcion', 'Descripción del Problema / Consulta') }}
                                <textarea name="descripcion" class="form-control" placeholder="Ingrese la descripción del problema..." rows="5"></textarea>
                                @if ($errors->has('descripcion'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('respuesta', 'Respuesta al Problema / Consulta') }}
                                <textarea name="respuesta" class="form-control" placeholder="Ingrese la respuesta al problema / consulta..." rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;">
                            <button type="submit" class="btn btn-sm btn-primary">Crear Ticket</button>
                        </div>
                        </form>

                    </div>

                    <div class="row">
                        <div class="col-md-12 center" style="text-align: center;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')


<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $(".es_cliente").change(function(){
            if($('.es_cliente option:selected').val() === "si"){
                $('.no_cliente').hide();
                $('.cliente').show();
            }else{
                $('.no_cliente').show();
                $('.cliente').hide();
            }

        });

        $('.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });


        $("#sel-cliente").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Cliente...",
            allowClear: true
        });

        $('#sel-cliente').change(function (){
          var idCliente = parseInt($('#sel-cliente').val());
          readServiciosFromCliente(idCliente);
        });

        $("#sel-serv-cliente").select2({
            theme: "bootstrap",
            placeholder: "Seleccione el Servicio...",
            allowClear: true
        });

        var data_table = $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });
        @if(isset($fromReference))
            readServiciosFromCliente({{$clientes->id}});
        @endif

    });


    //Esta funcion es para que al momento de seleccionar un cliente se carguen sus servicios en el select servicios
    function readServiciosFromCliente(idCliente){
        var id = idCliente;
        $.ajax({
          method: "POST",
          url: "/ventas/clientes/get-servicios",
          data: {
            "_token": "{{ csrf_token() }}",
            "idCliente" : id,
          },
          dataType: "json"
        })
          .done(function( response ) {
            for (var i = 0; i < Object.keys(response.cliente.getservicios).length; i++) {
              $('#sel-serv-cliente')
                  .append($("<option></option>")
                             .attr("value",response.cliente.getservicios[i].id)
                             .text(response.servicios_fixed[i].concepto + " " + response.servicios_fixed[i].sub_concepto + " ( $" + response.servicios_fixed[i].costo + " )"));
            }
        }).fail(function( response ) {
            alert('Ocurrio un error al cargar los servicios del cliente.');
        });
        $('#sel-serv-cliente').select2('refresh')
    };
</script>
@stop
