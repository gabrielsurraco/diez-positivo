@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
<style>
@page {
  size: A4 landscape;
}

@media print {
        #watermark {
			      display: block;
            position: fixed;
            top: 50%;
            left: 45%;
            z-index: -10;
			      opacity:0.15;
            -webkit-transform: rotate(-10deg); /* Chrome, Safari, Opera */
            transform: rotate(-10deg);
        }
        div {
          -webkit-print-color-adjust: exact;
        }
        .tr{
          height: 20px;
        }
        img {
            -webkit-print-color-adjust:exact;
        }
        .imgAnulado{
            position:absolute;
            left:160px;
            top:230px;
            z-index:-3;
            width: 250px;
        }
        .imgAnulado2{
            position:absolute;
            left:680px;
            top:230px;
            z-index:-3;
            width: 250px;
        }
}
</style>
@endsection


@section('content')
  <div class="hidden-print">
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right"><i class="fa fa-print"></i></span>
                    <h5>Generando Impresión</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">
                      <div class="col-sm-12 text-center">
                        <i class="fa fa-refresh fa-4x fa-spin"></i>
                          <p>Aguarde Por Favor...</p>
                      </div>
                  </div>

                </div>
            </div>
        </div>
      </div>
  </div>
      <div class="visible-print-block">
          @if($recibo->anulado == 1)
          <img src="{{ asset('/img/anulado.png') }}"  class="imgAnulado" alt="">
          <img src="{{ asset('/img/anulado.png') }}"  class="imgAnulado2" alt="">
          @endif
          <div class="row">
            <div class="col-xs-6" style="margin:0px; padding:5px;">
                <table border="1" width="100%">
                  <tr>
                    <td width="50%" align="right"><div style="font-size:34px; color:white!important; background-color:black!important; width:15%!important; text-align:center!important; font-weight:bold!important;">X</div></td>
                    <td width="50%">
                      <table width="100%">
                      <tr>
                        <td style="font-size:9px; text-align:right; padding-right:2%;">DOCUMENTO NO VALIDO COMO FACTURA</td>
                      </tr>
                      <tr>
                        <td style="font-size:14px; text-align:right;  font-weight:bold; padding-right:2%;"> ORIGINAL - CLIENTE</td>
                      </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td><table width="100%" cellspacing="5" cellpadding="5">
                    <tr>
                      <td style="text-align:center; padding:3px;"><img src="{{ asset('/img/internet_obera_membrete.jpg') }}" class="img-responsive" alt="Responsive image"></td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Razón Social:</strong>  Damian Francisco Ostrorog </td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Domicilio Comercial:</strong>  Corrientes 93, Oficina 104, Primer Piso Oberá Misiones. </td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CUIT:</strong>  20-28059849-7 </td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"> IVA Responsable Inscripto </td>
                    </tr>
                    </table></td>
                    <td style="vertical-align:bottom;"><table width="100%" cellspacing="5" cellpadding="5">
                    <tr>
                      <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>RECIBO N°:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"> <?php echo str_pad(1, 4, '0', STR_PAD_LEFT).'-'.str_pad($recibo->id, 8, '0', STR_PAD_LEFT); ?></td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>COBRÓ:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;">{{Auth::user()->name}}</td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>FECHA:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;">{{$recibo->fecha}}</td>
                    </tr>
                    </table></td>
                  </tr>
                  </table>
                  <table border="1" width="100%">
                  <tr class="tr">
                    <td colspan="2" style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CLIENTE:</strong> {{$cliente->codigo}} - {{$cliente->persona->apellido}} {{$cliente->persona->nombre}} {{$cliente->persona->razon_social}} </td>
                  </tr>

                  <tr class="tr">
                    <td colspan="2" style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CONCEPTO:</strong> </td>
                  </tr>
                  <?php
                  $count = 1;
                  ?>

                  @foreach($recibo->detalleRecibos as $value)
                      <?php
                      if($count > 13){
                         break;
                      }
                      ?>
                      <tr class="tr">
                        <td style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">@if($value->importe == 0.00) ---- @endif {{substr($value->concepto, 0, 80)}} </td><td style="font-size:12px; text-align:right; padding-right:2%; padding-left:2%;">@if($value->importe != 0.00) $ {{$value->importe}} @endif</td>
                      </tr>
                      <?php
                      $count++;
                      ?>
                  @endforeach

                  <?php
                  for ($i=$count; $i < 13; $i++) {
                    echo '<tr class="tr"><td></td><td></td></tr>';
                  }
                  ?>

                  <tr class="tr">
                    <td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">TOTAL: </td><td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">$ {{$recibo->importe_total}} </td>
                  </tr>

                  <tr class="tr">
                    <td style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;">FIRMA DE COBRADOR</td><td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">SALDO: $ {{$recibo->saldo_cliente}}</td>
                  </tr>

                  <tr>
                    <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>VER CONDICIONES GENERALES</strong> publicadas en <u>www.diezpositivo.com.ar</u> // Precios y Planes / CONDICIONES GENERALES. Desde el momento del pago tiene 30 días corridos para manifestarse al respecto. Vea cuadro de devoluciones por bajas. </td>
                  </tr>

                  <tr>
                    <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>MEDIO DE PAGO:</strong> Transferencia o Depósito.</br> Banco MACRO Caja de Ahorro Pesos 4-002-0948637775-8 </br>
CBU: 285000274-009486377758 / Damian Francisco Ostrorog. </br>
CUIT: 20-28059849-7 // <strong>TARJETA DE CRÉDITO:</strong>  por Mercado Libre // Oficina Oberá, calle corrientes 93, of. 104 // Rapipago Aristóbulo Av. Américas 770 // Rapipago Dos de Mayo calle 25 de Mayo // Municipalidad Corpus. Al momento del pago tiene 30 días corridos para manifestarse al respecto. Vea cuadro de devoluciones por bajas. </td>
                  </tr>

                  <tr>
                    <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>ADMINISTRACIÓN:</strong> +54 3755 651058 // <strong>COMERCIAL:</strong> +54 3755 444342 // <strong>OFICINA:</strong> +54 3755 428118/422248 // <strong>SER. TÉCNICO:</strong> +54 3755 541999 </td>
                  </tr>
                </table>
              </div>
              <div class="col-xs-6" style="margin-left:0px; padding:5px;">
                  <table border="1" width="100%">
                    <tr>
                      <td width="50%" align="right"><div style="font-size:34px; color:white!important; background-color:black!important; width:15%!important; text-align:center!important; font-weight:bold!important;">X</div></td>
                      <td width="50%">
                        <table width="100%">
                        <tr>
                          <td style="font-size:9px; text-align:right; padding-right:2%;">DOCUMENTO NO VALIDO COMO FACTURA</td>
                        </tr>
                        <tr>
                          <td style="font-size:14px; text-align:right;  font-weight:bold; padding-right:2%;"> DUPLICADO - COBRADOR</td>
                        </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td><table width="100%" cellspacing="5" cellpadding="5">
                      <tr>
                        <td style="text-align:center; padding:3px;"><img src="{{ asset('/img/internet_obera_membrete.jpg') }}" class="img-responsive" alt="Responsive image"></td>
                      </tr>
                      <tr>
                        <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Razón Social:</strong>  Damian Francisco Ostrorog </td>
                      </tr>
                      <tr>
                        <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Domicilio Comercial:</strong>  Corrientes 93, Oficina 104, Primer Piso Oberá Misiones. </td>
                      </tr>
                      <tr>
                        <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CUIT:</strong>  20-28059849-7 </td>
                      </tr>
                      <tr>
                        <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"> IVA Responsable Inscripto </td>
                      </tr>
                      </table></td>
                      <td style="vertical-align:bottom;"><table width="100%" cellspacing="5" cellpadding="5">
                      <tr>
                        <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>RECIBO N°:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"> <?php echo str_pad(1, 4, '0', STR_PAD_LEFT).'-'.str_pad($recibo->id, 8, '0', STR_PAD_LEFT); ?></td>
                      </tr>
                      <tr>
                        <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>COBRÓ:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;">{{Auth::user()->name}}</td>
                      </tr>
                      <tr>
                        <td width="30%" style="font-size:9pt; text-align:right; padding-right:2%; padding-left:2%;"><strong>FECHA:</strong></td><td width="70%" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;">{{$recibo->fecha}}</td>
                      </tr>
                      </table></td>
                    </tr>
                    </table>
                    <table border="1" width="100%">
                    <tr class="tr">
                      <td colspan="2" style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CLIENTE:</strong> {{$cliente->codigo}} - {{$cliente->persona->apellido}} {{$cliente->persona->nombre}} </td>
                    </tr>
                    <tr class="tr">
                      <td colspan="2" style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CONCEPTO:</strong> </td>
                    </tr>
                    <?php
                    $count = 1;
                    ?>

                    @foreach($recibo->detalleRecibos as $value)
                        <?php
                        if($count > 13){
                           break;
                        }
                        ?>
                        <tr class="tr">
                          <td style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">@if($value->importe == 0.00) ---- @endif {{substr($value->concepto, 0, 80)}} </td><td style="font-size:12px; text-align:right; padding-right:2%; padding-left:2%;">@if($value->importe != 0.00) $ {{$value->importe}} @endif</td>
                        </tr>
                        <?php
                        $count++;
                        ?>
                    @endforeach

                    <?php
                    for ($i=$count; $i < 13; $i++) {
                      echo '<tr class="tr"><td></td><td></td></tr>';
                    }
                    ?>

                    <tr class="tr">
                      <td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">TOTAL: </td><td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">$ {{$recibo->importe_total}} </td>
                    </tr>

                    <tr class="tr">
                      <td style="font-size:11pt; text-align:left; padding-right:2%; padding-left:2%;">FIRMA DE COBRADOR</td><td style="font-size:11pt; text-align:right; padding-right:2%; padding-left:2%;">SALDO: $ {{$recibo->saldo_cliente}}</td>
                    </tr>

                    <tr>
                      <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>VER CONDICIONES GENERALES</strong> publicadas en <u>www.diezpositivo.com.ar</u> // Precios y Planes / CONDICIONES GENERALES. Desde el momento del pago tiene 30 días corridos para manifestarse al respecto. Vea cuadro de devoluciones por bajas. </td>
                    </tr>

                    <tr>
                      <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>MEDIO DE PAGO:</strong> Transferencia o Depósito.</br> Banco MACRO Caja de Ahorro Pesos 4-002-0948637775-8 </br>
  CBU: 285000274-009486377758 / Damian Francisco Ostrorog. </br>
  CUIT: 20-28059849-7 // <strong>TARJETA DE CRÉDITO:</strong>  por Mercado Libre // Oficina Oberá, calle corrientes 93, of. 104 // Rapipago Aristóbulo Av. Américas 770 // Rapipago Dos de Mayo calle 25 de Mayo // Municipalidad Corpus. Al momento del pago tiene 30 días corridos para manifestarse al respecto. Vea cuadro de devoluciones por bajas. </td>
                    </tr>

                    <tr>
                      <td colspan="2" style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>ADMINISTRACIÓN:</strong> +54 3755 651058 // <strong>COMERCIAL:</strong> +54 3755 444342 // <strong>OFICINA:</strong> +54 3755 428118/422248 // <strong>SER. TÉCNICO:</strong> +54 3755 541999 </td>
                    </tr>
                  </table>
                </div>
          </div>
          <!-- Datos Cliente -->
       </div>
@endsection

@section('javascript')

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      window.print();
  });

  var beforePrint = function () {
      //alert('Functionality to run before printing.');
      $('#page-wrapper').css('min-height', '0px');
  };

  var afterPrint = function () {
      //alert('Functionality to run after printing');
      window.close();
      $('#page-wrapper').css('min-height', '800px');
  };

  if (window.matchMedia) {
      var mediaQueryList = window.matchMedia('print');

      mediaQueryList.addListener(function (mql) {
          //alert($(mediaQueryList).html());
          if (mql.matches) {
              beforePrint();
          } else {
              afterPrint();
          }
      });
  }

  window.onbeforeprint = beforePrint;
  window.onafterprint = afterPrint;
</script>

@stop
