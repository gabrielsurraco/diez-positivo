@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css" rel="stylesheet') }}">
<link href="{{ asset('/backend/css/style.css" rel="stylesheet') }}">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
              <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Listado de Recibos </h5>
                          <div class="ibox-tools">
                              <a class="collapse-link">
                                  <i class="fa fa-chevron-up"></i>
                              </a>
                              <a class="close-link">
                                  <i class="fa fa-times"></i>
                              </a>
                          </div>
                      </div>
                      <div class="ibox-content">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables" id="table-resultados">
                                    <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th>Fecha</th>
                                        <th>Cliente</th>
                                        <th>Importe</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($recibos as $valor)
                                        <tr>
                                            <td align="center"><?php echo str_pad($valor->id, 8, "0", STR_PAD_LEFT); ?></td>
                                            <td align="center">{{ $valor->fecha }}</td>
                                            <td>{{ $valor->cuenta->nombre }}</td>
                                            <td align="center">$ {{ $valor->importe_total }}</td>
                                            <td align="center">
                                              @if($valor->anulado == 1)
                                                <a href="/recibos/print/{{ $valor->id }}" target="_blank" type="button" class="btn btn-w-m btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="MOTIVO: {{$valor->motivo}}">ANULADO</a>
                                              @else
                                              <a href="/recibos/print/{{ $valor->id }}" target="_blank" class="btn btn-success btn-xs btn-bitbucket" data-toggle="tooltip" data-placement="top" title="Ver Recibo"><i class="fa fa-eye"></i></a>
                                              <a onclick="anularRecibo({{ $valor->id }})" class="btn btn-danger btn-xs btn-bitbucket" data-toggle="tooltip" data-placement="top" title="Anular Recibo"><i class="fa fa-trash"></i></a>
                                              @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
</div>
<!-- Fin Contenido Principal -->

<!-- Inicio de Modal -->
  <div class="modal inmodal fade" id="modal-anularRecibo" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title">Anular Recibo</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="">Motivo de Anulacion del Recibo:</label>
                      <textarea name="area-motivo-anular" class="form-control text-uppercase" rows="6" cols="80"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                  <button type="button" class="btn btn-danger" id="btn-anular-recibo">Anular</button>
              </div>
          </div>
      </div>
  </div>
<!-- Fin Modal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>


<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.select2').select2({
          theme:'bootstrap',
          placeholder: ""
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
        /*serverSide: true,
        ajax: {
            url: '/ventas/clientes/paginacion',
            type: 'POST',
            data: {
              "_token": "{{ csrf_token() }}",
            }
        },
        "columns": [
            { "data": "codigo" },
            { "data": "cliente" },
            { "data": "cuit" },
            { "data": "email" },
            { "data": "telefono" },
            { "data": "domicilio" },
            { "data": "estado" },
            { "data": "accion" }
        ]*/
      });
  });

  function anularRecibo(id){
      $('#modal-anularRecibo').modal('show');
      $('#btn-anular-recibo').on('click', function(event) {
        event.preventDefault();
        if($('[name="area-motivo-anular"]').val() == ''){
          swal("Atencion!", "Debe declarar un motivo de la anulacion del recibo.", "warning");
          return
        }
        $.ajax({
          url: '/recibos/anularRecibo',
          type: 'POST',
          dataType: 'html',
          data: { "_token": "{{ csrf_token() }}",
                  idRecibo: id,
                  motivo  : $('[name="area-motivo-anular"]').val().toUpperCase(),
                }
        })
        .done(function(msg) {
          $('#modal-anularRecibo').modal('hide');
          swal("Listo!", "Recibo anulado correctamente.", "success").then((result) => {
            location.reload();
          });

          console.log(msg);
        })
        .fail(function() {
          swal("Ups!", "Algo no esta bien por favor depure....", "error");
          console.log("error");
        });
      });
  }

</script>

@stop
