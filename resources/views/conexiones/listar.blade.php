@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Torres - Emisoras - Antenas </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                    <div class="col-lg-offset-3 col-lg-3">
                      <div class="form-group">
                          <label>Departamento de la Torre:</label>
                          <select id="departamentos" name="select-depto" class="form-control">
                              @foreach ($array_departamentos as $key => $value)
                                  <option value="{{$key}}">{{$value}}</option>
                              @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                          <label>Localidad de la Torre:</label>
                          <select id="localidades" class="form-control">

                          </select>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <button type="button" style="margin-top:23px;" onclick="buscarOrdenesCliente()" class="btn btn-primary">
                        <span class="fa fa-search"></span>
                      </button>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-lg-6">
                        <div class="dd dd-nodrag" id="nestable2">
                          <li class="dd-item dd-nodrag" data-id="1" id="listado-torres">
                              <!--<div class="dd-handle">
                                  <span class="label label-primary pull-right">ACTIVO</span>
                                  <span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset('/img/icons/server.png') }}" alt="">  HQ - Casa Central Oficina.
                              </div>-->
                              <!--
                              <ol class="dd-list">

                                <li class="dd-item" data-id="1">
                                    <div class="dd-handle">
                                        <span class="label label-primary pull-right">ACTIVO</span>
                                        <span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset('/img/icons/tower.png') }}" alt="">  TORRE - Cras ornare tristique.
                                    </div>
                                </li>
                                <li class="dd-item" data-id="1">
                                    <div class="dd-handle">
                                        <span class="label label-primary pull-right">ACTIVO</span>
                                        <span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset('/img/icons/tower.png') }}" alt="">  TORRE - Cras ornare tristique.
                                    </div>
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="2">
                                            <div class="dd-handle">
                                                <span class="pull-right"> 12:00 pm </span>
                                                <span class="label label-info"></span> <img width="20" src="{{ asset('/img/icons/emisor.png') }}" alt="">  EMISORA -  Vivamus vestibulum nulla nec ante.
                                            </div>
                                            <ol class="dd-list">
                                              <li class="dd-item" data-id="2">
                                                  <div class="dd-handle">
                                                      <span class="pull-right"> 12:00 pm </span>
                                                      <span class="label label-info"></span> <img width="20" src="{{ asset('/img/icons/emisor.png') }}" alt="">  EMISORA -  Vivamus vestibulum nulla nec ante.
                                                  </div>
                                                    <li class="dd-item" data-id="2">
                                                        <div class="dd-handle">
                                                            <span class="pull-right"> 12:00 pm </span>
                                                            <span class="label label-info"></span> <img width="20" src="{{ asset('/img/icons/emisor.png') }}" alt="">  EMISORA -  Vivamus vestibulum nulla nec ante.
                                                        </div>
                                                    </li>
                                                    <ol class="dd-list">
                                                      <li class="dd-item" data-id="2">
                                                          <div class="dd-handle">
                                                              <span class="pull-right"> 12:00 pm </span>
                                                              <span class="label label-info"></span> <img width="20" src="{{ asset('/img/icons/antena.png') }}" alt="">  ANTENA -  Vivamus vestibulum nulla nec ante.
                                                          </div>
                                                          <ol class="dd-list">
                                                            <li class="dd-item" data-id="2">
                                                                <div class="dd-handle">
                                                                    <span class="pull-right"> 12:00 pm </span>
                                                                    <span class="label label-info"></span> <img width="20" src="{{ asset('/img/icons/router.png') }}" alt="">  ROUTER -  Vivamus vestibulum nulla nec ante.
                                                                </div>
                                                            </li>
                                                          </ol>
                                                      </li>
                                                    </ol>
                                              </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>

                                </ol>-->
                                </li>


                            </ol>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="google-map" id="map3"></div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
          <div class="col-lg-12">
              <div class="ibox collapsed">
                  <div class="ibox-title">
                      <h5>Informacion de la Torre</h5>
                      <div class="ibox-tools">
                          <a class="collapse-link" id="info-torre">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                          <a class="close-link">
                              <i class="fa fa-times"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                    <div class="row" id="loading-content-torre">
                      <div class="col-lg-12">
                        <div class="spiner-example">
                            <div class="sk-spinner sk-spinner-three-bounce">
                                <div class="sk-bounce1"></div>
                                <div class="sk-bounce2"></div>
                                <div class="sk-bounce3"></div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div id="content-torre" class="hidden">
                        <div class="row">
                          <div class="col-lg-1">
                            <img alt="image" class="img-thumbnail" src="{{ asset("/img/icons/tower-gigant.jpg") }}" width="80">
                          </div>
                          <div class="col-lg-5">
                            <table>
                              <tr>
                                <td align="right"><h4>NOMBRE DE TORRE: </h4> </td>
                                <td style="padding-left:10px;"><h5 id="torre-nombre"></h5></td>
                              </tr>
                              <tr>
                                <td align="right"><h4>CANT. EQUIPOS: </h4></td>
                                <td style="padding-left:10px;"><h5 id="torre-cant-equipos"></h5></td>
                              </tr>
                            </table>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <h4 class="text-center">Equipos en Torre</h4>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tipo</th>
                                    <th>Nombre</th>
                                    <th>Dispositivo</th>
                                    <th>Depende De</th>
                                </tr>
                                </thead>
                                <tbody id="table-torre-equipos">

                                </tbody>
                            </table>
                          </div>

                        </div>
                    </div>
              </div>
          </div>
      </div>
</div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Nestable List -->
<script src="{{ asset('/backend/js/plugins/nestable/jquery.nestable.js') }}"></script>
<!--
 You need to include this script on any page that has a Google Map.
 When using Google Maps on your own site you MUST signup for your own API key at:
 https://developers.google.com/maps/documentation/javascript/tutorial#api_key
 After your sign up replace the key in the URL below..
-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

<!-- Page-Level Scripts -->
<script>
  var map3;
  $(document).ready(function() {
      $('.select2').select2({
          theme:'bootstrap',
          placeholder: ""
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });


      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
      });

      //pasamos las variables con datos a json_encode
      var jsonCode = '<?php echo json_encode($torres); ?>';
      var torresObj = JSON.parse(jsonCode);
      torresObj.forEach(function(el) {
        if(el.HQ == 1){
          $('#listado-torres').append('<div class="dd-handle dd-nodrag">'+
              '<span class="label label-primary pull-right"></span>'+
              '<span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset('/img/icons/server.png') }}" alt="">  HQ - '+ el.nombre+
          '</div>');
        }else{
          $('#listado-torres').append('<ol class="dd-list">'+
              '<li class="dd-item dd-nodrag" data-id-torre="'+el.id+'">'+
              '<div class="dd-handle">'+
                  '<button type="button" onClick="leerDatosTorre('+el.id+',null)" class="btn btn-w-m btn-info pull-right btn-xs" data-toggle="tooltip" data-placement="top" title="Detalle de Torre">VER DETALLE</button>'+
                  '<span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset("/img/icons/tower.png") }}" alt="">  TORRE - '+ el.nombre +
              '</div>'+
          '</li></ol>');
          if(el.equipos.length > 0){
            el.equipos.forEach(function(element){
              $('[data-id-torre="'+element.fk_torre+'"]').append('<ol class="dd-list">'+
                  '<li class="dd-item dd-nodrag" data-id-equipo="'+element.id+'">'+
                  '<div class="dd-handle">'+
                      '<span class="label label-primary pull-right"></span>'+
                      '<span class="label label-info" style="width:30px;"></span> <img width="20" src="{{ asset("/img/icons/antena.png") }}" alt="">  ENLACE - '+ element.nombre +
                  '</div>'+
              '</li></ol>');
            });
          }
        }
      });//fin foreach
      console.log(torresObj);


      // activate Nestable for list 2
      $('#nestable2').nestable({
          maxDepth: 0,
          noDragClass:'dd-nodrag'
      });


      // output initial serialised data
      //updateOutput($('#nestable').data('output', $('#nestable-output')));
      //updateOutput($('#nestable2').data('output', $('#nestable2-output')));

      $('#nestable-menu').on('click', function (e) {
          var target = $(e.target),
                  action = target.data('action');
          if (action === 'expand-all') {
              $('.dd').nestable('expandAll');
          }
          if (action === 'collapse-all') {
              $('.dd').nestable('collapseAll');
          }
      });
      $('#departamentos').val(13);
      $('#departamentos').trigger("change");

      $('[data-toggle="tooltip"]').tooltip();
      //




      ///Todo lo que es google Maps
      // When the window has finished loading google map
      google.maps.event.addDomListener(window, 'load', init);

      function init() {
          // Options for Google map
          // More info see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions

          var mapOptions3 = {
              center: new google.maps.LatLng(-27.485811, -55.119644),
              zoom: 16,
              mapTypeId: google.maps.MapTypeId.ROAD,
          };

          // Get all html elements for map
          var mapElement3 = document.getElementById('map3');

          // Create the Google Map using elements
          map3 = new google.maps.Map(mapElement3, mapOptions3);
      }
  });

  $('#departamentos').on('change',function(){
      if(this.value !== 0){
          var valor = this.value;
          $.ajax({
             type: "GET",
             url: "/localidades/get",
             data: "depto="+valor,
             success: function(data)
             {
                 $('#localidad').not(":nth-child(1)").empty();
                  var a = JSON.parse(data);
                  $('#localidades').html('');
                  $.each(a, function(key, value) {
                      $('#localidades')
                          .append($("<option></option>")
                                     .attr("value",value.id)
                                     .text(value.nombre));
                 });
             },
             error: function(msg){
                 alert(msg);
             }

          });
      }
  });

function leerDatosTorre(idTorre,idEnlace){
    $('#loading-content-torre').fadeIn('fast');
    if($('#content-torre').hasClass('hidden')){
      $("#info-torre").trigger( "click" );
    }else{
      $('#content-torre').fadeOut('fast');
    }

    $.ajax({
      method: "POST",
      url: "/conexiones/getDatosFromTorre",
      data: {
              "_token": "{{ csrf_token() }}",
              "idTorre": idTorre,
            },
      dataType: 'json'
    }).done(function( msg ) {
        console.log('datos torres:');
        console.log(msg);
        //actualizar posicion en google Maps
        if(msg.latitud != null & msg.longitud != null){
            myLatlng = new google.maps.LatLng(msg.latitud, msg.longitud);
            map3.setCenter(myLatlng);
        }
        //fin actualizacion google maps

        $('#loading-content-torre').fadeOut('fast');
        $('#content-torre').removeClass('hidden');
        $('#content-torre').fadeIn('fast');

        $('#torre-nombre').html(msg.nombre);
        $('#torre-cant-equipos').html(msg.equipos.length);
        $('#table-torre-equipos').html('');
        if(msg.equipos == null){
          $('#table-torre-equipos').html('<div class="alert alert-info">Sin Equipos en Torre</div>');
        }
        msg.equipos.forEach(function(element,index){
          if(idEnlace != null && idEnlace == element.id){
            if(element.depende_de == null){
              $('#table-torre-equipos').append('<tr><td>'+ (index + 1) +'</td><td>'+ element.tipo +'</td><td>'+ element.nombre +'</td><td>'+ element.articulo.marca +' - '+ element.articulo.modelo +'</td><td align="center"><span class="label label-warning label-xs">INTERNET</span></td></tr>');
            }else{
              $('#table-torre-equipos').append('<tr class="text-success"><td>'+ (index + 1) +'</td><td>'+ element.tipo +'</td><td>'+ element.nombre +'</td><td>'+ element.articulo.marca +' - '+ element.articulo.modelo +'</td><td align="center"><button onClick="leerDatosTorre('+element.depende_de.torre.id+','+ element.depende_de.equipo.id +')" type="button" class="btn btn-outline btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="'+element.depende_de.torre.nombre+' > '+ element.depende_de.equipo.nombre + '">Ver Dependencia</button></td></tr>');
            }
          }else{
              $('#table-torre-equipos').append('<tr><td>'+ (index + 1) +'</td><td>'+ element.tipo +'</td><td>'+ element.nombre +'</td><td>'+ element.articulo.marca +' - '+ element.articulo.modelo +'</td><td align="center"><button onClick="leerDatosTorre('+element.depende_de.torre.id+','+ element.depende_de.equipo.id +')" type="button" class="btn btn-outline btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="'+element.depende_de.torre.nombre+' > '+ element.depende_de.equipo.nombre + '">Ver Dependencia</button></td></tr>');
          }
        });
        $('[data-toggle="tooltip"]').tooltip();
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
}
</script>

@stop
