@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <form class="form-group" action="/conexiones/guardarEquipo" method="post">
  <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agregar Equipo </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Nombre del Equipo:</label>
                        <input type="text" class="form-control" name="inp-nombre-equipo" required>
                        @if ($errors->has('inp-nombre-equipo'))
                        <p class="help-block text-danger">{{ $errors->first('inp-nombre-equipo') }}</p>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                          <label>Articulo:</label>
                          <select class="select2_demo_1 form-control" name="sel-articulo" height="100%">
                              <option></option>
                               @foreach ($articulos as $value)
                                  <option value="{{$value->id}}">{{$value->marca}} {{$value->modelo}} @if($value->frecuencia != '')({{$value->frecuencia}})@endif</option>
                               @endforeach
                           </select>
                           @if ($errors->has('sel-articulo'))
                           <p class="help-block text-danger">{{ $errors->first('sel-articulo') }}</p>
                           @endif
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Tipo:</label>
                        <select class="form-control" name="sel-tipo-equipo">
                          <option value="ENLACE">ENLACE</option>
                          <option value="EMISORA">EMISORA</option>
                          <option value="SWITCH">SWITCH</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Serie del Equipo:</label>
                        <input type="text" class="form-control" name="inp-serie-equipo" required>
                        @if ($errors->has('inp-serie-equipo'))
                        <p class="help-block text-danger">{{ $errors->first('inp-serie-equipo') }}</p>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Pertenece a Torre:</label>
                        <select class="select2_demo_1 form-control" name="sel-torre-pertenece" height="100%">
                             <option></option>
                             @foreach ($torres as $value)
                                <option value="{{$value->id}}">{{$value->nombre}}</option>
                             @endforeach
                         </select>
                         @if ($errors->has('sel-torre-pertenece'))
                         <p class="help-block text-danger">{{ $errors->first('sel-torre-pertenece') }}</p>
                         @endif
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">IP del Equipo:</label>
                        <input type="text" class="form-control" name="inp-ip-equipo" required>
                        @if ($errors->has('inp-ip-equipo'))
                        <p class="help-block text-danger">{{ $errors->first('inp-ip-equipo') }}</p>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="col-lg-12 text-center">
                    <h3>Dependencia</h3>
                  </div>
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label style="display:block;">¿Tiene Dependencia?:</label>
                        <input id="check-ordenes-no-cobradas" name="check-dependencia" type="checkbox" class="js-switch" checked />
                        <strong class="text-navy" id="strong-si-no">SI</strong>
                      </div>
                    </div>
                    <div class="col-lg-3" id="depende-torre">
                      <div class="form-group">
                        <label for="">Depende de Torre:</label>
                        <select class="select2_demo_1 form-control" name="sel-torre-depende" height="100%">
                             <option></option>
                             @foreach ($torres as $value)
                                <option value="{{$value->id}}">{{$value->nombre}}</option>
                             @endforeach
                         </select>
                         @if ($errors->has('sel-torre-depende'))
                         <p class="help-block text-danger">{{ $errors->first('sel-torre-depende') }}</p>
                         @endif
                      </div>
                    </div>
                    <div class="col-lg-3" id="depende-equipo">
                      <div class="form-group">
                        <label for="">Depende de Equipo:</label>
                        <select class="select2_demo_1 form-control" name="sel-equipo-depende" height="100%">
                             <option></option>
                         </select>
                         @if ($errors->has('sel-equipo-depende'))
                         <p class="help-block text-danger">{{ $errors->first('sel-equipo-depende') }}</p>
                         @endif
                      </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save"></span> GUARDAR
                        </button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Nestable List -->
<script src="{{ asset('/backend/js/plugins/nestable/jquery.nestable.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);
  $(document).ready(function() {
      elem.onchange = function() {
        if(elem.checked){
          $('#strong-si-no').html('SI');
          $('#strong-si-no').removeClass('text-danger');
          $('#strong-si-no').addClass('text-navy');
          $('#depende-equipo').fadeIn();
          $('#depende-torre').fadeIn();
        }else{
          $('#strong-si-no').html('NO');
          $('#strong-si-no').removeClass('text-navy');
          $('#strong-si-no').addClass('text-danger');
          $('#depende-equipo').fadeOut();
          $('#depende-torre').fadeOut();
        }
      };
      $(".select2_demo_1").select2({
          theme:'bootstrap',
          placeholder: "Seleccione...",
          allowClear: true,
          language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
      });

      // activate Nestable for list 2
      $('#nestable2').nestable({
          group: 1
      });

      // output initial serialised data
      //updateOutput($('#nestable').data('output', $('#nestable-output')));
      //updateOutput($('#nestable2').data('output', $('#nestable2-output')));

      $('#nestable-menu').on('click', function (e) {
          var target = $(e.target),
                  action = target.data('action');
          if (action === 'expand-all') {
              $('.dd').nestable('expandAll');
          }
          if (action === 'collapse-all') {
              $('.dd').nestable('collapseAll');
          }
      });
      $('#departamentos').val(13);
      $('#departamentos').trigger("change");

      @if(session('mesage')['status'] == 200)
       swal(
          'Exito!',
          'Equipo Guardada Correctamente.',
          'success'
        );
      @endif
      @if(session('mesage')['status'] == 400)
       swal(
          'Error!',
          'Ocurrio un error al guardar el equipo. Intentolo de nuevo.',
          'error'
        );
      @endif
  });

  $('[name="sel-torre-depende"]').on('select2:select', function (e) {
      var data = e.params.data;
      $.ajax({
        url: '/conexiones/jsonGetEquiposFromTorre',
        type: 'POST',
        dataType: 'json',
        data: { "_token": "{{ csrf_token() }}",
                "idTorre": $('[name="sel-torre-depende"]').val(),
              }
      })
      .done(function(msg) {
        console.log("success");
        console.log(msg);
        $('[name="sel-equipo-depende"]').html('');
        msg.forEach(function(element) {
            $('[name="sel-equipo-depende"]').append('<option value="'+ element.id +'">'+element.nombre+' ('+element.tipo+')</option>')
        });
        $('[name="sel-equipo-depende"]').trigger('change');
      })
      .fail(function() {
        console.log("error");
      });
  });

  $('#departamentos').on('change',function(){
      if(this.value !== 0){
          var valor = this.value;
          $.ajax({
             type: "GET",
             url: "/localidades/get",
             data: "depto="+valor,
             success: function(data)
             {
                 $('#localidad').not(":nth-child(1)").empty();
                  var a = JSON.parse(data);
                  $('#localidades').html('');
                  $.each(a, function(key, value) {
                      $('#localidades')
                          .append($("<option></option>")
                                     .attr("value",value.id)
                                     .text(value.nombre));
                 });
             },
             error: function(msg){
                 alert(msg);
             }

          });
      }
  });

</script>

@stop
