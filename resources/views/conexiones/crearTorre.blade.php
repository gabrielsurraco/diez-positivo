@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agregar Torre </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  <form class="form-group" action="/conexiones/guardarTorre" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Nombre de la Torre:</label>
                        <input type="text" class="form-control" name="inp-nombre-torre" required>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                          <label>Departamento de la Torre:</label>
                          <select id="departamentos" name="select-depto" class="form-control">
                              @foreach ($array_departamentos as $key => $value)
                                  <option value="{{$key}}">{{$value}}</option>
                              @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                          <label>Localidad de la Torre:</label>
                          <select id="localidades" name="select-localidad" class="form-control">

                          </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Direccion:</label>
                        <input type="text" class="form-control" name="inp-direccion-torre">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Altura de la Torre:</label>
                        <input type="number" class="form-control" name="inp-altura-torre">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Latitud:</label>
                        <input type="text" class="form-control" name="inp-latitud-torre">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="">Longitud:</label>
                        <input type="text" class="form-control" name="inp-longitud-torre">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label style="display:block;">¿Es HQ?:</label>
                        <input id="check-es-hq" name="check-hq" type="checkbox" class="js-switch" />
                        <strong class="text-danger" id="strong-si-no">NO</strong>
                      </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save"></span> GUARDAR
                        </button>
                    </div>
                  </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Nestable List -->
<script src="{{ asset('/backend/js/plugins/nestable/jquery.nestable.js') }}"></script>
<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);
  $(document).ready(function() {
      elem.onchange = function() {
        if(elem.checked){
          $('#strong-si-no').html('SI');
          $('#strong-si-no').removeClass('text-danger');
          $('#strong-si-no').addClass('text-navy');
          $('#depende-equipo').fadeIn();
          $('#depende-torre').fadeIn();
        }else{
          $('#strong-si-no').html('NO');
          $('#strong-si-no').removeClass('text-navy');
          $('#strong-si-no').addClass('text-danger');
          $('#depende-equipo').fadeOut();
          $('#depende-torre').fadeOut();
        }
      };
      $('.select2').select2({
          theme:'bootstrap',
          placeholder: ""
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
      });

      // activate Nestable for list 2
      $('#nestable2').nestable({
          group: 1
      });

      // output initial serialised data
      //updateOutput($('#nestable').data('output', $('#nestable-output')));
      //updateOutput($('#nestable2').data('output', $('#nestable2-output')));

      $('#nestable-menu').on('click', function (e) {
          var target = $(e.target),
                  action = target.data('action');
          if (action === 'expand-all') {
              $('.dd').nestable('expandAll');
          }
          if (action === 'collapse-all') {
              $('.dd').nestable('collapseAll');
          }
      });
      $('#departamentos').val(13);
      $('#departamentos').trigger("change");

      @if(session('mesage')['status'] == 200)
       swal(
          'Exito!',
          'Torre Guardada Correctamente.',
          'success'
        );
      @endif
      @if(session('mesage')['status'] == 400)
       swal(
          'Error!',
          'Ocurrio un error al guardar la torre. Intentolo de nuevo.',
          'error'
        );
      @endif
  });

  $('#departamentos').on('change',function(){
      if(this.value !== 0){
          var valor = this.value;
          $.ajax({
             type: "GET",
             url: "/localidades/get",
             data: "depto="+valor,
             success: function(data)
             {
                 $('#localidad').not(":nth-child(1)").empty();
                  var a = JSON.parse(data);
                  $('#localidades').html('');
                  $.each(a, function(key, value) {
                      $('#localidades')
                          .append($("<option></option>")
                                     .attr("value",value.id)
                                     .text(value.nombre));
                 });
             },
             error: function(msg){
                 alert(msg);
             }

          });
      }
  });

</script>

@stop
