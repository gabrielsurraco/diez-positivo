<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Diez Positivo') }}</title>

    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style_10positivo.css') }}" rel="stylesheet">
    <style>
        .input-xs {
            height: 22px;
            padding: 2px 2px;
            font-size: 12px;
            line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
            border-radius: 3px;
          }
    </style>



    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">Bienvenido! <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Cerrar Sesión</a></li>
                            </ul>
                    </div>
                    <div class="logo-element">
                        10+
                    </div>
                </li>

                <li class="@if(Route::is('personas')) active @endif">
                    <a href="/personas"><i class="fa fa-user-o"></i> <span class="nav-label">Personas </span></a>
                </li>
                <li class="@if(Route::is('clientes.*')) active @endif">
                    <a href="/clientes"><i class="fa fa-id-card-o"></i> <span class="nav-label">Clientes </span></a>
                </li>
                <li class="@if(Route::is('proveedores')) active @endif">
                    <a href="/proveedores"><i class="fa fa-truck"></i> <span class="nav-label">Proveedores </span></a>
                </li>
                <li class="@if(Route::is('inversores')) active @endif">
                    <a href="/inversores"><i class="fa fa-bullhorn"></i> <span class="nav-label">Inversores </span></a>
                </li>
                <li class="@if(Route::is('tecnicos')) active @endif">
                    <a href="/tecnicos"><i class="fa fa-user-md"></i> <span class="nav-label">Técnicos </span></a>
                </li>
                <li class="@if(Route::is('servicios')) active @endif">
                    <a href="/servicios"><i class="fa fa-wifi"></i> <span class="nav-label">Servicios </span></a>
                </li>
                <li class="@if(Route::is('conexiones.*')) active @endif">
                    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Conexiones </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <!--<li><a href="#">Comprobantes de Venta</a></li>-->
                        <!--<li><a href="#">Presupuestos</a></li>-->
                        <!--<li><a href="#">Remitos</a></li>-->
                        <!--<li><a href="#">Cobranzas</a></li>-->
                        <!--<li><a href="#">Cuentas a Cobrar</a></li>-->
                        <!--<li><a href="#">Cuenta Corriente de Clientes</a></li>-->
                        <!--<li><a href="#">Abonos</a></li>-->
                        <!--<li class="@if(Route::is('ventas.clientes')) active @endif"><a href="/ventas/clientes">Clientes</a></li>-->
                        <!--<li><a href="#">Productos de Venta</a></li>-->
                        <!--<li><a href="#">Vendedores</a></li>-->
                        <li class="@if(Route::is('conexiones.listado')) active @endif"><a href="/conexiones/listar">Listado de Conexiones </a></li>
                        <li class="@if(Route::is('conexiones.agregarTorre')) active @endif"><a href="/conexiones/agregarTorre">Agregar Torre </a></li>
                        <li class="@if(Route::is('conexiones.agregarEquipo')) active @endif"><a href="/conexiones/agregarEquipo">Agregar Equipo </a></li>

                    </ul>
                </li>

                <li class="@if(Route::is('cuentaCorriente.*')) active @endif">
                    <a href="#"><i class="fa fa-usd"></i> <span class="nav-label">Cuenta Corriente </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <!--<li><a href="#">Comprobantes de Venta</a></li>-->
                        <!--<li><a href="#">Presupuestos</a></li>-->
                        <!--<li><a href="#">Remitos</a></li>-->
                        <!--<li><a href="#">Cobranzas</a></li>-->
                        <!--<li><a href="#">Cuentas a Cobrar</a></li>-->
                        <!--<li><a href="#">Cuenta Corriente de Clientes</a></li>-->
                        <!--<li><a href="#">Abonos</a></li>-->
                        <!--<li class="@if(Route::is('ventas.clientes')) active @endif"><a href="/ventas/clientes">Clientes</a></li>-->
                        <!--<li><a href="#">Productos de Venta</a></li>-->
                        <!--<li><a href="#">Vendedores</a></li>-->
                        <li class="@if(Route::is('cuentaCorriente.cuentaCorriente')) active @endif"><a href="/cuenta-corriente">Cuenta Corriente </a></li>
                        <li class="@if(Route::is('cuentaCorriente.ordenesVenta')) active @endif"><a href="/ventas/ordenesVenta">Ordenes de Venta</a></li>
                        <li class="@if(Route::is('cuentaCorriente.obligaciones')) active @endif"><a href="/obligaciones/">Obligaciones de Pago</a></li>
                        <li class="@if(Route::is('cuentaCorriente.cobros')) active @endif"><a href="/ventas/cobros">Cobranza</a></li>
                        <li class="@if(Route::is('cuentaCorriente.recibos')) active @endif"><a href="/recibos">Recibos</a></li>
                        <li class="@if(Route::is('cuentaCorriente.procesos')) active @endif"><a href="/procesos/">Procesos</a></li>
                    </ul>
                </li>

                <li class="@if(Route::is('compras.*')) active @endif">
                    <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Compras </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="@if(Route::is('compras.index')) active @endif"><a href="/compras/ordenCompra">Ordenes de Compra</a></li>
                    </ul>
                </li>

                <li class="@if(Route::is('stock.*')) active @endif">
                    <a href="#"><i class="fa fa-cubes"></i> <span class="nav-label">Stock </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="@if(Route::is('stock.almacenes.*')) active @endif"><a href="/stock/almacenes">Almacenes</a></li>
                        <li class="@if(Route::is('stock.articulos.*')) active @endif"><a href="/stock/articulos">Articulos</a></li>
                        <li class="@if(Route::is('stock.recep_mercaderias.*')) active @endif"><a href="/stock/recep_mercaderias">Recepción de Mercaderías</a></li>
                        <li class="@if(Route::is('stock.inventario.*')) active @endif"><a href="/stock/inventario">Inventario</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-suitcase"></i> <span class="nav-label">Contabilidad </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <!--<li><a href="#">Asientos Manuales</a></li>-->
                        <!--<li><a href="#">Libro Diario</a></li>-->
                        <!--<li><a href="#">Libro Mayor</a></li>-->
                        <!--<li><a href="#">Resultados Mensuales</a></li>-->
                        <!--<li><a href="#">Sumas y Saldos</a></li>-->
                        <!--<li><a href="#">Balance de Gestión</a></li>-->
                        <!--<li><a href="#">Estado de Resultados</a></li>-->
                        <!--<li><a href="#">Monedas</a></li>-->
                        <!--<li><a href="#">Centro de Costos</a></li>-->
                        <li class="@if(Route::is('asientos.manuales')) active @endif"><a href="/contabilidad/asientos">Asientos Manuales</a></li>
                        <li class="@if(Route::is('plan-de-cuentas')) active @endif"><a href="/contabilidad/plan-de-cuentas">Plan de Cuentas</a></li>
                        <!--<li><a href="#">Ejercicios Contables</a></li>-->
                    </ul>
                </li>
                <li class="@if(Route::is('tickets')) active @endif">
                    <a href="/tickets"><i class="fa fa-support"></i> <span class="nav-label">Tickets </span></a>
                </li>
                <li class="@if(Route::is('agenda')) active @endif">
                    <a href="/agenda"><i class="fa fa-calendar"></i> <span class="nav-label">Agenda</span></a>
                </li>
<!--                <li class="@if(Route::is('servicios')) active @endif">
                    <a href="/servicios"><i class="fa fa-shopping-basket"></i> <span class="nav-label">Servicios </span></a>
                </li>-->
                <li class="@if(Route::is('usuarios') || Route::is('usuarios.nuevo') || Route::is('usuarios.editar')) active @endif">
                    <a href="/usuarios"><i class="fa fa-users"></i> <span class="nav-label">Usuarios </span></a>
                </li>
<!--                <li>
                    <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Reportes </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="#" >Ventas <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="#">Tablero de Ventas</a></li>
                                <li><a href="#">Tablero de Presupuestos</a></li>
                                <li><a href="#">Cuentas a Cobrar</a></li>
                                <li><a href="#">Análisis de Facturas de Venta</a></li>
                                <li><a href="#">Comisiones por Vender</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" >Inventarios <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="#">Ingresos y Egresos</a></li>
                                <li><a href="#">Inventario</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" >Compras <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="#">Cuentas a Pagar</a></li>
                                <li><a href="#">Análisis de Facturas de Compra</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" >Finanzas <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="#">Cobranzas</a></li>
                                <li><a href="#">Pagos</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>-->

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" style="background: #e8e8e8;">
        <div class="row border-bottom m-b-md hidden-print">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">

                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Cerrar Sesión
                        </a>
                    </li>
                </ul>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </nav>
        </div>


        @yield('content')

        <div class="footer hidden-print">
<!--            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>-->
            <div>
                <strong>Copyright</strong> Diez Positivo &copy; 2017
            </div>
        </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('backend/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('backend/js/inspinia.js') }}"></script>
<script src="{{ asset('backend/js/plugins/pace/pace.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/backend/js/plugins/datapicker/locales/bootstrap-datepicker.es.js') }}"></script>
<!-- SweetAlert -->
<script src="{{ asset('/backend/js/plugins/sweetAlert/sweetalert.min.js') }}"></script>

<script src="{{ asset('/backend/js/funciones.js') }}"></script>
<!-- Moment -->
<script src="{{ asset('/backend/js/plugins/moment/moment-with-locales.js') }}"></script>

@yield('javascript')

</body>

</html>
