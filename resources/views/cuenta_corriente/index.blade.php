@extends('layouts.backend')


@section('css')
<!-- Required Stylesheets -->
<!--<link href="{{ asset('/bootstrap-treeview/src/css/bootstrap.css') }}" rel="stylesheet">-->

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">




@stop



@section('content')
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cuenta Corriente</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form class="datos-cta-cte" action="#">
                            <div class="col-lg-4">
                              <div class="form-group">
                                  <label>Cliente:</label>
                                  <select class="select2_demo_1 form-control" name="sel-cliente" height="100%">
                                       <option></option>
                                   </select>
                              </div>
                            </div>


                            <div class="form-group col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>{{ Form::text("cuit", null, array("class"=>"form-control cuit-input", "placeholder"=>"CUIT/L")) }}
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-arrow-right"></i></span>{{ Form::text("concepto", null, array("class"=>"form-control", "placeholder"=>"CONCEPTO...")) }}
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_desde" placeholder="FECHA DESDE" class="form-control date">
                                </div>
                                @if ($errors->has('fecha_desde'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_desde') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_hasta" placeholder="FECHA HASTA" class="form-control date" >
                                </div>
                                @if ($errors->has('fecha_hasta'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_hasta') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </form>
                        <div class="form-group col-md-2">
                            <button class="btn btn-md btn-success buscar_cta_cte"> <i class="fa fa-search"></i> Buscar</button>
                        </div>
                    </div>
                    <h3 class="persona"></h3>
                    <div class="hr-line-dashed"></div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <!--<div class="table-responsive">-->
                                <table class="table table-striped table-hover dataTables-example" id="tabla-resultados">
                                    <thead>
                                        <tr>
                                            <th>Nro. Secuencia</th>
                                            <th>Fecha</th>
                                            <th>Concepto</th>
                                            <th>Debe</th>
                                            <th>Haber</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="gradeX">
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td >
<!--                                                <a class="btn btn-xs btn-success" href="#" title="Editar"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs btn-warning" href="#" title="Seguimiento de Ticket's"><i class="fa fa-ticket"></i></a>
                                                <a class="btn btn-xs btn-primary" href="#" title="Servicios"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="btn btn-xs btn-danger"  title="Cuenta Corriente"><i class="fa fa-money"></i></a>-->
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4"></th>
                                            <th style="font-size: 20px; text-align: right;">SALDO</th>
                                            <th class="saldo"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                              <!--</div>-->
                        </div>
                    </div>
                    <div class="row" style="text-align: center;">
                        <a class="btn btn-danger btn-outline informarPago calcularRecargos" > <i class="fa fa-flag"></i> Aplicar Recargos</a>
                        <!--<a class="btn btn-danger btn-outline irCobranza"> <i class="fa fa-credit-card"></i> Cobrar</a>-->
                        <!--<a class="btn btn-warning btn-outline irOrdenVenta" href="#"> <i class="fa fa-money"></i> Orden de Venta</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection

@section('javascript')

    <!-- Select2 -->
    <script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
    <!-- Select2 Lenguaje pack español -->
    <script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>

    <!-- Data Tables -->
    <script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Chosen -->
    <script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>

    <!-- Sweet alert -->
    <script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- jQuery UI  -->
    <script src="{{ asset('/backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>

<SCRIPT language=Javascript>

    function isNumberKey(evt){
       var charCode = (evt.which) ? evt.which : evt.keyCode;
       if (charCode != 46 && charCode > 31
         && (charCode < 48 || charCode > 57))
          return false;

       return true;
    }

</SCRIPT>

    <script type="text/javascript">
        $(document).ready(function(){

            $('.chosen-select').chosen({width: "100%"});



            $(".calcularRecargos").click(function(){


                $.ajax({
                    type: "POST",
                    data: "_token="+ $("meta[name='csrf-token']").attr("content")+ "&cuenta_id="+ $("#myModal").attr("cuentaId"),
                    url: '/cuenta-corriente/calcularIntereses',
                    success: function(data){
                        var table = $('.dataTables-example').DataTable();
                        $.each(data.items, function(key, value){
                            table.row.add( [ value.nro_secuencia, value.fecha, value.concepto, value.debe, value.haber, 'TO DO' ] )
                                .draw();
                        });

                        if(typeof data.saldo != 'undefined'){
                            $(".saldo").html("<span style='color: "+data.color+";font-size: 20px;' >$ "+data.saldo+"</span>");
                        }else{
                            $(".saldo").html("<span style='font-size: 20px;' >$ 0</span>");
                        }
                    },
                    error: function(data){
                        console.log(data);
                    }
                });

            });


            $(".saveInfoPago").click(function(){
                var datos = $('#formInfoPago').serialize();

                $.ajax({
                    type: "POST",
                    data: "_token="+ $("meta[name='csrf-token']").attr("content") +"&"+ datos + "&cuenta_id="+ $("#myModal").attr("cuentaId"),
                    url: '/cuenta-corriente/setPago',
                    success: function(data){
                        console.log(data);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });

            });

            $(".informarPago").click(function(){
                var cuentaId = $("#myModal").attr("cuentaId");
                if(typeof cuentaId == "undefined" || cuentaId == ""){
                    alert("Favor primero busque la cuenta de un cliente!");
                    return false;
                }
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

            $('.i-checks').on('ifChecked', function(event){
                $('.buscaFactura').hide();
                $('.cargaFactura').show();
            });

            $('.i-checks').on('ifUnchecked', function(event){
                $('.buscaFactura').show();
                $('.cargaFactura').hide();
            });

            $(".select2_demo_1").select2({
                theme:'bootstrap'
            });

            $(".cuit-input").keypress(function(e) {
                if(e.which == 13) {
                    getCuentaCorriente();
                }
            });

            var getCuentaCorriente = function(){
                var datos   = $(".datos-cta-cte").serialize();
                var cuit    = $(".cuit-input").val();

                $.ajax({
                    type: "POST",
                    url: "/cuenta-corriente/buscar",
                    data: "_token="+ $("meta[name='csrf-token']").attr("content") +"&"+ datos,
                    success: function(data){
                        var table = $('.dataTables-example').DataTable();
                        table.rows().remove().draw();

                        if(typeof data.mensaje != 'undefined' && typeof data.mensaje.id_persona == 'undefined'){
                            alert("El cliente buscado no existe!");
                            //DESHABILITO INFORMAR PAGO SI EXISTE LA PERSONA
                            $("#myModal").attr("cuentaId", "");
                            return false;
                        }

                        //SETEO RUTA PARA IR A ORDEN DE VENTA
                        if(typeof data.mensaje.id_persona != 'undefined'){
//                            $('.irOrdenVenta').attr('href','/ventas/nuevaOrdenVenta/'+data.mensaje.id_persona);
//                            $('.irCobranza').attr('href','/cobros/nuevaCobranza/'+data.mensaje.id_persona);

                            //HABILITO INFORMAR PAGO SI EXISTE LA PERSONA
                            $("#myModal").attr("cuentaId", data.mensaje.fk_cuenta);

                            if(typeof data.mensaje.ov_pendientes != 'undefined'){
                                $.each( data.mensaje.ov_pendientes, function( key, value ) {
//                                    console.log(value);
                                    //CARGAR ORDENES DE VENTA AL SELECT
                                    var total = parseFloat(value.importe_iva21) + parseFloat(value.importe_iva105) + parseFloat(value.importe_neto);
                                    $(".chosen-select").append("<option value='"+value.id+"'> <strong>Ord. Vent.: </strong> #"+value.id+" -- <strong>TOTAL:</strong> $ "+total+"<strong> -- FECHA: </strong>"+value.fecha+"</option>");
                                });
                                $('.chosen-select').trigger("chosen:updated");
                            }

                        }else{
                            $('.irOrdenVenta').attr('href','#');
                            $('.irCobranza').attr('href','#');

                            //DESHABILITO INFORMAR PAGO SI EXISTE LA PERSONA
                            $("#myModal").attr("cuentaId", "");
                        }

                        if(typeof data.mensaje.items_cuenta_corriente != 'undefined'){
                            $.each( data.mensaje.items_cuenta_corriente, function( key, value ) {
                                table.row.add( [ value.nro_secuencia, value.fecha, value.descripcion, value.debe, value.haber, 'TO DO' ] )
                                .draw();
                            });
                            $(".saldo").html("<span style='color: "+data.mensaje.color+";font-size: 20px;' >$ "+data.mensaje.saldo+"</span>");
                            $(".persona").html(data.mensaje.persona);
                        }else{
                            $(".saldo").html("<span style='font-size: 20px;' >$ 0</span>");
                        }

                    },
                    error: function(data){

                        //DESHABILITO INFORMAR PAGO SI EXISTE LA PERSONA
                        $("#myModal").attr("cuentaId", "");

                    }
                });
            };

            $(".buscar_cta_cte").click(function(){
                getCuentaCorriente();
            });

            $('.input-group.date').datepicker({
                startView: 3,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('.dataTables-example').DataTable({
                "language": {
                  "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                data: [],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]
            });

            //buscar un cliente de la base de datos.
            $('[name="sel-cliente"]').select2({
              ajax: {
                method: "POST",
                url: '/clientes/jsonSearchCliente',
                dataType: 'json',
                data: function (params){
                      var query = {
                      palabraClave: params.term,
                      "_token": "{{ csrf_token() }}"
                      }

                      // Query parameters will be ?search=[term]&type=public
                      return query;
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              },
              minimumInputLength: 3,
              placeholder: "Buscar Cliente.",
              language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
              theme:'bootstrap'
            });

        });
    </script>
@stop
