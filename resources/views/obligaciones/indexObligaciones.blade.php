@extends('layouts.backend')


@section('css')
<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/datapicker/datepicker3.css')}}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Obligaciones a Pagar por el Cliente</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">

                    <div class="col-md-offset-3 col-md-4">
                      <div class="form-group">
                          <label>Seleccione Cliente:</label>
                          <select class="select2_demo_1 form-control" name="sel-cliente" height="100%">
                               <option></option>
                           </select>
                      </div>

                    </div>
                    <div class="col-md-1">
                      <button type="button" style="margin-top:23px;" onclick="buscarObligacionesCliente()" class="btn btn-primary">
                        <span class="fa fa-search"></span>
                      </button>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="">Solo Obligaciones No Facturadas</label>
                        <input style="diplay:inline-block;" id="check-ordenes-no-cobradas" type="checkbox" class="js-switch" checked />
                        <strong class="text-navy" id="strong-ordenes-no-cobradas">SI</strong>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive">
                      <table class="table table-striped dataTables" id="table-resultados" style="font-size: 11px;">
                          <thead>
                          <tr>
                              <th width="5%" style="text-align:center"><input type="checkbox" class="i-checks" data-index="all" id="check-all"></th>
                              <th>Fecha</th>
                              <th>Periodo</th>
                              <th>Abono Mensual</th>
                              <th>Descripcion</th>
                              <th>Precio Unitario</th>
                              <th>Alicuota IVA</th>
                              <th>IVA</th>
                              <th>NETO</th>
                              <th>TOTAL</th>
                              <th>Vencimiento</th>
                              <th>Factura</th>
                          </tr>
                          </thead>
                          <tbody>
                            <tr style="background-color: transparent;">
                              <td colspan="12"><div class="alert alert-info text-center">
                                  No hay datos.
                              </div></td>
                            </tr>
                          </tbody>
                      </table>
                  </div>

                  <div class="col-lg-offset-8 col-lg-4">
                    <ul class="list-group clear-list">
                                            <li class="list-group-item fist-item">
                                                <span class="pull-right" id="label-21"> $ 0.00 </span>
                                                IVA 21%
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right" id="label-1050"> $ 0.00 </span>
                                                IVA 10.50%
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right" id="label-total"> $ 0.00 </span>
                                                TOTAL
                                            </li>
                                        </ul>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-success disabled" id="btn-gen-factura"  onclick="openModalFactura();" type="button"><i class="fa fa-file-text-o"></i> Generar Factura</button>
                    </div>
                  </div>

                  <div class="hr-line-dashed"></div>
                  <h3 class="text-center">Facturas Generadas para el Cliente Seleccionado:</h3>
                  <div class="table-responsive">
                      <table class="table table-striped dataTables" id="table-resultados-facturas" style="font-size: 11px;">
                          <thead>
                          <tr>
                              <th>Fecha</th>
                              <th>Tipo Factura</th>
                              <th>Nro Factura</th>
                              <th>Nro Factura Fiscal</th>
                              <th>Importe Neto</th>
                              <th>Importe Iva</th>
                              <th>Importe Total</th>
                              <th>Estado</th>
                              <th></th>
                          </tr>
                          </thead>
                          <tbody>
                            <tr style="background-color: transparent;">
                              <td colspan="12"><div class="alert alert-info text-center">
                                  No hay datos.
                              </div></td>
                            </tr>
                          </tbody>
                      </table>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Fin Contenido Principal -->

<!-- Inicio modal Generar Factura -->
<div class="modal inmodal fade" id="modal-factura" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Generar Factura</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-xs-3">
                    <div class="form-group">
                        <label>Fecha:</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha" data-fecha="true" class="form-control">
                        </div>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                        <label>Tipo Conceptos:</label>
                        <select class="form-control" name="sel-tipo-concepto">
                          <option value="PRODUCTOS">PRODUCTOS</option>
                          <option value="SERVICIOS">SERVICIOS</option>
                          <option value="PRODUCTOS_SERVICIOS">PRODUCTOS Y SERVICIOS</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                        <label>Periodo:</label>
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="input-sm form-control" name="periodo-desde" value="05/14/2014"/>
                            <span class="input-group-addon">hasta</span>
                            <input type="text" class="input-sm form-control" name="periodo-hasta" value="05/22/2014" />
                        </div>
                    </div>

                  </div>
                  <div class="col-xs-2">
                    <div class="form-group">
                        <label>Venc Pago:</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha-vencimiento-pago" data-fecha="true" class="form-control">
                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-5">
                    <div class="form-group">
                        <label style="display:block;">Cliente:</label>
                        <span class="label label-success" style="display:inline-block;">BALMACEDA GUILLERMO FEDERICO</span>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <label for="">Condicion Fiscal:</label>
                      <select class="form-control" name="sel-condicionFiscal">
                        <option value="RESPONSABLE_INSCRIPTO">Resp. Inscripto</option>
                        <option value="CONSUMIDOR_FINAL">Consumidor Final</option>
                        <option value="EXENTO">Exento</option>
                        <option value="MONOTRIBUTISTA">Monotributista</option>
                        <option value="CLIENTE_EXTERIOR">Cliente de Exterior</option>
                      </select>
                    </div>

                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <label for="">Tipo de Factura:</label>
                      <select class="form-control cbte-tipo" name="sel-tipo-factura">
                        <option value="FACTURA-A" data-tipo="A">Factura A</option>
                        <option value="FACTURA-B" data-tipo="B">Factura B</option>
                      </select>
                    </div>

                  </div>

                </div>
                <div class="row">
                  <h3 class="text-center">Domicilio a facturar</h3>
                  <div class="col-xs-5">
                    <div class="form-group">
                      <label for="">Seleccione Domicilio de Facturacion:</label>
                      <select class="form-control" name="domicilio-factura">
                        <option></option>
                      </select>
                    </div>
                  </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="generarFactura()">Generar</button>
            </div>
        </div>
    </div>
</div>
<!-- Final modal Generar Factura -->

<!-- Inicio modal Ver Datalles -->
<div class="modal inmodal fade" id="modal-verDetalles" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Detalles de Factura</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12">
                    <table class="table table-striped dataTables" id="table-items-factura" style="font-size: 11px;">
                        <thead>
                        <tr>
                            <th>Cantidad</th>
                            <th>Concepto</th>
                            <th>Descripcion</th>
                            <th>Precio Unitario</th>
                            <th>Alicuota IVA</th>
                            <th>IVA</th>
                            <th>NETO</th>
                            <th>TOTAL</th>
                            <th>Vencimiento Pago</th>
                        </tr>
                        </thead>
                        <tbody>
                          <tr style="background-color: transparent;">
                            <td colspan="9"><div class="alert alert-info text-center">
                                No hay datos.
                            </div></td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-offset-8 col-lg-4">
                    <ul class="list-group clear-list">
                                            <li class="list-group-item fist-item">
                                                <span class="pull-right" id="label-items-21"> $ 0.00 </span>
                                                IVA 21%
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right" id="label-items-1050"> $ 0.00 </span>
                                                IVA 10.50%
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right" id="label-items-total"> $ 0.00 </span>
                                                TOTAL
                                            </li>
                                        </ul>
                  </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- Final modal Generar Factura -->


@endsection

@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('/backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- DatePicker  -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  var elem = document.querySelector('.js-switch');
  var init = new Switchery(elem);

  $(document).ready(function() {
    $(".select2_demo_1").select2({
        theme:'bootstrap'
    });

    $('.chosen-select').chosen({width: "100%"});

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    var date = new Date();
    $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
    $('[data-fecha="true"]').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es'
    });
    $('.input-daterange').datepicker({
              keyboardNavigation: false,
              forceParse: false,
              autoclose: true
          });
    elem.onchange = function() {
      if(elem.checked){
        $('#strong-ordenes-no-cobradas').html('SI');
        $('#strong-ordenes-no-cobradas').removeClass('text-danger');
        $('#strong-ordenes-no-cobradas').addClass('text-navy');
      }else{
        $('#strong-ordenes-no-cobradas').html('NO');
        $('#strong-ordenes-no-cobradas').removeClass('text-navy');
        $('#strong-ordenes-no-cobradas').addClass('text-danger');
      }
    };

    $('[name="sel-cliente"]').select2({
      ajax: {
        method: "POST",
        url: '/clientes/jsonSearchCliente',
        dataType: 'json',
        data: function (params){
              var query = {
              palabraClave: params.term,
              "_token": "{{ csrf_token() }}"
              }

              // Query parameters will be ?search=[term]&type=public
              return query;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      },
      minimumInputLength: 3,
      placeholder: "Buscar Cliente.",
      language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
      theme:'bootstrap'
    });
    $('[data-toggle="tooltip"]').tooltip();
  });

  var table;
  var table2;
  var globalImporteTotal = 0;
  var globaliva1050      = 0;
  var globaliva21        = 0;
  var globaltributos     = 0;
  var globalArrayOV      = null;
  function buscarObligacionesCliente(){
      if (table != null){
        table.destroy();
      }
      if (table2 != null){
        table2.destroy();
      }
      if ($('[name="sel-cliente"]').val() == ''){
        swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
        return
      }

      //-------------------Obligaciones del cliente---------------------------
      //Listamos todas las obligaciones del cliente.
      $.ajax({
        method: "POST",
        url: "/obligaciones/getObligacionesFromCuentaJSON",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val(), "soloFacturadas":elem.checked},
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          $('#btn-gen-factura').addClass('disabled');
          $('#table-resultados tbody').html('');
          msg.forEach(function (item){
            var disCheck = '';
            var fact = '-';
            if(item.fk_facturacion != null){
              disCheck = 'disabled';
              fact = String(item.fk_facturacion).padLeft(8,'0');
            }

            if(item.periodo_mensualidad == null){
                item.periodo_mensualidad = '-';
            }

            $('#table-resultados tbody').append('<tr>'+
                '<td align="center"><input type="checkbox" class="i-checks" '+disCheck+' data-index="'+ item.id +'" name="input[]"></td>'+
                '<td align="center">'+ item.fecha +'</td>'+
                '<td align="center">'+ item.periodo +'</td>'+
                '<td align="center">'+ item.periodo_mensualidad +'</td>'+
                '<td align="center">'+ item.cantidad +' - '+ item.concepto +' '+ item.descripcion +'</td>'+
                '<td align="center">$ '+ item.precio_unitario +'</td>'+
                '<td align="center" name="alicuotaIva">'+ item.alicuta_iva +' %</td>'+
                '<td align="center" name="importeIva">$ '+ item.importe_iva +'</td>'+
                '<td align="center">$ '+ item.importe_siniva +'</td>'+
                '<td align="center" name="importeTotal">$ '+ (parseFloat(item.importe_siniva) + parseFloat(item.importe_iva)).toFixed(2) +'</td>'+
                '<td align="center"> '+ item.fecha_vencimiento_pago +' </td>'+
                '<td align="center"> <span class="label label-success">'+ fact +'</span> </td>'+
            '</tr>');

            //CARGAR ORDENES DE VENTA AL SELECT
            $('.chosen-select').trigger("chosen:updated");

            $('#btn-gen-factura').removeClass('disabled');

          });
          table = $('#table-resultados').DataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]});
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            //funcion del boton seleccionar todos los elementos.
            $('#check-all').on('ifChanged', function(event){
              if(event.target.checked){
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('check');
                 });
              }else{
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('uncheck');
                 });
              }
            });

            //codigo para calcular el total de items seleccionados.
            $('[name="input[]"]').on('ifChanged', function(event){
                globalImporteTotal = 0;
                globaliva1050      = 0;
                globaliva21        = 0;
                globaltributos     = 0;
                $('[name="input[]"]:checked').each(function () {
                  //arrayov.push($(this).data('index'));
                  //var iva1050 = $(this).closest("tr").find('td[name="iva1050"]').text();
                  var alicuotaiva = $(this).closest("tr").find('td[name="alicuotaIva"]').text();
                  if (alicuotaiva == '21 %'){
                    var valor = $(this).closest("tr").find('td[name="importeIva"]').text();
                    globaliva21 += parseFloat(valor.substr(1, valor.length));
                  }
                  if (alicuotaiva == '10.5 %'){
                    var valor = $(this).closest("tr").find('td[name="importeIva"]').text();
                    globaliva1050 += parseFloat(valor.substr(1, valor.length));
                  }
                  var valor = $(this).closest("tr").find('td[name="importeTotal"]').text();
                  globalImporteTotal += parseFloat(valor.substr(1, valor.length));
                });
                $('#label-21').html('$ '+ globaliva21.toFixed(2));
                $('#label-1050').html('$ '+ globaliva1050.toFixed(2));
                $('#label-total').html('$ '+ globalImporteTotal.toFixed(2));
             });
            //fin codigo
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });

      //-------------------Facturas Generadas para el cliente---------------------------
      //Listamos todas las facturas generadas para el cliente.
      $.ajax({
        method: "POST",
        url: "/obligaciones/listarFacturasJSON",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val()},
        dataType: 'json'
      }).done(function( msg ) {
          console.log('-------------Facturas--------------');
          console.log(msg);
          $('#table-resultados-facturas tbody').html('');
          msg.forEach(function (item){
            //atendemos el estado de la factura
            if(item.estado == 'PRESUPUESTO'){
              item.estado = '<span class="label label-warning label-xs">'+item.estado+'</span>';
            }else{
              item.estado = '<span class="label label-info label-xs">'+item.estado+'</span>';
            }
            //atendemos el numero de factura si no existe ponemos -
            var btnGenerador = '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Detalles"><span class="fa fa-file-pdf-o"></span></button>';
            if(item.numero_factura_fiscal == null){
                item.numero_factura_fiscal = '-';
                btnGenerador = '<button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Detalles"><span class="fa fa-check"></span></button>';
            }

            $('#table-resultados-facturas tbody').append('<tr>'+
                '<td align="center">'+ item.fecha +'</td>'+
                '<td align="center">'+ item.tipo_factura +'</td>'+
                '<td align="center"><span class="label label-success">'+ String(item.id ).padLeft(8,'0') +'</span></td>'+
                '<td align="center">'+ item.numero_factura_fiscal +'</td>'+
                '<td align="center">$ '+ item.importeNeto +'</td>'+
                '<td align="center">$ '+ item.importeIva +'</td>'+
                '<td align="center">$ '+ item.importeTotal +'</td>'+
                '<td align="center">'+ item.estado +'</td>'+
                '<td align="center"><button type="button" onClick="verDetallesFactura('+item.id+')" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Detalles"><span class="fa fa-eye"></span></button> <button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Detalles"><span class="fa fa-print"></span></button> '+ btnGenerador +'</td>'+
            '</tr>');
          });

          table2 = $('#table-resultados-facturas').DataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]});


      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
      //-------------------Domicilios del cliente---------------------------
      //Listamos todos los domicilios del cliente para poder hacer factura.
      $.ajax({
        method: "POST",
        url: "/personas/jsonGetDomiciliosFromPersona",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val()},
        dataType: 'json'
      }).done(function( msg ) {
          console.log('--------- Domicilios -----------');
          console.log(msg);
          $('[name="domicilio-factura"]').html('');
          console.log(msg[0].domicilios);
          msg[0].domicilios.forEach(function (item){
            $('[name="domicilio-factura"]').append('<option value="'+item.id+'">'+ item.calle +' '+ item.altura+' ('+item.localidad.nombre+' - '+item.departamento.nombre+')</option>');
          });

      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });

}

function openModalFactura(){
      $('[name="input[]"]:checked').each(function () {
        var valor  = parseInt($(this).data('index'));
        var entrada = {idObligacion:valor};
        arrayoblig.push(entrada);
        console.log(arrayoblig);
      });

      if (arrayoblig.length == 0){
        swal("ups!", "Tiene que seleccionar al menos una obligacion para generar factura.", "warning");
        return
      }
    $('#modal-factura').modal('show');
}


var arrayoblig = Array();
function generarRecibo(){
    arrayoblig = Array();
    $('[name="input[]"]:checked').each(function () {
      console.log(globalArrayOV);
      var valor  = parseInt($(this).data('index'));
      var objeto = $.grep(globalArrayOV, function(e){ return e.id == valor; });

      var entrada = {idObligacion:$(this).data('index'), saldoObli: objeto};
      arrayoblig.push(entrada);
      console.log(arrayoblig);
    });

    if (arrayoblig.length == 0){
      swal("ups!", "Tiene que seleccionar al menos una orden de venta para generar recibo.", "warning");
      return
    }
    if ($('[name="sel-cliente"]').val() == ''){
      swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() == '' || $('[name="inp-pago-cliente"]').val() == 0){
      swal("ups!", "El pago del cliente no puede ser 0 ni vacio.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() > globalImporteTotal){
      swal("ups!", "El pago del cliente no puede ser superior al pago total.", "warning");
      return
    }
    $.ajax({
      method: "POST",
      url: "/cobranza/generarCobro",
      data: {
              "_token"      : "{{ csrf_token() }}",
              "idCliente"   : $('[name="sel-cliente"]').val(),
              "pagoCliente" : $('[name="inp-pago-cliente"]').val(),
              'arrayObli'   : arrayoblig
            },
      dataType: 'json'
    }).done(function( msg ) {
        console.log(msg);
        window.open('/recibos/print/'+ msg.idRecibo,'_blank');
        location.reload();
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
  }



  function generarFactura(){
      $.ajax({
        method: "POST",
        url: "/obligaciones/generarFacturaFromItems",
        data: {
                "_token"       : "{{ csrf_token() }}",
                "idCliente"    : $('[name="sel-cliente"]').val(),
                "domicilioFact": $('[name="domicilio-factura"]').val(),
                "tipoConcepto" : $('[name="sel-tipo-concepto"]').val(),
                "periodoDesde" : $('[name="periodo-desde"]').val(),
                "periodoHasta" : $('[name="periodo-hasta"]').val(),
                "vencPago"     : $('[name="inp-fecha-vencimiento-pago"]').val(),
                "total"        : globalImporteTotal,
                "iva21"        : globaliva21,
                "iva1050"      : globaliva1050,
                "tipoFactura"  : $('[name="sel-tipo-factura"]').val(),
                "items"        : arrayoblig,
              },
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          swal("Guardado!", "Factura Generada Correctamente.", "success").then((value) => {
            location.reload();
          });
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
    }
    var table3;
    function verDetallesFactura(idFactura){
        if (table3 != null){
          table3.destroy();
        }
        //-------------------Facturas Generadas para el cliente---------------------------
        //Listamos todas las facturas generadas para el cliente.
        $.ajax({
          method: "POST",
          url: "/obligaciones/listarItemsFacturaJSON",
          data: { "_token": "{{ csrf_token() }}", "idFacturacion": idFactura},
          dataType: 'json'
        }).done(function( msg ) {
            console.log('-------------Items Facturas--------------');
            console.log(msg);
            $('#table-items-factura tbody').html('');
            var iva21_item    = 0;
            var iva1050_item  = 0;
            var total_item = 0;
            msg.forEach(function (item){
              //sumamos los totales de iva y total de ventas para los label
              switch (item.alicuta_iva) {
                case 21:
                  iva21_item += parseFloat(item.importe_iva);
                  break;

                case 10.50:
                  iva1050_item += parseFloat(item.importe_iva);
                  break;
                default:
              }
              total_item += (parseFloat(item.importe_iva) + parseFloat(item.importe_siniva));

              $('#table-items-factura tbody').append('<tr>'+
                  '<td align="center">'+ item.cantidad +'</td>'+
                  '<td align="center">'+ item.concepto +'</td>'+
                  '<td align="center">'+ item.descripcion +'</td>'+
                  '<td align="center">$ '+ item.precio_unitario +'</td>'+
                  '<td align="center">'+ item.alicuta_iva +' %</td>'+
                  '<td align="center">$ '+ item.importe_iva +'</td>'+
                  '<td align="center">$ '+ item.importe_siniva +'</td>'+
                  '<td align="center">$ '+ (parseFloat(item.importe_iva) + parseFloat(item.importe_siniva)) +'</td>'+
                  '<td align="center">'+ item.fecha_vencimiento_pago +'</td>'+
              '</tr>');
            });
            $('#label-items-21').html('$ '+ iva21_item.toFixed(2));
            $('#label-items-1050').html('$ '+ iva1050_item.toFixed(2));
            $('#label-items-total').html('$ '+ total_item.toFixed(2));

            table3 = $('#table-items-factura').DataTable({
              "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
              },
              buttons: [
                  {extend: 'copy'},
                  {extend: 'csv'},
                  {extend: 'excel', title: 'ExampleFile'},
                  {extend: 'pdf', title: 'ExampleFile'},
                  {extend: 'print',
                      customize: function (win) {
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                                  .addClass('compact')
                                  .css('font-size', 'inherit');
                      }
                  }
              ]});
              $('#modal-verDetalles').modal('show');
        }).fail(function(mgs){
            swal({
              title: "Error!",
              text: "Algo esta mal, por favor Intentelo nuevamente.",
              type: "error",
              confirmButtonText: "Cerrar"
            });
        });

    }
</script>

@stop
