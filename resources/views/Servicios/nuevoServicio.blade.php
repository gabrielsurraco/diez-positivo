@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
      <form class="form-group" action="/servicios/guardar" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="inp-id-servicio" value="@if(isset($servicios)) {{$servicios->id}} @endif">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nuevo Servicio</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                     <div class="col-lg-offset-2 col-lg-2">
                       <div class="form-group @if ($errors->has('inp-nombre')) has-error @endif">
                           <label class="control-label">Zona del Servicio:</label>
                           <select class="form-control" name="sel-zona">
                                <option value="URBANO">URBANO</option>
                                <option value="RURAL">RURAL</option>
                                <option value="ESTANDAR">ESTANDAR</option>
                                <option value="SIMETRICO">SIMETRICO</option>
                                <option value="COMERCIAL">COMERCIAL</option>
                                <option value="DEDICADO">DEDICADO</option>
                                <option value="PREMIUM">PREMIUM</option>
                           </select>
                       </div>
                     </div>
                     <div class="col-lg-4">
                       <div class="form-group @if ($errors->has('inp-nombre')) has-error @endif">
                           <label class="control-label">Nombre de Servicio:</label>
                           <input type="text" name="inp-nombre" class="form-control text-uppercase" style="">
                           @if ($errors->has('inp-nombre'))
                           <span class="help-block">{{ $errors->first('inp-nombre') }}</span>
                           @endif
                       </div>
                     </div>
                     <div class="col-lg-2">
                       <div class="form-group">
                           <label class="control-label">Localidad:</label>
                           <select class="select2_demo_1 form-control" name="sel-localidad" height="100%">
                                <option value="" selected>Seleccionar...</option>
                                @if(isset($localidades))
                                @foreach ($localidades as $value)
                                <option value="{{$value->id}}">{{$value->nombre}}</option>
                                @endforeach
                                @endif
                            </select>
                       </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>

                  <div class="row">
                    <div class="col-lg-offset-4 col-lg-2">
                      <div class="form-group">
                          <label>Alicuota IVA:</label>
                          <select class="form-control" name="sel-alicuota-iva">
                              <option value="0">0 %</option>
                              <option value="10.5">10.50 %</option>
                              <option value="21" selected>21 %</option>
                          </select>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label class="control-label">Precio:</label>
                          <input type="number" min="0" step="0.01" name="inp-precio" class="form-control" required>
                      </div>
                    </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="row">
                      <div class="col-lg-12 text-center">
                          <h4>Descripcion del Servicio</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-offset-2 col-lg-8">
                         <textarea name="inp-descripcion" rows="8" cols="80" class="form-control"></textarea>
                      </div>

                  </div>

                  <div class="hr-line-dashed"></div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </form>
    </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $(".select2_demo_1").select2({
          theme:'bootstrap'
      });
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($servicios))
      $('[name="inp-nombre"]').val('{{ $servicios->nombre }}');
      $('[name="sel-zona"]').val('{{ $servicios->zona }}');
      $('[name="sel-localidad"]').val('{{ $servicios->fk_localidad }}').trigger('change');;
      $('[name="sel-alicuota-iva"]').val('{{ $servicios->alicuota_iva }}');
      $('[name="inp-precio"]').val('{{ $servicios->precio }}');
      $('[name="inp-descripcion"]').val('{{ $servicios->descripcion }}');
      @endif

      //Mensaje cuando se guarda.
      @if (isset($status) && $status == 1)
        swal({
          title: "Exito!",
          text: "Servicio Guardado Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
            window.location.href = '/servicios/';
        });
      @endif
      //Mensaje cuando ocurre un error al guardar.
      @if (isset($status) && $status == 0)
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            icon: "error",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      @endif
  });

</script>

@stop
