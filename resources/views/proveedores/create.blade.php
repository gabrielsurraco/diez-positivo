@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    
    
    <!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

@stop
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-md-12 m-b-md">

            <div class="panel panel-default  ">
                <div class="panel-heading"><strong>Alta de Proveedores</strong></div>
                
                <div class="panel-body" >
                    <p>Realice una búsqueda previa para verificar si ya existe la razón social.</p>
                    <div class="hr-line-dashed"></div>
                    <div class="row" >
                        <div class="form-group col-md-3 col-lg-offset-3">
                            <select class="form-control buscar_tipo_doc">
                                  <option value="">Seleccione el tipo de Documento...</option>
                                  <option value="80">CUIT</option>
                                  <option value="87">CDI</option>
                                  <option value="89">LE</option>
                                  <option value="90">LC</option>
                                  <option value="91">CI Extranjera</option>
                                  <option value="96">DNI</option>
                                  <option value="94">Pasaporte</option>
                                  <option value="00">CI Policía Federal</option>
                                  <option value="30">Certificado de Migración</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                            {{ Form::text("buscar_doc", null, array("class"=>"form-control buscar_doc", "placeholder"=>"Número de documento")) }}
                            <span class="input-group-btn"><a class="btn btn-default verificar_persona"><i class="fa fa-search"></i></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="formulario hidden">
                    {!! Form::open(['url' => '/proveedores/guardar', 'method' => 'post','id' => 'form-cuenta']) !!}
                            <div class="row">
                                <div class="form-group col-md-12 col-md-offset-5">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_persona" value="f" checked=""> Física </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_persona" value="j"> Jurídica </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-2">
                                    {{ Form::label('codigoProveedor', 'Código del Proveedor') }}
                                    {{ Form::text("codigoProveedor", $cod_proveedor_sugerido, array("class"=>"form-control", "placeholder"=>"Cod Proveedor")) }}
                                    @if ($errors->has('codigoProveedor'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('codigoProveedor') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-2 tipo_doc">
                                    {{ Form::label('tipo_doc', 'Tipo Documento') }}
                                    <select name="tipo_doc" class="form-control">
                                          <option value="80">CUIT</option>
                                          <option value="87">CDI</option>
                                          <option value="89">LE</option>
                                          <option value="90">LC</option>
                                          <option value="91">CI Extranjera</option>
                                          <option value="96">DNI</option>
                                          <option value="94">Pasaporte</option>
                                          <option value="00">CI Policía Federal</option>
                                          <option value="30">Certificado de Migración</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    {{ Form::label('cuit', 'Número Doc. *') }}
                                    {{ Form::text("cuit", null, array("class"=>"form-control", "placeholder"=>"Número de documento")) }}
                                    @if ($errors->has('cuit'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('cuit') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="persona-fisica">
                                    <div class="form-group col-md-3">
                                        {{ Form::label('apellido', 'Apellido *') }}
                                        {{ Form::text("apellido", null, array("class"=>"form-control", "placeholder"=>"Apellido")) }}
                                        @if ($errors->has('apellido'))
                                        <span class="help-block text-danger">
                                                <strong>{{ $errors->first('apellido') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{ Form::label('nombre', 'Nombre *') }}
                                        {{ Form::text("nombre", null, array("class"=>"form-control", "placeholder"=>"Nombre")) }}
                                        @if ($errors->has('nombre'))
                                        <span class="help-block text-danger">
                                                <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="persona-juridica" style="display: none;">
                                    <div class="form-group col-md-4">
                                        {{ Form::label('razon_social', 'Razón Social *') }}
                                        {{ Form::text("razon_social", null, array("class"=>"form-control", "placeholder"=>"Razón Social")) }}
                                        @if ($errors->has('razon_social'))
                                        <span class="help-block text-danger">
                                                <strong>{{ $errors->first('razon_social') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                

                                <div class="form-group col-md-4">
                                    {{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_nacimiento" class="form-control" value="10/11/2013">
                                    </div>
                                    @if ($errors->has('fecha_nacimiento'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::email("email", null, array("class"=>"form-control", "placeholder"=>"Correo")) }}
                                    @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::label('email_2', 'Email Alternativo') }}
                                    {{ Form::email("email_2", null, array("class"=>"form-control", "placeholder"=>"Correo Alternativo")) }}
                                    @if ($errors->has('email_2'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::label('cat_fiscal', 'Categoría Fiscal') }}
                                    {{ Form::select("cat_fiscal", array('5' => 'CONSUMIDOR FINAL','4' => 'IVA EXENTO','9' => 'CLIENTE DEL EXTERIOR','6' => 'MONOTRIBUTISTA','1' => 'RESPONSABLE INSCRIPTO','13' => 'MONOTRIBUTISTA SOCIAL') ,null, array("class"=>"form-control")) }}
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <strong>Domicilio del Proveedor</strong>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group col-md-4">
                                                {{ Form::label('provincia', 'Provincia') }}
                                                {{ Form::select("provincia", array('1' => 'Misiones') ,null, array("class"=>"form-control")) }}
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('departamento', 'Departamento *') }}
                                                {{ Form::select("departamento",array('Seleccionar...')+$array_departamentos ,null, array("class"=>"form-control")) }}
                                                @if ($errors->has('departamento'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('departamento') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('localidad', 'Localidad *') }}
                                                {{ Form::select("localidad", array('Seleccionar...') ,null, array("class"=>"form-control")) }}
                                                @if ($errors->has('localidad'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('localidad') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('calle', 'Calle *') }}
                                                {{ Form::text("calle", null, array("class"=>"form-control", "placeholder"=>"Nombre de la calle", "list" => "calles")) }}
                                                <datalist id="calles">
                                                    @foreach($calles as $calle)
                                                        <option value="{{ $calle['calle'] }}">
                                                    @endforeach
                                                </datalist>
                                                @if ($errors->has('calle'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('calle') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('altura', 'Número') }}
                                                {{ Form::number("altura", "0", array("class"=>"form-control","value" => "0", "placeholder"=>"Altura de la Calle")) }}
                                                @if ($errors->has('altura'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('altura') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('piso', 'Piso') }}
                                                {{ Form::number("piso", "0", array("class"=>"form-control", "placeholder"=>"Piso del Depto.")) }}
                                                @if ($errors->has('piso'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('piso') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('nro_depto', 'Depto.') }}
                                                {{ Form::text("nro_depto", null, array("class"=>"form-control", "placeholder"=>"Depto.")) }}
                                                @if ($errors->has('nro_depto'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('nro_depto') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-2">
                                                {{ Form::label('cp', 'Código Postal') }}
                                                {{ Form::text("cp", null, array("class"=>"form-control", "placeholder"=>"Código Postal")) }}
                                                @if ($errors->has('cp'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('cp') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('link_mapa', 'Link Mapa') }}
                                                {{ Form::text("link_mapa", null, array("class"=>"form-control", "placeholder"=>"Link Mapa Geolocalización")) }}
                                                @if ($errors->has('link_mapa'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('link_mapa') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('latitud', 'Latidud') }}
                                                {{ Form::text("latitud", null, array("class"=>"form-control", "placeholder"=>"Latitud")) }}
                                                @if ($errors->has('latitud'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('latitud') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                {{ Form::label('longitud', 'Longitud') }}
                                                {{ Form::text("longitud", null, array("class"=>"form-control", "placeholder"=>"Longitud")) }}
                                                @if ($errors->has('longitud'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('longitud') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                {{ Form::label('observaciones_dom', 'Observaciones del Domicilio') }}
                                                {{ Form::text("observaciones_dom", null, array("class"=>"form-control", "placeholder"=>"Observaciones del domicilio...")) }}
                                                @if ($errors->has('observaciones_dom'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('observaciones_dom') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <strong>Teléfonos de Contacto</strong>
                                        </div>
                                        <div class="panel-body">
                                            <a class="btn btn-primary btn-sm m-b-sm add-tel"><i class="fa fa-plus"></i> Agregar</a>
                                            <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tipo</th>
                                                    <th>Compañia</th>
                                                    <th>Cod Pais</th>
                                                    <th>Cód. Área</th>
                                                    <th>Número</th>
                                                    <th>Horario Contacto</th>
                                                    <th>Observaciones</th>
                                                    <th>Acción</th>
                                                </tr>
                                                </thead>
                                                <tbody class="body-tel">
                                                <tr class="tr-ejemplo hidden">
                                                    <td style="width: auto;">1</td>
                                                    <td style="width: auto;">
                                                        <select name="tipo[]">
                                                            <option value="fijo">Fijo</option>
                                                            <option value="movil">Móvil</option>
                                                        </select>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <select name="compania[]">
                                                            <option value="personal">Personal</option>
                                                            <option value="claro">Claro</option>
                                                            <option value="movistar">Movistar</option>
                                                            <option value="telecom">Telecom</option>
                                                            <option value="otro">Otro</option>
                                                        </select>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="cod_pais[]" placeholder="54"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="cod_area[]" placeholder="376"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="number" name="numero[]" placeholder="4823900"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="text" name="horario_contacto[]" placeholder="7-10 hs"/>
                                                    </td>
                                                    <td style="width: auto;">
                                                        <input type="text" name="observaciones[]" placeholder="Iphone 8"/>
                                                    </td>
                                                    <td style="width: auto;"><a class="btn btn-danger btn-xs remove-tel"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    {{ Form::label('observaciones_per', 'Observaciones del Proveedor') }}
                                    {{ Form::textarea("observaciones_per", null, array("class"=>"form-control", "placeholder"=>"Observaciones del Cliente...","rows" => "5")) }}
                                    @if ($errors->has('observaciones_per'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('observaciones_per') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>

                        <!--</div>-->
                        <div class="modal-footer">
                            <a href="/proveedores"><button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button></a>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
    $(document).ready(function(){
        var cont = 0;
        $(".add-tel").click(function(){
            cont = cont + 1;
            var tr = $(".tr-ejemplo").clone(true);
            tr.removeClass("hidden");
            tr.removeClass("tr-ejemplo");
            tr.find("td:eq(0)").text(cont);
            $(".body-tel").append(tr);
        });
        
        $(".remove-tel").click(function(){
            $(this).parent().parent().empty();
        });
        
        $("input[name='tipo_persona']").change(function(evnt){
            var asd = $(this).val();
            if(asd === 'f'){
                $(".persona-fisica").show();
                $(".persona-juridica").hide();
                $(".tipo_doc").removeClass("col-md-4")
                $(".tipo_doc").addClass("col-md-2");
            }else if(asd === 'j'){
                $(".persona-fisica").hide();
                $(".persona-juridica").show();
                $(".tipo_doc").removeClass("col-md-2")
                $(".tipo_doc").addClass("col-md-4");
            }
        });

        $(".select2_demo_3").select2({
            placeholder: "Seleccione una Opción...",
            allowClear: true
        });

        $('.input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('#departamento').on('change',function(){
            if(this.value !== 0){
                var valor = this.value;
                $.ajax({
                   type: "GET",
                   url: "/localidades/get",
                   data: "depto="+valor,
                   success: function(data)
                   {
                       $('#localidad').not(":nth-child(1)").empty();
                        var a = JSON.parse(data);
                        $.each(a, function(key, value) {
                            $('#localidad')
                                .append($("<option></option>")
                                           .attr("value",value.id)
                                           .text(value.nombre));
                       });
                   },
                   error: function(msg){
                       alert(msg);
                   }

                });
            }

        });
        
        $(".verificar_persona").click(function(){
            var tipo_doc    = $(".buscar_tipo_doc").val();
            var nro_doc     = $(".buscar_doc").val();
            
            if(tipo_doc == ""){
                swal("Verificar!", "Seleccione el tipo de documento", "warning");
            }else{
                $.ajax({
                    type: "POST",
                    url: "/personas/buscar",
                    data: {tipo_doc: tipo_doc, nro_doc: nro_doc, _token: $("meta[name='csrf-token']").attr("content")},
                    success: function(msg){
                        if(msg.respuesta.existe == 0){
                            swal("Persona Inexistente!", "A continuación, cargar los datos pertinentes", "warning");
                            $(".formulario").removeClass('hidden');
                        }else{
                            if(msg.respuesta.es_proveedor == 1){
                                swal("Ya existe el PROVEEDOR!", "Verificar en el listado de PROVEEDORES", "error");
                            }else{
                                $(".formulario").addClass('hidden');
                                swal({
                                    title: "La persona ya se encuentra registrada.",
                                    text: "¿Desea darle de ALTA como PROVEEDOR?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                })
                                .then((willCreate) => {
                                    
                                    if (willCreate) {
//                                        obj = msg.respuesta.persona[0];
//                                        var arr = Object.keys(obj).map(function(k) { return obj[k] });
//                                        var arr = $.map(obj, function(el) { return el });
                                        $.ajax({
                                            type: "POST",
                                            url: "/personas/setRol",
                                            data: {_token: $("meta[name='csrf-token']").attr("content") , id_persona: msg.respuesta.persona[0].id , rol: "proveedor", persona: msg.respuesta.persona[0]},
                                            success: function(msg){
                                                
                                            },
                                            error: function(){
                                                
                                            }
                                        });
                                    }
                                });
                            }
                            
                        }
                        
                    },
                    error: function(msg){
                        alert(msg);
                    }
                });
            }
            
        });

    });
</script>
@stop
