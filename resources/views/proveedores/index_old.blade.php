@extends('layouts.backend')


@section('css')
<!-- Required Stylesheets -->
<link href="{{ asset('/bootstrap-treeview/src/css/bootstrap.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- chosen select -->
<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@stop

@section('content')
    @if (Session::has('flash_notification.message'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-{{ session('flash_notification.level') }} ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session('flash_notification.message') !!}
                </div>
            </div>
        </div>
    </div>

    @endif

    <!-- Inicio Contenido Principal -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <!-- Filtro de buusqueda -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Proveedores</h5>
                        <div class="ibox-tools">
                              <a href="/proveedores/nuevo"><button class="btn btn-primary btn-sm" ><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Proveedor</button></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="#" class="form-horizontal">
                            {{ csrf_field() }}
                            <p>Realice una búsqueda previa para mostrar resultados.</p>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('razon_social', 'Razón Social') }}
                                    {{ Form::text("razon_social", null , array("class"=>"form-control", "placeholder"=>"Razón Social")) }}
                                    @if ($errors->has('razon_social'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('razon_social') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        {{ Form::label('cuit', 'CUIT/L') }}
                                        {{ Form::text("cuit", null , array("class"=>"form-control", "placeholder"=>"Sin guiones")) }}
                                        <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-primary" style="margin-top: 23px;">Buscar</button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="tabla-resultados">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Proveedor</th>
                                        <th>CUIT/L</th>
                                        <th>Email</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>1</td>
                                        <td>
                                            1
                                        </td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td class="center" style="text-align: center;">
                                            <a class="btn btn-xs btn-default" href="#" title="Ver"><i class="fa fa-eye"></i></a>
                                            <a class="btn btn-xs btn-success" href="#" title="Editar"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger"  title="Cuenta Corriente"><i class="fa fa-money"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Proveedor</th>
                                        <th>CUIT/L</th>
                                        <th>Email</th>
                                        <th>Acción</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin Filtro de buusqueda -->
        <!-- Tabla de Resultados -->
        <div class="row">
              
        </div>
          <!-- Fin Tabla de Resultados -->
    </div>

    <!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Required Javascript -->
<script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Chonsen Select -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>

<script>
$(document).ready(function () {

//    $('.dataTables-example').DataTable({
//      "language": {
//        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
//      },
//      buttons: [
//          {extend: 'copy'},
//          {extend: 'csv'},
//          {extend: 'excel', title: 'ExampleFile'},
//          {extend: 'pdf', title: 'ExampleFile'},
//          {extend: 'print',
//              customize: function (win) {
//                  $(win.document.body).addClass('white-bg');
//                  $(win.document.body).css('font-size', '10px');
//
//                  $(win.document.body).find('table')
//                          .addClass('compact')
//                          .css('font-size', 'inherit');
//              }
//          }
//      ]
//      
//    });



    //Agregado Guille
    $(".chosen-select").chosen({max_selected_options: 5});
    $('#departamentos').on('change',function(){
        if(this.value !== 0){
            var valor = this.value;
            $.ajax({
               type: "GET",
               url: "/localidades/get",
               data: "depto="+valor,
               success: function(data)
               {
                   $('#localidad').not(":nth-child(1)").empty();
                    var a = JSON.parse(data);
                    $.each(a, function(key, value) {
                        $('#localidades')
                            .append($("<option></option>")
                                       .attr("value",value.id)
                                       .text(value.nombre));
                   });
                   $(".chosen-select").trigger("chosen:updated");
               },
               error: function(msg){
                   alert(msg);
               }

            });
        }
    });

    @if(isset($datosFiltrados) && $datosFiltrados['bs_departamento'] != "default")
    $('#departamentos option').each(function(){
          if ($(this).val() == {{$datosFiltrados['bs_departamento']}}){
              $(this).attr('selected','selected');
          }
    });
    @endif

    //al inicializar trae todas las localidades del departamento obera
    $.ajax({
       type: "GET",
       url: "/localidades/get",
       data: "depto=13",
       success: function(data)
       {
           $('#localidad').not(":nth-child(1)").empty();
            var a = JSON.parse(data);
            $.each(a, function(key, value) {
                $('#localidades')
                    .append($("<option></option>")
                               .attr("value",value.id)
                               .text(value.nombre));
           });
           @if(isset($datosFiltrados))
           var js_array = [{{ $datosFiltrados['bs_localidades']}}];
           $('#localidades option').each(function(){
                 if (jQuery.inArray(parseInt($(this).val()), js_array) != -1){
                     $(this).attr('selected','selected');
                 }
           });
           $(".chosen-select").trigger("chosen:updated");
           @endif
           $(".chosen-select").trigger("chosen:updated");
       },
       error: function(msg){
           alert(msg);
       }

    });

});
</script>
@stop
