@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
<style>
.select2-dropdown.increasedzindexclass {
  z-index: 999999;
}
</style>

@stop


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
              <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Proveedores en Sistema </h5>
                          <div class="ibox-tools">
                              <a class="collapse-link">
                                  <i class="fa fa-chevron-up"></i>
                              </a>
                              <a class="close-link">
                                  <i class="fa fa-times"></i>
                              </a>
                          </div>
                      </div>
                      <div class="ibox-content">
                        <div class="row">
                          <div class="col-lg-12 text-right">
                                  <button  type="button" class="btn btn-sm btn-primary" onclick="nuevoProveedor()"> <i class="fa fa-plus"></i> Nuevo Proveedor</button>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables" id="table-resultados">
                                    <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th>Tipo Doc</th>
                                        <th>Num Doc</th>
                                        <th>Tipo Persona</th>
                                        <th>Apellido</th>
                                        <th>Nombre</th>
                                        <th>Razon Social</th>
                                        <th>Departamento</th>
                                        <th>Localidad</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($proveedores as $valor)
                                      <tr>
                                        <td>{{$valor->codigo}}</td>
                                        <td>{{(new \App\Helper)->getTipoDoc($valor->persona->tipo_doc)}}</td>
                                        <td>{{$valor->persona->cuit}}</td>
                                        <td>{{$valor->persona->es_persona_fisica == 1 ? 'FISICA' : 'JURIDICA' }}</td>
                                        <td>{{$valor->persona->apellido}}</td>
                                        <td>{{$valor->persona->nombre}}</td>
                                        <td>{{$valor->persona->razon_social}}</td>
                                        <td>{{$valor->persona->domicilio->departamento->nombre}}</td>
                                        <td>{{$valor->persona->domicilio->localidad->nombre}}</td>
                                        <td align="center"></td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                          </div>

                      </div>
                  </div>
              </div>

          </div>
</div>
<!-- Fin Contenido Principal -->

<div class="modal inmodal" id="modal-nuevo-proveedor" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Alta De Proveedor</h4>
                <small class="font-bold">Alta de cliente a partir de una persona ya cargada en sistema.</small>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-xs-6">
                  <div class="form-group" >
                    <label>Seleccione Persona:</label>
                    <select class="select2_demo_1 form-control"  name="sel-persona">
                         <option></option>
                     </select>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label for="">Codigo Proveedor:</label>
                    <input type="text" class="form-control" id="inp-codigo-proveedor" placeholder="">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">CERRAR</button>
                <button type="button" id="btn-guardar-proveedor" class="btn btn-primary">GUARDAR</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.select2_demo_1').select2({
          theme:'bootstrap'
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
        /*serverSide: true,
        ajax: {
            url: '/ventas/clientes/paginacion',
            type: 'POST',
            data: {
              "_token": "{{ csrf_token() }}",
            }
        },
        "columns": [
            { "data": "codigo" },
            { "data": "cliente" },
            { "data": "cuit" },
            { "data": "email" },
            { "data": "telefono" },
            { "data": "domicilio" },
            { "data": "estado" },
            { "data": "accion" }
        ]*/
      });


      //buscar una persona de la base de datos.
      $('[name="sel-persona"]').select2({
        ajax: {
          method: "POST",
          url: '/personas/jsonSearchPersonaNotProvee',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Buscar Persona",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap',
        dropdownParent: $('#modal-nuevo-proveedor'),
        dropdownCssClass: "increasedzindexclass",
        width: '100%'
      });
  });

  $('#btn-guardar-proveedor').on('click', function(event) {
      event.preventDefault();
      $.ajax({
        url: '/proveedores/guardarProveedor',
        type: 'post',
        dataType: 'json',
        data: {"_token": "{{ csrf_token() }}",
               idPersona : $('[name="sel-persona"]').val(),
               codigo    : $('#inp-codigo-proveedor').val(),
              }
      })
      .done(function() {
        swal("Exito!", "Proveedor Creado Correctamente.", "success").then((result) => { location.reload(); });        
      })
      .fail(function() {
        swal("Error", "Algo esta mal, intentelo de nuevo mas tarde.", "error");
      });

  });

  function nuevoProveedor(){
    $('#modal-nuevo-proveedor').modal('show');
  }



</script>

@stop
