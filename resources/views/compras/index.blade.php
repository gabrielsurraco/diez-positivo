@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css" rel="stylesheet') }}">
<link href="{{ asset('/backend/css/style.css" rel="stylesheet') }}">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
              <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Listado de Ordenes de Compra </h5>
                          <div class="ibox-tools">
                              <a class="collapse-link">
                                  <i class="fa fa-chevron-up"></i>
                              </a>
                              <a class="close-link">
                                  <i class="fa fa-times"></i>
                              </a>
                          </div>
                      </div>
                      <div class="ibox-content">
                          <div class="row">
                              <form class="form-group" action="buscarFiltrosOrdenCompra" method="post">
                              {{ csrf_field() }}
                              <div class="col-sm-2">
                                <div class="form-group" id="data_5">
                                    <label class="font-normal">Nro Orden:</label>
                                    <input type="text" placeholder="" name="nro-orden" value="@if(isset($old)) {{$old['nro-orden']}} @endif" class="form-control">
                                </div>
                              </div>
                              <div class="col-sm-3 m-b-xs">
                                <div class="form-group" id="data_5">
                                    <label class="font-normal">Proveedor:</label>
                                    <select class="input-sm form-control input-s-sm select2" name="sel-proveedor">
                                      <option></option>
                                      @foreach($proveedores as $valor)
                                      <option value="{{$valor->id}}">{{$valor->codigo}} - @if($valor->persona->apellido != null) {{$valor->persona->nombre}} {{$valor->persona->apellido}} @else {{$valor->persona->razon_social}} @endif</option>
                                      @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-4 m-b-xs">
                                <div class="form-group" id="data_5">
                                    <label class="font-normal">Seleccionar Fechas:</label>
                                    <div class="input-daterange input-group" id="datepicker" style="margin-top:2px;">
                                        <input type="text" class="input-sm form-control" data-fecha="true" name="start" value="@if(isset($old)) {{$old['start']}} @endif">
                                        <span class="input-group-addon">hasta</span>
                                        <input type="text" class="input-sm form-control" data-fecha="true" name="end" value="@if(isset($old)) {{$old['end']}} @endif">
                                    </div>
                                </div>
                              </div>
                              <div class="col-sm-1">
                                <button type="submit" name="btn-search" data-toggle="tooltip" data-placement="bottom" title="Buscar Segun Los Filtros Seleccionados." class="btn btn-primary" style="margin-top:22px;"><span class="fa fa-search"></span></button>
                              </div>
                              <div class="col-sm-2 text-right">
                                      <a href="/compras/nuevaOrdenCompra" type="button" class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> Nueva Orden</a>
                              </div>
                              </form>
                          </div>
                          <div class="table-responsive">
                              <table class="table table-striped" id="table-resultados">
                                  <thead>
                                  <tr>
                                      <th>Nro</th>
                                      <th>Fecha</th>
                                      <th>Proveedor</th>
                                      <th>Fecha Entrega Aprox. </th>
                                      <th>Importe Total</th>
                                      <th>Estado</th>
                                      <th>Acciones</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      @foreach ($ordenCompra as $valor)
                                      <?php
                                      //Buscamos el id de la orden de compra dentro del array con las sumas de los items para obtener el importe total
                                      $int = 0;
                                      if (count($importes) > 0){
                                          $int = array_search($valor->id, array_column($importes, 'fk_orden_de_compra'));
                                      }
                                      ?>
                                      <tr>
                                          <td>{{ $valor->id }}</td>
                                          <td>{{ $valor->fecha }}</td>
                                          @if($valor->proveedor->persona->apellido != '')
                                          <td>{{ $valor->proveedor->persona->apellido }} {{ $valor->proveedor->persona->nombre }}</td>
                                          @else
                                          <td>{{ $valor->proveedor->persona->razon_social }}</td>
                                          @endif
                                          <td>{{ $valor->fecha_entrega_producto_estimada }}</td>
                                          <td>$ {{ $importes[$int]->importe }}</td>
                                          <td>
                                            <?php
                                            switch($valor->estado){
                                              case "borrador":
                                                  echo '<span class="label label-warning">BORRADOR</span>';
                                                  break;
                                              case "solicitado":
                                                  echo '<span class="label label-primary">SOLICITADO</span>';
                                                  break;
                                              case "en transito":
                                                  echo '<span class="label label-info">EN TRANSITO</span>';
                                                  break;
                                              case "recibidos":
                                                  echo '<span class="label label-success">RECIBIDO</span>';
                                                  break;
                                              }
                                            ?>
                                          </td>
                                          <td>
                                            @if($valor->estado == 'borrador')
                                            <a href="/compras/editOrdenCompra/{{ $valor->id }}" class="btn btn-warning btn-xs btn-bitbucket"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if($valor->estado != 'borrador')
                                            <a href="/compras/printOrdenCompra/{{ $valor->id }}" class="btn btn-primary btn-xs btn-bitbucket"><i class="fa fa-print"></i></a>
                                            @endif
                                            <a onClick="deleteOrdenCompra({{ $valor->id }})" class="btn btn-danger btn-xs btn-bitbucket"><i class="fa fa-remove"></i></a>
                                          </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                              </table>
                          </div>
                          <div class="text-right">
                          {{ $ordenCompra->links() }}
                          </div>
                      </div>
                  </div>
              </div>

          </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.select2').select2({
          theme:'bootstrap',
          placeholder: "Proveedor"
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();
  });

  function deleteOrdenCompra(id){
      swal({
        title: "Esta Seguro?",
        text: "Esta a punto de eliminar esta orden de venta numero "+id,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
              method: "POST",
              url: "/compras/deleteOrdenCompra",
              data: { "_token": "{{ csrf_token() }}",
                      id: id,
                    }
            })
              .done(function( msg ) {
                swal.stopLoading();
                swal("Eliminado!", {
                  icon: "success",
                }).then((value) => {location.reload()});
            })
              .fail(function(mgs){
                swal({
                  title: "Error!",
                  text: "Algo esta mal, por favor Intentelo nuevamente.",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            });
        } else {
          swal("No se eliminó nada!");
        }
      });
  }

</script>

@stop
