@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nueva Orden de compra</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                     <div class="col-lg-offset-1 col-lg-3">
                       <div class="form-group">
                           <label>Fecha:</label>
                           <div class="input-group date">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha" data-fecha="true" class="form-control">
                           </div>
                       </div>
                     </div>
                     <div class="col-lg-4">
                       <div class="form-group" id="data_5">
                           <label class="font-normal">Proveedor:</label>
                           <select class="input-sm form-control input-s-sm select2" name="sel-proveedor">
                             <option></option>
                         </select>
                       </div>
                     </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                           <label>Fecha de Entrega Aprox. :</label>
                           <div class="input-group date">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-entrega-aprox" class="form-control" data-fecha="true">
                           </div>
                       </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>Caracteristicas del item</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Cantidad:</label>
                          <div class="input-group date">
                              <input type="number" min="0" name="cantidad-item" class="form-control" placeholder="0.00">
                          </div>
                      </div>
                    </div>

                    <div class="col-lg-8">
                      <div class="form-group">
                          <label>Articulo:</label>
                          <!--<input type="text" style="text-transform:uppercase" name="descripcion-item" class="form-control"> -->
                          <select class="select2_demo_1 form-control" name="descripcion-item" height="100%">
                               <option></option>
                           </select>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Importe:</label>
                          <div class="input-group m-b">
                              <span class="input-group-addon">$</span><input type="number" min="0" name="importe-item" class="form-control" placeholder="0.00">
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-success btn-outline" onclick="agregarNuevoItem();" type="button"><i class="fa fa-plus"></i><span class="bold"> Agregar</span></button>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-warning btn-sm btn-outline" onclick="borrarItemSeleccionado();" type="button"><i class="fa fa-trash-o"></i><span class="bold"> Borrar Seleccionados</span></button>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <div class="table-responsive">
                              <table class="table table-striped" id="table-items">
                                  <thead>
                                  <tr>
                                      <th width="5%" style="text-align:center"><input type="checkbox" class="i-checks" data-index="all" id="check-all"></th>
                                      <th width="15%" style="text-align:center">Cantidad </th>
                                      <th width="50%" style="text-align:center">Descripción </th>
                                      <th width="15%" style="text-align:center">Precio Unitario</th>
                                      <th width="15%" style="text-align:center">Importe</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <tr style="background-color: transparent;">
                                        <td colspan="5"><div class="alert alert-info text-center">
                                            No hay datos.
                                        </div></td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-10 text-right">
                        <h3><strong>Total:</strong></h3>
                    </div>
                    <div class="col-lg-2 text-right" id="totalImporte">
                        <h3><strong> $ 0.00</strong></h3>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-primary" onclick="save(1);" type="button"><i class="fa fa-check"></i> Finalizar</button>
                        <button class="btn btn-warning" onClick="save(0);" type="button"><i class="fa fa-save"></i> Guardar Borrador</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.select2').select2({
          placeholder: "Seleccione...",
          theme:'bootstrap'
      });
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($doc))
      var s = '<?php echo json_encode($doc); ?>';
      var obj = JSON.parse(s);
      for (var i=0; i < Object.keys(obj).length ; i++){
          var itemCompra = {
                             id               : obj[i].id,
                             cantidad         : obj[i].cantidad,
                             articulo_id      : obj[i].articulo.id,
                             articulo_desc    : obj[i].articulo.marca + ' - ' +obj[i].articulo.modelo,
                             importe          : obj[i].precio
                           }

          globalItems.push(itemCompra);
      }
      $('[name="inp-fecha"]').val('{{ $oc->fecha }}');
      $('[name="inp-entrega-aprox"]').val('{{ $oc->fecha_entrega_producto_estimada }}');
      globalIdOdenCompra = {{ $oc->id }};
      refrescarTabla();
      @endif

      $('[name="descripcion-item"]').select2({
        ajax: {
          method: "POST",
          url: '/stock/articulos/jsonSearch',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Busque el articulo a solicitar.",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap'
      });

      $('[name="sel-proveedor"]').select2({
        ajax: {
          method: "POST",
          url: '/proveedores/jsonSearchProveedor',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Buscar Proveedor.",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap'
      });
  });

  //configuraciones para crear orde de compra
  var globalItems = new Array();
  var globalIdOdenCompra = null;

  function agregarNuevoItem(){
       //cargamos los items en el array.
       var itemCompra = {
                          id               : globalItems.length + 1,
                          cantidad         : $('[name="cantidad-item"]').val(),
                          articulo_id      : $('[name="descripcion-item"]').val(),
                          articulo_desc    : $('[name="descripcion-item"]').text().toUpperCase(),
                          importe          : $('[name="importe-item"]').val()
                        }

       globalItems.push(itemCompra);
       $('[name="descripcion-item"]').val('');
       $('[name="importe-item"]').val('');
       $('[name="cantidad-item"]').val('').focus();
       refrescarTabla();
  }

  //esta funcion refrescara la tabla.
  function refrescarTabla(){
      var totalImporte = 0;
      $('#table-items tbody').html('');
      if (globalItems.length == 0){
        $('#table-items tbody').html('<tr style="background-color: transparent;">'+
          '<td colspan="5"><div class="alert alert-info text-center">No hay datos.</div></td>'+
        '</tr>');
      }else{
        var cont = 0;
        globalItems.forEach(function(item) {
        //agregamos los items a la tabla.
        var importeConj = item.cantidad * parseFloat(item.importe).toFixed(2);
        $('#table-items tbody').append('<tr>'+
                                        '<td><input type="checkbox" class="i-checks" data-index="'+ item.id +'" name="input[]"></td>'+
                                        '<td align="center">'+ item.cantidad +'</td>'+
                                        '<td>'+ item.articulo_desc +'</td>'+
                                        '<td align="center">$ '+ parseFloat(item.importe).toFixed(2) +'</td>'+
                                        '<td align="center">$ '+ parseFloat(importeConj).toFixed(2) +'</td>'+
                                        '</tr>');
        totalImporte += importeConj;
        cont++;
        });
      }
      $('#totalImporte').html('<h3><strong> $ '+ parseFloat(totalImporte).toFixed(2) +'</strong></h3>');

      $('[name="input[]"]').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });
  }

  function borrarItemSeleccionado(){
    var elementDelete = [];
    $('[name="input[]"]').each(function(){
       if($(this)[0].checked){
         elementDelete.push(parseInt($(this).data('index')));
       }
     });

     for(var i = 0; i < globalItems.length; i++) {
          var obj = globalItems[i];
          if(elementDelete.indexOf(obj.id) !== -1) {
              globalItems.splice(i, 1);
              i--;
          }
      }
     $('#check-all').iCheck('uncheck');
     refrescarTabla();
  }

  function save(valor){
    $.ajax({
      method: "POST",
      url: "/compras/guardarOrdenCompra",
      data: { "_token": "{{ csrf_token() }}",
              idOrdenCompra   : globalIdOdenCompra,
              fecha          : $('[name="inp-fecha"]').val(),
              proveedor      : $('[name="sel-proveedor"]').val(),
              entrega_aprox  : $('[name="inp-entrega-aprox"]').val(),
              estado         : valor == 1 ? 'solicitado' : 'borrador',
              globalItems    : JSON.stringify(globalItems)
            }
    })
      .done(function( msg ) {
        swal({
          title: "Exito!",
          text: "Orden de Compra Gurdada Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
          if(valor){
              window.location = '/compras/ordenCompra';
          }else{
              globalIdOdenCompra = msg.idOrdenCompra;
          }
        });
      })
      .fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          icon: "error",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });

  }

  //Key press -----------------------------------------------------------------
  $('[name="cantidad-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="descripcion-item"]').focus();
      }
  });
  $('[name="descripcion-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="importe-item"]').focus();
      }
  });
  $('[name="importe-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          agregarNuevoItem();
      }
  });
  //Fin Key press --------------------------------------------------------------
  //funcion del boton seleccionar todos los elementos.
  $('#check-all').on('ifChanged', function(event){
    if(event.target.checked){
      $('[name="input[]"]').each(function(){
         $(this).iCheck('check');
       });
    }else{
      $('[name="input[]"]').each(function(){
         $(this).iCheck('uncheck');
       });
    }
  });

</script>

@stop
