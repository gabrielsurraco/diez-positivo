@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css" rel="stylesheet') }}">
<link href="{{ asset('/backend/css/style.css" rel="stylesheet') }}">
<style>
@page {
  size: A4;
}
@media print {
        #watermark {
			      display: block;
            position: fixed;
            top: 50%;
            left: 45%;
            z-index: -10;
			      opacity:0.15;
            -webkit-transform: rotate(-10deg); /* Chrome, Safari, Opera */
            transform: rotate(-10deg);
        }
}
</style>
@endsection


@section('content')
  <div class="hidden-print">
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right"><i class="fa fa-print"></i></span>
                    <h5>Generando Impresión</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">
                      <div class="col-sm-12 text-center">
                        <i class="fa fa-refresh fa-4x fa-spin"></i>
                          <p>Aguarde Por Favor...</p>
                      </div>
                  </div>

                </div>
            </div>
        </div>
      </div>
  </div>

      <?php
      $cant_pag = ceil(count($doc)  / 35);
      $current_page = 1;
      $doc_position = 0;
      $totalImportes = 0;
      ?>
      @for ($cp = 1; $cp <= $cant_pag; $cp++)
      <div style="page-break-inside: avoid; margin-left:5px; margin-right:5px;" class="visible-print-block">
      <div id="watermark"> <img id="imagenMark" src="{{ asset('/img/diez.jpg') }}" class="img-responsive"></div>
          <div class="row">
            <div class="col-xs-12" style="outline: 1px solid #000; margin-top:50px;">
                <div class="col-xs-4 text-center">
                    <div class="col-xs-offset-1 col-xs-11" style="margin:5px;">
                      <img id="imagenLogo" src="{{ asset('/img/diez.jpg') }}" class="img-responsive">
                      </div>
                      <p style="font-size:14px; font-weight:bold;"><span style="font-size:14px; font-weight:bold;"></span>DIEZ POSITIVO</p>
                      <p style="font-size:10px;"><span style="font-size:10px;"></span>INTERNET - INFORMATICA</p>
                  </div>
                  <div class="col-xs-offset-1 col-xs-1" style="outline: 1px solid #000; text-align:center;">
                    <span style="font-size:36px;">X</span>
                  </div>
                  <div class="col-xs-offset-1 col-xs-5">
                      <label style="font-size:24px; font-weight:bold;">ORDEN DE COMPRA</label>
                      <p><span>Orden Compra Nro: </span><mark><?php echo str_pad(1, 4, '0', STR_PAD_LEFT).'-'.str_pad($oc->id, 8, '0', STR_PAD_LEFT); ?></mark></p>
                      <p><span>Fecha Creación: </span> <mark>{{$oc->fecha}}</mark></p>
                      <p><span>Fecha Entrega Aprox: </span> <mark>{{$oc->fecha_entrega_producto_estimada}}</mark></p>
                  </div>
              </div>
          </div>
          <!-- Datos Cliente -->
          <div class="row">
            <div class="col-xs-12" style="outline: 1px solid #000; margin-top:5px;">
                <div class="col-xs-12 text-center">
                    <h6>DATOS DEL PROVEEDOR</h6>
                </div>

                  <div class="row" style="font-size:9px;">
                      <div class="col-xs-6">
                          <p><span>PROVEEDOR: </span><mark>{{ $oc->proveedor->persona->apellido }} {{ $oc->proveedor->persona->nombre }} {{ $oc->proveedor->persona->razon_social }}</mark></p>
                      </div>
                  </div>
                  <div class="row" style="font-size:9px;">
                      <div class="col-xs-6">
                          <p><span>TIPO DOC.: </span><mark>{{(new \App\Helper)->getTipoDoc($oc->proveedor->persona->tipo_doc)}}</mark></p>
                      </div>
                      <div class="col-xs-6">
                          <p><span>NUMERO DOC.: </span><mark>{{ $oc->proveedor->persona->cuit }}</mark></p>
                      </div>
                  </div>
                  <div class="row" style="font-size:9px;">
                    <div class="col-xs-6">
                      <p><span>DIRECCION: </span> <mark>{{$oc->proveedor->persona->domicilio->calle}} {{$oc->proveedor->persona->domicilio->altura}} ({{$oc->proveedor->persona->domicilio->localidad->nombre}} - {{$oc->proveedor->persona->domicilio->provincia->nombre}})</mark></p>
                    </div>
                  </div>
                  <div class="row" style="font-size:9px;">
                      <div class="col-xs-4">
                          @if(count($oc->proveedor->persona->telefonos) > 0)
                          <p><span>TELEFONO 1: </span><mark>{{$oc->proveedor->persona->telefonos[0]->area}}-{{$oc->proveedor->persona->telefonos[0]->numero}}</mark></p>
                          @endif
                      </div>
                      <div class="col-xs-4">
                          @if(count($oc->proveedor->persona->telefonos) > 1)
                          <p><span>TELEFONO 2: </span><mark>{{$oc->proveedor->persona->telefonos[1]->area}}-{{$oc->proveedor->persona->telefonos[1]->numero}}</mark></p>
                          @endif
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-xs-12" style="outline:0px solid #000; padding:0px; margin-top:5px;">
              <table width="100%" border="1">
                  <tr style="font-size:14px;">
                    <th width="20%" style="text-align:center;">CANTIDAD</th>
                    <th style="text-align:center;" width="40%">DETALLE</th>
                    <th style="text-align:center;" width="20%">IMPORTE UNIT.</th>
                    <th style="text-align:center;"width="20%">TOTAL</th>
                  </tr>
                  <tbody style="font-size:12px;">
                    @for ($i = $doc_position; $i < count($doc); $i++)
                    <tr>
                    <td style="text-align:center;" valign="top">{{ $doc[$i]->cantidad }}</td>
                    <td style="max-width: 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"> {{ $doc[$i]->articulo->marca }} - {{ $doc[$i]->articulo->modelo }}</td>
                    <td style="text-align:center;" valign="top">$ {{ $doc[$i]->precio }}</td>
                    <td style="text-align:center;" valign="top">$ {{ sprintf("%01.2f",  round(((float)$doc[$i]->cantidad * (float)$doc[$i]->precio), 2)) }}</td>
                    </tr>
                    <?php
                     $totalImportes += round(((float)$doc[$i]->cantidad * (float)$doc[$i]->precio), 2);
                     if ($i == (35*$current_page)){
                       $current_page++;
                       $doc_position = $i;
                       break;
                     }
                     ?>
                     @endfor
                  </tbody>

                   @if ($cp == $cant_pag)
                   <tfoot>
                      <tr>
                      <td style=" border-right:hidden;"></td>
                      <td style=" border-right:hidden;"></td>
                        <td style="font-size:18px; font-weight:bold; text-align:right;">TOTAL:</td>
                        <td style="font-size:18px; font-weight:bold; text-align:center;">$ {{ sprintf("%01.2f", $totalImportes) }}</td>
                      </tr>
                    </tfoot>
                    @endif
                  </table>
              </div>
          </div>
          <div class="text-right p-lg">
             <hr class="hr-line-solid"></hr>
              <div>
                  10+ Diez Positivo Internet - Informática - <strong>Pagina {{$cp}} de {{$cant_pag}} </strong>
              </div>
          </div>
       </div>
       @endfor
@endsection

@section('javascript')

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      window.print();
  });

  var beforePrint = function() {
  	console.log('Functionality to run before printing.');
  };

  var afterPrint = function() {
  	console.log('Functionality to run after printing');
    window.location.href = '/compras/ordenCompra';
  };


  if (window.matchMedia) {
  	var mediaQueryList = window.matchMedia('print');
  	mediaQueryList.addListener(function(mql) {
  		if (mql.matches) {
        beforePrint();
  		} else {
        afterPrint();
  		}
  	});
  }

  window.onbeforeprint = beforePrint;
  window.onafterprint = afterPrint;
</script>

@stop
