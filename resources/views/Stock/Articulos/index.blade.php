@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css" rel="stylesheet') }}">
<link href="{{ asset('/backend/css/style.css" rel="stylesheet') }}">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
              <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Listado de Articulos </h5>
                          <div class="ibox-tools">
                              <a class="collapse-link">
                                  <i class="fa fa-chevron-up"></i>
                              </a>
                              <a class="close-link">
                                  <i class="fa fa-times"></i>
                              </a>
                          </div>
                      </div>
                      <div class="ibox-content">
                        <div class="row">
                          <div class="col-lg-12 text-right">
                                  <a href="/stock/articulos/nuevo" type="button" class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> Nuevo Articulo</a>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables" id="table-resultados">
                                    <thead>
                                    <tr>
                                        <th>Cod</th>
                                        <th>Marca - Modelo</th>
                                        <th>Frecuencia</th>
                                        <th>Porc. Gan.</th>
                                        <th>Alicuota IVA</th>
                                        <th>Precio</th>
                                        <th>Unidad</th>
                                        <th>Moneda</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($articulos as $valor)
                                        <tr>
                                            <td>{{ $valor->codigo_interno }}</td>
                                            <td>{{ $valor->marca }} {{ $valor->modelo }}</td>
                                            <td>{{ $valor->frecuencia }}</td>
                                            <td>{{ $valor->porcentaje_ganancia }} %</td>
                                            <td>{{ $valor->alicuota_iva }} %</td>
                                            <td>$ {{ $valor->precio }}</td>
                                            <td>{{ (new \App\Helper)->getUnidadfromID($valor->unidad) }}</td>
                                            <td>{{ $valor->moneda }}</td>
                                            <td>
                                              <a href="/stock/articulos/edit/{{ $valor->id }}" class="btn btn-warning btn-xs btn-bitbucket"><i class="fa fa-pencil"></i></a>
                                              <a onClick="deleteArticulo({{ $valor->id }})" class="btn btn-danger btn-xs btn-bitbucket"><i class="fa fa-remove"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.select2').select2({
          theme:'bootstrap',
          placeholder: "Proveedor"
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
        /*serverSide: true,
        ajax: {
            url: '/ventas/clientes/paginacion',
            type: 'POST',
            data: {
              "_token": "{{ csrf_token() }}",
            }
        },
        "columns": [
            { "data": "codigo" },
            { "data": "cliente" },
            { "data": "cuit" },
            { "data": "email" },
            { "data": "telefono" },
            { "data": "domicilio" },
            { "data": "estado" },
            { "data": "accion" }
        ]*/
      });
  });

  function deleteArticulo(id){
      swal({
        title: "Esta Seguro?",
        text: "Esta a punto de eliminar este articulo?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
              method: "POST",
              url: "/stock/articulos/delete",
              data: { "_token": "{{ csrf_token() }}",
                      id: id,
                    }
            })
              .done(function( msg ) {
                swal.stopLoading();
                swal("Eliminado!", {
                  icon: "success",
                }).then((value) => {location.reload()});
            })
              .fail(function(mgs){
                swal({
                  title: "Error!",
                  text: "Algo esta mal, por favor Intentelo nuevamente.",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            });
        } else {
          swal("No se eliminó nada!");
        }
      });
  }

</script>

@stop
