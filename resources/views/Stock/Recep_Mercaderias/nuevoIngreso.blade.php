@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
      <form class="form-group" action="/stock/recep_mercaderias/ingreso" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="inp-id-articulo" value="@if(isset($articulos)) {{$articulos->id}} @endif">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ingreso de Mercaderias</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Fecha:</label>
                           <div class="input-group date">
                             <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="inp-fecha" data-fecha="true" value="">
                           </div>
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Nro Remito:</label>
                           <input type="text" name="inp-nro-remito" class="form-control" required style="text-transform:uppercase">
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Nro de Factura:</label>
                           <div class="input-group"><input type="text" class="form-control" required style="text-transform:uppercase" name="inp-nro-factura"> <span class="input-group-btn">
                           <button type="button" data-toggle="tooltip" data-placement="top" title="Asignar Factura a Remitos" onclick="openModal();" class="btn btn-primary"><i class="fa fa-caret-square-o-up"></i></button> </span></div>
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Nro Orden de Compra:</label>
                           <div class="input-group"><input type="text" class="form-control" name="inp-idOrdenCompra" required style="text-transform:uppercase"> <span class="input-group-btn">
                           <button type="button" onclick="openModal();" class="btn btn-primary"><i class="fa fa-search"></i></button> </span></div>
                       </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>Detalle ingreso al inventario</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <table class="table table-bordered" id="table-articulos">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="35">Producto (Marca - Modelo)</th>
                                <th width="10%">Cantidad</th>
                                <th width="50%">Serie</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </form>
    </div>
</div>
<!-- Fin Contenido Principal -->

<!-- Modal para listar las ordendes de compra -->
<div class="modal inmodal fade" id="modal-orden-compra" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Ordenes de Compras</h4>
              <small class="font-bold">Este listado mostrará las ordenes de compras en estado SOLICITADO.</small>
          </div>
          <div class="modal-body">
              <div class="row">
                 <div class="col-md-12">
                   <div class="table-responsive">
                       <table class="table table-striped table-hover dataTables" id="table-resultados">
                           <thead>
                           <tr>
                               <th>Nro</th>
                               <th>Fecha</th>
                               <th>Proveedor</th>
                               <th>Fecha Entrega Aprox. </th>
                               <th>Importe Total</th>
                               <th>Estado</th>
                               <th>Acciones</th>
                           </tr>
                           </thead>
                           <tbody>

                           </tbody>
                       </table>
                   </div>
                 </div>
              </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
  </div>
</div>
<!-- fin modal listar ordenes de compra  -->


@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $(".select2_demo_1").select2({
          theme:'bootstrap'
      });
      $('[data-toggle="tooltip"]').tooltip();
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($articulos))
      $('[name="inp-codigo"]').val('{{ $articulos->codigo_interno }}');
      $('[name="inp-marca"]').val('{{ $articulos->marca }}');
      $('[name="inp-modelo"]').val('{{ $articulos->modelo }}');
      $('[name="inp-frecuencia"]').val('{{ $articulos->frecuencia }}');
      $('[name="inp-porc-ganancia"]').val('{{ $articulos->porcentaje_ganancia }}');
      $('[name="inp-precio"]').val('{{ $articulos->precio }}');
      $('[name="sel-alicuota-iva-"]').val('{{ $articulos->alicuota_iva }}');
      $('[name="sel-unidad"]').val('{{ $articulos->unidad }}');
      $('[name="sel-moneda"]').val('{{ $articulos->moneda }}');
      @endif

      //Mensaje cuando se guarda.
      @if (isset($status) && $status == 1)
        swal({
          title: "Exito!",
          text: "Articulo Guardado Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
            window.location.href = '/stock/articulos';
        });
      @endif
      //Mensaje cuando ocurre un error al guardar.
      @if (isset($status) && $status == 0)
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            icon: "error",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      @endif
  });

var table;
function openModal(){
    $.ajax({
      method: "POST",
      url: "/compras/jsonListOCSolicitadas",
      data: { "_token": "{{ csrf_token() }}"},
      dataType: 'json'
    }).done(function( msg ) {
        $('#table-resultados tbody').html('');
        msg.ordenCompra.forEach(function (item){
          var index = searchIndex(item.id, msg.importes);
          switch(item.estado) {
              case 'solicitado':
                  item.estado = '<span class="label label-primary">SOLICITADO</span>';
                  break;
              case 'borrador':
                  item.estado = '<span class="label label-warning">BORRADOR</span>';
                  break;
              case 'en transito':
                  item.estado = '<span class="label label-info">EN TRANSITO</span>';
                  break;
              case 'recibidos':
                  item.estado = '<span class="label label-success">RECIBIDO</span>';
                  break;
          }
          $('#table-resultados tbody').append('<tr>'+
              '<td>'+ item.id +'</td>'+
              '<td>'+ item.fecha +'</td>'+
              '<td align="center">'+ item.fk_proveedor +'</td>'+
              '<td align="center">'+ item.fecha_entrega_producto_estimada +'</td>'+
              '<td align="center">$ '+ parseFloat(msg.importes[index].importe).toFixed(2) +'</td>'+
              '<td align="center">'+ item.estado +'</td>'+
              '<td align="center">'+
                '<a  class="btn btn-success btn-xs btn-bitbucket" onclick="seleccionarOC('+ item.id +');" ><i data-toggle="tooltip" data-placement="top" title="Seleccionar Orden de Compra" class="fa fa-chevron-right"></i></a>'+
              '</td>'+
          '</tr>');
        });
        table = $('.dataTables').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
          },
          buttons: [
              {extend: 'copy'},
              {extend: 'csv'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},
              {extend: 'print',
                  customize: function (win) {
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
                  }
              }
          ]});
          $('[data-toggle="tooltip"]').tooltip();
          $('#modal-orden-compra').modal('show');
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });

}

function seleccionarOC(id){
    $('[name="inp-idOrdenCompra"]').val(id);
    $('#modal-orden-compra').modal('hide');
    table.destroy();
    $.ajax({
      method: "POST",
      url: "/compras/jsonListOrdenCompraWithDetalle",
      data: { "_token": "{{ csrf_token() }}", "idOrdenCompra": id},
      dataType: 'json'
    }).done(function( msg ) {
        console.log(msg);

        $('#table-articulos tbody').html('');
        var count = 1;
        msg.detalle.forEach(function (item){
          /*var index = searchIndex(item.id, msg.importes);
          switch(item.estado) {
              case 'solicitado':
                  item.estado = '<span class="label label-primary">SOLICITADO</span>';
                  break;
              case 'borrador':
                  item.estado = '<span class="label label-warning">BORRADOR</span>';
                  break;
              case 'en transito':
                  item.estado = '<span class="label label-info">EN TRANSITO</span>';
                  break;
              case 'recibidos':
                  item.estado = '<span class="label label-success">RECIBIDO</span>';
                  break;
          }*/
          $('#table-articulos>tbody').append('<tr>'+
              '<td style="display:none;"><input type="hidden" class="form-control" name="inp-'+ count +'-idArticulo[]" value="'+ item.id +'"></td>'+
              '<td style="vertical-align:middle">'+count+'</td>'+
              '<td style="vertical-align:middle">'+ item.articulo.marca +' - '+ item.articulo.modelo +'</td>'+
              '<td style="vertical-align:middle"><input type="number" step="0.01" class="form-control" data-count="'+count+'" name="inp-'+ count +'-cantidad[]" value="'+ item.cantidad +'"></td>'+
              '<td style="vertical-align:middle">'+
                '<div class="panel-group" id="accordion-'+ count +'">'+
                  '<div class="panel panel-info">'+
                      '<div class="panel-heading">'+
                          '<h5 class="panel-title">'+
                             '<i class="fa fa-info-circle"></i>'+
                              '<a data-toggle="collapse" data-parent="#accordion-'+ count +'" href="#collapse-'+count+'" aria-expanded="false">Agregar Detalles de Articulos #'+count+'</a>'+
                          '</h5>'+
                      '</div>'+
                      '<div id="collapse-'+count+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">'+
                          '<div class="panel-body">'+
                            '<table class="table table-bordered" id="table-serial-'+count+'">'+
                                  '<thead>'+
                                  '<tr>'+
                                      '<th>#</th>'+
                                      '<th>Serie Nro</th>'+
                                      '<th>Estado</th>'+
                                      '<th>Observación</th>'+
                                  '</tr>'+
                                  '</thead>'+
                                  '<tbody>'+

                                  '</tbody>'+
                            '</table>'+
                          '</div>'+
                      '</div>'+
                  '</div>'+
              '</div></td>'+
          '</tr>');
          for(var i = 0; i < item.cantidad; i++){
            $('#table-serial-'+count+'>tbody').append('<tr>'+
                '<td>'+ (i+1) +'</td>'+
                '<td><input type="text" name="inp-'+count+'-serie[]" value="" class="form-control"></td>'+
                '<td><select class="form-control" name="sel-'+count+'-estado[]">'+
                  '<option value="ACEPTADO">ACEPTADO</option>'+
                  '<option value="RECHAZADO">RECHAZADO</option>'+
                '</select></td>'+
                '<td><input type="text" name="inp-'+count+'-observacion[]" value="" class="form-control" placeholder="-"></td>'+
            '</tr>');
          }
          //En el caso de que exista una modificacion en la cantidad de articulos
          //recibidos esta funcion re genera la parte de serial.
          $('[name="inp-'+ count +'-cantidad[]"]').change(function() {
            var posicion = $(this).data("count");
            $('#table-serial-'+posicion+'>tbody').html('');
            for(var i = 0; i < $(this).val(); i++){
              $('#table-serial-'+posicion+'>tbody').append('<tr>'+
                  '<td>'+ (i+1) +'</td>'+
                  '<td><input type="text" name="inp-'+posicion+'-serie[]" value="" class="form-control"></td>'+
                  '<td><select class="form-control" name="sel-'+posicion+'-estado[]">'+
                    '<option value="ACEPTADO">ACEPTADO</option>'+
                    '<option value="RECHAZADO">RECHAZADO</option>'+
                    '<option value="NO">RECHAZADO</option>'+
                  '</select></td>'+
                  '<td><input type="text" name="inp-'+posicion+'-observacion[]" value="" class="form-control" placeholder="-"></td>'+
              '</tr>');
            }
          });
          count++;
        });
        return;
        table = $('.dataTables').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
          },
          buttons: [
              {extend: 'copy'},
              {extend: 'csv'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},
              {extend: 'print',
                  customize: function (win) {
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
                  }
              }
          ]});
          $('[data-toggle="tooltip"]').tooltip();
          $('#modal-orden-compra').modal('show');
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
}

function searchIndex(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].fk_orden_de_compra === nameKey) {
            return i;
        }
    }
}
</script>

@stop
