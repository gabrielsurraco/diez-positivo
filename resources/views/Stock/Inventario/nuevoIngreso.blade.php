@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
      <form class="form-group" action="/stock/articulos/guardar" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="inp-id-articulo" value="@if(isset($articulos)) {{$articulos->id}} @endif">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ingreso de Mercaderias</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Fecha:</label>
                           <div class="input-group date">
                             <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="inp-fecha" data-fecha="true" value="">
                           </div>
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Tipo de Comprobante:</label>
                           <select class="form-control" name="sel-tipo-comprobante">
                               <option value="FACTURA">FACTURA</option>
                               <option value="RECIBO">RECIBO</option>
                           </select>
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Nro Comprobante:</label>
                           <input type="text" name="inp-nro-comprobante" class="form-control" required style="text-transform:uppercase">
                       </div>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label class="control-label">Nro Orden de Compra:</label>
                           <select class="select2_demo_1 form-control" name="sel-nro-ordenCompra">
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="4">Option 4</option>
                                <option value="5">Option 5</option>
                            </select>
                       </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-offset-1 col-lg-2">
                      <div class="form-group">
                          <label class="control-label">% Ganancia:</label>
                          <input type="number" min="0" step="0.01" name="inp-porc-ganancia" class="form-control" required>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Alicuota IVA:</label>
                          <select class="form-control" name="sel-alicuota-iva">
                              <option value="0">0 %</option>
                              <option value="10.5">10.50 %</option>
                              <option value="21" selected>21 %</option>
                          </select>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Unidad:</label>
                          <select class="form-control" name="sel-unidad">
                            <option value="1">KILOGRAMO</option>
                            <option value="2">METROS</option>
                            <option value="3">METRO CUADRADO</option>
                            <option value="4">METRO CUBICO</option>
                            <option value="5">LITROS</option>
                            <option value="6">1000 KILOWATT HORA</option>
                            <option value="7" selected="">UNIDAD</option>
                            <option value="8">PAR</option>
                            <option value="9">DOCENA</option>
                            <option value="10">QUILATE</option>
                            <option value="11">MILLAR</option>
                            <option value="12">MEGA-U. INT. ACT. ANTIB</option>
                            <option value="13">UNIDAD INT. ACT. INMUNG</option>
                            <option value="14">GRAMO</option>
                            <option value="15">MILIMETRO</option>
                            <option value="16">MILIMETRO CUBICO</option>
                            <option value="17">KILOMETRO</option>
                            <option value="18">HECTOLITRO</option>
                            <option value="19">MEGA U. INT. ACT. INMUNG.</option>
                            <option value="20">CENTIMETRO</option>
                            <option value="21">KILOGRAMO ACTIVO</option>
                            <option value="22">GRAMO ACTIVO</option>
                            <option value="23">GRAMO BASE</option>
                            <option value="24">UIACTHOR</option>
                            <option value="25">JUEGO O PAQUETE MAZO DE NAIPES</option>
                            <option value="26">MUIACTHOR</option>
                            <option value="27">CENTIMETRO CUBICO</option>
                            <option value="28">UIACTANT</option>
                            <option value="29">TONELADA</option>
                            <option value="30">DECAMETRO CUBICO</option>
                            <option value="31">HECTOMETRO CUBICO</option>
                            <option value="32">KILOMETRO CUBICO</option>
                            <option value="33">MICROGRAMO</option>
                            <option value="34">NANOGRAMO</option>
                            <option value="35">PICOGRAMO</option>
                            <option value="36">MUIACTANT</option>
                            <option value="37">UIACTIG</option>
                            <option value="41">MILIGRAMO</option>
                            <option value="47">MILILITRO</option>
                            <option value="48">CURIE</option>
                            <option value="49">MILICURIE</option>
                            <option value="50">MICROCURIE</option>
                            <option value="51">U. INTER. ACT. HOR.</option>
                            <option value="52">MEGA U. INTER. ACT. HOR.</option>
                            <option value="53">KILOGRAMO BASE</option>
                            <option value="54">GRUESA</option>
                            <option value="55">MUIACTIG</option>
                            <option value="61">KG. BRUTO</option>
                            <option value="62">PACK</option>
                            <option value="63">HORMA</option>
                            <option value="98">OTRAS UNIDADES</option>
                            <option value="99">BONIFICACION</option>
                          </select>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label class="control-label">Precio:</label>
                          <input type="number" min="0" step="0.01" name="inp-precio" class="form-control" required>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Moneda:</label>
                          <select class="form-control" name="sel-moneda">
                            <option value="ARS" selected>PESOS ARGENTINO</option>
                            <option value="USD">DOLAR</option>
                          </select>
                      </div>
                    </div>

                    </div>
                    <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </form>
    </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $(".select2_demo_1").select2({
          theme:'bootstrap'
      });
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($articulos))
      $('[name="inp-codigo"]').val('{{ $articulos->codigo_interno }}');
      $('[name="inp-marca"]').val('{{ $articulos->marca }}');
      $('[name="inp-modelo"]').val('{{ $articulos->modelo }}');
      $('[name="inp-frecuencia"]').val('{{ $articulos->frecuencia }}');
      $('[name="inp-porc-ganancia"]').val('{{ $articulos->porcentaje_ganancia }}');
      $('[name="inp-precio"]').val('{{ $articulos->precio }}');
      $('[name="sel-alicuota-iva-"]').val('{{ $articulos->alicuota_iva }}');
      $('[name="sel-unidad"]').val('{{ $articulos->unidad }}');
      $('[name="sel-moneda"]').val('{{ $articulos->moneda }}');
      @endif

      //Mensaje cuando se guarda.
      @if (isset($status) && $status == 1)
        swal({
          title: "Exito!",
          text: "Articulo Guardado Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
            window.location.href = '/stock/articulos';
        });
      @endif
      //Mensaje cuando ocurre un error al guardar.
      @if (isset($status) && $status == 0)
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            icon: "error",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      @endif
  });



</script>

@stop
