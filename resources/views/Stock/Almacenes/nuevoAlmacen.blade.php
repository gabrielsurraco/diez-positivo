@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
      <form class="form-group" action="/stock/almacenes/guardar" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="inp-id-almacen" value="@if(isset($almacenes)) {{$almacenes->id}} @endif">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nuevo Almacen</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                     <div class="col-lg-offset-3 col-lg-6">

                       <div class="form-group @if ($errors->has('inp-nombre')) has-error @endif">
                           <label class="control-label">Nombre de Almacen:</label>
                           <input type="text" name="inp-nombre" class="form-control">
                           @if ($errors->has('inp-nombre'))
                           <span class="help-block">{{ $errors->first('inp-nombre') }}</span>
                           @endif
                       </div>
                     </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>Pertenencia del Almacen</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-offset-2 col-lg-3">
                      <div class="form-group">
                          <label>Tipo Persona:</label>
                          <select class="form-control" name="sel-tipo-persona">
                              <option value="CLIENTE">CLIENTE</option>
                              <option value="PROVEEDOR">PROVEEDOR</option>
                              <option value="TECNICO">TECNICO</option>
                              <option value="INVERSOR">INVERSOR</option>
                          </select>
                      </div>
                    </div>

                    <div class="col-lg-5">
                      <div class="form-group">
                          <label>Seleccionar Persona:</label>
                          <select class="select2_demo_1 form-control" name="sel-persona"  height="100%">
                            <option></option>
                          </select>
                      </div>
                    </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="row">
                      <div class="col-lg-12 text-center">
                          <h4>Detalles del Almacen</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-offset-1 col-lg-2">
                        <div class="form-group">
                            <label>Seccion:</label>
                            <input type="text" name="inp-seccion" value="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="form-group">
                            <label>Fila:</label>
                            <input type="text" name="inp-fila" value="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="form-group">
                            <label>Columna:</label>
                            <input type="text" name="inp-columna" value="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="form-group">
                            <label>Estante Numero:</label>
                            <input type="text" name="inp-estanteNro" value="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="form-group">
                            <label>Caja:</label>
                            <input type="text" name="inp-caja" value="" class="form-control">
                        </div>
                      </div>
                      </div>

                  <div class="hr-line-dashed"></div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </form>
    </div>
</div>
<!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($almacenes))
      $('[name="inp-nombre"]').val('{{ $almacenes->nombre }}');
      $('[name="inp-seccion"]').val('{{ $almacenes->seccion }}');
      $('[name="inp-fila"]').val('{{ $almacenes->fila }}');
      $('[name="inp-columna"]').val('{{ $almacenes->columna }}');
      $('[name="inp-estanteNro"]').val('{{ $almacenes->estante_nro }}');
      $('[name="inp-caja"]').val('{{ $almacenes->caja }}');
      @endif

      //Mensaje cuando se guarda.
      @if (isset($status) && $status == 1)
        swal({
          title: "Exito!",
          text: "Almacen Guardado Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
            window.location.href = '/stock/almacenes';
        });
      @endif
      //Mensaje cuando ocurre un error al guardar.
      @if (isset($status) && $status == 0)
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            icon: "error",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      @endif

      //buscar un cliente de la base de datos.
      $('[name="sel-persona"]').select2({
        ajax: {
          method: "POST",
          url: '/clientes/jsonSearchCliente',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Buscar Cliente.",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap'
      });

      $('[name="sel-tipo-persona"]').on('change', function(event) {
        event.preventDefault();
        switch (event.target.value) {
          case 'CLIENTE':
            //buscar un cliente de la base de datos.
            $('[name="sel-persona"]').select2({
              ajax: {
                method: "POST",
                url: '/clientes/jsonSearchCliente',
                dataType: 'json',
                data: function (params){
                      var query = {
                      palabraClave: params.term,
                      "_token": "{{ csrf_token() }}"
                      }

                      // Query parameters will be ?search=[term]&type=public
                      return query;
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              },
              minimumInputLength: 3,
              placeholder: "Buscar Cliente.",
              language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
              theme:'bootstrap'
            });
            break;
            case 'PROVEEDOR':
            //buscar un cliente de la base de datos.
            $('[name="sel-persona"]').select2({
              ajax: {
                method: "POST",
                url: '/proveedores/jsonSearchProveedor',
                dataType: 'json',
                data: function (params){
                      var query = {
                      palabraClave: params.term,
                      "_token": "{{ csrf_token() }}"
                      }

                      // Query parameters will be ?search=[term]&type=public
                      return query;
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
              },
              minimumInputLength: 3,
              placeholder: "Buscar Proveedor.",
              language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
              theme:'bootstrap'
            });
            break;
        }
        /* Act on the event */
      });
  });

  function save(valor){
    $.ajax({
      method: "POST",
      url: "/compras/guardarOrdenCompra",
      data: { "_token": "{{ csrf_token() }}",
              idOrdenCompra   : globalIdOdenCompra,
              fecha          : $('[name="inp-fecha"]').val(),
              proveedor      : $('[name="sel-proveedor"]').val(),
              entrega_aprox  : $('[name="inp-entrega-aprox"]').val(),
              estado         : valor == 1 ? 'solicitado' : 'borrador',
              globalItems    : JSON.stringify(globalItems)
            }
    })
      .done(function( msg ) {
        swal({
          title: "Exito!",
          text: "Orden de Compra Gurdada Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
          if(valor){
              window.location = '/compras/ordenCompra';
          }else{
              globalIdOdenCompra = msg.idOrdenCompra;
          }
        });
      })
      .fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          icon: "error",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
  }

</script>

@stop
