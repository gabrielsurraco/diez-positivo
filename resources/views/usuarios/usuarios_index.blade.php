@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Usuarios</div>
                    
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-sm btn-primary" href="/usuarios/nuevo"><i class="fa fa-plus-square"></i> Nuevo Usuario</a>
                        </div>
                    </div>
                    <div class="row m-t-lg">
                        <div class="col-md-12">
                            <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                   placeholder="Buscar en la tabla..">
                            <div class="table-responsive">
                            <table class="footable2 table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th data-hide="phone,tablet">DNI</th>
                                    <th data-hide="phone,tablet">Perfil</th>
                                    <th data-hide="phone,tablet">Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($usuarios as $key => $value)
                                    <tr class="gradeC">
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>{{ $value->dni }}</td>
                                        <td class="center">
                                            @if($value->es_cobrador)
                                                {{ 'Cobrador|' }}
                                            @endif
                                            @if($value->es_tecnico)
                                                {{ 'Técnico|' }}
                                            @endif
                                            @if($value->es_admin)
                                                {{ 'Admin|' }}
                                            @endif
                                            @if($value->es_administrativo)
                                                {{ 'Administrativo|' }}
                                            @endif
                                            @if($value->es_contabilidad)
                                                {{ 'Contabilidad' }}
                                            @endif
                                        </td>
                                        <td class="center">
                                            <a href="/usuarios/{{ $value->id }}/edit" title="Editar" class="btn btn-xs btn-primary" > <i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger eliminar" title="Eliminar" id_usuario='{{ $value->id }}'> <i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.footable2').footable();
        
        $("a.eliminar").click(function(){
            var id_usuario = $(this).attr('id_usuario');
            swal({
                title: "Esta Seguro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Eliminar!",
                closeOnConfirm: true
            }, function () {
                
                $.ajax({
                    type: "POST",
                    url: "/usuarios/eliminar",
                    data: "_token="+$("meta[name='csrf-token']").attr('content')+"&id_usuario="+id_usuario,
                    success: function(){
                        $("a.eliminar[id_usuario='"+id_usuario+"']").parent().parent().remove();
                    },
                    error: function(){

                    }
                });

            });
            
            
        });
    });
</script>
@stop
