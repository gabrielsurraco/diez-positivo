@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<!-- Chosen -->
<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
<style>
.select2-dropdown.increasedzindexclass {
  z-index: 999999;
}
</style>

@stop


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detalle Cliente </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                    <div class="row m-b-lg m-t-lg">
                        <div class="col-md-5">
                            <div class="profile-image">
                                <img src="{{ asset('/img/icons/icon-user.png') }}" class="img-circle circle-border m-b-md" alt="profile">
                            </div>
                            <div class="profile-info">
                                <div class="">
                                    <div>
                                        <h2 class="no-margins">
                                            {{$cliente->persona->apellido}} {{$cliente->persona->nombre}}
                                        </h2>
                                        <h4>{{(new \App\Helper)->getTipoDoc($cliente->persona->tipo_doc)}}: {{$cliente->persona->cuit}}</h4>
                                        <small>

                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <table class="table small m-b-xs">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>Codigo Cliente:</strong> {{$cliente->codigo}}
                                    </td>
                                    <td>
                                        <strong>Fecha Nacimiento:</strong> {{$cliente->persona->fecha_nacimiento}}
                                    </td>
                                    <td>
                                        <strong>Condicion Fiscal:</strong> {{(new \App\Helper)->getCondicionFiscal($cliente->persona->condicion_fiscal)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Direccion:</strong> {{$cliente->persona->domicilio->calle}} {{$cliente->persona->domicilio->altura}}
                                    </td>
                                    <td>
                                        <strong>Departamento:</strong> {{$cliente->persona->domicilio->departamento->nombre}}
                                    <td>
                                        <strong>Localidad:</strong> {{$cliente->persona->domicilio->localidad->nombre}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Cliente Nro:</strong> {{$cliente->codigo}}
                                    </td>
                                    <td>

                                    <td>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Conexiones del Cliente Activas</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-lg-12">
                  <table class="table">
                            <thead>
                                <tr>
                                    <th>Servicio</th>
                                    <th>Localidad</th>
                                    <th>Direccion</th>
                                    <th>Fecha Inicio</th>
                                    <th>Precio</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($cs != null)
                              @foreach ($cs as $a)
                              <tr class="gradeX">
                                <td>{{ $a->servicio->nombre }}</td>
                                <td align="center">{{ $a->servicio->localidad->nombre }}</td>
                                <td align="center">{{ $a->domicilio->calle }} {{ $a->domicilio->altura }}</td>
                                <td align="center">{{ $a->fecha_alta }}</td>
                                <td align="center">$ {{ $a->servicio->precio }}</td>
                                <td align="center">{{ strtoupper ($a->estado) }}</td>
                              </tr>
                              @endforeach
                              @endif
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="6" align="center"><a href="/clientes/{{$cliente->id}}/servicios" type="button" class="btn btn-success">
                                  <span class="fa fa-search"></span> Ver detalles
                                </a></td>
                              </tr>
                            </tfoot>
                        </table>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Parentescos con Cliente </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-lg-12">
                  <table class="table">
                            <thead>
                            <tr>
                                <th>Persona</th>
                                <th>Num Doc</th>
                                <th>Tipo Parentescos</th>
                                <th><button type="button" class="btn btn-success btn-xs" onclick="$('#modal-nueva-relacion').modal('show')" data-toggle="tooltip" data-placement="top" title="Agregar Parentescos A Cliente">
                                  <span class="fa fa-plus"></span>  AGREGAR
                                </button></th>
                            </tr>
                            </thead>
                            <tbody>
                              @if(count($parentescos) == 0)
                              <tr>
                                  <td colspan="4"><div class="alert alert-info text-center">
                                    Sin Datos
                                  </div></td>
                              </tr>

                              @endif
                              @php
                              $count = 1;
                              @endphp
                              @foreach ($parentescos as $value)
                              <tr>
                                  <td>{{$value->persona->apellido}} {{$value->persona->nombre}}</td>
                                  <td>{{$value->persona->cuit}}</td>
                                  <td><?php echo (new App\Helper)->getLabelFromRol($value->rol) ?></td>
                                  <td align="center"><button onclick="eliminarRelacion({{$value->id}})" type="button" class="btn btn-danger btn-xs">
                                    <span class="fa fa-remove"></span>
                                  </button></td>
                              </tr>
                              @php
                              $count++;
                              @endphp
                              @endforeach
                            </tbody>
                        </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- Fin Contenido Principal -->

<div class="modal inmodal" id="modal-nueva-relacion" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Asociar Relacion</h4>
                <small class="font-bold">Alta de cliente a partir de una persona ya cargada en sistema.</small>
            </div>
            <div class="modal-body">
              <input type="hidden" id="inp-hidden-idCliente" value="{{$cliente->id}}">
              <div class="row">
                <div class="col-xs-6">
                  <div class="form-group" >
                    <label>Seleccione Persona:</label>
                    <select class="select2_demo_1 form-control"  name="sel-persona">
                         <option></option>
                     </select>
                  </div>
                </div>
                <div class="col-xs-6">
                  <label>Seleccione Rol:</label>
                  <select data-placeholder=" " class="chosen-select" id="sel-rol" multiple style="width:350px;" tabindex="4">
                    <option value="1">Marido</option>
                    <option value="2">Mujer</option>
                    <option value="3">Hijo/a</option>
                    <option value="4">Pariente</option>
                    <option value="5">Tesorero</option>
                    <option value="6">Dueño</option>
                    <option value="7">Secretario</option>
                    <option value="8">Tecnico</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">CERRAR</button>
                <button type="button" id="btn-guardar-relacion" class="btn btn-primary">GUARDAR</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $('.chosen-select').chosen({width: "100%"});
      $('.select2_demo_1').select2({
          theme:'bootstrap'
      });
      var date = new Date();
      //$('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
        /*serverSide: true,
        ajax: {
            url: '/ventas/clientes/paginacion',
            type: 'POST',
            data: {
              "_token": "{{ csrf_token() }}",
            }
        },
        "columns": [
            { "data": "codigo" },
            { "data": "cliente" },
            { "data": "cuit" },
            { "data": "email" },
            { "data": "telefono" },
            { "data": "domicilio" },
            { "data": "estado" },
            { "data": "accion" }
        ]*/
      });

      //buscar una persona de la base de datos.
      $('[name="sel-persona"]').select2({
        ajax: {
          method: "POST",
          url: '/personas/jsonSearchPersona',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Buscar Persona",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap',
        dropdownParent: $('#modal-nueva-relacion'),
        dropdownCssClass: "increasedzindexclass",
        width: '100%'
      });

  });

$('#btn-guardar-relacion').on('click', function(event) {
    event.preventDefault();
    $.ajax({
      url: '/clientes/guardarParentescoRelacion',
      type: 'post',
      dataType: 'json',
      data: {"_token": "{{ csrf_token() }}",
             idPersona : $('[name="sel-persona"]').val(),
             idCliente : $('#inp-hidden-idCliente').val(),
             rol       : $("#sel-rol").chosen().val(),
            }
    })
    .done(function() {
      swal("Exito!", "Parentesco Creado Correctamente.", "success").then((value)=>{location.reload();});

    })
    .fail(function() {
      swal("Error", "Algo esta mal, intentelo de nuevo mas tarde.", "error");
    });
});

function eliminarRelacion(valor){
    swal({
      title: "Borrar Parentesco?",
      text: "Usted esta a punto de eliminar el parentesco entre la persona y el cliente.",
      icon: "warning",
      buttons: {
                  cancel: {
                    text: "Cancelar",
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                  },
                  confirm: {
                    text: "SI",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                  }
                },
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          method: "POST",
          url: "/clientes/borrarParentescoRelacion",
          data: { "_token": "{{ csrf_token() }}",
                  id: valor,
                }
        })
          .done(function( msg ) {
            swal.stopLoading();
            swal({
              title: "Listo!",
              text: "Parentesco Borrado Correctamente!",
              type: "success",
              icon: "success",
              confirmButtonText: "Aceptar"
            }).then((value) => {
              location.reload();}
            );
        })
          .fail(function(mgs){
            swal({
              title: "Error!",
              text: "Algo esta mal, por favor intentolo nuevamente.",
              type: "error",
              confirmButtonText: "Cerrar"
            });
        });
      } else {
        swal("No se hizo nada.");
      }
    });
}
</script>

@stop
