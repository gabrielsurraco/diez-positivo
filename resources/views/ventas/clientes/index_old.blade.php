@extends('layouts.backend')


@section('css')

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- chosen select -->
<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@stop

@section('content')
    @if (Session::has('flash_notification.message'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-{{ session('flash_notification.level') }} ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session('flash_notification.message') !!}
                </div>
            </div>
        </div>
    </div>

    @endif

    <!-- Inicio Contenido Principal -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <!-- Filtro de buusqueda -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtro Principal</h5>
                        <div class="ibox-tools">
                              <a href="/clientes/new"><button class="btn btn-primary btn-sm" ><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Cliente</button></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      <form method="post" action="/clientes" class="form-horizontal">
                              {{ csrf_field() }}
                              <p>Realice una búsqueda previa para mostrar resultados.</p>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                  <div class="col-sm-12">
                                      <div class="row">
                                          <label class="col-md-2 control-label">Apellido: </label>
                                          <div class="col-md-3"><input name="busqueda-apellido" type="text" class="form-control" value="@if(isset($datosFiltrados)) {{$datosFiltrados['bs_apellido']}} @endif"></div>
                                          <div class="col-md-1"><label class="control-label">Nombre: </label></div>
                                          <div class="col-md-3"><input name="busqueda-nombre" type="text" class="form-control" value="@if(isset($datosFiltrados)) {{$datosFiltrados['bs_nombre']}} @endif"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                  <div class="col-sm-12">
                                      <div class="row">
                                          <label class="col-md-2 control-label">Departamento: </label>
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                <select id="departamentos" data-placeholder="Todos los departamentos..." name="busqueda-departamento" class="form-control">
                                                        <option value="default">TODOS</option>
                                                    @foreach ($array_departamentos as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                                </div>
                                          </div>

                                          <label class="col-md-1 control-label">Localidad: </label>
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                <select id="localidades" data-placeholder="Seleccione localidad/es..." name="busqueda-localidades[]" id="localidades" class="chosen-select" multiple style="width:350px;" tabindex="4">

                                                </select>
                                                </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                  <div class="col-sm-12" style="text-align:center;">
                                      <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
                                  </div>
                              </div>
                          </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin Filtro de buusqueda -->
        <!-- Tabla de Resultados -->
        <div class="row">
              <div class="col-lg-12 m-t-lg">
                  <div class="ibox float-e-margins" id="menu-principal">
                      <div class="ibox-title">
                          <h5>Lista de Resultados</h5>
                          <div class="ibox-tools">
                              <a class="collapse-link" id="btn-minimize-table">
                                  <i class="fa fa-chevron-up"></i>
                              </a>
                          </div>
                      </div>
                      <div class="ibox-content">
                          <div class="row">
                              <div class="col-lg-12">
                                  @if (count($filtro) == 0)
                                  <div class="alert alert-info">
                                      No hay datos para mostrar.
                                  </div>
                                  @else
                                  <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables" >
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Cliente</th>
                                                        <th>CUIT/L</th>
                                                        <th>Email</th>
                                                        <th>Domicilio</th>
                                                        <th>Estado ($)</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($filtro != null)
                                                    @foreach ($filtro as $fil)
                                                    <tr class="gradeX">
                                                      <td>{{ $fil->clienteCodigo }}</td>
                                                      <td align="center">@if( $fil->clientePersonaFisica =="1" ) {{ str_limit( $fil->clienteApellido . " " .$fil->clienteNombre,30) }} @else {{ str_limit($fil->clienteNombre,30) }} @endif</td>
                                                      <td align="center">{{ $fil->clienteCUIT }}</td>
                                                      <td align="center">{{str_limit($fil->clienteEmail1,30)}}</td>
                                                      <td align="center">{{str_limit($fil->clienteLocalidad .' - '. $fil->clienteDomicilioCalle .' '. $fil->clienteDomicilioCalleAltura . ' CP: ' . $fil->clienteDomicilioCP, 40) }} @if(!$fil->clienteDomicilioPiso == "") {{  ' Piso: ' . $fil->clienteDomicilioPiso }} @endif</td>
                                                      <td align="center">Normal</td>
                                                      <td align="center">
                                                            <a class="btn btn-xs btn-success" href="{{  '/clientes/' . $fil->clienteID . '/edit'}}" title="Editar"><i class="fa fa-edit"></i></a>
                                                            <a class="btn btn-xs btn-warning" href="{{  '/tickets/nuevo/fromReference/' . $fil->clienteID }}" title="Seguimiento de Ticket's"><i class="fa fa-ticket"></i></a>
                                                            <a class="btn btn-xs btn-primary" href="{{  '/clientes/' . $fil->clienteID . '/servicios'}}" title="Servicios"><i class="fa fa-shopping-cart"></i></a>
                                                            <a class="btn btn-xs btn-danger"  title="Cuenta Corriente"><i class="fa fa-money"></i></a>
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Cliente</th>
                                                        <th>CUIT/L</th>
                                                        <th>Email</th>
                                                        <th>Domicilio</th>
                                                        <th>Estado ($)</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                    </div>

                                    @endif
                              </div>
                          </div>

                      </div>
                  </div>

              </div>
          </div>
          <!-- Fin Tabla de Resultados -->
    </div>

    <!-- Fin Contenido Principal -->
@endsection

@section('javascript')
<!-- Required Javascript -->
<script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Chonsen Select -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>

<script>
$(document).ready(function () {

    $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
      });

    //Agregado Guille
    $(".chosen-select").chosen({max_selected_options: 5});

    $('#departamentos').on('change',function(){
        if(this.value !== 0){
            var valor = this.value;
            $.ajax({
               type: "GET",
               url: "/localidades/get",
               data: "depto="+valor,
               success: function(data)
               {
                   $('#localidad').not(":nth-child(1)").empty();
                    var a = JSON.parse(data);
                    $.each(a, function(key, value) {
                        $('#localidades')
                            .append($("<option></option>")
                                       .attr("value",value.id)
                                       .text(value.nombre));
                   });
                   $(".chosen-select").trigger("chosen:updated");
               },
               error: function(msg){
                   alert(msg);
               }

            });
        }
    });

    @if(isset($datosFiltrados) && $datosFiltrados['bs_departamento'] != "default")
    $('#departamentos option').each(function(){
          if ($(this).val() == {{$datosFiltrados['bs_departamento']}}){
              $(this).attr('selected','selected');
          }
    });
    @endif

    //al inicializar trae todas las localidades del departamento obera
    $.ajax({
       type: "GET",
       url: "/localidades/get",
       data: "depto=13",
       success: function(data)
       {
           $('#localidad').not(":nth-child(1)").empty();
            var a = JSON.parse(data);
            $.each(a, function(key, value) {
                $('#localidades')
                    .append($("<option></option>")
                               .attr("value",value.id)
                               .text(value.nombre));
           });
           @if(isset($datosFiltrados))
           var js_array = [{{ $datosFiltrados['bs_localidades']}}];
           $('#localidades option').each(function(){
                 if (jQuery.inArray(parseInt($(this).val()), js_array) != -1){
                     $(this).attr('selected','selected');
                 }
           });
           $(".chosen-select").trigger("chosen:updated");
           @endif
           $(".chosen-select").trigger("chosen:updated");
       },
       error: function(msg){
           alert(msg);
       }

    });

    @if (!count($filtro) == 0)
    $('html, body').animate({
            scrollTop: $("#tabla-resultados").offset().top
    }, 800);
    @endif
});
</script>
@stop
