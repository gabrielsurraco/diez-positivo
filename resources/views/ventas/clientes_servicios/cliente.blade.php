@extends('layouts.backend')


@section('css')

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">

    @if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
    @endif

    <div class="col-lg-12 m-t-md">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    @if($cliente->persona->es_persona_fisica)
                        {{ str_limit($cliente->persona->apellido . " " .$cliente->persona->nombre , $limit = 100) }}
                    @else
                        {{ str_limit($cliente->persona->razon_social,100) }}
                    @endif
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="clients-list">
                    <ul class="nav nav-tabs">
                        <span class="pull-right small text-muted">1406 Elements</span>
                        <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class="fa fa-user"></i> Servicios</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                                <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                    <div class="m-t-md">
                                        <a  href="/clientes/{{ $cliente->id }}/agregar_servicios"><button class="btn btn-primary btn-xs p-xxs" > <i class="fa fa-plus"></i> Asignar Servicio</button></a>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables" >
                                                <thead>
                                                    <tr>
                                                        <th>Servicio</th>
                                                        <th>Localidad</th>
                                                        <th>Direccion</th>
                                                        <th>Fecha Inicio</th>
                                                        <th>Fecha Fin</th>
                                                        <th>Estado</th>
                                                        <th>Precio</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($cs != null)
                                                    @foreach ($cs as $a)
                                                    <?php
                                                    switch ($a->estado) {
                                                      case 'preventa':
                                                        $a->estado = '<span class="label label-info">PREVENTA</span>';
                                                        break;
                                                      case 'a_cobrar':
                                                        $a->estado = '<span class="label label-default ">A COBRAR</span>';
                                                        break;
                                                      case 'cobrado':
                                                        $a->estado = '<span class="label label-success ">COBRADO</span>';
                                                        break;
                                                      case 'agendado':
                                                        $a->estado = '<span class="label label-primary ">AGENDADO</span>';
                                                        break;
                                                      case 'pendiente':
                                                        $a->estado = '<span class="label label-warning ">PENDIENTE</span>';
                                                        break;
                                                      case 'primer_mes':
                                                        $a->estado = '<span class="label label-primary ">PRIMER MES</span>';
                                                        break;
                                                      case 'activo':
                                                        $a->estado = '<span class="label label-primary ">ACTIVO</span>';
                                                        break;
                                                      case 'promo':
                                                        $a->estado = '<span class="label label-info ">PROMO</span>';
                                                        break;
                                                      case 'bonificado':
                                                        $a->estado = '<span class="label label-success ">BONIFICADO</span>';
                                                        break;
                                                      case 'mudanza':
                                                        $a->estado = '<span class="label label-info ">MUDANZA</span>';
                                                        break;
                                                      case 'suspendido':
                                                        $a->estado = '<span class="label label-warning ">SUSPENDIDO</span>';
                                                        break;
                                                      case 'a_cortar':
                                                        $a->estado = '<span class="label label-warning ">A CORTADO</span>';
                                                        break;
                                                      case 'cortado':
                                                        $a->estado = '<span class="label label-danger ">CORTADO</span>';
                                                        break;
                                                      case 'reconectar':
                                                        $a->estado = '<span class="label label-warning ">RECONECTAR</span>';
                                                        break;
                                                      case 'baja':
                                                        $a->estado = '<span class="label label-danger ">BAJA</span>';
                                                        break;
                                                      case 'particular':
                                                        $a->estado = '<span class="label label-default ">PARTICULAR</span>';
                                                        break;
                                                    }
                                                    ?>
                                                    <tr class="gradeX">
                                                      <td>{{ $a->servicio->nombre }}</td>
                                                      <td align="center">{{ $a->servicio->localidad->nombre }}</td>
                                                      <td align="center">{{ $a->domicilio->calle }} {{ $a->domicilio->altura }}</td>
                                                      <td align="center">{{ $a->fecha_alta }}</td>
                                                      <td align="center"> @if($a->fecha_baja != null) {{ $a->fecha_baja }} @else - @endif</td>
                                                      <td align="center"><?php echo $a->estado; ?></td>
                                                      <td align="center">$ {{ $a->servicio->precio }}</td>
                                                      <td align="center">
                                                        @if($a->estado != '<span class="label label-danger ">BAJA</span>')
                                                        <a onClick="modalActualizarEstado(<?php echo $a->id; ?>)" data-toggle="tooltip" data-placement="top" title="Cambiar Estado del Servicio" class="btn btn-warning btn-xs btn-bitbucket"><i class="fa fa-bars"></i></a>
                                                        <a onclick="modalActualizarServicio({{$a->cliente->id}},{{$a->id}});" data-toggle="tooltip" data-placement="top" title="Actualizar Tipo de Servicio" class="btn btn-info btn-xs btn-bitbucket"><i class="fa fa-exchange"></i></a>
                                                        @else
                                                          -
                                                        @endif
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                      <th>Servicio</th>
                                                      <th>Localidad</th>
                                                      <th>Direccion</th>
                                                      <th>Fecha Inicio</th>
                                                      <th>Fecha Fin</th>
                                                      <th>Estado</th>
                                                      <th>Precio</th>
                                                      <th>Acciones</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 365.112px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-cambiar-estado" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-bars modal-icon"></i>
                <h4 class="modal-title">Actualizar Estado</h4>
                <small class="font-bold">En esta seccion puede actualizar el estado del Servicio del Cliente. Por favor seleccione del listado cual es el servicio que reemplazara al actual.</small>
            </div>
            <div class="modal-body">
                    <input type="hidden" id="inp-hidden-id-cliente-servicio" value="">
                    <select class="form-control" id="sel-estado-cliente-servicio">
                      <option value="preventa" @if (isset($a)) @if ($a->estado == 'preventa') {{selected}} @endif @endif>PREVENTA</option>
                      <option value="a_cobrar" @if (isset($a)) @if ($a->estado == 'a_cobrar') {{selected}} @endif @endif>A COBRAR</option>
                      <option value="cobrado" @if (isset($a)) @if ($a->estado == 'cobrado') {{selected}} @endif @endif>COBRADO</option>
                      <option value="agendado" @if (isset($a)) @if ($a->estado == 'agendado') {{selected}} @endif @endif>AGENDADO</option>
                      <option value="pendiente" @if (isset($a)) @if ($a->estado == 'pendiente') {{selected}} @endif @endif>PENDIENTE</option>
                      <option value="primer_mes" @if (isset($a)) @if ($a->estado == 'primer_mes') {{selected}} @endif @endif>PRIMER MES</option>
                      <option value="activo" @if (isset($a)) @if ($a->estado == 'activo') {{selected}} @endif @endif>ACTIVO</option>
                      <option value="promo" @if (isset($a)) @if ($a->estado == 'promo') {{selected}} @endif @endif>PROMO</option>
                      <option value="bonificado" @if (isset($a)) @if ($a->estado == 'bonificado') {{selected}} @endif @endif>BONIFICADO</option>
                      <option value="mudanza" @if (isset($a)) @if ($a->estado == 'mudanza') {{selected}} @endif @endif>MUDAANZA</option>
                      <option value="suspendido" @if (isset($a)) @if ($a->estado == 'suspendido') {{selected}} @endif @endif>SUSPENDIDO</option>
                      <option value="a_cortar" @if (isset($a)) @if ($a->estado == 'a_cortar') {{selected}} @endif @endif>A CORTAR</option>
                      <option value="cortado" @if (isset($a)) @if ($a->estado == 'cortado') {{selected}} @endif @endif>CORTADO</option>
                      <option value="reconectar" @if (isset($a)) @if ($a->estado == 'reconectar') {{selected}} @endif @endif>RECONECTAR</option>
                      <option value="baja" @if (isset($a)) @if ($a->estado == 'baja') {{selected}} @endif @endif>BAJA</option>
                      <option value="particular" @if (isset($a)) @if ($a->estado == 'particular') {{selected}} @endif @endif>PARTICULAR</option>
                    </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="updateEstadoServicio($('#inp-hidden-id-cliente-servicio').val(), $('#sel-estado-cliente-servicio'));">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-actualizar-servicio" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-wifi modal-icon"></i>
                <h4 class="modal-title">Actualizar Servicio</h4>
                <small class="font-bold">En esta seccion puede actualizar el Servicio del Cliente.</small>
            </div>
            <div class="modal-body">
                    <input type="hidden" id="inp-hidden-id-cliente-servicio" value="">
                    <div class="row">
                      <div class="col-lg-4">
                        {{ Form::label('provincia', 'Provincia') }}
                        {{ Form::select("provincia", array('1' => 'Misiones') ,null, array("class"=>"form-control")) }}
                      </div>
                      <div class="col-lg-4">
                        {{ Form::label('departamento', 'Departamento') }}
                        {{ Form::select("departamento",array('Seleccionar...') + $array_departamentos ,null, array("class"=>"form-control")) }}
                      </div>
                      <div class="col-lg-4">
                        {{ Form::label('localidad', 'Localidad') }}
                        {{ Form::select("localidad", array('Seleccionar...') ,null, array("class"=>"form-control")) }}
                      </div>
                    </div>
                    <div class="row" style="margin-top:20px;">
                      <div class="col-lg-offset-3 col-lg-6">
                        <div class="form-group">
                            <label class="control-label">Seleccionar Servicio:</label>
                            <select class="select2_demo_1 form-control" name="sel-id-servicio" height="100%">
                                 <option></option>
                             </select>
                        </div>
                      </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-actualizar-servicio">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<!-- Required Javascript -->
<script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script>
    $(document).ready(function () {
      $('.input-group.date').datepicker({
      startView: 0,
              todayBtn: "linked",
              keyboardNavigation: false,
              forceParse: false,
              autoclose: true,
              format: "dd/mm/yyyy"
      });
      $('.dataTables').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
      });
      $('[data-toggle="tooltip"]').tooltip();

      $('#departamento').on('change',function(){
          if(this.value !== 0){
              var valor = this.value;
              $.ajax({
                 type: "GET",
                 url: "/localidades/get",
                 data: "depto="+valor,
                 success: function(data)
                 {
                     $('#localidad').not(":nth-child(1)").empty();
                      var a = JSON.parse(data);
                      $.each(a, function(key, value) {
                          $('#localidad')
                              .append($("<option></option>")
                                         .attr("value",value.id)
                                         .text(value.nombre));
                     });
                     cargarServiciosFromLocalidad($('#localidad').val());
                 },
                 error: function(msg){
                     alert(msg);
                 }

              });
          }
      });

      $('#localidad').on('change', function(event) {
        cargarServiciosFromLocalidad($(this).val());
      });
    });

    function updateEstadoServicio(id, estado){
        var est = $(estado).val();
        swal({
          title: "Cambiar el Estado?",
          text: "Usted esta seguro que desea cambiar el estado de este servicio?",
          icon: "warning",
          buttons: ["No", "Si"],
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              method: "POST",
              url: "/clientes/changeStatusServicio",
              data: { "_token": "{{ csrf_token() }}",
                      id             : id,
                      estado         : est
                    }
            })
              .done(function( msg ) {
                swal({
                  title: "Exito!",
                  text: "Estado Editado Correctamente.",
                  type: "success",
                  icon: "success",
                  confirmButtonText: "Cerrar"
                }).then((value) => {
                  location.reload();}
                );
            })
              .fail(function(mgs){
                swal({
                  title: "Error!",
                  text: "Algo esta mal, por favor Intentelo nuevamente.",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            });
          } else {

          }
        });
    }

    function modalActualizarEstado(id){
      $('#inp-hidden-id-cliente-servicio').val(id);
      $('#modal-cambiar-estado').modal('show');
    }

    function modalActualizarServicio(idCliente, idClienteServicio){
      $('#modal-actualizar-servicio').modal('show');
      $('#btn-actualizar-servicio').on('click', function(event) {
          event.preventDefault();
          /* Act on the event */
          $.ajax({
            url: '/clientes/actualizar_servicios',
            type: 'POST',
            dataType: 'html',
            data: {
                    "_token": "{{ csrf_token() }}",
                    idServicioElegido: $('[name="sel-id-servicio"]').val(),
                    idClienteServicio: idClienteServicio,
                  }
          })
          .done(function(msg) {
            console.log("success");
            if(msg == 'OK'){
              $('#modal-actualizar-servicio').modal('hide');
              swal({
                title: "Exito!",
                text: "Servicio Cambiado Correctamente.",
                type: "success",
                icon: "success",
                confirmButtonText: "Cerrar"
              }).then((value) => {
                location.reload();}
              );
            }

          })
          .fail(function(msg) {
            swal({
              title: "Error!",
              text: "Algo esta mal, por favor Intentelo nuevamente.",
              type: "error",
              confirmButtonText: "Cerrar"
            });
          })
      });
    }

    function cargarServiciosFromLocalidad(idLocalidad){
      //$('#loading').fadeIn('400');
      $.ajax({
        method: "POST",
        url: "/clientes/getServiciosFromLocalidad",
        data: { "_token": "{{ csrf_token() }}",
                idLocalidad      : idLocalidad
              },
        dataType: 'JSON'
      })
        .done(function( msg ) {
            var cant = Object.keys(msg);
            $('[name="sel-id-servicio"]').html('<option></option>');
            Object.keys(msg).forEach(function(key) {
                //console.log(key, msg[key]);
                $('[name="sel-id-servicio"]').append('<option value="'+msg[key].id+'">'+msg[key].nombre+' ('+ msg[key].zona +')</option>');
            });


      })
        .fail(function(mgs){
          swal({
            title: "Error!",
            text: "Ocurrio un error al cargar los servicios de la localidad seleccionada. Intentelo nuevamente por favor.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
    }
</script>
@stop
