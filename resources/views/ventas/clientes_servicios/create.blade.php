@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

@stop
@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
      {!! Form::open(['url' => '/clientes/guardar_servicio', 'method' => 'post']) !!}
        {{ csrf_field() }}
        <input type="hidden" name="inp-id-servicio" value="@if(isset($servicios)) @endif">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Asociar Cliente con Servicio</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>¿Dónde estará el servicio?</h4>
                    </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-md-4">
                          {{ Form::label('provincia', 'Provincia') }}
                          {{ Form::select("provincia", array('1' => 'Misiones') ,null, array("class"=>"form-control")) }}
                      </div>

                      <div class="form-group col-md-4">
                          {{ Form::label('departamento', 'Departamento *') }}
                          {{ Form::select("departamento",array('Seleccionar...') + $array_departamentos ,null, array("class"=>"form-control")) }}
                          @if ($errors->has('departamento'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('departamento') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-4">
                          {{ Form::label('localidad', 'Localidad *') }}
                          {{ Form::select("localidad", array('Seleccionar...') ,null, array("class"=>"form-control")) }}
                          @if ($errors->has('localidad'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('localidad') }}</strong>
                          </span>
                          @endif
                      </div>
                  </div>
                  <div class="hr-line-dashed"></div>

                  <div class="row">
                     <div class="col-lg-offset-4 col-lg-4">
                       <div class="form-group">
                           <label class="control-label">Seleccionar Servicio:</label>
                           <select class="select2_demo_1 form-control" name="sel-id-servicio" height="100%">
                                <option></option>
                            </select>
                       </div>
                     </div>
                  </div>
                  <div class="row hidden" id="loading">
                     <div class="col-lg-12 text-center">
                           <div class="sk-spinner sk-spinner-wave">
                               <div class="sk-rect1"></div>
                               <div class="sk-rect2"></div>
                               <div class="sk-rect3"></div>
                               <div class="sk-rect4"></div>
                               <div class="sk-rect5"></div>
                           </div>
                     </div>
                  </div>

                  <div class="hr-line-dashed"></div>

                  <input type="hidden" name="id_cliente"  value="{{ $cliente->id }}"/>
                  <input type="hidden" name="id_persona"  value="{{ $cliente->persona->id }}"/>
                  <div class="row">
                      <div class="form-group col-lg-offset-4 col-md-4">
                          {{ Form::label('fecha_inicio', 'Fecha de Inicio') }}
                          <div class="input-group date">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_inicio" placeholder="dd/mm/yyyy" class="form-control fecha_inicio" value="">
                          </div>
                          @if ($errors->has('fecha_inicio'))
                              <span class="help-block text-warning" >
                                  <strong>
                                      {{ $errors->first('fecha_inicio') }}
                                  </strong>
                              </span>
                          @endif
                      </div>
                      <div class="form-group col-md-2 hidden">
                          {{ Form::label('fecha_fin', 'Fecha de Fin') }}
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_fin" id="fecha_fin" placeholder="dd/mm/yyyy" class="form-control " value="">
                          </div>
                          @if ($errors->has('fecha_fin'))
                              <span class="help-block text-warning" >
                                  <strong>
                                      {{ $errors->first('fecha_fin') }}
                                  </strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="hr-line-dashed"></div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>Información del Domicilio de Instalación:</h4>
                    </div>
                  </div>

                  <div class="row">


                      <div class="form-group col-md-4">
                          {{ Form::label('calle', 'Calle *') }}
                          {{ Form::text("calle", null, array("class"=>"form-control", "placeholder"=>"Nombre de la calle", "list" => "calles")) }}
                          <datalist id="calles">
                              @foreach($calles as $calle)
                                  <option value="{{ $calle['calle'] }}">
                              @endforeach
                          </datalist>
                          @if ($errors->has('calle'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('calle') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-2">
                          {{ Form::label('altura', 'Altura *') }}
                          {{ Form::number("altura", "0", array("class"=>"form-control","value" => "0", "placeholder"=>"Altura de la Calle")) }}
                          @if ($errors->has('altura'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('altura') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-2">
                          {{ Form::label('piso', 'Piso') }}
                          {{ Form::number("piso", "0", array("class"=>"form-control", "placeholder"=>"Piso del Depto.")) }}
                          @if ($errors->has('piso'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('piso') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-2">
                          {{ Form::label('nro_depto', 'Depto.') }}
                          {{ Form::text("nro_depto", null, array("class"=>"form-control", "placeholder"=>"Depto.")) }}
                          @if ($errors->has('nro_depto'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('nro_depto') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-2">
                          {{ Form::label('cp', 'Código Postal') }}
                          {{ Form::text("cp", null, array("class"=>"form-control", "placeholder"=>"Código Postal")) }}
                          @if ($errors->has('cp'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('cp') }}</strong>
                          </span>
                          @endif
                      </div>

                      <div class="form-group col-md-4">
                          {{ Form::label('calle_paralela', 'Calle Paralela') }}
                          {{ Form::text("calle_paralela", null, array("class"=>"form-control")) }}
                          @if ($errors->has('calle_paralela'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('calle_paralela') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-4">
                          {{ Form::label('calle_perpendicular_1', 'Calle Perpendicular 1') }}
                          {{ Form::text("calle_perpendicular_1", null, array("class"=>"form-control")) }}
                          @if ($errors->has('calle_perpendicular_1'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('calle_perpendicular_1') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-4">
                          {{ Form::label('calle_perpendicular_2', 'Calle Perpendicular 2') }}
                          {{ Form::text("calle_perpendicular_2", null, array("class"=>"form-control")) }}
                          @if ($errors->has('calle_perpendicular_2'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('calle_perpendicular_2') }}</strong>
                          </span>
                          @endif
                      </div>

                      <div class="form-group col-md-4">
                          {{ Form::label('link_mapa', 'Link Mapa') }}
                          {{ Form::text("link_mapa", null, array("class"=>"form-control", "placeholder"=>"Link Mapa Geolocalización")) }}
                          @if ($errors->has('link_mapa'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('link_mapa') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-4">
                          {{ Form::label('latitud', 'Latitud') }}
                          {{ Form::text("latitud", null, array("class"=>"form-control", "placeholder"=>"Latitud")) }}
                          @if ($errors->has('latitud'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('latitud') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-md-4">
                          {{ Form::label('longitud', 'Longitud') }}
                          {{ Form::text("longitud", null, array("class"=>"form-control", "placeholder"=>"Longitud")) }}
                          @if ($errors->has('longitud'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('longitud') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group col-lg-offset-3 col-md-6">
                          {{ Form::label('observaciones', 'Observaciones') }}
                          {{ Form::text("observaciones", null, array("class"=>"form-control", "placeholder"=>"Observaciones del domicilio...")) }}
                          @if ($errors->has('observaciones'))
                          <span class="help-block text-danger">
                              <strong>{{ $errors->first('observaciones') }}</strong>
                          </span>
                          @endif
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12 text-center">
                          <a href="{{ URL::previous() }}" ><button type="button" class="btn btn-white">Volver</button></a>
                          <button type="submit" class="btn btn-primary" >Guardar</button>
                      </div>
                  </div>
                </div>
            </div>
        </div>
      {!! Form::close() !!}
    </div>
</div>
<!-- Fin Contenido Principal -->


@endsection
@section('javascript')

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $(document).ready(function(){

//        var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];

        $(".select2_demo_3").select2({
            theme: "bootstrap",
            placeholder: "Buscar...",
            allowClear: false,
//            data: data,
        });

        $(".select2_demo_1").select2({
            theme:'bootstrap',
            placeholder: "Buscar..."
        });


        $('.input-group.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('#fecha_fin').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        $('.input-group.date').datepicker("setDate", new Date());

        $('#departamento').on('change',function(){
            if(this.value !== 0){
                var valor = this.value;
                $.ajax({
                   type: "GET",
                   url: "/localidades/get",
                   data: "depto="+valor,
                   success: function(data)
                   {
                       $('#localidad').not(":nth-child(1)").empty();
                        var a = JSON.parse(data);
                        $.each(a, function(key, value) {
                            $('#localidad')
                                .append($("<option></option>")
                                           .attr("value",value.id)
                                           .text(value.nombre));
                       });
                       cargarServiciosFromLocalidad($('#localidad').val());
                   },
                   error: function(msg){
                       alert(msg);
                   }

                });
            }
        });

        $('#localidad').on('change', function(event) {
          cargarServiciosFromLocalidad($(this).val());
        });

        swal({
          title: "Copia De Domicilio",
          text: "¿Desea copiar el domicilio del cliente y asignarlo a esta conexion?",
          icon: "info",
          buttons: ["NO", "SI"],
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              method: "POST",
              url: "/clientes/get-all-data",
              data: { "_token": "{{ csrf_token() }}",
                      idCliente      : {{ $cliente->id }}
                    },
              dataType: 'JSON'
            })
              .done(function( msg ) {
                  console.log(msg.datosPersona.domicilio);
                  $('[name="departamento"]').val(msg.datosPersona.domicilio[0].fk_departamento).triggerHandler('change');
                  setTimeout(function(){
                    $('[name="localidad"]').val(msg.datosPersona.domicilio[0].fk_localidad).trigger('change');
                  }, 2000);


                  $('[name="calle"]').val(msg.datosPersona.domicilio[0].calle);
                  $('[name="altura"]').val(msg.datosPersona.domicilio[0].altura);
                  $('[name="piso"]').val(msg.datosPersona.domicilio[0].piso);
                  $('[name="nro_depto"]').val(msg.datosPersona.domicilio[0].depto);
                  $('[name="cp"]').val(msg.datosPersona.domicilio[0].cp);
                  $('[name="calle_paralela"]').val(msg.datosPersona.domicilio[0].calle_paralela);
                  $('[name="calle_perpendicular_1"]').val(msg.datosPersona.domicilio[0].calle_perpendicular_1);
                  $('[name="calle_perpendicular_2"]').val(msg.datosPersona.domicilio[0].calle_perpendicular_2);
                  $('[name="link_mapa"]').val(msg.datosPersona.domicilio[0].link_mapa);
                  $('[name="latitud"]').val(msg.datosPersona.domicilio[0].latitud);
                  $('[name="longitud"]').val(msg.datosPersona.domicilio[0].longitud);

            })
              .fail(function(mgs){
                swal({
                  title: "Error!",
                  text: "Ocurrio un error al cargar los servicios de la localidad seleccionada. Intentelo nuevamente por favor.",
                  type: "error",
                  confirmButtonText: "Cerrar"
                });
            });
          } else {

          }
        });
    });

    function cargarServiciosFromLocalidad(idLocalidad){
      //$('#loading').fadeIn('400');
      $.ajax({
        method: "POST",
        url: "/clientes/getServiciosFromLocalidad",
        data: { "_token": "{{ csrf_token() }}",
                idLocalidad      : idLocalidad
              },
        dataType: 'JSON'
      })
        .done(function( msg ) {
            var cant = Object.keys(msg);
            $('[name="sel-id-servicio"]').html('<option></option>');
            Object.keys(msg).forEach(function(key) {
                //console.log(key, msg[key]);
                $('[name="sel-id-servicio"]').append('<option value="'+msg[key].id+'">'+msg[key].nombre+' ('+ msg[key].zona +')</option>');
            });


      })
        .fail(function(mgs){
          swal({
            title: "Error!",
            text: "Ocurrio un error al cargar los servicios de la localidad seleccionada. Intentelo nuevamente por favor.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
    }



</script>
@stop
