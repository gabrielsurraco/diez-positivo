@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="row wrapper border-bottom white-bg page-heading m-b-md">
    <div class="col-lg-10">
        <h2>Edición de Servicio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Ventas</a>
            </li>
            <li class="active">
                <strong>Servicios</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="container">
    <div class="row">
        @if (Session::has('flash_notification.message'))
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 ">
                        <div class="alert alert-{{ session('flash_notification.level') }} ">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! Session('flash_notification.message') !!}
                        </div>
                    </div>
                </div>
            </div>

        @endif
            
        <div class="col-md-12">
            {!! Form::open(['url' => '/servicios/guardar', 'method' => 'post','id' => 'form']) !!}
                <div class="row">
                    <input type="hidden" name="id_servicio"  value="{{ $servicio->id }}"/>
                    <input type="hidden" name="id_domicilio"  value="{{ $domicilio->id }}"/>
                    <div class="form-group col-md-6">
                        {{ Form::label('fecha_inicio', 'Fecha de Inicio') }}
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_inicio" class="form-control fecha_inicio" value="{{ $servicio->fecha_inicio }}">
                        </div>
                        @if ($errors->has('fecha_inicio'))
                            <span class="help-block text-warning" >
                                <strong>
                                    {{ $errors->first('altura') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('servicio', 'Servicio') }}
                        {{ Form::select('servicio', array('servicio_internet'=>'Servicio de Internet') , $servicio->concepto_servicio , array("class"=>"form-control")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('tipo_servicio', 'Tipo de Servicio') }}
                        {{ Form::select('tipo_servicio', array('comercial'=>'Comercial', 'dedicado' => 'Dedicado','estandar' => 'Estandar', 'premium' => 'Premium','rural' => 'Rural', 'simetrico' => 'Simétrico') ,$servicio->tipo_servicio, array("class"=>"form-control")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('ancho_banda', 'Ancho de Banda') }}
                        {{ Form::select('ancho_banda', array('1'=>'1 Mega', '2' => '2 Megas','3' => '3 Megas', '4' => '4 Megas','5' => '5 Megas', '6' => '6 Megas','7' => '7 Megas','8' => '8 Megas','9' => '9 Megas','10' => '10 Megas') ,$servicio->ancho_banda, array("class"=>"form-control")) }}
                    </div>
                    <div class="col-md-12">
                        <h4 >Domicilio</h4><hr style="margin-bottom: 15px; margin-top: 5px; border-top: 1px solid #ada4a4;" />
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('departamento', 'Departamento') }}
                        {{ Form::select('departamento', array('Seleccionar...') + $array_departamentos ,$domicilio->fk_departamento, array("class"=>"form-control")) }}
                        @if ($errors->has('departamento'))
                            <span class="help-block text-danger" >
                                <strong>
                                    {{ $errors->first('departamento') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('localidad', 'Localidad') }}
                        {{ Form::select('localidad', array('Seleccionar...') + $array_localidades ,$domicilio->fk_localidad, array("class"=>"form-control")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('calle', 'Calle') }}
                        {{ Form::text("calle", $domicilio->calle, array("class"=>"form-control", "placeholder"=>"Nombre de la Calle...")) }}
                        @if ($errors->has('calle'))
                            <span class="help-block text-danger" >
                                <strong>
                                    {{ $errors->first('calle') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('altura', 'Altura') }}
                        {{ Form::number("altura", $domicilio->altura, array("class"=>"form-control", "placeholder"=>"Altura")) }}
                        @if ($errors->has('altura'))
                            <span class="help-block text-danger" >
                                <strong>
                                    {{ $errors->first('altura') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('cp', 'CP:') }}
                        {{ Form::text("cp", $domicilio->cp , array("class"=>"form-control", "placeholder"=>"CP")) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('piso', 'Piso:') }}
                        {{ Form::text("piso", $domicilio->piso, array("class"=>"form-control", "placeholder"=>"Piso")) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('nro_depto', 'Depto.:') }}
                        {{ Form::text("nro_depto", $domicilio->nro_depto, array("class"=>"form-control", "placeholder"=>"Depto.")) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('latitud', 'Latitud:') }}
                        {{ Form::text("latitud", $domicilio->latitud, array("class"=>"form-control", "placeholder"=>"Latitud")) }}
                    </div>
                    <div class="form-group col-md-3">
                        {{ Form::label('longitud', 'Longitud:') }}
                        {{ Form::text("longitud", $domicilio->longitud, array("class"=>"form-control", "placeholder"=>"Longitud")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('link_mapa', 'Link Mapa:') }}
                        {{ Form::text("link_mapa", $domicilio->link_mapa, array("class"=>"form-control", "placeholder"=>"Link")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('observaciones', 'Observaciones') }}
                        {{ Form::text("observaciones", $domicilio->observaciones , array("class"=>"form-control", "placeholder"=>"Observaciones...")) }}
                    </div>
                </div>
                <div class="row">
                    @if($errors->any())
                    <div class="col-md-12 text-center">
                        <span class="help-block text-danger" >{{ $errors->first() . ' - Favor verificar lo ingresado!' }}</span>
                    </div>
                    @endif
                    
                    <div class="col-md-12 text-center">
                        <a href="/ventas/clientes/{{ $servicio->fk_cliente }}/servicios"><button type="button" class="btn btn-white">Volver</button></a>
                        <button type="submit" class="btn btn-primary" >Guardar</button>
                    </div>
                </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('javascript')

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script>
    $(document).ready(function(){

        
        $('.input-group.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });
        
        $('#departamento').on('change',function(){
            if(this.value !== 0){
                var valor = this.value; 
                $.ajax({
                   type: "GET",
                   url: "/localidades/get",
                   data: "depto="+valor,
                   success: function(data)
                   {
                       $('#localidad').not(":nth-child(1)").empty();
                        var a = JSON.parse(data);
                        $.each(a, function(key, value) {   
                            $('#localidad')
                                .append($("<option></option>")
                                           .attr("value",value.id)
                                           .text(value.nombre)); 
                       });
                   },
                   error: function(msg){
                       alert(msg);
                   }
                   
                });
            }
           
        });

    });
</script>
@stop



