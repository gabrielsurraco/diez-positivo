@extends('layouts.backend')


@section('css')
<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Registrar Pago del Cliente</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">
                    <div class="col-md-offset-1 col-md-3">
                      <div class="form-group">
                          <label>Fecha del Pago:</label>
                          <div class="input-group date">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha" data-fecha="true" class="form-control">
                          </div>
                      </div>

                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                          <label>Seleccione Cliente Para Cobrar:</label>
                          <select class="select2_demo_1 form-control" name="sel-cliente" height="100%">
                               <option></option>
                           </select>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <button type="button" style="margin-top:23px;" onclick="buscarDatosCuentaCliente()" class="btn btn-success">
                        <span class="fa fa-search"></span>
                      </button>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Importe Abonado:</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span><input type="number" step="0.01" name="inp-pago-importe" value="0" class="form-control">
                            </div>

                        </div>

                    </div>

                  </div>
                </div>
              </div>
          </div>
      </div> <!-- Fin Row Primer ROW -->

      <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>10 Ultimos Mov. En Cuenta Corriente Cliente:</h5>
                </div>

                <div class="ibox-content">
                  <table class="table table-striped" id="table-cc">
                      <thead>
                      <tr>
                          <th>Secuencia</th>
                          <th>Fecha</th>
                          <th>Concepto</th>
                          <th>Debe</th>
                          <th>Haber</th>
                      </tr>
                      </thead>
                      <tbody>

                      </tbody>
                  </table>

                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>10 Ultimas Facturas Generadas:</h5>
                </div>

                <div class="ibox-content">
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>#</th>
                          <th>Data</th>
                          <th>User</th>
                          <th>Value</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <td>1</td>
                          <td><span class="line">5,3,2,-1,-3,-2,2,3,5,2</span></td>
                          <td>Samantha</td>
                          <td class="text-navy"> <i class="fa fa-level-up"></i> 40% </td>
                      </tr>
                      <tr>
                          <td>2</td>
                          <td><span class="line">5,3,9,6,5,9,7,3,5,2</span></td>
                          <td>Jacob</td>
                          <td class="text-warning"> <i class="fa fa-level-down"></i> -20% </td>
                      </tr>
                      <tr>
                          <td>3</td>
                          <td><span class="line">1,6,3,9,5,9,5,3,9,6,4</span></td>
                          <td>Damien</td>
                          <td class="text-navy"> <i class="fa fa-level-up"></i> 26% </td>
                      </tr>
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div> <!-- Fin ROW segundo. -->

      <div class="row">
        <div class="col-lg-offset-1 col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right"></span>
                    <h5>Saldo Cuenta</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">60 420,600</h1>
                    <div class="stat-percent font-bold text-info">40% <i class="fa fa-level-up"></i></div>
                    <small>New orders</small>
                </div>
            </div>
        </div>

        <div class="col-lg-offset-2 col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right"></span>
                    <h5>Ultimo Pago Realizado</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">60 420,600</h1>
                    <div class="stat-percent font-bold text-success">24/01/2018</div>
                    <small>Fecha del Pago</small>
                </div>
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 text-center">
            <div class="ibox ">
                <div class="ibox-content">
                  <button class="btn btn-success" id="btn-gen-recibo"  onClick="generarRecibo();" type="button"><i class="fa fa-check"></i> Imputar Pago</button>
                  <a class="btn btn-primary informarPago" data-toggle="modal" data-target="#myModal" > <i class="fa fa-file-o"></i> Importar Pagos</a>
                </div>
            </div>
        </div>
      </div>
      <form class="formControl" action="/clientes/getDatosDeudaFromClienteJSON" method="post">
        {{csrf_field()}}
        <button type="submit" name="button">ENVIAR</button>
      </form>
</div>


@endsection

@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('/backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- DatePicker  -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>



<!-- Page-Level Scripts -->
<script>

  $(document).ready(function() {
    $(".select2_demo_1").select2({
        theme:'bootstrap'
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('.date').val(moment().format('DD/MM/YYYY'));

    $('.date').datepicker({
        format: "dd/mm/yyyy",
        calendarWeeks: true,
        autoclose: true
    });

    $('[name="sel-cliente"]').select2({
      ajax: {
        method: "POST",
        url: '/clientes/jsonSearchCliente',
        dataType: 'json',
        data: function (params){
              var query = {
              palabraClave: params.term,
              "_token": "{{ csrf_token() }}"
              }

              // Query parameters will be ?search=[term]&type=public
              return query;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      },
      minimumInputLength: 3,
      placeholder: "Buscar Cliente.",
      language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
      theme:'bootstrap'
    });


  });

  var table;
  var globalImporteTotal = 0;
  var globalArrayOV = null;
  function buscarDatosCuentaCliente(){
      if (table != null){
        table.destroy();
      }
      if ($('[name="sel-cliente"]').val() == ''){
        swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
        return
      }
      $.ajax({
        method: "POST",
        url: "/clientes/getDatosDeudaFromClienteJSON",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val()},
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          //Cargamos los datos de cuenta corriente en la tabla cuenta Corriente
          $('#table-cc tbody').html('');
          var val = msg.CuentaCorriente;
          val.forEach(function (item){
              $('#table-cc tbody').append('<tr>'+
                  '<td align="center">'+ item.nro_secuencia +'</td>'+
                  '<td align="center">'+ item.fecha +'</td>'+
                  '<td>'+ item.descripcion +'</td>'+
                  '<td align="center">'+ item.debe +'</td>'+
                  '<td align="center">'+ item.haber +'</td>'+
              '</tr>');
          });

          return
          globalArrayOV = msg;
          $('#table-resultados tbody').html('');
          msg.forEach(function (item){
            if(item.pagos_efectuados != null){
              var facturado = '<span class="label label-info">NO FACTURADO</span>';
              var btn_asociar_fac = ' <a class="btn btn-info btn-xs btn-bitbucket" onclick="asociarFactura('+ item.id +')"><i data-toggle="tooltip" data-placement="top" title="Asociar Factura" class="fa fa-file-text-o"></i></a>';
            }else{
              if(item.pagos_efectuados[0].nro_comprobante == null){
                var facturado = '<span class="label label-info">NO FACTURADO</span>';
                var btn_asociar_fac = ' <a class="btn btn-info btn-xs btn-bitbucket" onclick="asociarFactura('+ item.id +')"><i data-toggle="tooltip" data-placement="top" title="Asociar Factura" class="fa fa-file-text-o"></i></a>';
              }else{
                var facturado = '<span class="label label-success">FACTURADO</span>';
                var btn_asociar_fac = '';
              }
            }

            var totalObli = parseFloat(item.monto) ;
            var saldo = parseFloat(item.pagoTotalEfectuado) - parseFloat(item.monto);
            var cssDanger = '';
            if(saldo < 0){
                cssDanger = 'text-danger';
                var cobrado = '<span class="label label-danger">NO COBRADO</span>';
                var disabledVar = '';
            }else{
                cssDanger = 'text-success';
                var cobrado = '<span class="label label-primary">COBRADO</span>';
                var disabledVar = 'disabled';
            }

            //esto es para saber si es una orden de venta cobrable por completo o ya tiene un pago parcial.
            if(totalObli == (saldo * -1)){
              var cobrable = true;
            }else{
              var cobrable = false;
            }
            var fecha = item.orden_venta.fecha;
            $('#table-resultados tbody').append('<tr>'+
                '<td align="center"><input type="checkbox" '+ disabledVar +' class="i-checks" data-index="'+ item.id +'" data-cobrable="'+ cobrable +'" name="input[]"></td>'+
                '<td><a href="/ventas/editOrdenVenta/'+ item.orden_venta.id +'" target="_blank">'+ item.concepto +'</a></td>'+
                '<td>'+ item.orden_venta.fecha +'</td>'+
                '<td align="center">'+ facturado +'</td>'+
                '<td align="center">'+ cobrado +'</td>'+
                '<td align="center">$ '+ totalObli +'</td>'+
                '<td align="center" class="'+ cssDanger +'" name="saldo">$ '+ saldo +'</td>'+
                '<td align="center">'+
                  '<a  class="btn btn-success btn-xs btn-bitbucket" href="/ventas/editOrdenVenta/'+ item.id +'" target="_blank" ><i data-toggle="tooltip" data-placement="top" title="Ver Detalle Orden de Venta" class="fa fa-eye"></i></a>'+
                  btn_asociar_fac+
                '</td>'+
            '</tr>');

            //CARGAR ORDENES DE VENTA AL SELECT
            $(".chosen-select").append("<option value='"+item.id+"'> <strong>Ord. Vent.: </strong> #"+item.id+" -- <strong>TOTAL:</strong> $ "+saldo+"<strong> -- FECHA: </strong>"+fecha+"</option>");

            $('.chosen-select').trigger("chosen:updated");

          });
          table = $('#table-resultados').DataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]});
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            //funcion del boton seleccionar todos los elementos.
            $('#check-all').on('ifChanged', function(event){
              if(event.target.checked){
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('check');
                 });
              }else{
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('uncheck');
                 });
              }
            });
            $('[data-toggle="tooltip"]').tooltip();

            //codigo para calcular el total de items seleccionados.
            $('[name="input[]"]').on('ifChanged', function(event){
                globalImporteTotal = 0;
                $('[name="input[]"]:checked').each(function () {
                  //arrayov.push($(this).data('index'));
                  var valor = $(this).closest("tr").find('td[name="saldo"]').text();
                  valor = parseFloat(valor.substr(2, valor.length)) * -1
                  globalImporteTotal += valor;
                  console.log('valor ' + globalImporteTotal);
                });
                $('#label-importeTotal').html('$ '+ globalImporteTotal.toFixed(2));

                var importe = globalImporteTotal - $('[name="inp-pago-cliente"]').val();
                $('#label-saldo').html('$ '+ importe.toFixed(2));
             });
            //fin codigo
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });

}
var arrayoblig = Array();
function generarRecibo(){
    arrayoblig = Array();
    $('[name="input[]"]:checked').each(function () {
      console.log(globalArrayOV);
      var valor  = parseInt($(this).data('index'));
      var objeto = $.grep(globalArrayOV, function(e){ return e.id == valor; });

      var entrada = {idObligacion:$(this).data('index')};
      arrayoblig.push(entrada);
      console.log(arrayoblig);
    });
    return
    if (arrayoblig.length == 0){
      swal("ups!", "Tiene que seleccionar al menos una orden de venta para generar recibo.", "warning");
      return
    }
    if ($('[name="sel-cliente"]').val() == ''){
      swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() == '' || $('[name="inp-pago-cliente"]').val() == 0){
      swal("ups!", "El pago del cliente no puede ser 0 ni vacio.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() > globalImporteTotal){
      swal("ups!", "El pago del cliente no puede ser superior al pago total.", "warning");
      return
    }
    $.ajax({
      method: "POST",
      url: "/cobranza/generarCobro",
      data: {
              "_token": "{{ csrf_token() }}",
              "idCliente": $('[name="sel-cliente"]').val(),
              "pagoCliente" : $('[name="inp-pago-cliente"]').val(),
              'arrayObli': arrayoblig
            },
      dataType: 'json'
    }).done(function( msg ) {
        console.log(msg);
        window.open('/recibos/print/'+ msg.idRecibo,'_blank');
        location.reload();
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
  }


  function asociarFactura(id){
     $('#modal-assocFac').modal('show');
     $('#btn-save-asociacion').on('click', function(event) {
         event.preventDefault();
         /* Act on the event */
         $.ajax({
           url: '/pagoEfectuado/asociarFactura',
           type: 'POST',
           data: {
                    "_token": "{{ csrf_token() }}",
                    idOrdenVenta: id,
                    asocFactura: $.trim($('#inp-num-factura').val())
                 }
         })
         .done(function() {
           $('#modal-assocFac').modal('hide');
           swal("Asociado!", "Factura Asociada Correctamente.", "success");
           location.reload();
         })
         .fail(function() {
           swal("Error", "Algo esta mal, intentelo de nuevo mas tarde.", "error");
         });
     });
  }

  function informarPago(){
    arrayoblig = Array();
    $('[name="input[]"]:checked').each(function () {
      console.log(globalArrayOV);
      var valor  = parseInt($(this).data('index'));
      var objeto = $.grep(globalArrayOV, function(e){ return e.id == valor; });

      var entrada = {idObligacion:$(this).data('index'), saldoObli: objeto};
      arrayoblig.push(entrada);
      console.log(arrayoblig);
    });

    if (arrayoblig.length == 0){
      swal("ups!", "Tiene que seleccionar al menos una orden de venta para generar recibo.", "warning");
      return
    }
    if ($('[name="sel-cliente"]').val() == ''){
      swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
      return
    }
    if($('[name="monto"]').val() == '' || $('[name="monto"]').val() == 0){
      swal("ups!", "El pago del cliente no puede ser 0 ni vacio.", "warning");
      return
    }
    if($('[name="monto"]').val() > globalImporteTotal){
        swal("ups!", "El pago del cliente no puede ser superior al pago total.", "warning");
        return
    }
    $.ajax({
      url: '/cobranza/informarPago',
      type: 'POST',
      dataType: 'json',
      data: {
              "_token": "{{ csrf_token() }}",
              "idCliente"        : $('[name="sel-cliente"]').val(),
              "fecha"            : $('[name="fecha_pago"]').val(),
              "arrayOV"          : arrayoblig,
              "pagoCliente"      : $('[name="monto"]').val(),
              "reciboManualNum"  : $('[name="nro_comprobante"]').val(),
              "descripcion"      : $('[name="descripcion"]').val(),
            }
    })
    .done(function() {
      console.log("success");
      $('#myModal').modal('hide');
      swal("Exito!", "Pago informado correctamente.", "success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }
</script>

@stop
