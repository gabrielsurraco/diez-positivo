@extends('layouts.backend')


@section('css')


<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

@endsection


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                      <h5>Nueva Orden de Venta</h5>
                      <div class="ibox-tools">
                        <?php
                          if(isset($ov)){
                            switch ($ov->estado) {
                                case 'borrador':
                                  echo '<strong>ESTADO: </strong><span class="label label-warning-light pull-right">BORRADOR</span>';
                                  break;

                                case 'completa':
                                  echo '<strong>ESTADO: </strong><span class="label label-primary pull-right">COMPLETA</span>';
                                  break;

                                case 'pagado':
                                  echo '<strong>ESTADO: </strong><span class="label label-primary pull-right">PAGADO</span>';
                                  break;

                                case 'facturada':
                                  echo '<strong>ESTADO: </strong><span class="label label-info pull-right">FACTURADA</span>';
                                  break;
                              }
                          }
                        ?>
                      </div>
                </div>
                <div class="ibox-content">
                  @if(isset($ov))
                  <div class="row">
                    <div class="col-lg-offset-9 col-lg-3 text-right">
                      <div class="btn-group">
                          <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"><span class="fa fa-print"></span> Imprimir <span class="caret"></span></button>
                          <ul class="dropdown-menu">
                              <li><a href="/ventas/ordenesVenta/print/PRESUPUESTO/{{$ov->id}}">Presupuesto</a></li>
                              <li @if(isset($ov)) @if($ov->estado == 'borrador') class="disabled" @endif @endif><a href="#">Factura</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                          </ul>
                      </div>
                    </div>
                  </div>
                  @endif
                  <div class="row">
                     <div class="col-lg-3">
                       <div class="form-group">
                           <label>Fecha:</label>
                           <div class="input-group date">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha" data-fecha="true" class="form-control">
                           </div>
                       </div>
                     </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                           <label>Cliente:</label>
                           <select class="select2_demo_1 form-control" name="sel-cliente" height="100%">
                                <option></option>
                            </select>
                       </div>
                     </div>

                     <div class="col-lg-3 text-center">
                       <button class="btn btn-info btn-xs" onclick="openModal();" style="margin-top:30px;" type="button"><i class="fa fa-paste"></i> Buscar Servicios Asociados</button>
                     </div>

                     <div class="col-lg-3">
                       <div class="form-group">
                           <label>Fecha Vencimiento Pago:</label>
                           <div class="input-group date">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="inp-fecha-venc" data-fecha="true" class="form-control">
                           </div>
                       </div>
                     </div>

                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>Caracteristicas del item</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-1">
                      <div class="form-group">
                          <label>Cantidad:</label>
                          <div class="input-group date">
                              <input type="number" min="0" name="cantidad-item" class="form-control" placeholder="0.00">
                          </div>
                      </div>
                    </div>

                    <div class="col-lg-3">
                      <div class="form-group">
                          <label>Articulo - Servicio:</label>
                          <!--<input type="text" style="text-transform:uppercase" name="descripcion-item" class="form-control"> -->
                          <select class="select2_demo_1 form-control" name="descripcion-item" height="100%">
                               <option></option>
                           </select>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group">
                          <label>Observacion:</label>
                          <input type="text" class="form-control" name="inp-observacion" value="" style="text-transform: uppercase;">
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Alicuota IVA:</label>
                          <div class="input-group m-b">
                              <span class="input-group-addon"><span class="fa fa-university"></span></span>
                              <select class="form-control" name="sel-alicuota-iva">
                                <option value="0">0 %</option>
                                <option value="10.5">10.50 %</option>
                                <option value="21" selected>21 %</option>
                              </select>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-offset-6 col-lg-2">
                      <div class="form-group">
                          <label>Precio Unit.: <small>con IVA</small></label>
                          <div class="input-group m-b">
                              <span class="input-group-addon">$</span><input type="number" min="0" name="importe-item" class="form-control" placeholder="0.00">
                          </div>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Bonif:</label>
                          <div class="input-group m-b">
                              <span class="input-group-addon">%</span><input type="number" min="0" max="100" name="bonificacion-item" class="form-control" placeholder="0">
                          </div>
                      </div>
                    </div>

                    <div class="col-lg-2">
                      <div class="form-group">
                          <label>Total:</label>
                          <strong style="display:block; font-size:18pt;" id="total-item" class="text-success">$ 0.00</strong>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-success btn-outline" id="btn-agregarItem" onclick="agregarNuevoItem();" type="button"><i class="fa fa-plus"></i><span class="bold"> Agregar</span></button>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-warning btn-sm btn-outline" id="btn-borrarElementos" onclick="borrarItemSeleccionado();" type="button"><i class="fa fa-trash-o"></i><span class="bold"> Borrar Seleccionados</span></button>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <div class="table-responsive">
                              <table class="table table-striped" id="table-items">
                                  <thead>
                                  <tr>
                                      <th width="5%" style="text-align:center"><input type="checkbox" class="i-checks" data-index="all" id="check-all"></th>
                                      <th width="5%" style="text-align:center">Cantidad </th>
                                      <th width="20%" style="text-align:center">Descripción </th>
                                      <th width="20%" style="text-align:center">Observacion </th>
                                      <th width="15%" style="text-align:center">Precio Unit.</th>
                                      <th width="10%" style="text-align:center">Alicuota IVA</th>
                                      <th width="10%" style="text-align:center">Bonificacion</th>
                                      <th width="15%" style="text-align:center">Importe</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <tr style="background-color: transparent;">
                                        <td colspan="8"><div class="alert alert-info text-center">
                                            No hay datos.
                                        </div></td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-10 text-right text-success">
                        <h2><strong>TOTAL:</strong></h2>
                    </div>
                    <div class="col-lg-2 text-right text-success" id="totalImporte">
                        <h2><strong> $ 0.00</strong></h2>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-warning" id="btn-gen-guardar" onClick="save(0);" type="button"><i class="fa fa-save"></i> Guardar Borrador</button>
                        <button class="btn btn-primary" id="btn-gen-cerrar"  onClick="save(1);" type="button"><i class="fa fa-check"></i> Cerrar Orden Venta</button>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->

<!-- Modal para listar servicios del cliente -->
<div class="modal inmodal fade" id="modal-servicios-clientes" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Servicios del Cliente</h4>
              <small class="font-bold">Este listado muestra los servicios asociados al cliente seleccionado.</small>
          </div>
          <div class="modal-body">
              <div class="row">
                 <div class="col-md-12">
                   <div class="table-responsive">
                       <table class="table table-striped table-hover dataTables" id="table-servicios-clientes">
                           <thead>
                           <tr>
                               <th>Servicio</th>
                               <th>Localidad</th>
                               <th>Direccion</th>
                               <th>Fecha Inicio</th>
                               <th>Fecha Fin</th>
                               <th>Estado</th>
                               <th>Precio</th>
                               <th>Acciones</th>
                           </tr>
                           </thead>
                           <tbody>

                           </tbody>
                       </table>
                   </div>
                 </div>
              </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
  </div>
</div>
<!-- fin modal listar servicios del cliente  -->
@endsection

@section('javascript')
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>


<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      $(".select2_demo_1").select2({
          theme:'bootstrap'
      });
      var date = new Date();
      $('[data-fecha="true"]').val((date.getDate() < 10 && '0') + date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $('[data-fecha="true"]').datepicker({
          format: 'dd/mm/yyyy',
          language: 'es'
      });
      $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });

      //si viene por edit carga los valores.
      @if(isset($dov))
      var s = '<?php echo json_encode($dov); ?>';
      var obj = JSON.parse(s);
      console.log('objeto');
      console.log(obj);
      console.log('fin objeto');
      for (var i=0; i < Object.keys(obj).length ; i++){
            var descript = '-';
            if(obj[i].fk_articulo != null){
                descript = obj[i].articulo.marca + ' ' + obj[i].articulo.modelo;
            }else if(obj[i].fk_servicio != null){
                descript = obj[i].servicio.nombre;
            }
            var itemVenta = {
                           id                 : globalItems.length + 1,
                           idDetalleOV        : obj[i].id,
                           cantidad           : obj[i].cantidad,
                           observacion        : obj[i].observacion,
                           articulo_id        : (obj[i].fk_articulo != null ? obj[i].fk_articulo : null),
                           servicio_id        : (obj[i].fk_servicio != null ? obj[i].fk_servicio : null),
                           descripcion_item   : descript,
                           precioUnitario     : obj[i].precio_unitario,
                           alicuota_iva       : obj[i].alicuota_iva,
                           bonificacion       : obj[i].bonificacion,
                           total              : (obj[i].precio_unitario - (obj[i].precio_unitario * obj[i].bonificacion / 100)) * obj[i].cantidad
                         }
          globalItems.push(itemVenta);
      }
      $('[name="inp-fecha"]').val('{{ $ov->fecha }}');
      $('[name="sel-cliente"]').append($("<option/>").val("{{$ov->fk_persona}}").text("{{$ov->persona->apellido}} {{$ov->persona->nombre}}")).val("{{$ov->fk_persona}}").trigger("change");
      if(obj != null){
          $('[name="inp-fecha-venc"]').val('{{$ov->vencimiento_pago}}');
      }
      @if($ov->estado == 'completa')
      $('#btn-gen-recibo').removeClass('hidden');
      $('#btn-gen-factura').removeClass('hidden');
      $('#btn-gen-guardar').addClass('hidden');
      $('#btn-gen-cerrar').addClass('hidden');
      $('#btn-borrarElementos').addClass('hidden');
      $('#btn-agregarItem').addClass('hidden');
      @endif
      globalIdOdenVenta = {{ $ov->id }};
      refrescarTabla();
      @endif

      //busca el articulo o servicio en la base de datos.
      $('[name="descripcion-item"]').select2({
        ajax: {
          method: "POST",
          url: '/ventas/ordenesVenta/jsonSearchArticuloServicio',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Busque el articulo a solicitar.",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap'
      });

      //cuando se selecciona un item que puede ser articulo o servicio esta funcion busca el precio del mismo.
      $('[name="descripcion-item"]').on("change", function(e) {
           //con esta funcion tomamos el precio del articulo o servicio de la BD
           if($('[name="descripcion-item"]').val() == ''){
             return
           }
           $.ajax({
             method: "POST",
             url: "/clientes/getPrecioIVAArticuloOServicio",
             data: { "_token": "{{ csrf_token() }}",
                         id  : $(e.target).val()
                   }
           })
             .done(function( msg ) {
               console.log(msg);
                $('[name="importe-item"]').val(String(msg.precio).trim());
                $('[name="sel-alicuota-iva"]').val(String(msg.iva).trim());
                calculoTotalItem();
             })
             .fail(function(mgs){
               swal({
                 title: "Error!",
                 text: "Algo esta mal, por favor Intentelo nuevamente.",
                 icon: "error",
                 type: "error",
                 confirmButtonText: "Cerrar"
               });
           });
      });


      //buscar un cliente de la base de datos.
      $('[name="sel-cliente"]').select2({
        ajax: {
          method: "POST",
          url: '/clientes/jsonSearchCliente',
          dataType: 'json',
          data: function (params){
                var query = {
                palabraClave: params.term,
                "_token": "{{ csrf_token() }}"
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
          }
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        minimumInputLength: 3,
        placeholder: "Buscar Cliente.",
        language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
        theme:'bootstrap'
      });

  });

  //configuraciones para crear orde de compra
  var globalItems = new Array();
  var globalIdOdenVenta = null;
  var globalImporteTotal = 0;

  //agrega nuevo item al array de items.
  function agregarNuevoItem(){
       //cargamos los items en el array.
       if($('[name="cantidad-item"]').val() == 0 || $('[name="cantidad-item"]').val() == ''){
          swal("Atencion!", "El campo cantidad no puede ser 0 o vacio.", "warning");
          return
       }
       var item = $('[name="descripcion-item"]').val().split('-');
       var descripcion = $('[name="descripcion-item"]').select2('data');
       if(item[1] == 'art'){
         var itemVenta = {
                            id                 : globalItems.length + 1,
                            cantidad           : $('[name="cantidad-item"]').val(),
                            articulo_id        : item[0],
                            servicio_id        : null,
                            observacion        : $('[name="inp-observacion"]').val().toUpperCase(),
                            descripcion_item   : String(descripcion[0].text).trim(),
                            precioUnitario     : $('[name="importe-item"]').val(),
                            alicuota_iva       : $('[name="sel-alicuota-iva"]').val(),
                            bonificacion       : $('[name="bonificacion-item"]').val(),
                            total              : (parseFloat($('[name="importe-item"]').val()).toFixed(2) - (parseFloat($('[name="importe-item"]').val()).toFixed(2) * $('[name="bonificacion-item"]').val() / 100)) * $('[name="cantidad-item"]').val()
                          }
       }else{
         var itemVenta = {
                           id                 : globalItems.length + 1,
                           cantidad           : $('[name="cantidad-item"]').val(),
                           articulo_id        : null,
                           servicio_id        : item[0],
                           observacion        : $('[name="inp-observacion"]').val().toUpperCase(),
                           descripcion_item   : String(descripcion[0].text).trim(),
                           precioUnitario     : $('[name="importe-item"]').val(),
                           alicuota_iva       : $('[name="sel-alicuota-iva"]').val(),
                           bonificacion       : $('[name="bonificacion-item"]').val(),
                           total              : (parseFloat($('[name="importe-item"]').val()).toFixed(2) - (parseFloat($('[name="importe-item"]').val()).toFixed(2) * $('[name="bonificacion-item"]').val() / 100)) * $('[name="cantidad-item"]').val()
                          }
       }


       globalItems.push(itemVenta);
       $('[name="cantidad-item"]').val('').focus();
       $('[name="descripcion-item"]').select2("val", "");
       refrescarTabla();
  }

  //esta funcion refrescara la tabla en base al array de items.
  function refrescarTabla(){
      var totalImporte = 0;
      $('#table-items tbody').html('');
      if (globalItems.length == 0){
        $('#table-items tbody').html('<tr style="background-color: transparent;">'+
          '<td colspan="8"><div class="alert alert-info text-center">No hay datos.</div></td>'+
        '</tr>');
      }else{
        var cont = 0;
        globalItems.forEach(function(item) {
        //agregamos los items a la tabla.
        var importeConj = item.cantidad * (parseFloat(item.precioUnitario).toFixed(2) - (parseFloat(item.precioUnitario).toFixed(2) * item.bonificacion / 100));
        $('#table-items tbody').append('<tr>'+
                                        '<td align="center"><input type="checkbox" class="i-checks" data-index="'+ item.id +'" name="input[]"></td>'+
                                        '<td align="center">'+ item.cantidad +'</td>'+
                                        '<td>'+ item.descripcion_item +'</td>'+
                                        '<td>'+ item.observacion +'</td>'+
                                        '<td align="center">$ '+ parseFloat(item.precioUnitario).toFixed(2) +'</td>'+
                                        '<td align="center">'+ parseFloat(item.alicuota_iva).toFixed(2) +'</td>'+
                                        '<td align="center">'+ item.bonificacion +' %</td>'+
                                        '<td align="center">$ '+ parseFloat(item.total).toFixed(2) +'</td>'+
                                        '</tr>');
        totalImporte += importeConj;
        cont++;
        });
      }
      $('#totalImporte').html('<h2><strong> $ '+ parseFloat(totalImporte).toFixed(2) +'</strong></h2>');
      globalImporteTotal = parseFloat(totalImporte).toFixed(2);
      $('[name="input[]"]').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
      });
  }

  function borrarItemSeleccionado(){
    var elementDelete = [];
    $('[name="input[]"]').each(function(){
       if($(this)[0].checked){
         elementDelete.push(parseInt($(this).data('index')));
       }
     });

     for(var i = 0; i < globalItems.length; i++) {
          var obj = globalItems[i];
          if(elementDelete.indexOf(obj.id) !== -1) {
              globalItems.splice(i, 1);
              i--;
          }
      }
     $('#check-all').iCheck('uncheck');
     refrescarTabla();
  }

  function save(valor){
    if($('[name="sel-cliente"]').val() == ''){
        swal("Atencion!", "Debe seleccionar un cliente.", "warning");
        return
    }
    if(valor == 1){
      var est = 'completa';
    }else{
      var est = 'borrador';
    }
    if(globalItems.length == 0){
      swal("Atencion!", "Debe agregar al menos un item para generar la orden de venta.", "warning");
      return
    }
    $.ajax({
      method: "POST",
      url: "/ventas/ordenesVenta/save",
      data: { "_token": "{{ csrf_token() }}",
              idOrdenVenta   : globalIdOdenVenta,
              idCliente      : $('[name="sel-cliente"]').val(),
              fecha          : $('[name="inp-fecha"]').val(),
              fechaVenc      : $('[name="inp-fecha-venc"]').val(),
              estado         : est,
              globalItems    : JSON.stringify(globalItems)
            }
    })
      .done(function( msg ) {
        swal({
          title: "Exito!",
          text: "Orden de Venta Gurdada Correctamente.",
          icon: "success",
          type: "success",
          confirmButtonText: "Cerrar"
        }).then((value) => {
          console.log(msg);
          window.location = '/ventas/ordenesVenta';
        });
      })
      .fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          icon: "error",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });

  }

  //Key press -----------------------------------------------------------------
  $('[name="cantidad-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="descripcion-item"]').focus();
      }
  });
  $('[name="cantidad-item"]').on('change', function (e) {
        calculoTotalItem();
  });
  $('[name="descripcion-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="importe-item"]').focus();
      }
  });
  $('[name="sel-alicuota-iva"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="importe-item"]').focus();
      }
  });
  $('[name="importe-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          $('[name="bonificacion-item"]').focus();
      }
  });
  $('[name="bonificacion-item"]').on('keypress', function (e) {
      if (e.which == 13) {
          e.preventDefault();
          agregarNuevoItem();
      }
  });

  $('[name="importe-item"]').on('change', function (e) {
      calculoTotalItem();
  });

  $('[name="bonificacion-item"]').on('change', function (e) {
      calculoTotalItem();
  });

  //Fin Key press --------------------------------------------------------------
  //funcion del boton seleccionar todos los elementos.
  $('#check-all').on('ifChanged', function(event){
    if(event.target.checked){
      $('[name="input[]"]').each(function(){
         $(this).iCheck('check');
       });
    }else{
      $('[name="input[]"]').each(function(){
         $(this).iCheck('uncheck');
       });
    }
  });

  var table;
  function openModal(){
      if (table != null){
        table.destroy();
      }
      if ($('[name="sel-cliente"]').val() == ''){
        swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
        return
      }
      $.ajax({
        method: "POST",
        url: "/clientes/getServiciosClienteJSON",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val()},
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          $('#table-servicios-clientes tbody').html('');
          msg.forEach(function (item){
            switch (item.estado) {
              case 'a_coordinar':
                item.estado = '<span class="label label-waring ">A COORDINAR</span>';
                break;
              case 'coordinado':
                item.estado = '<span class="label label-primary ">COORDINADO</span>';
                break;
              case 'activo':
                item.estado = '<span class="label label-success ">ACTIVO</span>';
                break;
              case 'suspendido':
                item.estado = '<span class="label label-danger ">SUSPENDIDO</span>';
                break;
              case 'mudanza':
                item.estado = '<span class="label label-warning ">MUDANZA</span>';
                break;
              case 'promo':
                item.estado = '<span class="label label-info ">PROMO</span>';
                break;
              case 'baja':
                item.estado = '<span class="label label-danger ">BAJA</span>';
                break;
            }
            if(item.fecha_baja == null){
                item.fecha_baja = '-';
            }

            $('#table-servicios-clientes tbody').append('<tr>'+
                '<td>'+ item.servicio.nombre +' ('+ item.servicio.zona +')</td>'+
                '<td>'+ item.domicilio.localidad.nombre +'</td>'+
                '<td>'+ item.domicilio.calle +' '+ item.domicilio.altura +'</td>'+
                '<td align="center">'+ item.fecha_alta +'</td>'+
                '<td align="center">'+ item.fecha_baja +'</td>'+
                '<td align="center">'+ item.estado +'</td>'+
                '<td align="center">$ '+ parseFloat(item.servicio.precio).toFixed(2) +'</td>'+
                '<td align="center">'+
                  '<a  class="btn btn-success btn-xs btn-bitbucket" onclick="seleccionarServicio('+ item.id +');" ><i data-toggle="tooltip" data-placement="top" title="Seleccionar Servicio" class="fa fa-chevron-right"></i></a>'+
                '</td>'+
            '</tr>');
          });
          table = $('#table-servicios-clientes').DataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]});
            $('[data-toggle="tooltip"]').tooltip();
            $('#modal-servicios-clientes').modal('show');
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
  }

  function seleccionarServicio(id){
      $('#modal-servicios-clientes').modal('hide');
      $.ajax({
        method: "POST",
        url: "/clientes/getServiciosClienteFromIDJSON",
        data: { "_token": "{{ csrf_token() }}", "idClienteServicio": id},
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          var itemVenta = {
                             id                 : globalItems.length + 1,
                             cantidad           : 1,
                             articulo_id        : null,
                             servicio_id        : msg.servicio.id,
                             descripcion_item   : msg.servicio.nombre,
                             precioUnitario     : msg.servicio.precio,
                             alicuota_iva       : msg.servicio.alicuota_iva,
                             bonificacion       : 0,
                             total              :  msg.servicio.precio * 1
                           }

          globalItems.push(itemVenta);
          refrescarTabla();
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });
  }

  function calculoTotalItem(){
    var itemPrecio = $('[name="importe-item"]').val();
    var cantidad = $('[name="cantidad-item"]').val();
    var bonificacion = $('[name="bonificacion-item"]').val();
    if (parseFloat($('[name="bonificacion-item"]').val()).toFixed(2) != 0.00){
        itemPrecio = itemPrecio - (itemPrecio * bonificacion /100);
    }
    var total = itemPrecio * cantidad;
    $('#total-item').html('$ '+total.toFixed(2));
  }

</script>

@stop
