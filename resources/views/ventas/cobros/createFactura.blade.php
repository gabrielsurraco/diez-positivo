@extends('layouts.backend')


@section('css')
<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
<!-- Date Picker -->
<link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop


@section('content')
<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nueva Factura</h5>
                    <div class="ibox-tools">
                        <a href="" class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-plus" aria-hidden="true"></i> Nueva Factura</a>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Destinatario:
                              </div>
                              <div class="panel-body">
                                <div class="form-group col-md-4 m-b">
                                    <label>Apellido y Nombre o Razón Social</label>
                                    <select name="sel-sujeto" class="form-control select2_demo_3">
                                        <option selected value="default">seleccionar...</option>
                                      @foreach ($clientes as $value)
                                        <option value="{{$value->id}}">&nbsp;{{$value->persona->apellido}} {{$value->persona->nombre}} </option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Condición Ante El IVA</label>
                                    <select name="sel-condicion-iva" class="form-control select2_demo_3">
                                        <option value="1">&nbsp;IVA Responsable Inscripto</option>
                                        <option value="4">&nbsp;IVA Exento</option>
                                        <option value="5" selected>&nbsp;Consumidor Final</option>
                                        <option value="6">&nbsp;Responsable Monotributo</option>
                                        <option value="9">&nbsp;Cliente del Exterior</option>
                                        <option value="13">&nbsp;Monotributista Social</option>

                                    </select>
                                </div>
                                <div class="form-group col-md-2 m-b">
                                    <label>Tipo de Doc.</label>
                                    <select name="sel-tipo-documento" id="sel-tipo-documento" class="form-control">
                                          <option value="80">CUIT</option>
                                          <option value="87">CDI</option>
                                          <option value="89">LE</option>
                                          <option value="90">LC</option>
                                          <option value="91">CI Extranjera</option>
                                          <option value="96">DNI</option>
                                          <option value="94">Pasaporte</option>
                                          <option value="00">CI Policía Federal</option>
                                          <option value="30">Certificado de Migración</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Número Documento</label>
                                    <input class="form-control" name="inp-numDoc"/>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Dirección</label>
                                    <input class="form-control" name="inp-direccion"/>
                                </div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Departamento</label>
                                    <input class="form-control" name="inp-departamento"/>
                                </div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Localidad</label>
                                    <input class="form-control" name="inp-localidad"/>
                                </div>
                                <div class="form-group col-md-3 m-b">
                                    <label>Código Postal</label>
                                    <input class="form-control" name="inp-codigoPostal"/>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Detalles del Comprobante:
                              </div>
                              <div class="panel-body">
                                <div class="row">
                                  <div class="form-group col-lg-offset-1 col-md-3 m-b">
                                      <label>Tipo Factura</label>
                                      <select name="sel-sujeto" class="form-control select2_demo_3">
                                          <option value="1" data-tipo="A">Factura A</option>
                                          <option value="2" data-tipo="A">Nota de Débito A</option>
                                          <option value="3" data-tipo="A">Nota de Crédito A</option>
                                          <option value="4" data-tipo="A">Recibos A</option>
                                          <option value="6" data-tipo="B">Factura B</option>
                                          <option value="7" data-tipo="B">Nota de Débito B</option>
                                          <option value="8" data-tipo="B">Nota de Crédito B</option>
                                          <option value="9" data-tipo="B">Recibos B</option>
                                      </select>
                                  </div>
                                  <div class="form-group col-md-2 m-b">
                                      <label>Punto de Venta</label>
                                      <p class="form-control-static text-center"> 1001 </p>
                                  </div>
                                  <div class="form-group col-md-2 m-b">
                                      <label>Nro Comprobante</label>
                                      <p class="form-control-static"> 2 </p>
                                  </div>
                                  <div class="form-group col-md-3 m-b">
                                      <label>Tipo de Moneda</label>
                                      <select name="sel-sujeto" class="form-control">
                                          <option value="ARS" data-tipo="A">ARS</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                                <div class="row">
                                  <div class="form-group col-lg-offset-1 col-md-3 m-b">
                                      <label>Concepto</label>
                                      <select name="sel-sujeto" class="form-control">
                                          <option value="1">Productos</option>
                                          <option value="2">Servicios</option>
                                          <option value="3">Productos y Servicios</option>
                                      </select>
                                  </div>
                                  <div class="form-group col-lg-4 m-b">
                                      <label>Fecha de Servicio</label>
                                      <div class="input-daterange input-group" style="padding-top:2px; padding-bottom:2px;" id="datepickerange">
                                          <input type="text" class="input-sm form-control" name="start"/>
                                          <span class="input-group-addon">hasta</span>
                                          <input type="text" class="input-sm form-control" name="end"/>
                                      </div>
                                  </div>

                                  <div class="form-group col-md-3 m-b">
                                      <label>Nro Remito (opcional)</label>
                                      <input class="form-control"/>
                                  </div>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Conceptos del Comprobante:
                              </div>
                              <div class="panel-body">
                                <div class="row">
                                  <div class="form-group col-md-2 m-b">
                                      <label>Cod.</label>
                                      <input class="form-control" name="concepto-cod"/>
                                  </div>
                                  <div class="form-group col-md-4 m-b">
                                    <label>Concepto</label>
                                    <div class="input-group">
                                          <select name="sel-concepto" class="form-control select2_demo_3">
                                            @foreach ($conceptos as $concepto)
                                                <option value="{{$concepto->id}}">{{$concepto->id}} - {{$concepto->concepto->concepto}} {{$concepto->sub_concepto}}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                  </div>
                                  <div class="form-group col-md-2 m-b">
                                      <label>Cantidad</label>
                                      <input class="form-control" type="number" min="1" name="concepto-cantidad"/>
                                  </div>
                                  <div class="form-group col-md-2 m-b">
                                      <label>Unidad</label>
                                      <select name="sel-unidad" class="form-control">
                                        <option value="1">KILOGRAMO</option>
                                        <option value="2">METROS</option>
                                        <option value="3">METRO CUADRADO</option>
                                        <option value="4">METRO CUBICO</option>
                                        <option value="5">LITROS</option>
                                        <option value="6">1000 KILOWATT HORA</option>
                                        <option value="7" selected="">UNIDAD</option>
                                        <option value="8">PAR</option>
                                        <option value="9">DOCENA</option>
                                        <option value="10">QUILATE</option>
                                        <option value="11">MILLAR</option>
                                        <option value="12">MEGA-U. INT. ACT. ANTIB</option>
                                        <option value="13">UNIDAD INT. ACT. INMUNG</option>
                                        <option value="14">GRAMO</option>
                                        <option value="15">MILIMETRO</option>
                                        <option value="16">MILIMETRO CUBICO</option>
                                        <option value="17">KILOMETRO</option>
                                        <option value="18">HECTOLITRO</option>
                                        <option value="19">MEGA U. INT. ACT. INMUNG.</option>
                                        <option value="20">CENTIMETRO</option>
                                        <option value="21">KILOGRAMO ACTIVO</option>
                                        <option value="22">GRAMO ACTIVO</option>
                                        <option value="23">GRAMO BASE</option>
                                        <option value="24">UIACTHOR</option>
                                        <option value="25">JUEGO O PAQUETE MAZO DE NAIPES</option>
                                        <option value="26">MUIACTHOR</option>
                                        <option value="27">CENTIMETRO CUBICO</option>
                                        <option value="28">UIACTANT</option>
                                        <option value="29">TONELADA</option>
                                        <option value="30">DECAMETRO CUBICO</option>
                                        <option value="31">HECTOMETRO CUBICO</option>
                                        <option value="32">KILOMETRO CUBICO</option>
                                        <option value="33">MICROGRAMO</option>
                                        <option value="34">NANOGRAMO</option>
                                        <option value="35">PICOGRAMO</option>
                                        <option value="36">MUIACTANT</option>
                                        <option value="37">UIACTIG</option>
                                        <option value="41">MILIGRAMO</option>
                                        <option value="47">MILILITRO</option>
                                        <option value="48">CURIE</option>
                                        <option value="49">MILICURIE</option>
                                        <option value="50">MICROCURIE</option>
                                        <option value="51">U. INTER. ACT. HOR.</option>
                                        <option value="52">MEGA U. INTER. ACT. HOR.</option>
                                        <option value="53">KILOGRAMO BASE</option>
                                        <option value="54">GRUESA</option>
                                        <option value="55">MUIACTIG</option>
                                        <option value="61">KG. BRUTO</option>
                                        <option value="62">PACK</option>
                                        <option value="63">HORMA</option>
                                        <option value="98">OTRAS UNIDADES</option>
                                        <option value="99">BONIFICACION</option>
                                      </select>
                                  </div>
                                  <div class="form-group col-md-2 m-b">
                                      <label>Imp. Unitario con IVA</label>
                                      <input class="form-control" type="number" min="1" placeholder="0.00" name="unitario-con-iva"/>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-offset-4 col-md-2 m-b">
                                        <label>Bonif. (%)</label>
                                        <input class="form-control" type="number" min="1" placeholder="0.00" name="bonificacion"/>
                                    </div>
                                    <div class="form-group col-md-2 m-b">
                                        <label>Alicota</label>
                                        <input class="form-control" type="number" min="1" placeholder="0.00" name="alicuotaIva"/>
                                    </div>
                                    <div class="form-group col-md-2 m-b">
                                        <label>Imp de IVA</label>
                                        <input class="form-control" type="number" min="1" placeholder="0.00" name="importe-iva"/>
                                    </div>
                                    <div class="form-group col-md-2 m-b">
                                        <label>Total</label>
                                        <input class="form-control" type="number" min="1" placeholder="0.00" name="total-item"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 text-center">
                                        <button class="btn btn-success " type="button" onclick="agregarNuevoItem();"><i class="fa fa-plus"></i><span class="bold"> Agregar</span></button>
                                    </div>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                                <div class="row">
                                    <div class="form-group col-lg-12" style="text-align:center;">
                                      <table class="table table-bordered" id="tb-conceptos">
                                        <thead>
                                        <tr>
                                            <th>Cod</th>
                                            <th>Concepto</th>
                                            <th>Cantidad</th>
                                            <th>Unidad</th>
                                            <th>Imp. Uni. con Iva</th>
                                            <th>Bonificacion</th>
                                            <th>Alicuota</th>
                                            <th>Imp. IVA</th>
                                            <th>Total</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Otros Tributos del Comprobante:
                              </div>
                              <div class="panel-body">
                                <div class="row">
                                  <div class="form-group col-md-3 m-b">
                                      <label>Tipo de Tributo</label>
                                      <select name="sel-sujeto" class="form-control">
                                        <option value="1">Impuestos nacionales</option>
                                        <option value="2">Impuestos provinciales</option>
                                        <option value="3">Impuestos municipales</option>
                                        <option value="4">Impuestos Internos</option>
                                        <option value="99">Otro</option>
                                      </select>
                                  </div>
                                  <div class="form-group col-md-3 m-b">
                                      <label>Descripción</label>
                                      <input class="form-control"/>
                                  </div>
                                  <div class="form-group col-md-3 m-b">
                                      <label>Base Imponible</label>
                                      <input class="form-control" placeholder="0.00"/>
                                  </div>
                                  <div class="form-group col-md-3 m-b">
                                      <label>Alicuota (%)</label>
                                      <input class="form-control" placeholder="0.00"/>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 text-center">
                                        <button class="btn btn-success " type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button>
                                    </div>
                                </div>
                                <div class="hr-line-dashed col-lg-12"></div>
                                <div class="row">
                                  <div class="form-group col-lg-12" style="text-align:center;">
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                          <th>Cod</th>
                                          <th>Concepto</th>
                                          <th>Cantidad</th>
                                          <th>Unidad</th>
                                          <th>Imp. Uni. con Iva</th>
                                          <th>Bonificacion</th>
                                          <th>Alicuota</th>
                                          <th>Imp. IVA</th>
                                          <th>Total</th>
                                          <th></th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td style="vertical-align:middle;">1</td>
                                          <td style="vertical-align:middle;">Abono 3MB ESTANDAR</td>
                                          <td style="vertical-align:middle;">10</td>
                                          <td style="vertical-align:middle;">Unidad</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;">0</td>
                                          <td style="vertical-align:middle;">21 %</td>
                                          <td style="vertical-align:middle;">112.80</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;"><a class="btn btn-danger btn-xs">
                                                                              <i class="fa fa-remove"></i>
                                                                          </a></td>
                                      </tr>
                                      <tr>
                                          <td style="vertical-align:middle;">2</td>
                                          <td style="vertical-align:middle;">Abono 5MB ESTANDAR</td>
                                          <td style="vertical-align:middle;">10</td>
                                          <td style="vertical-align:middle;">Unidad</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;">0</td>
                                          <td style="vertical-align:middle;">21 %</td>
                                          <td style="vertical-align:middle;">112.80</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;"><a class="btn btn-danger btn-xs">
                                                                              <i class="fa fa-remove"></i>
                                                                          </a></td>
                                      </tr><tr>
                                          <td style="vertical-align:middle;">3</td>
                                          <td style="vertical-align:middle;">Abono 10MB ESTANDAR</td>
                                          <td style="vertical-align:middle;">10</td>
                                          <td style="vertical-align:middle;">Unidad</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;">0</td>
                                          <td style="vertical-align:middle;">21 %</td>
                                          <td style="vertical-align:middle;">112.80</td>
                                          <td style="vertical-align:middle;">650.00</td>
                                          <td style="vertical-align:middle;"><a class="btn btn-danger btn-xs">
                                                                              <i class="fa fa-remove"></i>
                                                                          </a></td>
                                      </tr>
                                      </tbody>
                                  </table>
                                  </div>
                                </div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Importes del Comprobante:
                              </div>
                              <div class="panel-body">
                                <div class="col-lg-6">
                                  <div class="form-group col-md-12 m-b">
                                      <label>Observaciones</label>
                                      <textarea class="form-control" rows="4"></textarea>
                                  </div>
                                </div>

                                <div class="col-lg-6 form-horizontal">
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-7 text-right control-label">Importe Neto</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-7 text-right control-label">Importe IVA</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-7 text-right control-label">Importe Total</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Importes por Alicuotas de IVA:
                              </div>
                              <div class="panel-body">
                                <div class="col-lg-6 form-horizontal">
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-5 text-right control-label">Neto Imponible 0%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-5 text-right control-label">Neto Imponible 10,50%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-5 text-right control-label">Neto Imponible 21%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-horizontal">
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-4 text-right control-label">Importe IVA 0%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-4 text-right control-label">Importe IVA 10,50%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="form-group"><label class="col-sm-4 text-right control-label">Importe IVA 21%</label>
                                            <div class="col-sm-5"><input type="text" class="form-control text-right" placeholder="0.00"></div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>

                          <div class="panel panel-success">
                              <div class="panel-heading">
                                  <i class="fa fa-info-circle"></i> Forma de Pago:
                              </div>
                              <div class="panel-body">

                                    <div class="row">
                                      <div class="form-group col-md-3 m-b">
                                          <label>Tipo de Pago</label>
                                          <select name="sel-sujeto" class="form-control">
                                            <option value="">Seleccione</option>
                                            <option value="Mecopa">Cobranza electrónica</option>
                                            <option value="Contado">Contado</option>
                                            <option value="CuentaCorriente">Cuenta corriente</option>
                                            <option value="TarjetaDebito">Tarjeta de débito</option>
                                            <option value="TarjetaCredito">Tarjeta de crédito</option>
                                            <option value="Cheque">Cheque</option>
                                            <option value="Ticket">Ticket</option>
                                            <option value="TransferenciaBancaria">Transferencia bancaria</option>
                                            <option value="MercadoPago">MercadoPago</option>
                                            <option value="Otro">Otro</option>
                                          </select>
                                      </div>
                                      <div class="form-group col-md-4 m-b">
                                          <label>Detalle</label>
                                          <input class="form-control" placeholder=""/>
                                      </div>
                                      <div class="form-group col-md-3 m-b">
                                          <label>Importe</label>
                                          <input class="form-control text-right"  placeholder="0.00"/>
                                      </div>
                                      <div class="form-group col-md-2 text-center m-b">
                                          <button type="button" class="btn btn-danger" style="margin-top:22px;"><i class="fa fa-remove"></i></button>
                                      </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-12 text-center">
                                            <button class="btn btn-success " type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button>
                                        </div>
                                    </div>
                              </div>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->

@endsection

@section('javascript')

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- DataPicker español -->
<script src="{{ asset('/backend/js/plugins/datapicker/locales/bootstrap-datepicker.es.js') }}"></script>
<!-- Moment con Locales -->
<script src="{{ asset('/backend/js/plugins/moment/moment-with-locales.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
    //*************************************************************************
    //Agregado Guille - Uniting <info@uniting.com.ar>
    //*************************************************************************
    $(document).ready(function() {

        $('.select2_demo_3').select2({
            theme: "bootstrap",
            allowClear: true
        });

        $('#datepickerange input[name="start"]').val(moment().format("DD/MM/YYYY"));
        $('#datepickerange input[name="end"]').val(moment().add(30, 'days').format("DD/MM/YYYY"));
        $('#datepickerange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                language: 'es'
        });


    });


    //al cambiar la condcion ante el iva actualiza el campo tipo de documento.
    $('[name="sel-condicion-iva"]').on('change', function(){
      switch($('[name="sel-condicion-iva"]').val()) {
        case "1":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>');
            break;
        case "5":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>'+
                                                    '<option value="87">CDI</option>'+
                                                    '<option value="89">LE</option>'+
                                                    '<option value="90">LC</option>'+
                                                    '<option value="91">CI Extranjera</option>'+
                                                    '<option value="96">DNI</option>'+
                                                    '<option value="94">Pasaporte</option>'+
                                                    '<option value="00">CI Policía Federal</option>'+
                                                    '<option value="30">Certificado de Migración</option>');
            break;
        case "6":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>');
            break;
        case "9":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>'+
                                                    '<option value="91">CI Extranjera</option>'+
                                                    '<option value="94">Pasaporte</option>'+
                                                    '<option value="99">Doc. (Otro)</option>');
            break;
        case "13":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>');
            break;
        case "4":
            $('[name="sel-tipo-documento"]').html('');
            $('[name="sel-tipo-documento"]').append('<option value="80">CUIT</option>');
            break;
      }
    });

    //al seleccionar un cliente, autocompleta todos los campos necesarios.
    $('[name="sel-sujeto"]').on('change', function(){
      var $cli_id = $('[name="sel-sujeto"]').val();
      $.ajax({
        method: "POST",
        url: "/ventas/clientes/get-all-data",
        data: { "_token": "{{csrf_token()}}", idCliente: $cli_id },
        dataType: "json"
      })
      .done(function( msg ) {
        console.log(msg);
          $('[name="sel-condicion-iva"]').val(msg.datosPersona.cat_fiscal).prop('disabled', true);
          $('[name="sel-tipo-documento"]').val(msg.datosPersona.tipo_doc).prop('disabled', true);
          $('[name="inp-numDoc"]').val(msg.datosPersona.cuit).prop('disabled', true);
          $('[name="inp-direccion"]').val(msg.datosPersona.domicilio[0].calle + " " + msg.datosPersona.domicilio[0].altura).prop('disabled', true);
          $('[name="inp-localidad"]').val(msg.datosPersona.domicilio[0].localidad.nombre).prop('disabled', true);
          $('[name="inp-departamento"]').val(msg.datosPersona.domicilio[0].departamento.nombre).prop('disabled', true);
          $('[name="inp-codigoPostal"]').val(msg.datosPersona.domicilio[0].cp).prop('disabled', true);
      })
      .fail(function( msg ) {
        swal({
              title: "Error!",
              text: "Ocurrio un error al intentar leer los datos del cliente.",
              icon: "error",
              button: "Aceptar",
              dangerMode: true,
        });
      });
    });

    //Al indicar un codigo selecciona el concepto
    $('[name="concepto-cod"]').on('blur', function(e){
        console.log($(this).val());
        $('[name="sel-concepto"]').val($(this).val()).trigger('change');;
    });

    $('[name="sel-concepto"]').on('change', function(){
      $.ajax({
        method: "POST",
        url: "/",
        data: { name: "John", location: "Boston" }
      })
        .done(function( msg ) {
          alert( "Data Saved: " + msg );
      });
    });

    //configuraciones para facturacion
    var globalCompra = [];

    function agregarNuevoItem(){
         //cargamos los items en el array.
         let itemCompra = { codigo        : $('[name="concepto-cod"]').val(),
                            concepto      : $('[name="sel-concepto"]').select2('data')[0]['text'],
                            cod_concepto  : $('[name="sel-concepto"]').val(),
                            cantidad      : $('[name="concepto-cantidad"]').val(),
                            unidad        : $('[name="sel-unidad"] :selected').text(),
                            unitConIva    : parseFloat($('[name="unitario-con-iva"]').val()).toFixed(2),
                            bonif         : $('[name="bonificacion"]').val(),
                            alicuota      : $('[name="alicuotaIva"]').val(),
                            impIva        : parseFloat($('[name="importe-iva"]').val()).toFixed(2),
                            total         : parseFloat($('[name="total-item"]').val()).toFixed(2)}

         globalCompra.push(itemCompra);
         console.log("Lista de Compra del Cliente");
         console.log(globalCompra);
         console.log("Fin lista de compra. ------------------");
         refrescarTabla();
    }

    //esta funcion refrescara la tabla.
    function refrescarTabla(){
        $('#tb-conceptos tbody').html('');
        globalCompra.forEach(function(item) {
          //agregamos los items a la tabla.
          $('#tb-conceptos tbody').append('<tr>'+
                                          '<td style="vertical-align:middle;">'+ item.codigo +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.concepto +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.cantidad +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.unidad +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.unitConIva +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.bonif +' %</td>'+
                                          '<td style="vertical-align:middle;">'+ item.alicuota +' %</td>'+
                                          '<td style="vertical-align:middle;">'+ item.impIva +'</td>'+
                                          '<td style="vertical-align:middle;">'+ item.total +'</td>'+
                                          '<td style="vertical-align:middle;"><a class="btn btn-danger btn-xs" onClick="borrarItem(this);">'+
                                          '<i class="fa fa-remove"></i>'+
                                          '</a></td>'+
                                          '</tr>');
        });
    }

    function borrarItem(elemento){
        var index = $(elemento).parent().parent().index();
        globalCompra.splice(index, 1);
        refrescarTabla();
    }



</script>

@stop
