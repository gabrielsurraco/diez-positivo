@extends('layouts.backend')


@section('css')
<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Ordenes de Venta</h5>
                    <div class="ibox-tools">
                        <a href="/ventas/nuevaOrdenVenta" class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-plus" aria-hidden="true"></i> Nueva Orden Venta</a>
                    </div>
                </div>
                <div class="ibox-content">

                  <div class="table-responsive">
                      <table class="table table-striped dataTables" id="table-resultados">
                          <thead>
                          <tr>
                              <th>Nro</th>
                              <th>Fecha</th>
                              <th>Cliente</th>
                              <th>IVA 10.50 %</th>
                              <th>IVA 21 %</th>
                              <th>Neto sin IVA</th>
                              <th>Total</th>
                              <th>Estado</th>
                              <th>Acciones</th>
                          </tr>
                          </thead>
                          <tbody>
                              @foreach ($ordenVenta as $valor)
                              <tr>
                                  <td>{{ $valor->id }}</td>
                                  <td>{{ $valor->fecha }}</td>
                                  <td>{{ $valor->persona->apellido }} {{ $valor->persona->nombre }}</td>
                                  <td>$ {{ $valor->importe_iva105 }}</td>
                                  <td>$ {{ $valor->importe_iva21 }}</td>
                                  <td>$ {{ $valor->importe_neto }}</td>
                                  <td>$ {{ $valor->importe_neto + $valor->importe_iva105 + $valor->importe_iva21 }}</td>
                                  <td>
                                    <?php
                                    switch($valor->estado){
                                      case "borrador":
                                          echo '<span class="label label-warning">BORRADOR</span>';
                                          break;
                                      case "completa":
                                          echo '<span class="label label-success">COMPLETA</span>';
                                          break;
                                      case "pagado":
                                          echo '<span class="label label-primary">PAGADO</span>';
                                          break;
                                      case "facturada":
                                          echo '<span class="label label-info">FACTURADA</span>';
                                          break;
                                      }
                                    ?>
                                  </td>
                                  <td align="center">
                                    @if($valor->estado == 'borrador')<a href="/ventas/editOrdenVenta/{{ $valor->id }}" class="btn btn-warning btn-xs btn-bitbucket"><i class="fa fa-pencil"></i></a>@endif
                                    @if($valor->estado != 'borrador')<a href="/ventas/editOrdenVenta/{{ $valor->id }}" class="btn btn-success btn-xs btn-bitbucket"><i class="fa fa-eye"></i></a>@endif
                                    <a onClick="deleteOrdenCompra({{ $valor->id }})" class="btn btn-danger btn-xs btn-bitbucket"><i class="fa fa-remove"></i></a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->



<!--
  <div class="row wrapper border-bottom white-bg page-heading">

      @if(session()->has('msg'))
      <div class="alert alert-success">
          {{ session()->get('msg') }}
      </div>
      @endif

      <div class="col-lg-12 m-t-md">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>

                  </h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><a href="#">Config option 1</a>
                          </li>
                          <li><a href="#">Config option 2</a>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <div class="clients-list">
                      <ul class="nav nav-tabs">
                          <span class="pull-right small text-muted">1406 Elements</span>
                          <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class="fa fa-user"></i> Servicios</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i class="fa fa-briefcase"></i> Artículos</a></li>
                      </ul>
                      <div class="tab-content">
                          <div id="tab-1" class="tab-pane active">
                              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                                  <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                      <div class="m-t-md">
                                          <a  href="/ventas/clientes/"><button class="btn btn-primary btn-xs p-xxs" > <i class="fa fa-plus"></i> Asignar Servicio</button></a>
                                          <div class="table-responsive">
                                              <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                  <thead>
                                                      <tr>
                                                          <th>Concepto</th>
                                                          <th>Sub-Concepto</th>
                                                          <th>Fecha Inicio</th>
                                                          <th>Fecha Fin</th>
                                                          <th>Estado</th>
                                                          <th>Precio</th>
                                                          <th>Acción</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>


                                                  </tbody>
                                                  <tfoot>
                                                      <tr>
                                                          <th>Concepto</th>
                                                          <th>Sub-Concepto</th>
                                                          <th>Fecha Inicio</th>
                                                          <th>Fecha Fin</th>
                                                          <th>Estado</th>
                                                          <th>Precio</th>
                                                          <th>Acción</th>
                                                      </tr>
                                                  </tfoot>
                                              </table>
                                          </div>
                                      </div>
                                  </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 365.112px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                          </div>
                          <div id="tab-2" class="tab-pane">
                              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                                  <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                      <div class="m-t-md">
                                          <a  href="/ventas/clientes/"><button class="btn btn-danger btn-xs p-xxs" > <i class="fa fa-plus"></i> Asignar Artículo</button></a>
                                          <div class="table-responsive">
                                              <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                  <thead>
                                                      <tr>
                                                          <th>Concepto</th>
                                                          <th>Sub-Concepto</th>
                                                          <th>Fecha Inicio</th>
                                                          <th>Fecha Fin</th>
                                                          <th>Estado</th>
                                                          <th>Precio</th>
                                                          <th>Acción</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>


                                                  </tbody>
                                                  <tfoot>
                                                      <tr>
                                                          <th>Concepto</th>
                                                          <th>Sub-Concepto</th>
                                                          <th>Fecha Inicio</th>
                                                          <th>Fecha Fin</th>
                                                          <th>Estado</th>
                                                          <th>Precio</th>
                                                          <th>Acción</th>
                                                      </tr>
                                                  </tfoot>
                                              </table>
                                          </div>
                                      </div>
                                  </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 365.112px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                          </div>
                      </div>

                  </div>



              </div>
          </div>

      </div>

      <!--    <div class="sk-spinner sk-spinner-double-bounce">
              <div class="sk-double-bounce1"></div>
              <div class="sk-double-bounce2"></div>
          </div>-->

  <!-- </div> -->
@endsection

@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {

    $('.dataTables').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      },
      buttons: [
          {extend: 'copy'},
          {extend: 'csv'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},
          {extend: 'print',
              customize: function (win) {
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
              }
          }
      ]
    });

  });

</script>

@stop
