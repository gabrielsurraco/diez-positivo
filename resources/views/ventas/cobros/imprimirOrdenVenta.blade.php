@extends('layouts.backend')


@section('css')

<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">
<style>

@media print {
        #watermark {
			      display: block;
            position: fixed;
            top: 50%;
            left: 45%;
            z-index: -10;
			      opacity:0.15;
            -webkit-transform: rotate(-10deg); /* Chrome, Safari, Opera */
            transform: rotate(-10deg);
        }
        div {
          -webkit-print-color-adjust: exact;
        }
        .tr{
          height: 20px;
        }
        img {
            -webkit-print-color-adjust:exact;
        }
        @page {
          size: landscape;
          orientation: landscape;
          size: A4;
        }
        thead tr th{
          border-collapse: collapse;
          border:1px black solid;
          padding: 2px;
        }
        table tbody tr td{
          padding: 2px;
        }
}
</style>
@endsection


@section('content')
  <div class="hidden-print">
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right"><i class="fa fa-print"></i></span>
                    <h5>Generando Impresión</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">
                      <div class="col-sm-12 text-center">
                        <i class="fa fa-refresh fa-4x fa-spin"></i>
                          <p>Aguarde Por Favor...</p>
                      </div>
                  </div>

                </div>
            </div>
        </div>
      </div>
  </div>
      <div class="visible-print-block">
          <div class="row">
            <div class="col-xs-12" style="margin:0px; padding:5px;">
                <table border="1" width="100%">
                  <tr>
                    <td width="50%" align="right"><div style="font-size:34px; color:white!important; background-color:black!important; width:15%!important; text-align:center!important; font-weight:bold!important;">X</div></td>
                    <td width="50%">
                      <table width="100%">
                      <tr>
                        <td style="font-size:9px; text-align:right; padding-right:2%;">DOCUMENTO NO VALIDO COMO FACTURA</td>
                      </tr>
                      <tr>
                        <td style="font-size:14px; text-align:right;  font-weight:bold; padding-right:2%;"> ORDEN DE VENTA - PRESUPUESTO</td>
                      </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td><table width="100%" cellspacing="5" cellpadding="5">
                    <tr>
                      <td style="text-align:center; padding:3px;"><img src="{{ asset('/img/internet_obera_membrete.jpg') }}" class="img-responsive" alt="Responsive image"></td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Razón Social:</strong>  Damian Francisco Ostrorog </td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Domicilio Comercial:</strong>  Corrientes 93, Oficina 104, Primer Piso Oberá Misiones. </td>
                    </tr>
                    <tr>
                      <td style="font-size:9pt; text-align:left; padding-right:2%; padding-left:2%;"> IVA Responsable Inscripto </td>
                    </tr>
                    </table></td>

                    <td style="vertical-align:top; padding:5px;"><table width="100%" cellspacing="5" cellpadding="5">
                    @if($ordenVenta->estado  == 'facturada')
                    <tr>
                      <td width="100%" colspan="4" style="font-size:15pt; text-align:left; padding-right:5%; padding-left:5%;"><strong>FACTURA A</strong></td>
                    </tr>
                    <tr>
                      <td width="25%" style="font-size:7pt; text-align:left;"><strong>Punto de Venta:</strong></td><td width="25%" style="font-size:7pt; text-align:left;"> 0003</td>
                      <td width="25%" style="font-size:7pt; text-align:left;"><strong>Comp. Nro:</strong></td><td width="25%" style="font-size:7pt; text-align:left;"> {{$ordenVenta->id}}</td>
                    </tr>
                    @else
                    <tr>
                      <td width="100%" colspan="4" style="font-size:15pt; text-align:left; padding-right:5%; padding-left:5%;"><strong>ORDEN DE VENTA</strong></td>
                    </tr>
                    <tr>
                      <td width="25%" style="font-size:7pt; text-align:left;"><strong>Comp. Nro:</strong></td><td width="25%" style="font-size:7pt; text-align:left;"> <?php echo str_pad($ordenVenta->id, 8, '0', STR_PAD_LEFT); ?></td>
                    </tr>
                    @endif
                    <tr>
                      <td width="30%" style="font-size:7pt; text-align:left;"><strong>Fecha de Emisión:</strong></td><td width="70%" colspan="3" style="font-size:7pt; text-align:left;"> {{$ordenVenta->fecha}} </td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-size:7pt; text-align:left; height:14px"></td><td></td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-size:7pt; text-align:left;"><strong>CUIT:</strong></td><td width="70%" colspan="3" style="font-size:7pt; text-align:left;"> 20-28059849-7</td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-size:7pt; text-align:left;"><strong>INGRESOS BRUTOS:</strong></td><td width="70%" colspan="3" style="font-size:7pt; text-align:left;"> 20280598497</td>
                    </tr>

                    <tr>
                      <table width="100%">
                        <tr>
                          <td width="50%" style="font-size:7pt; text-align:left;"><strong>Fecha de Inicio de Actividad:</strong></td><td width="50%" colspan="3" style="font-size:7pt; text-align:left;"> 04/04/2011</td>
                        </tr>
                      </table>
                    </tr>

                    </table></td>
                  </tr>
                  </table>
                  @if($ordenVenta->estado == 'facturada')
                  <table style="border:1px black solid;" width="100%">
                  <tr class="tr">
                    <td width="20%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Periodo Facturado Desde:</strong></td><td width="15%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">CLIENTE:</td>
                    <td width="8%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Hasta:</strong></td><td width="15%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">CLIENTE:</td>
                    <td width="20%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Fecha de Vto para el pago:</strong></td><td width="15%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">CLIENTE:</td>
                  </tr>
                  </table>
                  @endif
                  <table style="border:1px black solid;" width="100%">
                    <tr>
                      <td>
                      <table width="100%">
                        <tr class="tr">
                          <td width="5%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>CUIT:</strong> </td><td with="20%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"> {{$ordenVenta->persona->cuit}} </td>
                          <td width="30%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Apellido y Nombre / Razón Social:</strong> </td><td width="45%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">{{$ordenVenta->persona->apellido}} {{$ordenVenta->persona->nombre}} {{$ordenVenta->persona->razon_social}}</td>
                        </tr>
                      </table>
                      <table width="100%">
                        <tr class="tr">
                          <td width="25%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Condición Frente al IVA:</strong> </td><td with="35%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;">{{(new App\Helper)->getCondicionFiscal($ordenVenta->persona->condicion_fiscal)}}</td>
                          <td width="20%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"><strong>Domicilio Comercial:</strong> </td><td width="30%" style="font-size:7pt; text-align:left; padding-right:2%; padding-left:2%;"> {{$ordenVenta->persona->domicilio->calle}} {{$ordenVenta->persona->domicilio->altura}} ({{$ordenVenta->persona->domicilio->departamento->nombre}} - {{$ordenVenta->persona->domicilio->localidad->nombre}})</td>
                        </tr>
                      </table>
                      </td>
                    </tr>
                  </table>
                  <table width="100%">
                    <thead style="font-size:7pt; background:#b6b6b6 !important;">
                      <th style="text-align:center;">Codigo</th>
                      <th>Producto / Servicio</th>
                      <th align="center">Cantidad</th>
                      <th align="center">U. Medida</th>
                      <th align="center">Precio Unit.</th>
                      <th align="center">% Bonif</th>
                      <th align="center">Subtotal</th>
                      <th align="center">Alicuota IVA</th>
                      <th align="center">Subtotal c/IVA</th>
                    </thead>
                    <tbody style="font-size:7pt; border:1px black solid;">
                      @php $cont = 1; @endphp
                      @foreach ($ordenVenta->detalleOrdenVenta as $value)
                      <tr>
                        <td></td>
                        <td>@if($value->fk_articulo != null) {{$value->articulo->marca}} - {{$value->articulo->modelo}} @endif
                            @if($value->fk_servicio != null) {{$value->servicio->nombre}} - {{$value->servicio->zona}} @endif
                        </td>
                        <td align="right">{{$value->cantidad}}</td>
                        <td align="center">unidades</td>
                        <td align="right">{{$value->precio_unitario}}</td>
                        <td align="right">{{$value->bonificacion}}</td>
                        <td align="center">{{$value->alicuota_iva}}%</td>
                        <td align="right">{{$value->importeIva}}</td>
                        <td align="right">{{$value->importeIva + $value->importe_totalsiniva}}</td>
                      </tr>
                      @php $cont++; @endphp
                      @endforeach

                      @while($cont < 35)
                      <tr style="height:16.6px;"><td colspan="9"></td></tr>
                      @php $cont++; @endphp
                      @endwhile
                    </tbody>
                </table>

                <table style="border:1px black solid;" width="100%">
                  <tr>
                    <td>
                    <table width="100%">
                      <tr class="tr">
                        <td width="20%" style="font-size:7pt; text-align:right;"><strong>IVA 10.5%:</strong> </td><td width="5%" style="font-size:7pt; text-align:left;">$ {{$ordenVenta->importe_iva105}}</td>
                        <td width="10%" style="font-size:7pt; text-align:right;"><strong>IVA 21%:</strong> </td><td width="20%" style="font-size:7pt; text-align:left;">$ {{$ordenVenta->importe_iva21}} </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr class="tr">
                        <td width="5%" style="font-size:10pt; text-align:right;"><strong>Bruto:</strong> </td><td width="20%" style="font-size:10pt; text-align:left;">$ {{$ordenVenta->importe_neto}} </td>
                        <td width="20%" style="font-size:10pt; text-align:right;"><strong>Impuestos:</strong> </td><td width="20%" style="font-size:10pt; text-align:left;">$ {{ $ordenVenta->importe_iva105 + $ordenVenta->importe_iva21}}</td>
                        <td width="20%" style="font-size:10pt; text-align:right;"><strong>Total:</strong> </td><td width="20%" style="font-size:10pt; text-align:left;">$ {{$ordenVenta->importe_neto + $ordenVenta->importe_iva105 + $ordenVenta->importe_iva21}} </td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>

                <table style="border:1px black solid;" width="100%">
                  <tr class="tr">
                    <td width="100%" style="font-size:10pt; text-align:center;"><strong>Bruto:</strong> </td>
                  </tr>
                </table>

                <table style="border:1px black solid;" width="100%">
                  <tr>
                    <td>
                    <table width="100%">
                      <tr class="tr">
                        <td width="2%" style="font-size:7pt; text-align:right;"><strong>CAE:</strong> </td><td width="5%" style="font-size:7pt; text-align:left;">20356949723 </td>
                        <td width="5%" style="font-size:7pt; text-align:right;"><strong>Fecha Vto del CAE:</strong> </td><td width="20%" style="font-size:7pt; text-align:left;">20356949723 </td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
              </div>
          </div>
          <!-- Datos Cliente -->
       </div>
@endsection

@section('javascript')

<!-- Page-Level Scripts -->
<script>
  $(document).ready(function() {
      window.print();
  });

  var beforePrint = function () {
      //alert('Functionality to run before printing.');
      $('#page-wrapper').css('min-height', '0px');
  };

  var afterPrint = function () {
      //alert('Functionality to run after printing');
      window.close();
      $('#page-wrapper').css('min-height', '800px');
  };

  if (window.matchMedia) {
      var mediaQueryList = window.matchMedia('print');

      mediaQueryList.addListener(function (mql) {
          //alert($(mediaQueryList).html());
          if (mql.matches) {
              beforePrint();
          } else {
              afterPrint();
          }
      });
  }

  window.onbeforeprint = beforePrint;
  window.onafterprint = afterPrint;
</script>

@stop
