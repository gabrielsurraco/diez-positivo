@extends('layouts.backend')


@section('css')
<!-- Font Awesone -->
<link href="{{ asset('/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- FooTable -->
<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('/backend/css/switchery/switchery.min.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/iCheck/custom.css') }}" rel="stylesheet">

<link href="{{ asset('/backend/css/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop


@section('content')

<!-- Inicio Contenido Principal -->
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Cobros</h5>
                </div>
                <div class="ibox-content">
                  <div class="row">

                    <div class="col-md-offset-3 col-md-4">
                      <div class="form-group">
                          <label>Seleccione Cliente Para Cobrar:</label>
                          <select class="select2_demo_1 form-control" name="sel-cliente" height="100%">
                               <option></option>
                           </select>
                      </div>

                    </div>
                    <div class="col-md-1">
                      <button type="button" style="margin-top:23px;" onclick="buscarOrdenesCliente()" class="btn btn-primary">
                        <span class="fa fa-search"></span>
                      </button>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="">Solo Obligaciones No Cobradas</label>
                        <input style="diplay:inline-block;" id="check-ordenes-no-cobradas" type="checkbox" class="js-switch" checked />
                        <strong class="text-navy" id="strong-ordenes-no-cobradas">SI</strong>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive">
                      <table class="table table-striped dataTables" id="table-resultados">
                          <thead>
                          <tr>
                              <th width="5%" style="text-align:center"><input type="checkbox" class="i-checks" data-index="all" id="check-all"></th>
                              <th>Obligacion</th>
                              <th>Fecha</th>
                              <th>Estado Fac.</th>
                              <th>Estado Cob.</th>
                              <th>Importe</th>
                              <th>Saldo</th>
                              <th>Acciones</th>
                          </tr>
                          </thead>
                          <tbody>
                            <tr style="background-color: transparent;">
                              <td colspan="8"><div class="alert alert-info text-center">
                                  No hay datos.
                              </div></td>
                            </tr>
                          </tbody>
                      </table>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-offset-8 col-lg-2 text-right">
                        <h3 class="text-success"><strong>Total a Pagar: </strong></h3>
                    </div>
                    <div class="col-lg-2 text-center">
                        <h3 class="text-success"><strong id="label-importeTotal">$ 0.00</strong></h3>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-offset-8 col-lg-2 text-right">
                        <h3 class="text-warning"><strong>Pago del Cliente: </strong></h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="input-group">
                          <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                          <input type="number" step="0.01" name="inp-pago-cliente" class="form-control" value="0">
                        </div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-offset-8 col-lg-2 text-right">
                        <h3 class="text-primary"><strong>Saldo: </strong></h3>
                    </div>
                    <div class="col-lg-2 text-center">
                        <h3 class="text-primary"><strong id="label-saldo">$ 0.00</strong></h3>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-success" id="btn-gen-recibo"  onClick="generarRecibo();" type="button"><i class="fa fa-file-text-o"></i> Generar Recibo</button>
                        <a class="btn btn-success btn-outline informarPago" data-toggle="modal" data-target="#myModal" > <i class="fa fa-info-circle"></i> Informar Pago</a>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Contenido Principal -->
<!-- Inicio de Modal -->
  <div class="modal inmodal fade" id="modal-assocFac" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title">Asociar Factura</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <label for="">Ingrese Nro de Factura:</label>
                      <input type="text" id="inp-num-factura" class="form-control" id="" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                  <button type="button" class="btn btn-primary" id="btn-save-asociacion">Guardar</button>
              </div>
          </div>
      </div>
  </div>
<!-- Fin Modal -->



<div class="modal inmodal" id="myModal" cuentaId="" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content FadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--<i class="fa fa-credit-card modal-icon"></i>-->
                <h4 class="modal-title">Informes de Pagos</h4>
                <small class="font-bold">A continuación, favor ingresar los datos solicitados.</small>
            </div>
            <div class="modal-body">
                <form action="#" method="POST" id="formInfoPago">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Fecha de Pago</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_pago" class="form-control date">
                        </div>
                    </div>

                    <div class="form-group col-md-12 cargaFactura">
                        <label>Nro. de Recibo:</label>
                        <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-hashtag"></i></span> {{ Form::text('nro_comprobante', null, array("class"=>"form-control")) }}

                        </div>
                        <!--<a class="btn btn-md btn-primary m-t-xs"> Seleccionar Ordenes de Venta.</a>-->
                    </div>

                    <div class="form-group col-md-12 buscaFactura" style="display: none;">
                        <label>Seleccione la Factura:</label>
                        <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-institution"></i></span> {{ Form::select('factura', array('' => 'Seleccione la factura...'),null,array("class"=>"form-control select2_demo_1", "style" => "width: 100%;" , "placeholder"=>"Cod Cliente")) }}
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Descripción:</label>
                        <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-comment-o"></i></span> <input type="text" name="descripcion" value="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-md-push-6">

                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-usd fa-2x"></i></span> {{ Form::text('monto', null , array("class" => "form-control input-lg", "onkeypress" => "return isNumberKey(event)"  ,"style" => "font-size:25px;","rows" => "1", "placeholder"=>"0.00")) }}

                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary saveInfoPago" onclick="informarPago();">Guardar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>
<!-- Select2 Lenguaje pack español -->
<script src="{{ asset('/backend/js/plugins/select2/select2-lenguaje-spanish.js') }}"></script>
<!-- Chosen -->
<script src="{{ asset('/backend/js/plugins/chosen/chosen.jquery.js') }}"></script>
<!-- jquery-mask -->
<script src="{{ asset('/backend/js/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ asset('/backend/js/plugins/switchery/switchery.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('/backend/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- DatePicker  -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>



<!-- Page-Level Scripts -->
<script>
  var elem = document.querySelector('.js-switch');
  var init = new Switchery(elem);

  $(document).ready(function() {
    $(".select2_demo_1").select2({
        theme:'bootstrap'
    });


    $('.chosen-select').chosen({width: "100%"});

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('.date').val(moment().format('DD/MM/YYYY'));

    $('.date').datepicker({
        format: "dd/mm/yyyy",
        calendarWeeks: true,
        autoclose: true
    });

    elem.onchange = function() {
      if(elem.checked){
        $('#strong-ordenes-no-cobradas').html('SI');
        $('#strong-ordenes-no-cobradas').removeClass('text-danger');
        $('#strong-ordenes-no-cobradas').addClass('text-navy');
      }else{
        $('#strong-ordenes-no-cobradas').html('NO');
        $('#strong-ordenes-no-cobradas').removeClass('text-navy');
        $('#strong-ordenes-no-cobradas').addClass('text-danger');
      }
    };

    $('[name="sel-cliente"]').select2({
      ajax: {
        method: "POST",
        url: '/clientes/jsonSearchCliente',
        dataType: 'json',
        data: function (params){
              var query = {
              palabraClave: params.term,
              "_token": "{{ csrf_token() }}"
              }

              // Query parameters will be ?search=[term]&type=public
              return query;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      },
      minimumInputLength: 3,
      placeholder: "Buscar Cliente.",
      language: select2_lenguaje_spanish, //esta variable es global y viene del archivo select2-lenguaje-spanish
      theme:'bootstrap'
    });

    $('[name="inp-pago-cliente"]').on('change', function(event) {
      event.preventDefault();
      /* Act on the event */
      var importe = globalImporteTotal - $('[name="inp-pago-cliente"]').val();
      $('#label-saldo').html('$ '+ importe.toFixed(2));
    });

  });

  var table;
  var globalImporteTotal = 0;
  var globalArrayOV = null;
  function buscarOrdenesCliente(){
      if (table != null){
        table.destroy();
      }
      if ($('[name="sel-cliente"]').val() == ''){
        swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
        return
      }
      $.ajax({
        method: "POST",
        url: "/clientes/getOrdenesVentaFromClienteJSON",
        data: { "_token": "{{ csrf_token() }}", "idCliente": $('[name="sel-cliente"]').val(), "soloCobradas":elem.checked},
        dataType: 'json'
      }).done(function( msg ) {
          console.log(msg);
          globalArrayOV = msg;
          $('#table-resultados tbody').html('');
          msg.forEach(function (item){
            if(item.pagos_efectuados != null){
              var facturado = '<span class="label label-info">NO FACTURADO</span>';
              var btn_asociar_fac = ' <a class="btn btn-info btn-xs btn-bitbucket" onclick="asociarFactura('+ item.id +')"><i data-toggle="tooltip" data-placement="top" title="Asociar Factura" class="fa fa-file-text-o"></i></a>';
            }else{
              if(item.pagos_efectuados[0].nro_comprobante == null){
                var facturado = '<span class="label label-info">NO FACTURADO</span>';
                var btn_asociar_fac = ' <a class="btn btn-info btn-xs btn-bitbucket" onclick="asociarFactura('+ item.id +')"><i data-toggle="tooltip" data-placement="top" title="Asociar Factura" class="fa fa-file-text-o"></i></a>';
              }else{
                var facturado = '<span class="label label-success">FACTURADO</span>';
                var btn_asociar_fac = '';
              }
            }

            var totalObli = parseFloat(item.monto) ;
            var saldo = parseFloat(item.pagoTotalEfectuado) - parseFloat(item.monto);
            var cssDanger = '';
            if(saldo < 0){
                cssDanger = 'text-danger';
                var cobrado = '<span class="label label-danger">NO COBRADO</span>';
                var disabledVar = '';
            }else{
                cssDanger = 'text-success';
                var cobrado = '<span class="label label-primary">COBRADO</span>';
                var disabledVar = 'disabled';
            }

            //esto es para saber si es una orden de venta cobrable por completo o ya tiene un pago parcial.
            if(totalObli == (saldo * -1)){
              var cobrable = true;
            }else{
              var cobrable = false;
            }
            var fecha = item.orden_venta.fecha;
            $('#table-resultados tbody').append('<tr>'+
                '<td align="center"><input type="checkbox" '+ disabledVar +' class="i-checks" data-index="'+ item.id +'" data-cobrable="'+ cobrable +'" name="input[]"></td>'+
                '<td><a href="/ventas/editOrdenVenta/'+ item.orden_venta.id +'" target="_blank">'+ item.concepto +'</a></td>'+
                '<td>'+ item.orden_venta.fecha +'</td>'+
                '<td align="center">'+ facturado +'</td>'+
                '<td align="center">'+ cobrado +'</td>'+
                '<td align="center">$ '+ totalObli +'</td>'+
                '<td align="center" class="'+ cssDanger +'" name="saldo">$ '+ saldo +'</td>'+
                '<td align="center">'+
                  '<a  class="btn btn-success btn-xs btn-bitbucket" href="/ventas/editOrdenVenta/'+ item.id +'" target="_blank" ><i data-toggle="tooltip" data-placement="top" title="Ver Detalle Orden de Venta" class="fa fa-eye"></i></a>'+
                  btn_asociar_fac+
                '</td>'+
            '</tr>');

            //CARGAR ORDENES DE VENTA AL SELECT
            $(".chosen-select").append("<option value='"+item.id+"'> <strong>Ord. Vent.: </strong> #"+item.id+" -- <strong>TOTAL:</strong> $ "+saldo+"<strong> -- FECHA: </strong>"+fecha+"</option>");

            $('.chosen-select').trigger("chosen:updated");

          });
          table = $('#table-resultados').DataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]});
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            //funcion del boton seleccionar todos los elementos.
            $('#check-all').on('ifChanged', function(event){
              if(event.target.checked){
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('check');
                 });
              }else{
                $('[name="input[]"]:not(:disabled)').each(function(){
                   $(this).iCheck('uncheck');
                 });
              }
            });
            $('[data-toggle="tooltip"]').tooltip();

            //codigo para calcular el total de items seleccionados.
            $('[name="input[]"]').on('ifChanged', function(event){
                globalImporteTotal = 0;
                $('[name="input[]"]:checked').each(function () {
                  //arrayov.push($(this).data('index'));
                  var valor = $(this).closest("tr").find('td[name="saldo"]').text();
                  valor = parseFloat(valor.substr(2, valor.length)) * -1
                  globalImporteTotal += valor;
                  console.log('valor ' + globalImporteTotal);
                });
                $('#label-importeTotal').html('$ '+ globalImporteTotal.toFixed(2));

                var importe = globalImporteTotal - $('[name="inp-pago-cliente"]').val();
                $('#label-saldo').html('$ '+ importe.toFixed(2));
             });
            //fin codigo
      }).fail(function(mgs){
          swal({
            title: "Error!",
            text: "Algo esta mal, por favor Intentelo nuevamente.",
            type: "error",
            confirmButtonText: "Cerrar"
          });
      });

}
var arrayoblig = Array();
function generarRecibo(){
    arrayoblig = Array();
    $('[name="input[]"]:checked').each(function () {
      console.log(globalArrayOV);
      var valor  = parseInt($(this).data('index'));
      var objeto = $.grep(globalArrayOV, function(e){ return e.id == valor; });

      var entrada = {idObligacion:$(this).data('index')};
      arrayoblig.push(entrada);
      console.log(arrayoblig);
    });
    return
    if (arrayoblig.length == 0){
      swal("ups!", "Tiene que seleccionar al menos una orden de venta para generar recibo.", "warning");
      return
    }
    if ($('[name="sel-cliente"]').val() == ''){
      swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() == '' || $('[name="inp-pago-cliente"]').val() == 0){
      swal("ups!", "El pago del cliente no puede ser 0 ni vacio.", "warning");
      return
    }
    if($('[name="inp-pago-cliente"]').val() > globalImporteTotal){
      swal("ups!", "El pago del cliente no puede ser superior al pago total.", "warning");
      return
    }
    $.ajax({
      method: "POST",
      url: "/cobranza/generarCobro",
      data: {
              "_token": "{{ csrf_token() }}",
              "idCliente": $('[name="sel-cliente"]').val(),
              "pagoCliente" : $('[name="inp-pago-cliente"]').val(),
              'arrayObli': arrayoblig
            },
      dataType: 'json'
    }).done(function( msg ) {
        console.log(msg);
        window.open('/recibos/print/'+ msg.idRecibo,'_blank');
        location.reload();
    }).fail(function(mgs){
        swal({
          title: "Error!",
          text: "Algo esta mal, por favor Intentelo nuevamente.",
          type: "error",
          confirmButtonText: "Cerrar"
        });
    });
  }


  function asociarFactura(id){
     $('#modal-assocFac').modal('show');
     $('#btn-save-asociacion').on('click', function(event) {
         event.preventDefault();
         /* Act on the event */
         $.ajax({
           url: '/pagoEfectuado/asociarFactura',
           type: 'POST',
           data: {
                    "_token": "{{ csrf_token() }}",
                    idOrdenVenta: id,
                    asocFactura: $.trim($('#inp-num-factura').val())
                 }
         })
         .done(function() {
           $('#modal-assocFac').modal('hide');
           swal("Asociado!", "Factura Asociada Correctamente.", "success");
           location.reload();
         })
         .fail(function() {
           swal("Error", "Algo esta mal, intentelo de nuevo mas tarde.", "error");
         });
     });
  }

  function informarPago(){
    arrayoblig = Array();
    $('[name="input[]"]:checked').each(function () {
      console.log(globalArrayOV);
      var valor  = parseInt($(this).data('index'));
      var objeto = $.grep(globalArrayOV, function(e){ return e.id == valor; });

      var entrada = {idObligacion:$(this).data('index'), saldoObli: objeto};
      arrayoblig.push(entrada);
      console.log(arrayoblig);
    });

    if (arrayoblig.length == 0){
      swal("ups!", "Tiene que seleccionar al menos una orden de venta para generar recibo.", "warning");
      return
    }
    if ($('[name="sel-cliente"]').val() == ''){
      swal("ups!", "Tiene que seleccionar un cliente primero.", "warning");
      return
    }
    if($('[name="monto"]').val() == '' || $('[name="monto"]').val() == 0){
      swal("ups!", "El pago del cliente no puede ser 0 ni vacio.", "warning");
      return
    }
    if($('[name="monto"]').val() > globalImporteTotal){
        swal("ups!", "El pago del cliente no puede ser superior al pago total.", "warning");
        return
    }
    $.ajax({
      url: '/cobranza/informarPago',
      type: 'POST',
      dataType: 'json',
      data: {
              "_token": "{{ csrf_token() }}",
              "idCliente"        : $('[name="sel-cliente"]').val(),
              "fecha"            : $('[name="fecha_pago"]').val(),
              "arrayOV"          : arrayoblig,
              "pagoCliente"      : $('[name="monto"]').val(),
              "reciboManualNum"  : $('[name="nro_comprobante"]').val(),
              "descripcion"      : $('[name="descripcion"]').val(),
            }
    })
    .done(function() {
      console.log("success");
      $('#myModal').modal('hide');
      swal("Exito!", "Pago informado correctamente.", "success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }
</script>

@stop
