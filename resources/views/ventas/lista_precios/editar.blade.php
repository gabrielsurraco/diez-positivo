@extends('layouts.backend')

@section('css')
    <link href="{{ asset('/backend/css/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="row wrapper border-bottom  page-heading m-b-md">
    <div class="col-lg-10">
        <h2>Item</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Lista de Precio</a>
            </li>
            <li class="active">
                <strong>Item</strong>
            </li>
        </ol>
    </div>
</div>
    <div class="row">
        @if(session()->has('msg'))
            <div class="alert alert-success">
                {{ session()->get('msg') }}
            </div>
        @endif
        <div class="col-md-6">
            {!! Form::open(['url' => '/lista-precios/guardar', 'method' => 'post']) !!}
                    <input type="hidden" name="id_item" value="{{ $sub_concepto->id }}" />
                    <div class="form-group col-md-6">
                        {{ Form::label('concepto', 'Concepto') }}
                        {{ Form::select('concepto', $all_conceptos , $sub_concepto->fk_lista_precio , array("class"=>"form-control")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('sub_concepto', 'Sub-Concepto') }}
                        {{ Form::text("sub_concepto", $sub_concepto->sub_concepto, array("class"=>"form-control", "placeholder"=>"Descripcion...")) }}
                        @if ($errors->has('sub_concepto'))
                            <span class="help-block text-danger" >
                                <strong>
                                    {{ $errors->first('sub_concepto') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('moneda', 'Moneda') }}
                        {{ Form::select('moneda', array('ars' => "ARS", 'usd' => "USD") , $sub_concepto->moneda , array("class"=>"form-control")) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('costo', 'Costo') }}
                        {{ Form::text("costo", $sub_concepto->costo, array("class"=>"form-control", "placeholder"=>"$")) }}
                        @if ($errors->has('costo'))
                            <span class="help-block text-danger" >
                                <strong>
                                    {{ $errors->first('costo') }}
                                </strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <a href="/ventas/lista-precios" class="btn btn-sm btn-danger"> Cancelar</a>
                        <button type="submit" class="btn btn-sm btn-success"> Guardar</button>
                    </div>
                
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('javascript')

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script>
    $(document).ready(function(){

        
        $('.input-group.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });
        
//        $('#departamento').on('change',function(){
//            if(this.value !== 0){
//                var valor = this.value; 
//                $.ajax({
//                   type: "GET",
//                   url: "/localidades/get",
//                   data: "depto="+valor,
//                   success: function(data)
//                   {
//                       $('#localidad').not(":nth-child(1)").empty();
//                        var a = JSON.parse(data);
//                        $.each(a, function(key, value) {   
//                            $('#localidad')
//                                .append($("<option></option>")
//                                           .attr("value",value.id)
//                                           .text(value.nombre)); 
//                       });
//                   },
//                   error: function(msg){
//                       alert(msg);
//                   }
//                   
//                });
//            }
//           
//        });

    });
</script>
@stop



