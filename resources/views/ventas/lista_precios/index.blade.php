@extends('layouts.backend')


@section('css')
<!-- Required Stylesheets -->
<link href="{{ asset('/bootstrap-treeview/src/css/bootstrap.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')
    @if (Session::has('flash_notification.message'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-{{ session('flash_notification.level') }} ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session('flash_notification.message') !!}
                </div>
            </div>
        </div>
    </div>

    @endif
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
          <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Productos y Servicios</h5>
                      <div class="ibox-tools">
                          <a data-toggle="modal" data-target="#myModal2"><button class="btn btn-primary btn-sm" ><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Item</button></a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row">
                          <div class="col-lg-12" >
                              <div class="table-responsive">
                                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                                      <thead>
                                          <tr>
                                              <th>#</th>
                                              <th>CONCEPTO</th>
                                              <th>SUB-CONCEPTO</th>
                                              <th>CATEGORIA</th>
                                              <th>UNIDAD</th>
                                              <th>MONEDA</th>
                                              <th>PRECIO</th>
                                              <th>ACCI&Oacute;N</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach($listado as $item)
                                              <tr class="gradeX">
                                                  <td>{{ $item->id }}</td>
                                                  <td>{{ $item->concepto->concepto }}</td>
                                                  <td>{{ $item->sub_concepto }}</td>
                                                  <td align="center">
                                                    @if($item->categoria == "Servicio")
                                                        <span class="label label-primary">{{ $item->categoria }}</span>
                                                    @else
                                                        <span class="label label-success">{{ $item->categoria }}</span>
                                                    @endif
                                                    </td>
                                                  <td>{{ $item->unidad }}</td>
                                                  <td>{{ strtoupper($item->moneda) }}</td>
                                                  <td>{{ $item->costo }}</td>
                                                  <td class="center" >
                                                      <a class="btn btn-sm btn-success" href="{{  '/lista-precios/'.$item->id .'/edit' }}" title="Editar"><i class="fa fa-edit"> </i></a>
                                                      <a class="btn btn-sm btn-danger rm-sub-concepto"  title="Eliminar"><i class="fa fa-trash"></i> </a>
                                                  </td>
                                              </tr>
                                          @endforeach
                                      </tbody>
                                      <tfoot>
                                          <tr>
                                              <th>#</th>
                                              <th>CONCEPTO</th>
                                              <th>SUB-CONCEPTO</th>
                                              <th>CATEGORIA</th>
                                              <th>UNIDAD</th>
                                              <th>MONEDA</th>
                                              <th>PRECIO</th>
                                              <th>ACCI&Oacute;N</th>
                                          </tr>
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
        </div>
   </div>




<div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo Concepto</h4>
            </div>
            {!! Form::open(['url' => '#', 'method' => 'post','id' => 'form']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 form-group ">
                        {{ Form::label('concepto', 'Concepto') }}
                        {{ Form::text("concepto", null, array("class"=>"form-control valor-concepto", "placeholder"=>"Descripción del concepto...")) }}
                        <span class="help-block text-danger error-calle" style="display: none;margin: 0px;">
                            <strong>
                            </strong>
                        </span>
                        <p class="m-t-md">(*) El nombre del concepto debe ser sin espacios y sin s&iacute;mbolos</p>
                    </div>
                    <div class="col-md-6 form-group ">
                        {{ Form::label('categoria', 'Categoría del Plan de Cuentas') }}
                        {{ Form::select("categoria",array("default" => "Seleccionar...") + $all_categorias, null, array("class"=>"form-control categoria")) }}
                        <span class="help-block text-danger error-calle" style="display: none;margin: 0px;">
                            <strong>
                            </strong>
                        </span>
                        <p class="m-t-md">(*) Primero debe crear la categoria en el plan de cuentas</p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <p class="text-danger pull-left" id="mensaje" style="display: none;"></p>
                <a class="btn btn-white" href="#myModal2" data-toggle="modal" data-dismiss="modal">Cancelar</a>
                <a class="btn btn-primary"  id="guardar" href="#myModal2" data-toggle="modal" data-dismiss="modal">Guardar</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                <div class="modal-content animated bounceInRight">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

                                        </div>
                                        <div class="modal-body">
                                            <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                                                printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                                                remaining essentially unchanged.</p>
                                                    <div class="form-group"><label>Sample Input</label> <input type="email" placeholder="Enter your email" class="form-control"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<div class="modal inmodal " id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            {!! Form::open(['url' => '#', 'method' => 'post','id' => 'sub-c']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-wifi modal-icon"></i>
                <h3 class="modal-title">Alta del Producto o Servicio</h3>
                <small class="font-bold">Proporcione la siguiente información acerca del producto o servicio:</small>
            </div>
            <div class="modal-body">
                <div class="row m-t-sm">

                  <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Alta del Producto o Servicio </h5>
                            <div class="ibox-tools">
                                <a class="btn btn-primary btn-xs " href="#myModal3" data-toggle="modal" data-dismiss="modal"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Concepto</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form role="form">
                                        <div class="row">
                                            <div class="form-group col-lg-3"><label>Concepto</label> {{ Form::select("concepto", $all_conceptos ,null, array("class"=>"form-control concepto")) }}</div>
                                            <div class="form-group col-lg-3"><label>Sub-Concepto</label> {{ Form::text("sub_concepto", null, array("class"=>"form-control", "placeholder"=>"Descripción..." )) }}</div>
                                            <div class="form-group col-lg-2"><label>Categoria</label>{{ Form::select("categoria", array('Servicio' => "Servicio",'Producto' => "Producto") ,null, array("class"=>"form-control")) }}</div>
                                            <div class="form-group col-lg-2"><label>Unidad</label><select name="sel-unidad" class="form-control">
                                                                                                    <option value="1">KILOGRAMO</option>
                                                                                                    <option value="2">METROS</option>
                                                                                                    <option value="3">METRO CUADRADO</option>
                                                                                                    <option value="4">METRO CUBICO</option>
                                                                                                    <option value="5">LITROS</option>
                                                                                                    <option value="6">1000 KILOWATT HORA</option>
                                                                                                    <option value="7" selected="">UNIDAD</option>
                                                                                                    <option value="8">PAR</option>
                                                                                                    <option value="9">DOCENA</option>
                                                                                                    <option value="10">QUILATE</option>
                                                                                                    <option value="11">MILLAR</option>
                                                                                                    <option value="12">MEGA-U. INT. ACT. ANTIB</option>
                                                                                                    <option value="13">UNIDAD INT. ACT. INMUNG</option>
                                                                                                    <option value="14">GRAMO</option>
                                                                                                    <option value="15">MILIMETRO</option>
                                                                                                    <option value="16">MILIMETRO CUBICO</option>
                                                                                                    <option value="17">KILOMETRO</option>
                                                                                                    <option value="18">HECTOLITRO</option>
                                                                                                    <option value="19">MEGA U. INT. ACT. INMUNG.</option>
                                                                                                    <option value="20">CENTIMETRO</option>
                                                                                                    <option value="21">KILOGRAMO ACTIVO</option>
                                                                                                    <option value="22">GRAMO ACTIVO</option>
                                                                                                    <option value="23">GRAMO BASE</option>
                                                                                                    <option value="24">UIACTHOR</option>
                                                                                                    <option value="25">JUEGO O PAQUETE MAZO DE NAIPES</option>
                                                                                                    <option value="26">MUIACTHOR</option>
                                                                                                    <option value="27">CENTIMETRO CUBICO</option>
                                                                                                    <option value="28">UIACTANT</option>
                                                                                                    <option value="29">TONELADA</option>
                                                                                                    <option value="30">DECAMETRO CUBICO</option>
                                                                                                    <option value="31">HECTOMETRO CUBICO</option>
                                                                                                    <option value="32">KILOMETRO CUBICO</option>
                                                                                                    <option value="33">MICROGRAMO</option>
                                                                                                    <option value="34">NANOGRAMO</option>
                                                                                                    <option value="35">PICOGRAMO</option>
                                                                                                    <option value="36">MUIACTANT</option>
                                                                                                    <option value="37">UIACTIG</option>
                                                                                                    <option value="41">MILIGRAMO</option>
                                                                                                    <option value="47">MILILITRO</option>
                                                                                                    <option value="48">CURIE</option>
                                                                                                    <option value="49">MILICURIE</option>
                                                                                                    <option value="50">MICROCURIE</option>
                                                                                                    <option value="51">U. INTER. ACT. HOR.</option>
                                                                                                    <option value="52">MEGA U. INTER. ACT. HOR.</option>
                                                                                                    <option value="53">KILOGRAMO BASE</option>
                                                                                                    <option value="54">GRUESA</option>
                                                                                                    <option value="55">MUIACTIG</option>
                                                                                                    <option value="61">KG. BRUTO</option>
                                                                                                    <option value="62">PACK</option>
                                                                                                    <option value="63">HORMA</option>
                                                                                                    <option value="98">OTRAS UNIDADES</option>
                                                                                                    <option value="99">BONIFICACION</option>
                                                                                                  </select>
                                            </div>
                                            <div class="form-group col-lg-2"><label>Moneda</label>{{ Form::select("moneda", array("ars" => "ARS", "usd" => "USD") ,null, array("class"=>"form-control")) }}</div>
                                            <div class="form-group col-lg-3"><label>Importe de Compra</label><input type="text" class="form-control"></div>
                                            <div class="form-group col-lg-3"><label>Gan. Sobre la Compra %</label><input type="text" class="form-control"></div>
                                            <div class="form-group col-lg-3"><label>Alicuota</label><select class="form-control" name="Producto[idAlicuota]">
                                                                                                        <option value="0">Exento</option>
                                                                                                        <option value="3">0%</option>
                                                                                                        <option value="4">10.5%</option>
                                                                                                        <option value="5">21%</option>
                                                                                                        <option value="6">27%</option>
                                                                                                        <option value="8">5%</option>
                                                                                                        <option value="9">2.5%</option>
                                                                                                        <option value="100">No gravado</option>
                                                                                                        <option value="101" selected="">--</option>
                                                                                                    </select></div>
                                            <div class="form-group col-lg-3"><label>Importe de Venta</label><input type="text" class="form-control"></div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="item-save">Guardar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!-- Required Javascript -->
<script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script>
$(document).ready(function () {
        var tr_count    = 1;
        var tr_max      = 10;
        var body_sub_conceptos = $("#item-body").children().clone(true);



        /*
         * Valida que no se ingrese una coma en PRECIO
         */
        $('.numero').keypress(function(e){
            if(e.which == 44){
              return false;
            }
        });

        /*
         * Guarda los subconceptos agregados
         */
        $('#item-save').click(function(e){
            e.preventDefault();
//            var cant = $("#item-body tr").length;
//            alert(cant);

//            var i = 0;
            var datos = new Array();
            $('#item-body  > tr').each(function() {
                var concepto        = $(this).find("td:eq(0) select option:selected").val();
                var sub_concepto    = $(this).find("td:eq(1) input").val();
                var categoria       = $(this).find("td:eq(2) select option:selected").val();
                var unidad          = $(this).find("td:eq(3) select option:selected").val();
                var moneda          = $(this).find("td:eq(4) select option:selected").val();
                var precio          = $(this).find("td:eq(5) input").val();
                datos.push({concepto: concepto, sub_concepto: sub_concepto, categoria: categoria, unidad: unidad, moneda: moneda, precio: precio});
            });

            var token = $('meta[name=csrf-token]').attr("content");
//            var a = {asd: "aa"};
//            alert(datos);

            $.ajax({
                type: "post",
                url: "/lista-precios/new-sub-conceptos",
                data: { _token: token, data: datos  },
                success: function(data){
                    $('#myModal2').modal('toggle');
                    swal("Exito!", data.mensaje, "success");
                    var t = $('.dataTables-example').DataTable();
                    var action = '<a class="btn btn-sm btn-success" href="/ventas/clientes/servicios" title="Editar"><i class="fa fa-edit"> </i></a> <a class="btn btn-sm btn-danger"  title="Eliminar"><i class="fa fa-trash"></i> </a>';
                    $.each(data.datos, function(k, v) {
                        t.row.add( [
                            v.id,
                            v.fk_lista_precio,
                            v.sub_concepto,
                            v.categoria,
                            v.unidad,
                            v.moneda.toUpperCase(),
                            v.costo,
                            action
                        ] ).draw( false );
                    });
                    $('.dataTables-example tbody tr:last td:eq(3)').css('text-align', 'center');
//                    var body = $("#item-body").children().clone(true);
                    $("#item-body").find('*').remove();
                    $("#item-body").append(body_sub_conceptos);
                },
                error: function(msg){
                    alert(msg.responseJSON.mensaje);
                }
            });

        });

    $('.editar').click(function(){
        var id_concepto     = $(".editar").attr("id_concepto");
        var nombre_concepto = $(".editar").attr("nombre_concepto");

        $("#guardar").hide();
        $("#actualizar").show();

        $("#concepto").val(nombre_concepto);
        $("#concepto").attr("id_concepto",id_concepto);
        $('#myModal').modal('show');
    });

    $('#myModal').on('hidden.bs.modal', function () {
        $("#actualizar").hide();
        $("#guardar").show();
        $("#concepto").removeAttr("id_concepto");
        $("#concepto").val("");
    });

    $('.subconceptos').click(function(){
        alert("funciona");
    });

    $("#actualizar").click(function(){
        var token = $('meta[name=csrf-token]').attr("content");
        var concepto = $('#concepto').val();
        var id_concepto = $('#concepto').attr("id_concepto");

        $.ajax({
            type: "POST",
            url: "/lista-precios/update",
            data: {_token: token, concepto: concepto, id_concepto: id_concepto },
            success: function(data){
                $('#myModal').modal('toggle');

//                var tr = $("<tr></tr>");
//                tr.append($("<td>"+data.data.id_concepto+"</td>"));
//                tr.append($("<td>"+data.data.concepto+"</td>"));
//                var td = $("<td></td>");
//                td.append("<a class='btn btn-sm btn-warning subconceptos' title='Sub-Conceptos'> Sub-Conceptos</a>");
//                td.append(" <a class='btn btn-sm btn-success editar' title='Editar'> <i class='fa fa-edit'> </i></a>");
//                td.append(" <a class='btn btn-sm btn-danger eliminar' title='Eliminar'> <i class='fa fa-trash'></i> </a>");
//                tr.append(td);

//                $("#body_concepto").append(tr);
                swal("Ã‰xito!", "Se modificÃ³ el concepto correctamente!", "success");
            },
            error: function(msg){
                console.log(msg);
                alert(msg);
            }
        });
    });

    $('#guardar').click(function(){
        var token = $('meta[name=csrf-token]').attr("content");
        var concepto = $('.valor-concepto').val();
        var categoria = $('.categoria').val();
        $.ajax({
            type: "POST",
            url: "/lista-precios/new",
            data: {_token: token, concepto: concepto, categoria: categoria },
            success: function(data){
                var option = $("<option>"+ data.data.concepto +"</option>");
                option.attr('value',data.data.id_concepto);
                $('select.concepto').append(option);
                $('.valor-concepto').val("");
                swal("Éxito!", "Se agregó el concepto correctamente!", "success");
            },
            error: function(msg){
                alert(msg);
            }
        });
    });

    $('.dataTables-example').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]

    });

});
</script>
@stop
