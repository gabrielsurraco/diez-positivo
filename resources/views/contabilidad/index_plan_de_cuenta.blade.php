@extends('layouts.backend')


@section('css')
<!-- Required Stylesheets -->
<!--<link href="{{ asset('/bootstrap-treeview/src/css/bootstrap.css') }}" rel="stylesheet">-->

<!-- Sweet Alert -->
<link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Data Tables -->
<link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('backend/css/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">

    @if(session()->has('msg'))
    <div class="alert alert-success">
        {{ session()->get('msg') }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cuentas</h5>
                    
                    <div class="ibox-tools">
                        <a class="btn btn-primary btn-sm "  data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> Nueva Cuenta</a>
                    </div>
                    
                </div>
                <div class="ibox-content">
                    
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Cuenta</th>
                                <th>Imputable</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody id="body-tabla">
                            @foreach($cuentas as $cuenta)
                            <tr class="gradeX">
                                <td>{{ $cuenta->codigo }}</td>
                                <td>{{ $cuenta->nombre }}</td>
                                <td><?php echo ($cuenta->imputable) ? "Si" : "No"; ?></td>
                                <td class="center">
                                    <a href="/contabilidad/plan-de-cuentas/categoria/{{$cuenta->id}}/edit" class="btn btn-xs btn-warning editar"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-xs btn-danger eliminar" id="{{ $cuenta->id }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Código</th>
                                <th>Cuenta</th>
                                <th>Imputable</th>
                                <th>Acción</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="modal inmodal" id="myModal"  role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Nueva Cuenta</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => '#', 'method' => 'post','id' => 'form-cuenta']) !!}
                    <div class="row">
                        <div class="form-group col-md-6">
                            {{ Form::label('nombre', 'Nombre') }}
                            {{ Form::text("nombre", null, array("class"=>"form-control nombre_cuenta", "placeholder"=>"Nombre de la Cuenta")) }}
                            <span class="help-block text-warning error-nombre" style="display: none">
                                <strong>
                                </strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('imputable', 'Cuenta Imputable?') }}
                            {{ Form::select("imputable",array('No','Si') ,null, array("class"=>"form-control")) }}
                        </div>
                        <div class="form-group col-md-12">
                             {{ Form::label('cuenta', 'Pertenece a') }}
                            {{ Form::select("cuenta",array("" => "") ,null, array("class" => "form-control cuentas" ,"style" => "width: 100%;")) }}
                            <span class="help-block text-warning error-cuenta" style="display: none">
                                <strong>
                                </strong>
                            </span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary"  id="guardar-cuenta">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<!-- Required Javascript -->
<script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>


<script src="{{ asset('backend/js/plugins/select2/select2.min.js') }} "></script>

<script>
    $(document).ready(function(){
        
        var token = $('meta[name=csrf-token]').attr("content");
        
        $('.dataTables-example').DataTable({
            pageLength: 10,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });
        
        $(".cuentas").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-cuentas/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term,
                  page: params.page,
                };
              },
              processResults: function (data, params) {
                return {
                  results: data.results,
                  pagination: {
                    more: data.pagination.more
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Cuenta...",
            allowClear: true
        });
        
        $("#guardar-cuenta").click(function(e) {
            e.preventDefault();
            var datos = $("#form-cuenta").serialize();
            $.ajax({
                type: 'POST',
                data: datos,
                url: '/contabilidad/guardar-cuenta',
                success:function(data){
                    $('#myModal').modal('toggle');
                    var table = $('.dataTables-example').DataTable();
                        
                    if(data.success.imputable == 1 )
                        imputable = "Si";
                    else
                        imputable = "No";

                    table.row.add([
                        data.success.codigo,
                        data.success.nombre,
                        imputable,
                        "<a href='/contabilidad/plan-de-cuentas/categoria/"+data.success.id+"/edit' class='btn btn-xs btn-warning editar'><i class='fa fa-edit'></i></a> <a class='btn btn-xs btn-danger eliminar'><i class='fa fa-trash'></i></a>"
                    ]).draw();
                    
                        
                    swal({
                        title: "Buen Trabajo!",
                        text: "La cuenta fue creada correctamente!",
                        type: "success"
                    });
                },
                error: function(mensaje){
                    $('.error-nombre strong').html(" ");
                    $('.error-nombre').show();
                    $('.error-nombre strong').html(JSON.parse(mensaje.responseText).errors.nombre);
                    
                    $('.error-cuenta strong').html(" ");
                    $('.error-cuenta').show();
                    $('.error-cuenta strong').html(JSON.parse(mensaje.responseText).errors.cuenta);
                },
                complete: function(){
                    $(".nombre_cuenta").val("");
                    $(".cuentas").val('').change();
                }
            });
        });
        
        $("#body-tabla").on("click", "a.eliminar", function(){
//                var a = event.currentTarget.id;
            var a = $(this).attr("id");
            var tr = $(this).parent().parent();
            swal({
                title: "Está Seguro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirmar!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    type: "POST",
                    url: '/contabilidad/plan-de-cuentas/delete',
                    data: {_token : token, id: Number(a)},
                    success: function(data) {
                        tr.empty();
//                        $('.footable2').trigger('footable_initialize');
                        swal("Eliminado!", "La cuenta ha sido eliminada.", "success");
                    },
                    error: function (data) {
                        swal("Error al Eliminar!", "Contactarse con el programador.", "warning");
                    },
                    beforeSend: function(a) {

                    },
                    complete: function(){
                    }
                });
            });
        });


    });
</script>
@stop
