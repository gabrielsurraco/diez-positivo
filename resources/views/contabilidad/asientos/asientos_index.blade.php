@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <!--<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">-->
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

    <!-- Data Tables -->
    <link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Asientos</div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-sm btn-primary" href="/contabilidad/asientos/nuevo"><i class="fa fa-plus-square"></i> Nuevo Asiento</a>
                        </div>
                    </div>
                    <div class="row m-t-lg">
                        <form id="asientos-form" method="POST">
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                                {{ Form::label('fecha_desde', 'Fecha Desde') }}
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_desde" placeholder="dd/mm/yyyy" class="form-control date" value="{{ date('d/m/Y') }}">
                                </div>
                                @if ($errors->has('fecha_desde'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_desde') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('fecha_hasta', 'Fecha Hasta') }}
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha_hasta" placeholder="dd/mm/yyyy" class="form-control date" value="{{ date('d/m/Y') }}">
                                </div>
                                @if ($errors->has('fecha_hasta'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha_hasta') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('nro_asiento', 'Nro. Asiento') }}
                                {{ Form::number("nro_asiento", null , array("class"=>"form-control ", "placeholder"=>"0")) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('cuenta', 'Cuenta') }}
                                <select name="cuenta" class="form-control select2_demo_3 " style="width: 100% !important;">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('descripcion', 'Descripción') }}
                                {{ Form::text("descripcion", null , array("class"=>"form-control ", "placeholder"=>"")) }}
                            </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-primary" id="buscar-asientos"><i class="fa fa-search"></i> Buscar</a>
                            </div>
                        </div>

                    </div>
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <div class="col-md-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Nro. Asiento</th>
                                        <th>Cuenta</th>
                                        <th>Debe</th>
                                        <th>Haber</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Nro. Asiento</th>
                                        <th>Cuenta</th>
                                        <th>Debe</th>
                                        <th>Haber</th>
                                        <th>Descripción</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div style="text-align: center; " class="m-b-md">
                                <div><strong>Total Debe: $ </strong> <label class="total-debe">0</label> - <strong>Total Haber: $ </strong> <label class="total-haber">0</label></div>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<!--<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>-->

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });



//        $("#buscar-asientos").click(function(
//                alert("asd");
//        ));

        $(".select2_demo_3").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-cuentas/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term, // search term
                  page: params.page,
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.results,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Cuenta...",
            allowClear: true
        });

//        $('.footable2').footable();

        var data_table = $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });

        $("#buscar-asientos").click(function(){
            var datos = $("#asientos-form").serialize();
            var token = $("meta[name='csrf-token']").attr('content');
            $.ajax({
                type: "POST",
                url: "/contabilidad/asientos/buscar",
                data: "_token="+token+"&"+datos,
                success: function(data){
                    data_table.rows().remove().draw();
                    if(data.mensaje.length > 0){
                        $(".total-debe").html(data.mensaje[0][6]);
                        $(".total-haber").html(data.mensaje[0][7]);
                    }
                    for(var cont = 0; cont < data.mensaje.length; cont++){
                        data_table.row.add(data.mensaje[cont]);
                        data_table.draw();
                    }
                },
                error: function(data){

                }
            });
        });

//        $("a.eliminar").click(function(){
//            var id_usuario = $(this).attr('id_usuario');
//            swal({
//                title: "Esta Seguro?",
//                type: "warning",
//                showCancelButton: true,
//                confirmButtonColor: "#DD6B55",
//                confirmButtonText: "Eliminar!",
//                closeOnConfirm: true
//            }, function () {
//
//                $.ajax({
//                    type: "POST",
//                    url: "/usuarios/eliminar",
//                    data: "_token="+$("meta[name='csrf-token']").attr('content')+"&id_usuario="+id_usuario,
//                    success: function(){
//                        $("a.eliminar[id_usuario='"+id_usuario+"']").parent().parent().remove();
//                    },
//                    error: function(){
//
//                    }
//                });
//
//            });
//
//
//        });
    });
</script>
@stop
