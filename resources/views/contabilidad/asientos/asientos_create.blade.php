@extends('layouts.backend')
@section('css')
    <!-- FooTable -->
    <!--<link href="{{ asset('/backend/css/footable/footable.core.css') }}" rel="stylesheet">-->
    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('/backend/css/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/css/select2/select2-bootstrap.css') }}" rel="stylesheet">

    <!-- Data Tables -->
    <link href="{{ asset('/backend/css/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('content')

@if (Session::has('msg'))
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="alert alert-info ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('msg') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Nuevo Asiento Manual</div>

                <div class="panel-body">

                    <div class="row m-t-lg">
                        <form id="asientos-form" method="POST">
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                                {{ Form::label('fecha', 'Fecha') }}
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="fecha" type="text" name="fecha" placeholder="dd/mm/yyyy" class="form-control date" value="{{ date('d/m/Y') }}">
                                </div>
                                @if ($errors->has('fecha'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('fecha') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-3">
                                {{ Form::label('cuenta', 'Cuenta') }}
                                <select id="cuenta" name="cuenta" class="form-control select2_demo_3 " style="width: 100% !important;">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('debe', 'Debe') }}
                                {{ Form::text("debe", 0 , array("class"=>"form-control ", "placeholder"=>"0", "id" => "debe","value" => "0" )) }}
                            </div>
                            <div class="form-group col-md-2">
                                {{ Form::label('haber', 'Haber') }}
                                {{ Form::text("haber", 0 , array("class"=>"form-control ", "placeholder"=>"0", "id" => "haber","value" => "0")) }}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('descripcion', 'Descripción') }}
                                {{ Form::text("descripcion", null , array("class"=>"form-control ", "placeholder"=>"","id" => "descripcion")) }}
                            </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-primary" id="agregar-asiento"><i class="fa fa-plus"></i> Agregar</a>
                            </div>
                        </div>

                    </div>
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Cuenta</th>
                                    <th>Debe</th>
                                    <th>Haber</th>
                                    <th>Descripción</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody id="body-tabla">
                                <tr id="tr-ejemplo" style="display: none;">
                                    <td>fecha</td>
                                    <td>cuenta</td>
                                    <td>debe</td>
                                    <td>haber</td>
                                    <td>descripcion</td>
                                    <td><a class='btn btn-xs btn-danger eliminar' title='Eliminar'><i class='fa fa-trash' ></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="text-align: center; " class="m-b-md">
                                <div><strong>Total Debe: $ </strong> <label class="total-debe">0</label> - <strong>Total Haber: $ </strong> <label class="total-haber">0</label></div>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 center" style="text-align: center;"><a class="btn btn-sm btn-primary" id="impactar-asientos"> Impactar Asientos</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<!-- FooTable -->
<!--<script src="{{ asset('/backend/js/plugins/footable/footable.all.min.js') }}"></script>-->

<!-- Sweet alert -->
<script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('/backend/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/backend/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Data Tables -->
<script src="{{ asset('/backend/js/plugins/dataTables/datatables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.date').datepicker({
            startView: 0,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd/mm/yyyy"
        });

        var suma_debe_asientos = parseFloat(0);
        var suma_haber_asientos = parseFloat(0);

        $("#agregar-asiento").click(function(){
            var fecha = $("#fecha").val();
            var debe = parseFloat($("#debe").val());
            var haber = parseFloat($("#haber").val());
            var descripcion = $("#descripcion").val();
            var cuenta = $("#cuenta option:selected");

            if(fecha === ""){
                alert("Seleccionar la FECHA");
                return false
            }

            if((debe === "" && haber === "") || (debe == parseFloat(0) && haber == parseFloat(0))){
                alert("Ingrese valor en DEBE o HABER");
                return false;
            }

            if(isNaN(debe) || debe === ""){
                debe = parseFloat(0);
            }

            if(isNaN(haber) || haber === ""){
                haber = parseFloat(0);
            }

            if(cuenta.val() === ""){
                alert("Seleccionar la CUENTA");
                return false
            }



            suma_debe_asientos  = (parseFloat(suma_debe_asientos)  + parseFloat(debe)).toFixed(2);
            suma_haber_asientos = (parseFloat(suma_haber_asientos) + parseFloat(haber)).toFixed(2);

            var tr  = $("#tr-ejemplo").clone(true);
            tr.find("td:eq(0)").html(fecha);
            tr.find("td:eq(1)").html(cuenta.text());
            tr.find("td:eq(1)").attr("cuenta_id",cuenta.val());
            tr.find("td:eq(2)").html(debe.toFixed(2));
            tr.find("td:eq(3)").html(haber.toFixed(2));
            tr.find("td:eq(4)").html(descripcion);
            tr.show();

            $("#body-tabla").append(tr);

            $("#debe").val(" ");
            $("#debe").val(" ");
            $("#haber").val(" ");
            $("#descripcion").val(" ");

            $(".total-debe").html(suma_debe_asientos);
            $(".total-haber").html(suma_haber_asientos);

            $(".select2_demo_3").each(function () { //added a each loop here
                $(this).select2('val', '')
            });

        });

        $(".eliminar").click(function(e){
            debe = $(this).parent().parent().find("td:eq(2)").html();
            haber = $(this).parent().parent().find("td:eq(3)").html();
            suma_debe_asientos = (parseFloat(suma_debe_asientos) - parseFloat(debe)).toFixed(2);
            suma_haber_asientos = (parseFloat(suma_haber_asientos) - parseFloat(haber)).toFixed(2);
            $(this).parent().parent().remove();

            $(".total-debe").html(suma_debe_asientos);
            $(".total-haber").html(suma_haber_asientos);
        });

        $("#impactar-asientos").click(function(){

            if(suma_debe_asientos === parseFloat(0)){
                alert("Agregue Asientos");
                return false;
            }

            if((suma_debe_asientos - suma_haber_asientos) !== parseFloat(0)){
                alert("La suma del DEBE y HABER deben ser iguales");
                return false;
            }

            var datos = [];
            $('#body-tabla  > tr:not(:first-child)').each(function() {
                datos.push({
                    fecha: $(this).find("td:eq(0)").html(),
                    cuenta_id: $(this).find("td:eq(1)").attr('cuenta_id'),
                    debe: $(this).find("td:eq(2)").html(),
                    haber: $(this).find("td:eq(3)").html(),
                    descripcion: $(this).find("td:eq(4)").html()
                });

            });

            console.log(datos);


            var token = $("meta[name='csrf-token']").attr('content');
            $.ajax({
                url: '{{ url("/contabilidad/asientos/guardar") }}',
                type: "POST",
                data: "_token="+token+"&datos="+JSON.stringify(datos),
                success: function(data){
                    console.log(data);
                    swal("Asientos Manuales", data.mensaje, "success");
                    $('#body-tabla  > tr:not(:first-child)').each(function() {
                            $(this).remove();
                    });
                    suma_debe_asientos  = parseFloat(0).toFixed(2);
                    suma_haber_asientos = parseFloat(0).toFixed(2);
                    $(".total-debe").html(suma_debe_asientos);
                    $(".total-haber").html(suma_haber_asientos);
                },
                error: function(data){
                    console.log(data);
                    swal("Asientos Manuales", data.mensaje, "danger");
                }
            });

        });

        $(".select2_demo_3").select2({
            theme: "bootstrap",
            ajax: {
              url: '{{ url("api/get-cuentas/") }}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                return {
                  q: params.term, // search term
                  page: params.page,
                };
              },
              processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                  results: data.results,
                  pagination: {
                    more: (params.page * 30) < data.total_count
                  }
                };
              },
              cache: true
            },
            placeholder: "Seleccione la Cuenta...",
            allowClear: true
        });

//        $('.footable2').footable();

        var data_table = $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });

        $("#buscar-asientos").click(function(){
            var datos = $("#asientos-form").serialize();
            var token = $("meta[name='csrf-token']").attr('content');
            $.ajax({
                type: "POST",
                url: "/contabilidad/asientos/buscar",
                data: "_token="+token+"&"+datos,
                success: function(data){
                    data_table.rows().remove().draw();

                    for(var cont = 0; cont < data.mensaje.length; cont++){
                        data_table.row.add(data.mensaje[cont]);
                        data_table.draw();
                    }
                },
                error: function(data){

                }
            });
        });

//        $("a.eliminar").click(function(){
//            var id_usuario = $(this).attr('id_usuario');
//            swal({
//                title: "Esta Seguro?",
//                type: "warning",
//                showCancelButton: true,
//                confirmButtonColor: "#DD6B55",
//                confirmButtonText: "Eliminar!",
//                closeOnConfirm: true
//            }, function () {
//
//                $.ajax({
//                    type: "POST",
//                    url: "/usuarios/eliminar",
//                    data: "_token="+$("meta[name='csrf-token']").attr('content')+"&id_usuario="+id_usuario,
//                    success: function(){
//                        $("a.eliminar[id_usuario='"+id_usuario+"']").parent().parent().remove();
//                    },
//                    error: function(){
//
//                    }
//                });
//
//            });
//
//
//        });
    });
</script>
@stop
