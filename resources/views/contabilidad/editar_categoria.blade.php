@extends('layouts.backend')


@section('css')
<!-- Required Stylesheets -->
<link href="{{ asset('/bootstrap-treeview/src/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{ asset('/backend/css/sweetalert/sweetalert.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Plan de Cuentas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Contabilidad</a>
            </li>
            <li class="active">
                <strong>Editar Cuenta</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        @if(session()->has('msg'))
            <div class="alert alert-success">
                {{ session()->get('msg') }}
            </div>
        @endif
        <div class="col-md-5">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Editar Cuenta</h5>
                </div>
                <div class="ibox-content">
                         
                    
                        {!! Form::open(['url' => '/contabilidad/plan-de-cuentas/categoria/update', 'method' => 'post','id' => 'form-cuenta']) !!}
                        <input hidden="" name="id" type="number" value="{{ $cuenta->id }}">
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('nombre', 'Nombre') }}
                                {{ Form::text("nombre", $cuenta->nombre , array("class"=>"form-control", "placeholder"=>"Nombre de la Cuenta")) }}
                                @if ($errors->has('descripcion'))
                                    <span class="help-block text-warning" >
                                        <strong>
                                            {{ $errors->first('nombre') }}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a  href="/contabilidad/plan-de-cuentas"><button type="button" class="btn btn-white" >Cancelar</button></a>
                                <button type="submit" class="btn btn-primary"  >Guardar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-3">
            
        </div>
        </div>
    </div>
@endsection

@section('javascript')
    <!-- Required Javascript -->
    <script src="{{ asset('/bootstrap-treeview/src/js/bootstrap-treeview.js') }}"></script>
    
    <!-- Sweet alert -->
    <script src="{{ asset('/backend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    
    <script>
        $(document).ready(function(){
                      
        });
    </script>
@stop
